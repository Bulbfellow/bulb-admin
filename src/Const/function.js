import React from 'react'

export function convertdate(str) {
    var date = new Date(str),
        mnth = ("0" + (date.getMonth() + 1)).slice(-2),
        day = ("0" + date.getDate()).slice(-2);
    return [date.getFullYear(), mnth, day].join("-");
}


export function convertDatePickerTime(str) {
    var month, day, year, hours, minutes, seconds;
    var date = new Date(str),
        month = ("0" + (date.getMonth() + 1)).slice(-2),
        day = ("0" + date.getDate()).slice(-2);
    hours = ("0" + date.getHours()).slice(-2);
    minutes = ("0" + date.getMinutes()).slice(-2);

    var mySQLDate = [day, month, date.getFullYear()].join("-");
    var mySQLTime = [hours, minutes].join(":");
    return [mySQLDate, mySQLTime].join(", ");
}

export function Dateonly(str) {
    var month, day, year, hours, minutes, seconds;
    var date = new Date(str),
        month = ("0" + (date.getMonth() + 1)).slice(-2),
        day = ("0" + date.getDate()).slice(-2);
    hours = ("0" + date.getHours()).slice(-2);
    minutes = ("0" + date.getMinutes()).slice(-2);

    var mySQLDate = [day].join("-");
    var mySQLTime = [hours, minutes].join(":");
    return [mySQLDate];
}

const monthNames = ["January", "February", "March", "April", "May", "June",
  "July", "August", "September", "October", "November", "December"
];

export function MonthName(str){
    var date = new Date(str)
    return monthNames[date.getMonth()]
}

// const monthNames = ["January", "February", "March", "April", "May", "June",
//   "July", "August", "September", "October", "November", "December"
// ];
const shortMonthNames = ["Jan", "Feb", "Mar", "Apr", "May", "Jun",
  "Jul", "Aug", "Sep", "Oct", "Nov]", "Dec"
];
const days = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'];

function DayName(str){
	var d = new Date(str);
	var dayName = days[d.getDay()];
    return dayName;
}

function MonthNames(str){
    var date = new Date(str)
    return monthNames[date.getMonth()]
}
function shortMonthName(str){
    var date = new Date(str)
    return shortMonthNames[date.getMonth()]
}

function formatAMPM(hours) {
    var ampm = hours >= 12 ? 'pm' : 'am';
    return ampm;
  }
  
// myFunction("2020-03-03T11:00:00Z",1) ; // 24-12-2019, 04:30AM
// myFunction("2020-03-03T11:00:00Z",2) ; // 03  Mar, 2020 at 16:30 pm
// myFunction("2020-03-03T11:00:00Z",3) ; // Tuesday, March 03 2020
// myFunction("2020-03-03T11:00:00Z",4) ; // Mar 03, 2020

export function Commondateconvert(dateString, type) {
        var response = "";
     	var month, day, year, hours, minutes, seconds;
           var date = new Date(dateString),
           
        month = ("0" + (date.getMonth() + 1)).slice(-2),
        day = ("0" + date.getDate()).slice(-2);
        hours = ("0" + date.getHours()).slice(-2);
        minutes = ("0" + date.getMinutes()).slice(-2);

        var ShorMonthName = shortMonthName(dateString)
        var monthName = MonthNames(dateString)
        var AMPM = formatAMPM(hours);
        var dayName = DayName(dateString);

        if(type === 1){
            response = day+"-"+month+"-"+date.getFullYear()+", "+hours+":"+minutes+AMPM.toUpperCase();
        }else if(type === 2){
            response = day+"  "+ShorMonthName+", "+date.getFullYear()+" at "+hours+":"+minutes+" "+AMPM;
        }else if(type === 3){
            response = dayName+", "+monthName+" "+day+" "+date.getFullYear();
        }else if(type === 4){
            response = monthName +" "+day+", "+date.getFullYear();
        }
        return response;
}


