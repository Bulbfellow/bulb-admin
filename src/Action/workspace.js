import React from 'react'
import * as ACTION_TYPE from './actiontypes'
import Axios from 'axios'
import { Api } from '../Const/Api'
import { Redirect } from 'react-router-dom';


function logout() {
    localStorage.clear();
    window.location.pathname = "/"
}



export const addWorkspaceSuccess = payload => {
    return {
        type: ACTION_TYPE.WORKSPACE_ADD_SUCCESS,
        payload
    }
}

export const addWorkspaceLoading = () => {
    return {
        type: ACTION_TYPE.WORKSPACE_ADD_LOADING,

    }
}

export const addWorkspaceError = payload => {
    return {
        type: ACTION_TYPE.WORKSPACE_ADD_ERROR,
        payload
    }
}

export const getWorkspaceSuccess = payload => {
    return {
        type: ACTION_TYPE.WORKSPACE_GET_SUCCESS,
        payload
    }
}

export const getWorkspaceLoading = () => {
    return {
        type: ACTION_TYPE.WORKSPACE_GET_LOADING,

    }
}

export const getWorkspaceError = payload => {
    return {
        type: ACTION_TYPE.WORKSPACE_GET_ERROR,
        payload
    }
}

export const putWorkspaceSuccess = payload => {
    return {
        type: ACTION_TYPE.WORKSPACE_PUT_SUCCESS,
        payload
    }
}

export const putWorkspaceLoading = () => {
    return {
        type: ACTION_TYPE.WORKSPACE_PUT_LOADING,

    }
}

export const putWorkspaceError = payload => {
    return {
        type: ACTION_TYPE.WORKSPACE_PUT_ERROR,
        payload
    }
}

export const DeleteWorkspaceSuccess = payload => {
    return {
        type: ACTION_TYPE.WORKSPACE_LIST_DELETE_SUCCESS,
        payload
    }
}

export const DeleteWorkspaceLoading = () => {
    return {
        type: ACTION_TYPE.WORKSPACE_LIST_DELETE_LOADING,

    }
}

export const DeleteWorkspaceError = payload => {
    return {
        type: ACTION_TYPE.WORKSPACE_LIST_DELETE_ERROR,
        payload
    }
}
export const addWorkspace = (data) => {
    return dispatch => {
        Axios.post(Api.BaseUrl + "/api/workspace", data,
            { 'headers': { 'Authorization': localStorage.getItem('adminToken') } })
            .then(resp => {
                dispatch(addWorkspaceLoading())
                if (resp.status === 200) {
                    if (resp.data.status === "1") {
                        dispatch(addWorkspaceSuccess(resp.data.message));
                    }
                    if (resp.data.status === "0") {
                        dispatch(addWorkspaceError(resp.data.message));
                    }
                    if (resp.data.status === "2") {
                        logout()
                        return false
                    }
                    else {
                        dispatch(addWorkspaceError(resp.data.message));
                    }
                } else {
                    dispatch(addWorkspaceError(resp.message));
                }

            })
            .catch(err => {
                dispatch(addWorkspaceError(err.message));
            });
    };
};

export const WorkspaceListing = () => {
    return dispatch => {
        Axios.get(Api.BaseUrl + "/api/workspace")
            .then(resp => {
                dispatch(getWorkspaceLoading());
                if (resp.data.status === 2) {
                    logout()
                    return false
                }
                if (resp.status === 200) {
                    dispatch(getWorkspaceSuccess(resp.data.data.list));

                } else {
                    dispatch(getWorkspaceError("error"));
                }
                dispatch(getWorkspaceLoading(false));
            })
            .catch(err => {
                dispatch(getWorkspaceError(err.message));
            });
    };
};



// export const updateWorkspace = (data) => {
//     console.log("Data addWorkspace", data)
//     return dispatch => {
//         Axios.put(Api.BaseUrl + "/api/workspace/" + data.id, {
//             title:data.title,
//             image_url:data.image_url,
//             description:data.description,
//         },
//             { 'headers': { 'Authorization': localStorage.getItem('adminToken') } })
//             .then(resp => {
//                 console.log("resp", resp)
//                 dispatch(putWorkspaceLoading())
//                 if (resp.status === 200) {
//                     if (resp.data.status === "1") {
//                         dispatch(putWorkspaceSuccess(resp.data.data.list));
//                     }
//                     if (resp.data.status === "0") {
//                         dispatch(putWorkspaceError(resp.data.message));
//                     }
//                     if (resp.data.status === "2") {
//                         logout()
//                         return false
//                     }
//                     else {
//                         dispatch(putWorkspaceError(resp.data.message));
//                     }
//                 } else {
//                     dispatch(putWorkspaceError(resp.data.message));
//                 }

//             })
//             .catch(err => {
//                 dispatch(putWorkspaceError(err.message));
//             });
//     };
// };
export const deleteWorkspace = userid => {
    return dispatch => {
        Axios.delete(Api.BaseUrl + "/api/workspace/" + userid, {
            'headers': { 'Authorization': localStorage.getItem('adminToken') }
        })
            .then(resp => {
                if (resp.data.status === 2) {
                    logout()
                    return false
                }
                dispatch(DeleteWorkspaceLoading(false));
            })
            .catch(err => {
                dispatch(DeleteWorkspaceError(err.message));
            });
    };
};