import * as ACTION_TYPE from './actiontypes'
import Axios from 'axios'
import { Api } from '../Const/Api'

function logout() {
    localStorage.clear();
    window.location.pathname = "/"
}

export const addAdminSuccess = payload => {
    return {
        type: ACTION_TYPE.MANAGE_ADD_ADMIN_SUCCESS,
        payload
    }
}

export const addAdminLoading = () => {
    return {
        type: ACTION_TYPE.MANAGE_ADD_ADMIN_LOADING
    }
}

export const addAdminError = payload => {
    return {
        type: ACTION_TYPE.MANAGE_ADD_ADMIN_ERROR,
        payload
    }
}

export const AddAdminAction = (data) => {
    return dispatch => {
        Axios.post(Api.BaseUrl + "​/api/admin", data, {
            'headers': { 'Authorization': localStorage.getItem('adminToken') },
        })
            .then((resp) => {
                dispatch(addAdminLoading());
                if (resp.data.status === 2) {
                    logout()
                    return false
                }
                if (resp.status === 200) {
                    dispatch(addAdminSuccess(resp.data.message));
                }
                else {
                    dispatch(addAdminError(resp.data.message));
                }
            })
            .catch(err => {
                dispatch(addAdminError(err.message));
            });
    };
};

export const getAdminSuccess = payload => {
    return {
        type: ACTION_TYPE.MANAGE_GET_ADMIN_SUCCESS,
        payload
    }
}

export const getAdminLoading = () => {
    return {
        type: ACTION_TYPE.MANAGE_GET_ADMIN_LOADING,
    }
}

export const getAdminError = payload => {
    return {
        type: ACTION_TYPE.MANAGE_GET_ADMIN_ERROR,
        payload
    }
}

export const getAdminCount = payload => {
    return {
        type: ACTION_TYPE.MANAGE_GET_ADMIN_COUNT_SUCCESS,
        payload
    }
}

export const GetAdminList = (slice) => {
    return dispatch => {
        Axios.get(Api.BaseUrl+'/api/admin?limit=7&page=' + slice, {
            'headers': { 'Authorization': localStorage.getItem('adminToken') },
        })
            .then((resp) => {
                dispatch(getAdminLoading());
                if (resp.data.status === 2) {
                    logout()
                    return false
                }
                if (resp.status === 200) {
                    dispatch(getAdminSuccess(resp.data.data.list));
                    dispatch(getAdminCount(resp.data.data))
                } else {
                    dispatch(getAdminError("unable to show list please check the internet connection"));
                }
            })
            .catch(err => {
                dispatch(getAdminError(err.message));
            });
    };
};



export const deleteAdminSuccess = payload => {
    return {
        type: ACTION_TYPE.MANAGE_DELETE_ADMIN_SUCCESS,
        payload
    }
}

export const deleteAdminLoading = payload => {
    return {
        type: ACTION_TYPE.MANAGE_DELETE_ADMIN_LOADING,
        payload
    }
}

export const deleteAdminError = payload => {
    return {
        type: ACTION_TYPE.MANAGE_DELETE_ADMIN_ERROR,
        payload
    }
}

export const DeleteAdminList = (deleteID) => {
    return dispatch => {
        Axios.delete(Api.BaseUrl + "​/api/admin/" + deleteID, {
            'headers': { 'Authorization': localStorage.getItem('adminToken') },
        })
            .then((resp) => {
                dispatch(deleteAdminLoading());
                if (resp.data.status === 2) {
                    logout()
                    return false
                }
                if (resp.status === 200) {
                    dispatch(deleteAdminSuccess(resp.data.data.list));
                } else {
                    dispatch(deleteAdminError("error"));
                }
            })
            .catch(err => {
                dispatch(deleteAdminError(err.message));
            });
    };
};



export const updateAdminSuccess = payload => {
    return {
        type: ACTION_TYPE.MANAGE_PUT_ADMIN_SUCCESS,
        payload
    }
}

export const updateAdminLoading = () => {
    return {
        type: ACTION_TYPE.MANAGE_PUT_ADMIN_LOADING,
    }
}

export const updateAdminError = payload => {
    return {
        type: ACTION_TYPE.MANAGE_PUT_ADMIN_ERROR,
        payload
    }
}

export const updateAdminList = (data) => {
    return dispatch => {
        Axios.put(Api.BaseUrl + "​/api/admin/" + data.admin_id, data, {
            'headers': { 'Authorization': localStorage.getItem('adminToken') },
        })
            .then((resp) => {
                dispatch(deleteAdminLoading());
                if (resp.data.status === 2) {
                    logout()
                    return false
                }
                if (resp.status === 200) {
                    dispatch(updateAdminSuccess(resp.data.message));
                } else {
                    dispatch(updateAdminError("something went wrong please try again"));
                }
            })
            .catch(err => {
                dispatch(updateAdminError(err.message));
            });
    };
};

export const searchAdminSuccess = payload => {
    return {
        type: ACTION_TYPE.MANAGE_GET_ADMIN_SEARCH_SUCCESS,
        payload
    }
}

export const searchAdminLoading = () => {
    return {
        type: ACTION_TYPE.MANAGE_GET_ADMIN_SEARCH_LOADING
    }
}

export const searchAdminError = payload => {
    return {
        type: ACTION_TYPE.MANAGE_GET_ADMIN_SEARCH_ERROR,
        payload
    }
}

export const searchAdminCount = payload => {
    return {
        type: ACTION_TYPE.MANAGE_GET_ADMIN_SEARCH_COUNT_SUCCESS,
        payload
    }
}
export const SearchAdminList = (filter) => {
    return dispatch => {
        Axios.get(Api.BaseUrl+`/api/adminsbysearch?text=${filter.Search}&limit=7&page=` + filter.page, {
            'headers': { 'Authorization': localStorage.getItem('adminToken') },
        })
            .then((resp) => {
                dispatch(searchAdminLoading());
                if (resp.data.status === 2) {
                    logout()
                    return false
                }
                if (resp.status === 200) {
                    dispatch(searchAdminSuccess(resp.data.data.list));
                    dispatch(searchAdminCount(resp.data.data))
                } else {
                    dispatch(searchAdminError("error"));
                }
            })
            .catch(err => {
                dispatch(searchAdminError(err.message));
            });
    };
};