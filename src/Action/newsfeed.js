import React from 'react'
import * as ACTION_TYPE from './actiontypes'
import Axios from 'axios'
import { Api } from '../Const/Api'


function logout() {
    localStorage.clear();
    window.location.pathname = "/"
}

export const newsfeedAddEventSuccess = payload => {
    return {
        type: ACTION_TYPE.NEWSFEED_ADD_EVENT_SUCCESS,
        payload
    }
}

export const newsfeedAddEventLoading = () => {
    return {
        type: ACTION_TYPE.NEWSFEED_ADD_EVENT_LOADING,
    }
}

export const newsfeedAddEventError = payload => {
    return {
        type: ACTION_TYPE.NEWSFEED_ADD_EVENT_ERROR,
        payload
    }
}


export const newsfeedDeleteEventSuccess = payload => {
    return {
        type: ACTION_TYPE.NEWSFEED_DELETE_EVENT_SUCCESS,
        payload
    }
}

export const newsfeedDeleteEventLoading = payload => {
    return {
        type: ACTION_TYPE.NEWSFEED_DELETE_EVENT_LOADING,
        payload
    }
}

export const newsfeedDeleteEventError = payload => {
    return {
        type: ACTION_TYPE.NEWSFEED_DELETE_EVENT_ERROR,
        payload
    }
}

export const newsfeedUpdateEventSuccess = payload => {
    return {
        type: ACTION_TYPE.NEWSFEED_UPDATE_EVENT_SUCCESS,
        payload
    }
}

export const newsfeedUpdateEventLoading = () => {
    return {
        type: ACTION_TYPE.NEWSFEED_UPDATE_EVENT_LOADING,
    }
}

export const newsfeedUpdateEventError = payload => {
    return {
        type: ACTION_TYPE.NEWSFEED_UPDATE_EVENT_ERROR,
        payload
    }
}


export const newsfeedGetEventSuccess = payload => {
    return {
        type: ACTION_TYPE.NEWSFEED_GET_EVENT_SUCCESS,
        payload
    }
}

export const newsfeedGetEventLoading = payload => {
    return {
        type: ACTION_TYPE.NEWSFEED_GET_EVENT_LOADING,
        payload
    }
}

export const newsfeedGetEventError = payload => {
    return {
        type: ACTION_TYPE.NEWSFEED_GET_EVENT_ERROR,
        payload
    }
}



export const addEvent = (data) => {
    return dispatch => {
        Axios.post(Api.BaseUrl + "/api/events", data, { 'headers': { 'Authorization': localStorage.getItem('adminToken') } })
            .then(resp => {
                dispatch(newsfeedAddEventLoading());
                if (resp.status === 200) {
                    if (resp.data.status === "1") {
                        dispatch(newsfeedAddEventSuccess(resp.data.status));
                    }
                    if (resp.data.status === "0") {
                        dispatch(newsfeedAddEventError(resp.data.message));
                    }
                    if (resp.data.status === "2") {
                        logout()
                        return false
                    }
                } else {
                    dispatch(newsfeedAddEventError("error"));
                }
            })
            .catch(err => {
                dispatch(newsfeedAddEventError(err.message));
            });
    };
};


// export const updateEvent = (data) => {
//     return dispatch => {
//         Axios.put(Api.BaseUrl + "/api/event/" + data.id, {
//             title: data.title,
//             image_url: data.image_url,
//             description: data.description,
//             date: data.date,
//             time: data.time,
//             venue: data.venue,
//             link: data.link,
//             event_post: data.event_post,
//             event_type: data.event_type
//         }, { 'headers': { 'Authorization': localStorage.getItem('adminToken') } })
//             .then(resp => {
//                 dispatch(newsfeedUpdateEventLoading())
//                 console.log('++++reducer apis response', resp)
//                 if (resp.status === 200) {
//                     if (resp.data.status === "1") {
//                         dispatch(newsfeedUpdateEventSuccess(resp.data.status));
//                     }
//                     if (resp.data.status === "0") {
//                         dispatch(newsfeedUpdateEventError(resp.data.message));
//                     }
//                     if (resp.data.status === "2") {
//                         logout()
//                         return false
//                     }
//                 } else {
//                     dispatch(newsfeedUpdateEventError("error"));
//                 }
//             })
//             .catch(err => {
//                 dispatch(newsfeedUpdateEventError(err.message));
//             });
//     };
// };




//************Press */

export const newsfeedAddPressSuccess = payload => {
    return {
        type: ACTION_TYPE.NEWSFEED_ADD_PRESS_SUCCESS,
        payload
    }
}

export const newsfeedAddPressLoading = () => {
    return {
        type: ACTION_TYPE.NEWSFEED_ADD_PRESS_LOADING
    }
}

export const newsfeedAddPressError = payload => {
    return {
        type: ACTION_TYPE.NEWSFEED_ADD_PRESS_ERROR,
        payload
    }
}


export const newsfeedDeletePressSuccess = payload => {
    return {
        type: ACTION_TYPE.NEWSFEED_DELETE_EVENT_SUCCESS,
        payload
    }
}

export const newsfeedDeletePressLoading = payload => {
    return {
        type: ACTION_TYPE.NEWSFEED_DELETE_EVENT_LOADING,
        payload
    }
}

export const newsfeedDeletePressError = payload => {
    return {
        type: ACTION_TYPE.NEWSFEED_DELETE_EVENT_ERROR,
        payload
    }
}

export const newsfeedUpdatePressSuccess = payload => {
    return {
        type: ACTION_TYPE.NEWSFEED_UPDATE_PRESS_SUCCESS,
        payload
    }
}

export const newsfeedUpdatePressLoading = payload => {
    return {
        type: ACTION_TYPE.NEWSFEED_UPDATE_PRESS_LOADING,
        payload
    }
}

export const newsfeedUpdatePressError = payload => {
    return {
        type: ACTION_TYPE.NEWSFEED_UPDATE_PRESS_ERROR,
        payload
    }
}


export const newsfeedGetPressSuccess = payload => {
    return {
        type: ACTION_TYPE.NEWSFEED_GET_PRESS_SUCCESS,
        payload
    }
}

export const newsfeedGetPressLoading = payload => {
    return {
        type: ACTION_TYPE.NEWSFEED_GET_PRESS_LOADING,
        payload
    }
}

export const newsfeedGetPressError = payload => {
    return {
        type: ACTION_TYPE.NEWSFEED_GET_PRESS_ERROR,
        payload
    }
}


// export const addPress = (data) => {
//     return dispatch => {
//         Axios.post(Api.BaseUrl + "/api/presslist", data, {
//             'headers': { 'Authorization': localStorage.getItem('adminToken') },
//         })
//             .then(resp => {
//                 dispatch(newsfeedAddPressLoading());
//                 console.log('++++reducer apis response', resp)
//                 if (resp.status === 200) {
//                     if (resp.data.status === "1") {
//                         dispatch(newsfeedAddPressSuccess(resp.data.status));
//                     }
//                     if (resp.data.status === "0") {
//                         dispatch(newsfeedAddPressError(resp.data.message));
//                     }
//                     if (resp.data.status === "2") {
//                         logout()
//                         return false
//                     }            
//                 } else {
//                     dispatch(newsfeedAddPressError("error"));
//                 }
//             })
//             .catch(err => {
//                 dispatch(newsfeedAddPressError(err.message));
//             });
//     };
// };

// export const updatePress = (data) => {
//     console.log("data", data)

//     return dispatch => {
//         Axios.put(Api.BaseUrl + "/api/press/" + data.id, {
//             title: data.title,
//             image_url: data.image_url,
//             description: data.description,
//             date: data.date
//         }, {
//             'headers': { 'Authorization': localStorage.getItem('adminToken') },
//         })
//             .then(resp => {
//                 console.log('++++reducer apis response', resp)
//                 if (resp.status === 200) {
//                     if (resp.data.status === "1") {
//                         dispatch(newsfeedUpdatePressSuccess(resp.data.data.list));
//                     }
//                     if (resp.data.status === "0") {
//                         dispatch(newsfeedUpdatePressError(resp.data.message));
//                     }
//                     if (resp.data.status === "2") {
//                         logout()
//                         return false
//                     }
//                     else {
//                         dispatch(newsfeedUpdatePressError(resp.data.message));
//                         console.log('dgxxgd')
//                     }
//                     // console.log('enter in the response 200 STATUS')
//                 } else {
//                     dispatch(newsfeedUpdatePressError("error"));
//                 }
//                 dispatch(newsfeedUpdatePressLoading(false));
//             })
//             .catch(err => {
//                 dispatch(newsfeedUpdatePressError(err.message));
//             });
//     };
// };

// ----- ******  TAg

export const newsfeedAddTagSuccess = payload => {
    return {
        type: ACTION_TYPE.NEWSFEED_ADD_TAG_SUCCESS,
        payload
    }
}

export const newsfeedAddTagLoading = payload => {
    return {
        type: ACTION_TYPE.NEWSFEED_ADD_TAG_LOADING,
        payload
    }
}

export const newsfeedAddTagError = payload => {
    return {
        type: ACTION_TYPE.NEWSFEED_ADD_TAG_ERROR,
        payload
    }
}

export const newsfeedUpdateTagSuccess = payload => {
    return {
        type: ACTION_TYPE.NEWSFEED_UPDATE_TAG_SUCCESS,
        payload
    }
}

export const newsfeedUpdateTagLoading = payload => {
    return {
        type: ACTION_TYPE.NEWSFEED_UPDATE_TAG_LOADING,
        payload
    }
}

export const newsfeedUpdateTagError = payload => {
    return {
        type: ACTION_TYPE.NEWSFEED_UPDATE_TAG_ERROR,
        payload
    }
}

export const newsfeedGetTagSuccess = payload => {
    return {
        type: ACTION_TYPE.NEWSFEED_GET_TAG_SUCCESS,
        payload
    }
}

export const newsfeedGetTagLoading = () => {
    return {
        type: ACTION_TYPE.NEWSFEED_GET_TAG_LOADING,

    }
}

export const newsfeedGetTagError = payload => {
    return {
        type: ACTION_TYPE.NEWSFEED_GET_TAG_ERROR,
        payload
    }
}

export const newsfeedDeleteTagSuccess = payload => {
    return {
        type: ACTION_TYPE.NEWSFEED_DELETE_TAG_SUCCESS,
        payload
    }
}

export const newsfeedDeleteTagLoading = payload => {
    return {
        type: ACTION_TYPE.NEWSFEED_DELETE_TAG_LOADING,
        payload
    }
}

export const newsfeedDeleteTagError = payload => {
    return {
        type: ACTION_TYPE.NEWSFEED_DELETE_TAG_ERROR,
        payload
    }
}

// export const addTag = (data) => {
//     console.log("data-------------------", data)


//     return dispatch => {
//         Axios.post(Api.BaseUrl + "/api/tags", {
//             title: data.title,
//             image_url: data.imgUrl,
//             description: data.desc,
//             date: data.date,
//             tag_type: data.selecttag

//         }, {
//             'headers': { 'Authorization': localStorage.getItem('adminToken') },

//         })
//             .then(resp => {
//                 console.log('++++reducer apis response', resp)
//                 if (resp.status === 200) {
//                     if (resp.data.status === "1") {
//                         dispatch(newsfeedAddTagSuccess(resp.data.data.list));
//                     }
//                     if (resp.data.status === "0") {
//                         dispatch(newsfeedAddTagError(resp.data.message));
//                     }
//                     if (resp.data.status === "2") {
//                         logout()
//                         return false
//                     }
//                     else {
//                         dispatch(newsfeedAddTagError(resp.data.message));
//                         console.log('dgxxgd')
//                     }
//                     // console.log('enter in the response 200 STATUS')
//                 } else {
//                     dispatch(newsfeedAddTagError("error"));
//                 }


//                 dispatch(newsfeedAddTagLoading(false));
//             })
//             .catch(err => {
//                 dispatch(newsfeedAddTagError(err.message));
//             });
//     };
// };

// export const updateTag = (data) => {
//     console.log("data-------------------", data)


//     return dispatch => {
//         Axios.put(Api.BaseUrl + "/api/tag/" + data.id, {
//             title: data.title,
//             image_url: data.image_url,
//             description: data.description,
//             date: data.date,
//             tag_type: data.tag_type

//         }, {
//             'headers': { 'Authorization': localStorage.getItem('adminToken') },

//         })
//             .then(resp => {
//                 console.log('++++reducer apis response', resp)
//                 if (resp.status === 200) {
//                     if (resp.data.status === "1") {
//                         dispatch(newsfeedUpdateTagSuccess(resp.data.data.list));
//                     }
//                     if (resp.data.status === "0") {
//                         dispatch(newsfeedUpdateTagError(resp.data.message));
//                     }
//                     if (resp.data.status === "2") {
//                         logout()
//                         return false
//                     }
//                     else {
//                         dispatch(newsfeedUpdateTagError(resp.data.message));
//                         console.log('dgxxgd')
//                     }
//                     // console.log('enter in the response 200 STATUS')
//                 } else {
//                     dispatch(newsfeedUpdateTagError("error"));
//                 }


//                 dispatch(newsfeedUpdateTagLoading(false));
//             })
//             .catch(err => {
//                 dispatch(newsfeedUpdateTagError(err.message));
//             });
//     };
// };


export const newsfeedDeleteTag = deleteid => {
    return dispatch => {
        Axios.delete(Api.BaseUrl + "/api/tag/" + deleteid, {
            'headers': { 'Authorization': localStorage.getItem('adminToken') }
        })
            .then(resp => {

                dispatch(newsfeedDeleteTagLoading(false));
                if (resp.data.status === "2") {
                    logout()
                    return false
                }
            })
            .catch(err => {
                dispatch(newsfeedDeleteTagError(err.message));
            });
    };
};

export const newsfeedGetTag = (pageNO) => {


    return dispatch => {
        Axios.get(Api.BaseUrl + "/api/tags?limit=7&page=" + pageNO)
            .then(resp => {
                dispatch(newsfeedGetTagLoading());
                if (resp.data.status === 2) {
                    logout()
                    return false
                }
                if (resp.status === 200) {
                    dispatch(newsfeedGetTagLoading(false));
                    dispatch(newsfeedGetTagSuccess(resp.data.data));
                } else {
                    dispatch(newsfeedGetTagError("error"));
                }
                dispatch(newsfeedGetTagLoading(false));
            })
            .catch(err => {
                dispatch(newsfeedGetTagError(err.message));
            });

    };

};


//----
export const eventDelete = deleteid => {
    return dispatch => {
        Axios.delete(Api.BaseUrl + "/api/event/" + deleteid, {
            'headers': { 'Authorization': localStorage.getItem('adminToken') }
        })
            .then(resp => {
                dispatch(newsfeedDeleteEventLoading(false));
                if (resp.data.status === "2") {
                    logout()
                    return false
                }
            })
            .catch(err => {
                dispatch(newsfeedDeleteEventError(err.message));
            });
    };
};



export const getEvent = (pageNO) => {


    return dispatch => {
        Axios.get(Api.BaseUrl + "/api/events?limit=7&page=" + pageNO)
            .then(resp => {
                // if (resp.data.status == 2) {
                //     logout()
                //     return false
                // }
                if (resp.status === 200) {
                    dispatch(newsfeedGetEventLoading(false));
                    dispatch(newsfeedGetEventSuccess(resp.data.data));
                } else {
                    dispatch(newsfeedGetEventError("error"));
                }
                dispatch(newsfeedGetEventLoading(false));
            })
            .catch(err => {
                dispatch(newsfeedGetEventError(err.message));
            });

    };

};

export const getPress = (pageNO) => {
    return dispatch => {
        Axios.get(Api.BaseUrl + "/api/presslist?limit=7&page=" + pageNO)
            .then(resp => {
                if (resp.data.status === 2) {
                    logout()
                    return false
                }
                if (resp.status === 200) {
                    dispatch(newsfeedGetPressLoading(false));
                    dispatch(newsfeedGetPressSuccess(resp.data.data));
                } else {
                    dispatch(newsfeedGetPressError("error"));
                }
                dispatch(newsfeedGetPressLoading(false));
            })
            .catch(err => {
                dispatch(newsfeedGetPressError(err.message));
            });

    };

};


export const newsfeedPressDelete = deleteid => {
    return dispatch => {
        Axios.delete(Api.BaseUrl + "/api/press/" + deleteid, {
            'headers': { 'Authorization': localStorage.getItem('adminToken') }
        })
            .then(resp => {

                dispatch(newsfeedDeletePressLoading(false));
                if (resp.data.status === "2") {
                    logout()
                    return false
                }
            })
            .catch(err => {
                dispatch(newsfeedDeletePressError(err.message));
            });
    };
};

//  -------------------Gallery-----------------

export const newsfeedAddGallerySuccess = payload => {
    return {
        type: ACTION_TYPE.NEWSFEED_ADD_GALLERY_SUCCESS,
        payload
    }
}

export const newsfeedAddGalleryLoading = payload => {
    return {
        type: ACTION_TYPE.NEWSFEED_ADD_GALLERY_LOADING,
        payload
    }
}

export const newsfeedAddGalleryError = payload => {
    return {
        type: ACTION_TYPE.NEWSFEED_ADD_GALLERY_ERROR,
        payload
    }
}


export const newsfeedDeleteGallerySuccess = payload => {
    return {
        type: ACTION_TYPE.NEWSFEED_DELETE_GALLERY_SUCCESS,
        payload
    }
}

export const newsfeedDeleteGalleryLoading = payload => {
    return {
        type: ACTION_TYPE.NEWSFEED_DELETE_GALLERY_LOADING,
        payload
    }
}

export const newsfeedDeleteGalleryError = payload => {
    return {
        type: ACTION_TYPE.NEWSFEED_DELETE_GALLERY_ERROR,
        payload
    }
}

export const newsfeedUpdateGallerySuccess = payload => {
    return {
        type: ACTION_TYPE.NEWSFEED_UPDATE_GALLERY_SUCCESS,
        payload
    }
}

export const newsfeedUpdateGalleryLoading = payload => {
    return {
        type: ACTION_TYPE.NEWSFEED_UPDATE_GALLERY_LOADING,
        payload
    }
}

export const newsfeedUpdateGalleryError = payload => {
    return {
        type: ACTION_TYPE.NEWSFEED_UPDATE_GALLERY_ERROR,
        payload
    }
}


export const newsfeedGetGallerySuccess = payload => {
    return {
        type: ACTION_TYPE.NEWSFEED_GET_GALLERY_SUCCESS,
        payload
    }
}

export const newsfeedGetGalleryLoading = () => {
    return {
        type: ACTION_TYPE.NEWSFEED_GET_GALLERY_LOADING,

    }
}

export const newsfeedGetGalleryError = payload => {
    return {
        type: ACTION_TYPE.NEWSFEED_GET_GALLERY_ERROR,
        payload
    }
}


// export const newsfeedaddGallery = (data) => {
//     console.log("data-------------------", data)


//     return dispatch => {
//         Axios.post(Api.BaseUrl + "/api/gallerylist", {
//             title: data.title,
//             image_urls: data.image_urls,
//             description: data.description,
//             date: data.date,
//             time: data.time,
//             venue: data.venue
//         }, {
//             'headers': { 'Authorization': localStorage.getItem('adminToken') },

//         })
//             .then(resp => {
//                 console.log('++++reducer apis response', resp)
//                 if (resp.status === 200) {
//                     if (resp.data.status === "1") {
//                         dispatch(newsfeedAddGallerySuccess(resp.data.data.list));
//                     }
//                     if (resp.data.status === "0") {
//                         dispatch(newsfeedAddGalleryError(resp.data.message));
//                     }
//                     if (resp.data.status === "2") {
//                         logout()
//                         return false
//                     }
//                     else {
//                         dispatch(newsfeedAddGalleryError(resp.data.message));
//                         console.log('dgxxgd')
//                     }
//                     // console.log('enter in the response 200 STATUS')
//                 } else {
//                     dispatch(newsfeedAddGalleryError("error"));
//                 }


//                 dispatch(newsfeedAddGalleryLoading(false));
//             })
//             .catch(err => {
//                 dispatch(newsfeedAddGalleryError(err.message));
//             });
//     };
// };



export const newsfeedgetGallery = (pageNO) => {
    console.log("pageNO", pageNO)
    return dispatch => {
        Axios.get(Api.BaseUrl + "/api/gallerylist?limit=7&page=" + pageNO)
            .then(resp => {
                dispatch(newsfeedGetGalleryLoading());
                if (resp.data.status === 2) {
                    logout()
                    return false
                }
                if (resp.status === 200) {
                    dispatch(newsfeedGetGallerySuccess(resp.data.data));
                } else {
                    dispatch(newsfeedGetGalleryError("error"));
                }
                dispatch(newsfeedGetGalleryLoading(false));
            })
            .catch(err => {
                dispatch(newsfeedGetGalleryError(err.message));
            });

    };

};

export const newsfeedGalleryDelete = deleteid => {
    return dispatch => {
        Axios.delete(Api.BaseUrl + "/api/gallery/" + deleteid, {
            'headers': { 'Authorization': localStorage.getItem('adminToken') }
        })
            .then(resp => {

                dispatch(newsfeedDeleteGalleryLoading(false));
                if (resp.data.status === "2") {
                    logout()
                    return false
                }
            })
            .catch(err => {
                dispatch(newsfeedDeleteGalleryError(err.message));
            });
    };
};


export const newsfeedUpdateGallery = (data) => {


    return dispatch => {
        Axios.put(Api.BaseUrl + "/api/gallery/" + data.id, {
            title: data.title,
            image_urls: data.image_urls,
            description: data.description,
            date: data.date,
            time: data.time,
            venue: data.venue
        }, {
            'headers': { 'Authorization': localStorage.getItem('adminToken') },
        })
            .then(resp => {
                if (resp.status === 200) {
                    if (resp.data.status === "1") {
                        dispatch(newsfeedUpdateGallerySuccess(resp.data.data.list));
                    }
                    if (resp.data.status === "0") {
                        dispatch(newsfeedUpdateGalleryError(resp.data.message));
                    }
                    if (resp.data.status === "2") {
                        logout()
                        return false
                    }
                    else {
                        dispatch(newsfeedUpdateGalleryError(resp.data.message));
                       
                    }
                } else {
                    dispatch(newsfeedUpdateGalleryError("error"));
                }


                dispatch(newsfeedUpdateGalleryLoading(false));
            })
            .catch(err => {
                dispatch(newsfeedUpdateGalleryError(err.message));
            });
    };
};

