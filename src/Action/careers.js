import React from 'react'
import * as ACTION_TYPE from './actiontypes'
import Axios from 'axios'
import { Api } from '../Const/Api'
import { Redirect } from 'react-router-dom';


function logout() {
    localStorage.clear();
    window.location.pathname = "/"
}



export const addCareerSuccess = payload => {
    return {
        type: ACTION_TYPE.ADD_CAREERS_SUCCESS,
        payload
    }
}

export const addCareerLoading = payload => {
    return {
        type: ACTION_TYPE.ADD_CAREERS_LOADING,
        payload
    }
}

export const addCareerError = payload => {
    return {
        type: ACTION_TYPE.ADD_CAREERS_ERROR,
        payload
    }
}



export const joblistingDataSuccess = payload => {
    return {
        type: ACTION_TYPE.CAREERS_GET_LISTING_SUCCESS,
        payload
    }
}

export const joblistingDataLoading = payload => {
    return {
        type: ACTION_TYPE.CAREERS_GET_LISTING_LOADING,
        payload
    }
}

export const joblistingDataError = payload => {
    return {
        type: ACTION_TYPE.CAREERS_GET_LISTING_ERROR,
        payload
    }
}

export const joblistingDataCount = payload => {
    return {
        type: ACTION_TYPE.CAREERS_GET_LISTING_COUNT_SUCCESS,
        payload
    }
}

export const careergetformdataSuccess = payload => {
    return {
        type: ACTION_TYPE.CAREERS_GETFORM_DATA_SUCCESS,
        payload
    }
}

export const careergetformdataLoading = payload => {
    return {
        type: ACTION_TYPE.CAREERS_GETFORM_DATA_LOADING,
        payload
    }
}

export const careergetformdataError = payload => {
    return {
        type: ACTION_TYPE.CAREERS_GETFORM_DATA_ERROR,
        payload
    }
}



export const CareersFormfromUserListing = (datatype) => dispatch => Axios.get(Api.BaseUrl + "/api/career?status=" + datatype, {
    'headers': { 'Authorization': localStorage.getItem('adminToken') }
})
    .then(resp => {
        if (resp.data.status === 2) {
            logout()
            return false
        }
        if (resp.status === 200) {
            dispatch(careergetformdataLoading(false));
            dispatch(careergetformdataSuccess(resp.data.data.list));
        } else {
            dispatch(careergetformdataError("error"));
        }
        dispatch(careergetformdataLoading(false));
    })
    .catch(err => {
        dispatch(careergetformdataError(err.message));
    });
    // /api/career/{id}

    export const Careermultiple = (data) => {
        return dispatch => {
            Axios.put(Api.BaseUrl + "/api/career", data, {
                'headers': { 'Authorization': localStorage.getItem('adminToken') }
            })
                .then(response => {
                    dispatch(addCareerLoading(false));
                })
                .catch(err => {
                    dispatch(addCareerError(err.message));
                });
        }
    }



export const addCareers = (data) => {
    return dispatch => {
        Axios.post(Api.BaseUrl + "/api/carrerpost", {
            title: data.title,
            logo_url: data.Url,
            company_name: data.company_name,
            job_type: data.jobtype,
            description: data.description,
            entry_closure: data.entryCloser,
            location: data.location_id,
            company_name: data.company_name
        }, { 'headers': { 'Authorization': localStorage.getItem('adminToken') } })
            .then(resp => {
                if (resp.status === 200) {
                    if (resp.data.status === "1") {
                        dispatch(addCareerSuccess(resp.data.data.list));
                    }
                    if (resp.data.status === "0") {
                        dispatch(addCareerError(resp.data.message));
                    }
                    if (resp.data.status === "2") {
                        logout()
                        return false
                    }
                    else {
                        dispatch(addCareerError(resp.data.message));
                    }
                } else {
                    dispatch(addCareerError("error"));
                }
                dispatch(addCareerLoading(false));
            })
            .catch(err => {
                dispatch(addCareerError(err.message));
            });
    };
};

export const CareersJobListing = (data) => {
    return dispatch => {
        Axios.get(Api.BaseUrl + `/api/careerpostbytitle/?text=${data.searchdata}&type=${data.contract}&limit=${data.datalimit}&page=` + data.currentPage)
            .then(resp => {
                dispatch(joblistingDataLoading());
                if (resp.data.status === 2) {
                    logout()
                    return false
                }
                if (resp.status === 200) {
                    dispatch(joblistingDataSuccess(resp.data.data.list));
                    dispatch(joblistingDataCount(resp.data.data));
                } else {
                    dispatch(careergetformdataError("error"));
                }
            })
            .catch(err => {
                dispatch(careergetformdataError(err.message));
            });
    };
};



export const CareersFormdatafromUserDelete = userid => {
    // alert('api called')
    return dispatch => {
        Axios.delete(Api.BaseUrl+'/api/career/' + userid, {
            'headers': { 'Authorization': localStorage.getItem('adminToken') }
        })
            .then(resp => {
                if (resp.data.status === 2) {
                    logout()
                    return false
                }
                dispatch(careergetformdataLoading(false));
            })
            .catch(err => {
                dispatch(careergetformdataError(err.message));
            });
    };
};


export const CareersJobListingDelete = userid => {
    return dispatch => {
        Axios.delete(Api.BaseUrl + '/api/carrerpost/' + userid, {
            'headers': { 'Authorization': localStorage.getItem('adminToken') }
        })
            .then(resp => {
                if (resp.data.status === 2) {
                    logout()
                    return false
                }
                dispatch(addCareerLoading(false));
            })
            .catch(err => {
                dispatch(addCareerError(err.message));
            });
    };
};


export const CareersJobListingUpdate = data => {
    return dispatch => {
        Axios.put(Api.BaseUrl+'/api/carrerpost/' + data.sr_no,
            {
                title: data.title,
                logo_url: data.logo_url,
                job_type: data.job_type,
                description: data.description,
                entry_closure: data.entry_closure,
                location: data.location,
                company_name: data.company_name
            }, {
            'headers': { 'Authorization': localStorage.getItem('adminToken') }
        })
            .then(resp => {
                if (resp.status === 200) {
                    if (resp.data.status === "1") {
                        dispatch(addCareerSuccess(resp.data.data.list));
                        // alert("Data Saved")

                    }
                    if (resp.data.status === "0") {
                        dispatch(addCareerError(resp.data.message));
                    }
                    if (resp.data.status === "2") {
                        logout()
                        return false
                    }
                    else {
                        dispatch(addCareerError(resp.data.message));
                    }
                } else {
                    dispatch(addCareerError("error"));
                }
                dispatch(addCareerLoading(false));
            })
            .catch(err => {
                dispatch(addCareerError(err.message));
            });
    };
};

