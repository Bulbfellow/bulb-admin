import React from 'react'
import * as ACTION_TYPE from './actiontypes'
import Axios from 'axios'
import { Api } from '../Const/Api'

export const logout = () => {
    localStorage.clear();
    window.location.pathname = "/"
}

export const partnershipSuccess = payload => {
    return {
        type: ACTION_TYPE.PARTNERSHIP_USER_SUCCESS,
        payload
    }
}

export const partnershipLoading = payload => {
    return {
        type: ACTION_TYPE.PARTNERSHIP_USER_LOADING,
        payload
    }
}

export const partnershipError = payload => {
    return {
        type: ACTION_TYPE.PARTNERSHIP_USER_ERROR,
        payload
    }
}
//------------ HIRE A DEVELOPER START -----------------
export const hireadeveloperSuccess = payload => {
    return {
        type: ACTION_TYPE.HIREADEVELOPER_SUCCESS,
        payload
    }
}

export const hireadeveloperLoading = payload => {
    return {
        type: ACTION_TYPE.HIREADEVELOPER_LOADING,
        payload
    }
}

export const hireadeveloperError = payload => {
    return {
        type: ACTION_TYPE.HIREADEVELOPER_ERROR,
        payload
    }
}
//------------ HIRE A DEVELOPER END -----------------

//-------------REQUEST A TOUR ---------

export const requestatourSuccess = payload => {
    return {
        type: ACTION_TYPE.REQUESTATOUR_SUCCESS,
        payload
    }
}

export const requestatourLoading = payload => {
    return {
        type: ACTION_TYPE.REQUESTATOUR_LOADING,
        payload
    }
}

export const requestatourError = payload => {
    return {
        type: ACTION_TYPE.REQUESTATOUR_ERROR,
        payload
    }
}
// -----------        EnD REQUEST A TOUR

export const scheduleavisitSuccess = payload => {
    return {
        type: ACTION_TYPE.SCHEDULEAVISIT_SUCCESS,
        payload
    }
}

export const scheduleavisitLoading = payload => {
    return {
        type: ACTION_TYPE.SCHEDULEAVISIT_LOADING,
        payload
    }
}

export const scheduleavisitError = payload => {
    return {
        type: ACTION_TYPE.SCHEDULEAVISIT_ERROR,
        payload
    }
}

export const applyforincubationSuccess = payload => {
    return {
        type: ACTION_TYPE.APPLYFORINCUBATION_SUCCESS,
        payload
    }
}

export const applyforincubationLoading = payload => {
    return {
        type: ACTION_TYPE.APPLYFORINCUBATION_LOADING,
        payload
    }
}

export const applyforincubationError = payload => {
    return {
        type: ACTION_TYPE.APPLYFORINCUBATION_ERROR,
        payload
    }
}

export const submitamideaSuccess = payload => {
    return {
        type: ACTION_TYPE.SUBMITAIDEA_SUCCESS,
        payload
    }
}

export const submitamideaLoading = payload => {
    return {
        type: ACTION_TYPE.SUBMITAIDEA_LOADING,
        payload
    }
}

export const submitamideaError = payload => {
    return {
        type: ACTION_TYPE.SUBMITAIDEA_ERROR,
        payload
    }
}


export const newsletterSuccess = payload => {
    return {
        type: ACTION_TYPE.NEWSLETTER_USER_SUCCESS,
        payload
    }
}

export const newsletterLoading = payload => {
    return {
        type: ACTION_TYPE.NEWSLETTER_USER_LOADING,
        payload
    }
}

export const newsletterError = payload => {
    return {
        type: ACTION_TYPE.NEWSLETTER_USER_ERROR,
        payload
    }
}




export const partnership = (datatype) => dispatch => Axios.get(Api.BaseUrl + "/api/partnership?status=" + datatype, {
    'headers': { 'Authorization': localStorage.getItem('adminToken') }
})
    .then(resp => {
        if (resp.status === 200) {
            if (resp.data.status === "1") {
                dispatch(partnershipSuccess(resp.data.data.list));
            }
            if (resp.data.status === "0") {
                dispatch(partnershipError(resp.data.message));
            }
            if (resp.data.status === "2") {
                logout()
                return false
            }
            else {
                dispatch(partnershipError(resp.data.message));
            }
        } else {
            dispatch(partnershipError("error"));
        }
        dispatch(partnershipLoading(false));
    })
    .catch(err => {
        dispatch(partnershipError(err.message));
    });

export const Partnershipmultiple = (data) => {
  
    return dispatch => {
        Axios.put(Api.BaseUrl + "/api/partnership", data, {
            'headers': { 'Authorization': localStorage.getItem('adminToken') }
        })
            .then(response => {
                dispatch(partnershipLoading(false));
            })
            .catch(err => {
                dispatch(partnershipError(err.message));
            });
    }
}

export const hireadeveloper = (datatype) => dispatch => Axios.get(Api.BaseUrl + "/api/hire?status=" + datatype, {
    'headers': { 'Authorization': localStorage.getItem('adminToken') }
})
    .then(resp => {
        if (resp.data.status == 2) {
            logout()
            return false
        }
        if (resp.status === 200) {
            dispatch(hireadeveloperLoading(false));
            dispatch(hireadeveloperSuccess(resp.data.data.list));
        } else {
            dispatch(hireadeveloperError("error"));
        }
        dispatch(hireadeveloperLoading(false));
    })
    .catch(err => {
        dispatch(hireadeveloperError(err.message));
    });

export const requestatour = (datatype) => dispatch => Axios.get(Api.BaseUrl + "/api/requesttour?status=" + datatype, {
    'headers': { 'Authorization': localStorage.getItem('adminToken') }
})
    .then(resp => {
        if (resp.data.status == 2) {
            logout()
            return false
        }
        if (resp.status === 200) {
            dispatch(requestatourLoading(false));
            dispatch(requestatourSuccess(resp.data.data.list));
        } else {
            dispatch(requestatourError("error"));
        }
        dispatch(requestatourLoading(false));
    })
    .catch(err => {
        dispatch(requestatourError(err.message));
    });


export const requestatourmultiple = (data) => {
    return dispatch => {
        Axios.put(Api.BaseUrl + "/api/requesttour", data, {
            'headers': { 'Authorization': localStorage.getItem('adminToken') }
        })
            .then(response => {
                dispatch(requestatourLoading(false));
            })
            .catch(err => {
                dispatch(requestatourError(err.message));
            });
    }
}

export const scheduleavisit = (datatype) => dispatch => Axios.get(Api.BaseUrl + "/api/schedulevisit?status=" + datatype, {
    'headers': { 'Authorization': localStorage.getItem('adminToken') }
})
    .then(resp => {
        if (resp.data.status == 2) {
            logout()
            return false
        }
        if (resp.status === 200) {
            dispatch(scheduleavisitLoading(false));
            dispatch(scheduleavisitSuccess(resp.data.data.list));
        } else {
            dispatch(scheduleavisitError("error"));
        }
        dispatch(scheduleavisitLoading(false));
    })
    .catch(err => {
        dispatch(scheduleavisitError(err.message));
    });



export const applyforincubation = (datatype) => dispatch => Axios.get(  Api.BaseUrl + "/api/applyforincubator?status=" + datatype, {
    'headers': { 'Authorization': localStorage.getItem('adminToken') }
})
    .then(resp => {
        if (resp.data.status == 2) {
            logout()
            return false
        }
        if (resp.status === 200) {
            dispatch(applyforincubationLoading(false));
            dispatch(applyforincubationSuccess(resp.data.data.list));
        } else {
            dispatch(applyforincubationError("error"));
        }
        dispatch(applyforincubationLoading(false));
    })
    .catch(err => {
        dispatch(applyforincubationError(err.message));
    });

export const submitamidea = (datatype) => dispatch => Axios.get(Api.BaseUrl + "/api/submitidea?status=" + datatype, {
    'headers': { 'Authorization': localStorage.getItem('adminToken') }
})
    .then(resp => {
        if (resp.data.status == 2) {
            logout()
            return false
        }
        if (resp.status === 200) {
            dispatch(submitamideaLoading(false));
            dispatch(submitamideaSuccess(resp.data.data.list));
        } else {
            dispatch(submitamideaError("error"));
        }
        dispatch(submitamideaLoading(false));
    })
    .catch(err => {
        dispatch(submitamideaError(err.message));
    });


export const HireaDevelopermultiple = (data) => {
    return dispatch => {
        Axios.put(Api.BaseUrl + "/api/hire", data, {
            'headers': { 'Authorization': localStorage.getItem('adminToken') }
        })
            .then(response => {
                dispatch(partnershipLoading(false));
            })
            .catch(err => {
                dispatch(partnershipError(err.message));
            });
    }
}

export const scheduleavisitmultiple = (data) => {
    return dispatch => {
        Axios.put(Api.BaseUrl + "/api/schedulevisit", data, {
            'headers': { 'Authorization': localStorage.getItem('adminToken') }
        })
            .then(response => {
                dispatch(scheduleavisitLoading(false));
            })
            .catch(err => {
                dispatch(scheduleavisitError(err.message));
            });
    }
}

export const SubmitIdeamultiple = (data) => {
    // alert('call')
    return dispatch => {
        Axios.put(Api.BaseUrl + "/api/submitidea", data, {
            'headers': { 'Authorization': localStorage.getItem('adminToken') }
        })
            .then(response => {
                dispatch(partnershipLoading(false));
            })
            .catch(err => {
                dispatch(partnershipError(err.message));
            });
    }
}

export const Applyincubatormultiple = (data) => {
    return dispatch => {
        Axios.put(Api.BaseUrl + "​/api/applyforincubator", data, {
            'headers': { 'Authorization': localStorage.getItem('adminToken') }
        })
            .then(response => {
                dispatch(partnershipLoading(false));
            })
            .catch(err => {
                dispatch(partnershipError(err.message));
            });
    }
}



export const partnershipDelete = userid => {
    return dispatch => {
        Axios.delete(Api.BaseUrl + "/api/partnership/" + userid, {
            'headers': { 'Authorization': localStorage.getItem('adminToken') }
        })
            .then(resp => {
                dispatch(partnershipLoading(false));
            })
            .catch(err => {
                dispatch(partnershipError(err.message));
            });
    };
};

export const submitideaDelete = userid => {
    return dispatch => {
        Axios.delete(Api.BaseUrl + "/api/submitidea/" + userid, {
            'headers': { 'Authorization': localStorage.getItem('adminToken') }
        })
            .then(resp => {
                dispatch(submitamideaLoading(false));
            })
            .catch(err => {
                dispatch(submitamideaError(err.message));
            });
    };
};

export const hireadeveloperDelete = userid => {
    return dispatch => {
        Axios.delete(Api.BaseUrl + "/api/hire/" + userid, {
            'headers': { 'Authorization': localStorage.getItem('adminToken') }
        })
            .then(resp => {
                dispatch(partnershipLoading(false));
            })
            .catch(err => {
                dispatch(partnershipError(err.message));
            });
    };
};

export const requesttourDelete = userid => {
    return dispatch => {
        Axios.delete(Api.BaseUrl + "/api/requesttour/" + userid, {
            'headers': { 'Authorization': localStorage.getItem('adminToken') }
        })
            .then(resp => {
                dispatch(partnershipLoading(false));
            })
            .catch(err => {
                dispatch(partnershipError(err.message));
            });
    };
};

export const schedulevisitDelete = userid => {
    return dispatch => {
        // alert('delete called', userid)
        Axios.delete(Api.BaseUrl + "/api/schedulevisit/" + userid, {
            'headers': { 'Authorization': localStorage.getItem('adminToken') }
        })
            .then(resp => {
                dispatch(partnershipLoading(false));
            })
            .catch(err => {
                dispatch(partnershipError(err.message));
            });
    };
};

export const applyincubatorDelete = userid => {
    // alert('api is callled')
    return dispatch => {
        // alert('delete called', userid)
        Axios.delete(Api.BaseUrl + "/api/applyforincubator/" + userid, {
            'headers': { 'Authorization': localStorage.getItem('adminToken') }
        })
            .then(resp => {
                dispatch(partnershipLoading(false));
            })
            .catch(err => {
                dispatch(partnershipError(err.message));
            });
    };
};


export const newsletterGet = (type) => {
    return dispatch => {
        Axios.get(Api.BaseUrl + "/api/subscriber?type=" +type, {
            'headers': { 'Authorization': localStorage.getItem('adminToken') }
        })
            .then(resp => {
                dispatch(newsletterLoading());
                if (resp.status === 200) {
                    if (resp.data.status === "1") {
                        dispatch(newsletterSuccess(resp.data.data.result));
                    }
                    if (resp.data.status === "0") {
                        dispatch(newsletterError(resp.data.message));
                    }
                    if (resp.data.status === "2") {
                        logout()
                        return false
                    }
                    else {
                        dispatch(newsletterError(resp.data.message));
                    }
                } else {
                    dispatch(newsletterError("error"));
                }
                
            })
            .catch(err => {
                dispatch(newsletterError(err.message));
            });
    };
};
