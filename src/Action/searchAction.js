import React from 'react'
import * as ACTION_TYPE from './actiontypes'
import Axios from 'axios'
import { Api } from '../Const/Api'
import { Redirect } from 'react-router-dom';

function logout() {
    localStorage.clear();
    window.location.pathname = "/"
}

export const searchSuccess = payload => {
    return {
        type: ACTION_TYPE.DATA_SEARCH_SUCCESS,
        payload
    }
}

export const searchLoading = () => {
    return {
        type: ACTION_TYPE.DATA_SEARCH_LOADING,
        
    }
}

export const searchError = payload => {
    return {
        type: ACTION_TYPE.DATA_SEARCH_ERROR,
        payload
    }
}


export const SearchEventList = (filter) => {
    return dispatch => {
        Axios.get( Api.BaseUrl +`/api/eventsbysearch?text=${filter.Search}&limit=7&page=` + filter.currentPage, {
            'headers': { 'Authorization': localStorage.getItem('adminToken') },
        })
        .then((resp) => {
            dispatch(searchLoading());
            if (resp.data.status === 2) {
                logout()
                return false
            }
            if (resp.status === 200) {
                dispatch(searchSuccess(resp.data.data));
            } else {
                dispatch(searchError("error"));
            }
        })
            .catch(err => {
                dispatch(searchError(err.message));
            });
    };
};

export const SearchTagList = (filter) => {
    return dispatch => {
        Axios.get( Api.BaseUrl +`/api/tagsbysearch?text=${filter.Search}&limit=7&page=` + filter.currentPage, {
            'headers': { 'Authorization': localStorage.getItem('adminToken') },
        })
        .then((resp) => {
            dispatch(searchLoading());
            if (resp.data.status === 2) {
                logout()
                return false
            }
            if (resp.status === 200) {
                dispatch(searchSuccess(resp.data.data));
            } else {
                dispatch(searchError("error"));
            }
        })
            .catch(err => {
                dispatch(searchError(err.message));
            });
    };
};

export const SearchPressList = (filter) => {
    return dispatch => {
        Axios.get( Api.BaseUrl +`/api/presslistbysearch?text=${filter.Search}&limit=7&page=` + filter.currentPage, {
            'headers': { 'Authorization': localStorage.getItem('adminToken') },
        })
        .then((resp) => {
            dispatch(searchLoading());
            if (resp.data.status === 2) {
                logout()
                return false
            }
            if (resp.status === 200) {
                dispatch(searchSuccess(resp.data.data));
            } else {
                dispatch(searchError("error"));
            }
        })
            .catch(err => {
                dispatch(searchError(err.message));
            });
    };
}

export const SearchGalleryList = (filter) => {
    return dispatch => {
        Axios.get( Api.BaseUrl +`​/api/gallerybysearch?text=${filter.Search}&limit=7&page=` + filter.currentPage, {
            'headers': { 'Authorization': localStorage.getItem('adminToken') },
        })
        .then((resp) => {
            dispatch(searchLoading());
            if (resp.data.status === 2) {
                logout()
                return false
            }
            if (resp.status === 200) {
                dispatch(searchSuccess(resp.data.data));
            } else {
                dispatch(searchError("error"));
            }
        })
            .catch(err => {
                dispatch(searchError(err.message));
            });
    };
};


export const SearchCareersList = (filter) => {
    return dispatch => {
        Axios.get( Api.BaseUrl +`​/api​/gallerybysearch?text=${filter.Search}&limit=7&page=` + filter.currentPage, {
            'headers': { 'Authorization': localStorage.getItem('adminToken') },
        })
        .then((resp) => {
            dispatch(searchLoading());
            if (resp.data.status === 2) {
                logout()
                return false
            }
            if (resp.status === 200) {
                dispatch(searchSuccess(resp.data.data));
            } else {
                dispatch(searchError("error"));
            }
        })
            .catch(err => {
                dispatch(searchError(err.message));
            });
    };
};