
import React from 'react'
import * as ACTION_TYPE from './actiontypes'
import Axios from 'axios'
import { Api } from '../Const/Api'
import { Redirect } from 'react-router-dom';

function logout() {
    localStorage.clear();
    window.location.pathname = "/"
}

export const adminActivitySuccess = payload => {
    return {
        type: ACTION_TYPE.ADMIN_ACTIVITY_SUCCESS,
        payload
    }
}

export const adminActivityLoading = payload => {
    return {
        type: ACTION_TYPE.ADMIN_ACTIVITY_LOADING,
        payload
    }
}

export const adminActivityError = payload => {
    return {
        type: ACTION_TYPE.ADMIN_ACTIVITY_ERROR,
        payload
    }
}
// .then(resp => {
//     if (resp.status === 200) {
//         if (resp.data.status === "1") {
//             dispatch(partnershipSuccess(resp.data.data.list));
//         }
//         if (resp.data.status === "0") {
//             dispatch(partnershipError(resp.data.message));
//         }
//         if (resp.data.status === "2") {
//             logout()
//             return false
//         }
//         else {
//             dispatch(partnershipError(resp.data.message));
//         }
//     } else {
//         dispatch(partnershipError("error"));
//     }
//     dispatch(partnershipLoading(false));
// })


export const Adminactvity = () => {
    return dispatch => {
        Axios.get(`${Api.BaseUrl}/api/adminactivities`, {
            'headers': { 'Authorization': localStorage.getItem('adminToken') }
        })
            .then(resp => {   
                if (resp.status === 200) {
                dispatch(adminActivityLoading(false));  
                if (resp.data.status === "1") {
                    dispatch(adminActivitySuccess(resp.data.data.list));
                }
                if (resp.data.status === "0") {
                    dispatch(adminActivityError(resp.data.message));
                }
                if (resp.data.status === "2") {
                    logout()
                    return false
                }
            }      
                else {
                    dispatch(adminActivityError("error"));
                }
                dispatch(adminActivityLoading(false));
            })
            .catch(err => {
                dispatch(adminActivityError(err.message));
            });
    };
};
export const AdminactvityDelete = userid => {
    return dispatch => {
        Axios.delete(Api.BaseUrl+'/api/adminactivities/' + userid, {
            'headers': { 'Authorization': localStorage.getItem('adminToken') }
        })
            .then(resp => {

                if (resp.data.status == 2) {
                    logout()
                    return false
                }
                dispatch(adminActivityLoading(false));
            })
            .catch(err => {
                console.log('errrorrrrrr-----JOB LIST', err)
                dispatch(adminActivityError(err.message));
            });
    };
};