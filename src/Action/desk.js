import React from 'react'
import * as ACTION_TYPE from './actiontypes'
import Axios from 'axios'
import { Api } from '../Const/Api'

function logout() {
    localStorage.clear();
    window.location.pathname = "/"
}

export const workspaceSuccess = payload => {
    return {
        type: ACTION_TYPE.WORKSPACE_REQUEST_SUCCESS,
        payload
    }
}

export const workspaceLoading = payload => {
    return {
        type: ACTION_TYPE.WORKSPACE_REQUEST_LOADING,
        payload
    }
}

export const workspaceError = payload => {
    return {
        type: ACTION_TYPE.WORKSPACE_REQUEST_ERROR,
        payload
    }
}

export const workspaceDeleteSuccess = payload => {
    return {
        type: ACTION_TYPE.WORKSPACE_DELETE_SUCCESS,
        payload
    }
}

export const workspaceDeleteLoading = payload => {
    return {
        type: ACTION_TYPE.WORKSPACE_DELETE_LOADING,
        payload
    }
}

export const workspaceDeleteError = payload => {
    return {
        type: ACTION_TYPE.WORKSPACE_DELETE_ERROR,
        payload
    }
}

//................

export const enterpriseSuccess = payload => {
    return {
        type: ACTION_TYPE.ENTERPRISE_DESK_SUCCESS,
        payload
    }
}

export const enterpriseLoading = payload => {
    return {
        type: ACTION_TYPE.ENTERPRISE_DESK_LOADING,
        payload
    }
}

export const enterpriseError = payload => {
    return {
        type: ACTION_TYPE.ENTERPRISE_DESK_ERROR,
        payload
    }
}

export const enterpriseDeleteSuccess = payload => {
    return {
        type: ACTION_TYPE.ENTERPRISE_DESK_DELETE_SUCCESS,
        payload
    }
}

export const enterpriseDeleteLoading = payload => {
    return {
        type: ACTION_TYPE.ENTERPRISE_DESK_DELETE_LOADING,
        payload
    }
}

export const enterpriseDeleteError = payload => {
    return {
        type: ACTION_TYPE.ENTERPRISE_DESK_DELETE_ERROR,
        payload
    }
}

// /api/joinplan

export const workspace = (datatype) => dispatch => Axios.get(Api.BaseUrl + "/api/joinplan?status=" + datatype, {
    'headers': { 'Authorization': localStorage.getItem('adminToken') }
})
    .then(resp => {

        if (resp.data.status === 2) {
            logout()
            return false
        }
        if (resp.status === 200) {
            dispatch(workspaceLoading(false));
            dispatch(workspaceSuccess(resp.data.data.list));
        } else {
            dispatch(workspaceError("error"));
        }
        dispatch(workspaceLoading(false));
    })
    .catch(err => {
        dispatch(workspaceError(err.message));
    });

export const Workspacemultiple = (data) => {
    return dispatch => {
        Axios.put(Api.BaseUrl + "/api/joinplan", data, {
            'headers': { 'Authorization': localStorage.getItem('adminToken') }
        })
            .then(response => {
                dispatch(workspaceLoading(false));
            })
            .catch(err => {
                dispatch(workspaceError(err.message));
            });
    }
}


export const workspaceDelete = (userid) => {
    return dispatch => {
        Axios.delete(Api.BaseUrl + "/api/joinplan/" + userid, {
            'headers': { 'Authorization': localStorage.getItem('adminToken') }
        })
            .then(resp => {
                if (resp.data.status === 2) {
                    logout()
                    return false
                }
                if (resp.status === 200) {
                    dispatch(workspaceDeleteLoading(false));
                    dispatch(workspaceDeleteSuccess(resp.data.data.list));
                } else {
                    dispatch(workspaceDeleteError("error"));
                }
                dispatch(workspaceDeleteLoading(false));
            })
            .catch(err => {
                dispatch(workspaceDeleteError(err.message));
            });
    };
};


export const enterprise = (datatype) => dispatch => Axios.get(Api.BaseUrl + "/api/innovate?status=" + datatype, {
            'headers': { 'Authorization': localStorage.getItem('adminToken') }
        })
            .then(resp => {
                if (resp.data.status === 2) {
                    logout()
                    return false
                }
                if (resp.status === 200) {
                    dispatch(enterpriseLoading(false));
                    dispatch(enterpriseSuccess(resp.data.data.list));
                } else {
                    dispatch(enterpriseError("error"));
                }
                dispatch(enterpriseLoading(false));
            })
            .catch(err => {
                dispatch(enterpriseError(err.message));
            });
    

export const enterpriseDelete = (userid) => {
    // alert('enter prise')
    return dispatch => {
        Axios.delete(Api.BaseUrl + "/api/innovate/" + userid, {
            'headers': { 'Authorization': localStorage.getItem('adminToken') }
        })
            .then(resp => {
                if (resp.data.status === 2) {
                    logout()
                    return false
                }
                if (resp.status === 200) {
                    dispatch(enterpriseDeleteLoading(false));
                    dispatch(enterpriseDeleteSuccess(resp.data.data.list));
                } else {
                    dispatch(enterpriseDeleteError("error"));
                }
                dispatch(enterpriseDeleteLoading(false));
            })
            .catch(err => {
                dispatch(enterpriseDeleteError(err.message));
            });
    };
};


export const Enterprisemultiple = (data) => {
    return dispatch => {
        Axios.put(Api.BaseUrl + "/api/innovate", data, {
            'headers': { 'Authorization': localStorage.getItem('adminToken') }
        })
            .then(response => {
                dispatch(workspaceLoading(false));
            })
            .catch(err => {
                dispatch(workspaceError(err.message));
            });
    }
}
