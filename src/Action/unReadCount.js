import React from 'react'
import * as ACTION_TYPE from './actiontypes'
import Axios from 'axios'
import { Api } from '../Const/Api'



export const unReadCountSuccess = payload =>{
    return {
        type : ACTION_TYPE.UNREAD_COUNT_SUCCESS,
        payload
    }
} 

export const unReadCountLoading = payload =>{
    return {
        type : ACTION_TYPE.UNREAD_COUNT_LOADING,
        payload
    }
} 

export const unReadCountError = payload =>{
    return {
        type : ACTION_TYPE.UNREAD_COUNT_ERROR,
        payload
    }
} 


export const unReadCount = () => {
    return dispatch => {
        Axios.get(Api.BaseUrl + "/api/getunreaddatacount", {
            'headers': { 'Authorization': localStorage.getItem('adminToken') }
        })
            .then(resp => {
                if (resp.status === 200) {
                    if (resp.data.status === "1") {
                        dispatch(unReadCountSuccess(resp.data.data));
                    }
                    if (resp.data.status === "0") {
                        dispatch(unReadCountError(resp.data.message));
                    }
                    else {
                        dispatch(unReadCountError(resp.data.message));
                    }
                 
                } else {
                    dispatch(unReadCountError("error"));
                }


                dispatch(unReadCountLoading(false));
            })
            .catch(err => {
                dispatch(unReadCountError(err.message));
            });
    };
};