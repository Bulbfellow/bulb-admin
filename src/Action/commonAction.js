import * as ACTION_TYPE from './actiontypes'
import Axios from 'axios'
import { Api } from '../Const/Api'

export const UploadfileSuccess = payload => {
    return {
        type: ACTION_TYPE.UPLOAD_FILE_SUCCESS,
        payload
    }
}
export const UploadfileLoading = () => {
    return {
        type: ACTION_TYPE.UPLOAD_FILE_LOADING,
    }
}
export const UploadfileError = payload => {
    return {
        type: ACTION_TYPE.UPLOAD_FILE_ERROR,
        payload
    }
}

export const Uploadfile = (file) => {
    let formData = new FormData()
    formData.append(
        'file',
        file
    )
    return dispatch => {
        Axios.post(Api.BaseUrl+'/api/file-upload', formData)
            .then(resp => {
                console.log('img', resp)
                dispatch(UploadfileLoading());
                if (resp.status === 200) {
                    if (resp.data.status === "1") {
                        dispatch(UploadfileSuccess(resp.data.data.image_url));
                    }
                    if (resp.data.status === "0") {
                        dispatch(UploadfileError(resp.data.message));
                    }
                } else {
                    dispatch(UploadfileError("we are unable to fetch your data Please check your internet connection or try again"));
                }
            })
            .catch(err => {
                dispatch(UploadfileError(err.message));
            });
    };
};
