import React, { Component } from 'react';
import './App.css';
import { Provider } from 'react-redux'
import { createStore, applyMiddleware } from 'redux'
import rootReducers from './Reducer'
import thunk from 'redux-thunk';
import { composeWithDevTools } from 'redux-devtools-extension';
import Navigation from './Container/Navigation';
import './Assets/css/style.css';
import './Assets/css/fonts.css';


const store = createStore(rootReducers, {},
  composeWithDevTools(
    applyMiddleware(thunk))
)

class App extends Component {
  render() {
    return (
      <Provider store={store}>
        <Navigation />
      </Provider>
    )
  }
}

export default App;