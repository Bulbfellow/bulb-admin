import * as types from "../Action/actiontypes";

const initialState = {
    resp: [],
    loading: true,
    error: '',
    pagination :[]
};

export const AddCarrear = (state = initialState, { type, payload }) => {
    switch (type) {
        case types.ADD_CAREERS_SUCCESS:
            return {
                ...state,
                loading: false,
                resp: payload
            };
        case types.ADD_CAREERS_ERROR:
            return {
                ...state,
                loading: true,
                error: payload
            };
        case types.ADD_CAREERS_LOADING:
            return {
                ...state,
                resp: payload
            };
        default:
            return state;
    }
}

export const CarrearJobListingData = (state = initialState, { type, payload }) => {
    switch (type) {
        case types.CAREERS_GET_LISTING_SUCCESS:
            return {
                ...state,
                loading: false,
                resp: payload
            };
        case types.CAREERS_GET_LISTING_ERROR:
            return {
                ...state,
                loading: false,
                error: payload
            };
        case types.CAREERS_GET_LISTING_LOADING:
            return {
                ...state,
                loading: true
            };
        case types.CAREERS_GET_LISTING_COUNT_SUCCESS:
                return {
                    ...state,
                    loading : false,
                    pagination: payload
                };
        default:
            return state;
    }
}

export const Carrerformlist = (state = initialState, { type, payload }) => {
    switch (type) {
        case types.CAREERS_GETFORM_DATA_SUCCESS:
            return {
                ...state,
                loading: true,
                resp: payload
            };
        case types.CAREERS_GETFORM_DATA_ERROR:
            return {
                ...state,
                loading: true,
                error: payload
            };
        case types.CAREERS_GETFORM_DATA_LOADING:
            return {
                ...state,
                loading: payload
            };
        default:
            return state;
    }
}