import * as types from "../Action/actiontypes";

const initialState = {
    resp: [],
    loading: true,
    error: ''
};

export const UnReadcount = (state = initialState, { type, payload }) => {
    // console.log("reducer", payload)
    switch (type) {
        case types.UNREAD_COUNT_SUCCESS:
            return {
                ...state,
                loading: true,
                resp: payload
            };
        case types.UNREAD_COUNT_ERROR:
            return {
                ...state,
                loading: true,
                error: payload
            };
        case types.UNREAD_COUNT_LOADING:
            return {
                ...state,
                loading: payload
            };
        default:
            return state;
    }
}