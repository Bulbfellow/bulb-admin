import * as types from "../Action/actiontypes";

const initialState = {
    resp: [],
    loading: true,
    error: ''
};
export const FileuploadReducer = (state = initialState, { type, payload }) => {
    switch (type) {
        case types.UPLOAD_FILE_SUCCESS:
            return {
                ...state,
                loading: false,
                resp: payload
            };
        case types.UPLOAD_FILE_ERROR:
            return {
                ...state,
                loading: false,
                error: payload
            };
        case types.UPLOAD_FILE_LOADING:
            return {
                ...state,
                loading: true
            };
        default:
            return state;
    }
}