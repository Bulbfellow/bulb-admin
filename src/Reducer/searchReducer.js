import * as types from "../Action/actiontypes";

const initialState = {
    resp: [],
    loading: true,
    error: '',
    pagination : []
};


export const SearchEventData = (state = initialState, { type, payload }) => {
    switch (type) {
        case types.DATA_SEARCH_SUCCESS:
            return {
                ...state,
                loading: false,
                resp: payload.list,
                pagination : payload
            };
        case types.DATA_SEARCH_ERROR:
            return {
                ...state,
                loading: false,
                error: payload
            };
        case types.DATA_SEARCH_LOADING:
            return {
                ...state,
                loading: true
            };
        default:
            return state;
    }
}


export const SearchTagData = (state = initialState, { type, payload }) => {
    switch (type) {
        case types.DATA_SEARCH_SUCCESS:
            return {
                ...state,
                loading: false,
                resp: payload.list,
                pagination : payload
            };
        case types.DATA_SEARCH_ERROR:
            return {
                ...state,
                loading: false,
                error: payload
            };
        case types.DATA_SEARCH_LOADING:
            return {
                ...state,
                loading: true
            };
        default:
            return state;
    }
}


export const SearchPressData = (state = initialState, { type, payload }) => {
    switch (type) {
        case types.DATA_SEARCH_SUCCESS:
            return {
                ...state,
                loading: false,
                resp: payload.list,
                pagination : payload
            };
        case types.DATA_SEARCH_ERROR:
            return {
                ...state,
                loading: false,
                error: payload
            };
        case types.DATA_SEARCH_LOADING:
            return {
                ...state,
                loading: true
            };
        default:
            return state;
    }
}


export const SearchGalleryData = (state = initialState, { type, payload }) => {
    switch (type) {
        case types.DATA_SEARCH_SUCCESS:
            return {
                ...state,
                loading: false,
                resp: payload.list,
                pagination : payload
            };
        case types.DATA_SEARCH_ERROR:
            return {
                ...state,
                loading: false,
                error: payload
            };
        case types.DATA_SEARCH_LOADING:
            return {
                ...state,
                loading: true
            };
        default:
            return state;
    }
}

export const SearchCareersData = (state = initialState, { type, payload }) => {
    switch (type) {
        case types.DATA_SEARCH_SUCCESS:
            return {
                ...state,
                loading: false,
                resp: payload.list,
                pagination : payload
            };
        case types.DATA_SEARCH_ERROR:
            return {
                ...state,
                loading: false,
                error: payload
            };
        case types.DATA_SEARCH_LOADING:
            return {
                ...state,
                loading: true
            };
        default:
            return state;
    }
}