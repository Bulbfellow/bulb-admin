import { combineReducers } from 'redux'
import classesadd from './Addclass'
import loginReducer from './Login'
import { partnership, hireadeveloper, requestatour, scheduleavisit, applyforincubation, submitaidea, newsletterGetData } from './Applicationrequest'
import {
  NewsfeedEventAdd, NewsfeedEventGet, NewsfeedEventDelete,
  NewsfeedEventUpdateReducer, NewsfeedPressAdd, NewsfeedPressGet,
  NewsfeedPressDelete, NewsfeedTagAdd, NewsfeedTagGet, NewsfeedTagDelete,
  NewsfeedGalleryDelete, NewsfeedGalleryGet, NewsfeedGalleryAdd, NewsfeedGalleryUpdate
} from './newsfeed';
import { workspace, enterprise, workspaceDelete, enterpriseDelete } from './Desk'
import { AddCarrear, Carrerformlist, CarrearJobListingData } from './Careers'
import { UnReadcount } from './UnReadcount'
import { SearchEventData, SearchPressData, SearchTagData, SearchGalleryData } from './searchReducer'
import { AdminActivity } from './activity'
import { AddWorkSpace, GetWorkSpace, PutWorkSpace, DeleteWorkSpace } from './Workspace'
import { addAdmin, getAdminList, deleteAdmin, updateAdmin, getAdminListBySearch } from './manageAdmin'
import { FileuploadReducer } from './commonReducer'

export default combineReducers({
  classesadd: classesadd,
  login: loginReducer,
  partnership: partnership,
  hireadeveloper: hireadeveloper,
  requestatour: requestatour,
  scheduleavisit: scheduleavisit,
  applyforincubation: applyforincubation,
  submitaidea: submitaidea,
  NewsfeedEventAdd: NewsfeedEventAdd,
  NewsfeedEventGet: NewsfeedEventGet,
  NewsfeedPressDelete: NewsfeedPressDelete,
  NewsfeedEventDelete: NewsfeedEventDelete,
  NewsfeedEventUpdateReducer: NewsfeedEventUpdateReducer,
  NewsfeedPressAdd: NewsfeedPressAdd,
  NewsfeedPressGet: NewsfeedPressGet,
  NewsfeedTagAdd: NewsfeedTagAdd,
  workspace: workspace,
  enterprise: enterprise,
  AddCarrear: AddCarrear,
  Carrerformlist: Carrerformlist,
  CarrearJobListingData: CarrearJobListingData,
  newsletterGetData: newsletterGetData,
  NewsfeedTagDelete: NewsfeedTagDelete,
  NewsfeedTagGet: NewsfeedTagGet,
  NewsfeedGalleryDelete: NewsfeedGalleryDelete,
  NewsfeedGalleryGet: NewsfeedGalleryGet,
  NewsfeedGalleryAdd: NewsfeedGalleryAdd,
  NewsfeedGalleryUpdate: NewsfeedGalleryUpdate,
  AdminActivity: AdminActivity,
  addAdmin: addAdmin,
  workspaceDelete: workspaceDelete,
  enterpriseDelete: enterpriseDelete,
  getAdminList: getAdminList,
  deleteAdmin: deleteAdmin,
  updateAdmin: updateAdmin,
  UnReadcount: UnReadcount,
  getAdminListBySearch: getAdminListBySearch,
  SearchEventData: SearchEventData,
  SearchPressData: SearchPressData,
  SearchTagData: SearchTagData,
  SearchGalleryData: SearchGalleryData,
  AddWorkSpace: AddWorkSpace,
  GetWorkSpace: GetWorkSpace,
  PutWorkSpace: PutWorkSpace,
  DeleteWorkSpace: DeleteWorkSpace,
  FileuploadReducer: FileuploadReducer
})