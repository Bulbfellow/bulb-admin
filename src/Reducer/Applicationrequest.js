import * as types from "../Action/actiontypes";

const initialState = {
    resp: [],
    loading: true,
    error: ''
};

export const partnership = (state = initialState, { type, payload }) => {
    switch (type) {
        case types.PARTNERSHIP_USER_SUCCESS:
            return {
                ...state,
                loading: true,
                resp: payload
            };
        case types.PARTNERSHIP_USER_ERROR:
            return {
                ...state,
                loading: true,
                error: payload
            };
        case types.PARTNERSHIP_USER_LOADING:
            return {
                ...state,
                loading: payload
            };
        default:
            return state;
    }
}

export const hireadeveloper = (state = initialState, { type, payload }) => {
    switch (type) {
        case types.HIREADEVELOPER_SUCCESS:
            return {
                ...state,
                loading: true,
                resp: payload
            };
        case types.HIREADEVELOPER_ERROR:
            return {
                ...state,
                loading: true,
                error: payload
            };
        case types.HIREADEVELOPER_LOADING:
            return {
                ...state,
                loading: payload
            };
        default:
            return state;
    }
}

export const requestatour = (state = initialState, { type, payload }) => {
    switch (type) {
        case types.REQUESTATOUR_SUCCESS:
            return {
                ...state,
                loading: true,
                resp: payload
            };
        case types.REQUESTATOUR_ERROR:
            return {
                ...state,
                loading: true,
                error: payload
            };
        case types.REQUESTATOUR_LOADING:
            return {
                ...state,
                loading: payload
            };
        default:
            return state;
    }
}

export const scheduleavisit = (state = initialState, { type, payload }) => {
    switch (type) {
        case types.SCHEDULEAVISIT_SUCCESS:
            return {
                ...state,
                loading: true,
                resp: payload
            };
        case types.SCHEDULEAVISIT_ERROR:
            return {
                ...state,
                loading: true,
                error: payload
            };
        case types.SCHEDULEAVISIT_LOADING:
            return {
                ...state,
                loading: payload
            };
        default:
            return state;
    }
}

export const applyforincubation = (state = initialState, { type, payload }) => {
    switch (type) {
        case types.APPLYFORINCUBATION_SUCCESS:
            return {
                ...state,
                loading: true,
                resp: payload
            };
        case types.APPLYFORINCUBATION_ERROR:
            return {
                ...state,
                loading: true,
                error: payload
            };
        case types.APPLYFORINCUBATION_LOADING:
            return {
                ...state,
                loading: payload
            };
        default:
            return state;
    }
}

export const submitaidea = (state = initialState, { type, payload }) => {
    switch (type) {
        case types.SUBMITAIDEA_SUCCESS:
            return {
                ...state,
                loading: true,
                resp: payload
            };
        case types.SUBMITAIDEA_ERROR:
            return {
                ...state,
                loading: true,
                error: payload
            };
        case types.SUBMITAIDEA_LOADING:
            return {
                ...state,
                loading: payload
            };
        default:
            return state;
    }
}

export const newsletterGetData = (state = initialState, { type, payload }) => {
    switch (type) {
        case types.NEWSLETTER_USER_SUCCESS:
            return {
                ...state,
                loading: false,
                resp: payload
            };
        case types.NEWSLETTER_USER_ERROR:
            return {
                ...state,
                loading: false,
                error: payload
            };
        case types.NEWSLETTER_USER_LOADING:
            return {
                ...state,
                loading: true
            };
        default:
            return state;
    }
}