const data = {
    loginstatus: localStorage.getItem('loginStatus'),
    admintoken: localStorage.getItem('adminToken'),
    adminName: localStorage.getItem('adminName'),
    adminId: localStorage.getItem('adminId'),
    adminRole: localStorage.getItem('adminRole')
}

const loginReducer = (state = data, { type, payload }) => {
    switch (type) {
        case 'LOGIN_STATUS':
            return {
                ...state,
                loginstatus: payload
            }
        case 'ADMIN_NAME':
            return {
                ...state,
                adminName: payload
            }
        case 'ADMIN_ID':
            return {
                ...state,
                adminId: payload
            }
        case 'ADMIN_ROLE':
            return {
                ...state,
                adminRole: payload
            }
        case 'LOGIN_INFO_USER':
            return {
                ...state,
                logindata: payload
            }
        default:
            return state;
    }
}

export default loginReducer
