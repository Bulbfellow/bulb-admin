import * as types from "../Action/actiontypes";

const initialState = {
    resp: [],
    loading: true,
    error: ''
};

export const workspace = (state = initialState, { type, payload }) => {
    // console.log('payload----------',payload)
    switch (type) {
        case types.WORKSPACE_REQUEST_SUCCESS:
            return {
                ...state,
                loading: true,
                resp: payload
            };
        case types.WORKSPACE_REQUEST_ERROR:
            return {
                ...state,
                loading: true,
                error: payload
            };
        case types.WORKSPACE_REQUEST_LOADING:
            return {
                ...state,
                loading: payload
            };
        default:
            return state;
    }
}

export const workspaceDelete = (state = initialState, { type, payload }) => {
    // console.log('payload----------',payload)
    switch (type) {
        case types.WORKSPACE_DELETE_SUCCESS:
            return {
                ...state,
                loading: true,
                resp: payload
            };
        case types.WORKSPACE_DELETE_ERROR:
            return {
                ...state,
                loading: true,
                error: payload
            };
        case types.WORKSPACE_DELETE_LOADING:
            return {
                ...state,
                loading: payload
            };
        default:
            return state;
    }
}

export const enterprise = (state = initialState, { type, payload }) => {
    // console.log('payload----------',payload)
    switch (type) {
        case types.ENTERPRISE_DESK_SUCCESS:
            return {
                ...state,
                loading: true,
                resp: payload
            };
        case types.ENTERPRISE_DESK_ERROR:
            return {
                ...state,
                loading: true,
                error: payload
            };
        case types.ENTERPRISE_DESK_LOADING:
            return {
                ...state,
                loading: payload
            };
        default:
            return state;
    }
}

export const enterpriseDelete = (state = initialState, { type, payload }) => {
    // console.log('payload----------',payload)
    switch (type) {
        case types.ENTERPRISE_DESK_DELETE_SUCCESS:
            return {
                ...state,
                loading: true,
                resp: payload
            };
        case types.ENTERPRISE_DESK_DELETE_ERROR:
            return {
                ...state,
                loading: true,
                error: payload
            };
        case types.ENTERPRISE_DESK_DELETE_LOADING:
            return {
                ...state,
                loading: payload
            };
        default:
            return state;
    }
}