import * as types from "../Action/actiontypes";

const initialState = {
    resp: [],
    loading: true,
    error: '',
    pagination: [],
    statuscode :''
};

export const NewsfeedEventAdd = (state = initialState, { type, payload }) => {
    switch (type) {
        case types.NEWSFEED_ADD_EVENT_SUCCESS:
            return {
                ...state,
                loading: false,
                resp: payload
            };
        case types.NEWSFEED_ADD_EVENT_ERROR:
            return {
                ...state,
                loading: false,
                error: payload
            };
        case types.NEWSFEED_ADD_EVENT_LOADING:
            return {
                ...state,
                loading: true
            };
        default:
            return state;
    }
}

export const NewsfeedEventGet = (state = initialState, { type, payload }) => {
    // console.log("payload news", payload)
    switch (type) {
        case types.NEWSFEED_GET_EVENT_SUCCESS:
            return {
                ...state,
                loading: false,
                resp: payload.list,
                pagination: payload
            };
        case types.NEWSFEED_GET_EVENT_ERROR:
            return {
                ...state,
                loading: true,
                error: payload
            };
        case types.NEWSFEED_GET_EVENT_LOADING:
            return {
                ...state,
                loading: true
            };
        default:
            return state;
    }
}

export const NewsfeedEventDelete = (state = initialState, { type, payload }) => {
    // console.log("payload news", payload)
    switch (type) {
        case types.NEWSFEED_DELETE_EVENT_SUCCESS:
            return {
                ...state,
                loading: true,
                resp: payload
            };
        case types.NEWSFEED_DELETE_EVENT_ERROR:
            return {
                ...state,
                loading: true,
                error: payload
            };
        case types.NEWSFEED_DELETE_EVENT_LOADING:
            return {
                ...state,
                loading: payload
            };
        default:
            return state;
    }
}

export const NewsfeedEventUpdateReducer = (state = initialState, { type, payload }) => {
    switch (type) {
        case types.NEWSFEED_UPDATE_EVENT_SUCCESS:
            return {
                ...state,
                loading: false,
                statuscode: payload
            };
        case types.NEWSFEED_UPDATE_EVENT_ERROR:
            return {
                ...state,
                loading: false,
                error: payload
            };
        case types.NEWSFEED_UPDATE_EVENT_LOADING:
            return {
                ...state,
                loading: true
            };
        default:
            return state;
    }
}


// **************** ------ PRESS Start **************//

export const NewsfeedPressAdd = (state = initialState, { type, payload }) => {
    switch (type) {
        case types.NEWSFEED_ADD_PRESS_SUCCESS:
            return {
                ...state,
                loading: false,
                resp: payload
            };
        case types.NEWSFEED_ADD_PRESS_ERROR:
            return {
                ...state,
                loading: false,
                error: payload
            };
        case types.NEWSFEED_ADD_PRESS_LOADING:
            return {
                ...state,
                loading: true
            };
        default:
            return state;
    }
}

export const NewsfeedPressGet = (state = initialState, { type, payload }) => {
    switch (type) {
        case types.NEWSFEED_GET_PRESS_SUCCESS:
            return {
                ...state,
                loading: false,
                resp: payload.list,
                pagination: payload
            };
        case types.NEWSFEED_GET_PRESS_ERROR:
            return {
                ...state,
                loading: false,
                error: payload
            };
        case types.NEWSFEED_GET_PRESS_LOADING:
            return {
                ...state,
                loading: payload
            };
        default:
            return state;
    }
}

export const NewsfeedPressDelete = (state = initialState, { type, payload }) => {
    switch (type) {
        case types.NEWSFEED_DELETE_PRESS_SUCCESS:
            return {
                ...state,
                loading: true,
                resp: payload
            };
        case types.NEWSFEED_DELETE_PRESS_ERROR:
            return {
                ...state,
                loading: true,
                error: payload
            };
        case types.NEWSFEED_DELETE_PRESS_LOADING:
            return {
                ...state,
                loading: payload
            };
        default:
            return state;
    }
}

export const NewsfeedPressUpdate = (state = initialState, { type, payload }) => {
    switch (type) {
        case types.NEWSFEED_UPDATE_PRESS_SUCCESS:
            return {
                ...state,
                loading: false,
                resp: payload
            };
        case types.NEWSFEED_UPDATE_PRESS_ERROR:
            return {
                ...state,
                loading: true,
                error: payload
            };
        case types.NEWSFEED_UPDATE_PRESS_LOADING:
            return {
                ...state,
                loading: payload
            };
        default:
            return state;
    }
}


//_________________******** Tag 
export const NewsfeedTagAdd = (state = initialState, { type, payload }) => {
    switch (type) {
        case types.NEWSFEED_ADD_TAG_SUCCESS:
            return {
                ...state,
                loading: true,
                resp: payload
            };
        case types.NEWSFEED_ADD_TAG_ERROR:
            return {
                ...state,
                loading: true,
                error: payload
            };
        case types.NEWSFEED_ADD_TAG_LOADING:
            return {
                ...state,
                loading: payload
            };
        default:
            return state;
    }
}

export const NewsfeedTagGet = (state = initialState, { type, payload }) => {
    switch (type) {
        case types.NEWSFEED_GET_TAG_SUCCESS:
            return {
                ...state,
                loading: false,
                resp: payload.list,
                pagination: payload
            };
        case types.NEWSFEED_GET_TAG_ERROR:
            return {
                ...state,
                loading: false,
                error: payload
            };
        case types.NEWSFEED_GET_TAG_LOADING:
            return {
                ...state,
                loading: true
            };
        default:
            return state;
    }
}

export const NewsfeedTagDelete = (state = initialState, { type, payload }) => {
    switch (type) {
        case types.NEWSFEED_DELETE_TAG_SUCCESS:
            return {
                ...state,
                loading: true,
                resp: payload
            };
        case types.NEWSFEED_DELETE_TAG_ERROR:
            return {
                ...state,
                loading: true,
                error: payload
            };
        case types.NEWSFEED_DELETE_TAG_LOADING:
            return {
                ...state,
                loading: payload
            };
        default:
            return state;
    }
}

export const NewsfeedTagUpdate = (state = initialState, { type, payload }) => {
    switch (type) {
        case types.NEWSFEED_UPDATE_TAG_SUCCESS:
            return {
                ...state,
                loading: true,
                resp: payload
            };
        case types.NEWSFEED_UPDATE_TAG_ERROR:
            return {
                ...state,
                loading: true,
                error: payload
            };
        case types.NEWSFEED_UPDATE_TAG_LOADING:
            return {
                ...state,
                loading: payload
            };
        default:
            return state;
    }
}

// - Gallery

export const NewsfeedGalleryAdd = (state = initialState, { type, payload }) => {
    switch (type) {
        case types.NEWSFEED_ADD_GALLERY_SUCCESS:
            return {
                ...state,
                loading: true,
                resp: payload
            };
        case types.NEWSFEED_ADD_GALLERY_ERROR:
            return {
                ...state,
                loading: true,
                error: payload
            };
        case types.NEWSFEED_ADD_GALLERY_LOADING:
            return {
                ...state,
                loading: payload
            };
        default:
            return state;
    }
}

export const NewsfeedGalleryGet = (state = initialState, { type, payload }) => {
    switch (type) {
        case types.NEWSFEED_GET_GALLERY_SUCCESS:
            return {
                ...state,
                loading: false,
                resp: payload.list,
                pagination: payload
            };
        case types.NEWSFEED_GET_GALLERY_ERROR:
            return {
                ...state,
                loading: false,
                error: payload
            };
        case types.NEWSFEED_GET_GALLERY_LOADING:
            return {
                ...state,
                loading: true
            };
        default:
            return state;
    }
}

export const NewsfeedGalleryDelete = (state = initialState, { type, payload }) => {
    switch (type) {
        case types.NEWSFEED_DELETE_GALLERY_SUCCESS:
            return {
                ...state,
                loading: true,
                resp: payload
            };
        case types.NEWSFEED_DELETE_GALLERY_ERROR:
            return {
                ...state,
                loading: true,
                error: payload
            };
        case types.NEWSFEED_DELETE_GALLERY_LOADING:
            return {
                ...state,
                loading: payload
            };
        default:
            return state;
    }
}

export const NewsfeedGalleryUpdate = (state = initialState, { type, payload }) => {
    switch (type) {
        case types.NEWSFEED_UPDATE_GALLERY_SUCCESS:
            return {
                ...state,
                loading: true,
                resp: payload
            };
        case types.NEWSFEED_UPDATE_GALLERY_ERROR:
            return {
                ...state,
                loading: true,
                error: payload
            };
        case types.NEWSFEED_UPDATE_GALLERY_LOADING:
            return {
                ...state,
                loading: payload
            };
        default:
            return state;
    }
}