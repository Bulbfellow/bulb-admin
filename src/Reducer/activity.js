import * as types from "../Action/actiontypes";

const initialState = {
    resp: [],
    loading: true,
    error: ''
};

export const AdminActivity = (state = initialState, { type, payload }) => {
    switch (type) {
        case types.ADMIN_ACTIVITY_SUCCESS:
            return {
                ...state,
                loading: true,
                resp: payload
            };
        case types.ADMIN_ACTIVITY_ERROR:
            return {
                ...state,
                loading: true,
                error: payload
            };
        case types.ADMIN_ACTIVITY_LOADING:
            return {
                ...state,
                loading: payload
            };
        default:
            return state;
    }
}