import * as types from "../Action/actiontypes";

const initialState = {
    resp: [],
    loading: true,
    error: '',
    pagination: [],
    message :''
};

export const addAdmin = (state = initialState, { type, payload }) => {
    switch (type) {
        case types.MANAGE_ADD_ADMIN_SUCCESS:
            return {
                ...state,
                loading: false,
                message: payload
            };
        case types.MANAGE_ADD_ADMIN_ERROR:
            return {
                ...state,
                loading: false,
                message: payload
            };
        case types.MANAGE_ADD_ADMIN_LOADING:
            return {
                ...state,
                loading: true
            };
        default:
            return state;
    }
}

export const getAdminList = (state = initialState, { type, payload }) => {
    switch (type) {
        case types.MANAGE_GET_ADMIN_SUCCESS:
            return {
                ...state,
                loading: false,
                resp: payload
            };
        case types.MANAGE_GET_ADMIN_ERROR:
            return {
                ...state,
                loading: false,
                error: payload
            };
        case types.MANAGE_GET_ADMIN_LOADING:
            return {
                ...state,
                loading: true
            };
        case types.MANAGE_GET_ADMIN_COUNT_SUCCESS:
            return {
                ...state,
                loading: false,
                pagination: payload
            };
        default:
            return state;
    }
}

export const getAdminListBySearch = (state = initialState, { type, payload }) => {
    switch (type) {
        case types.MANAGE_GET_ADMIN_SEARCH_SUCCESS:
            return {
                ...state,
                loading: false,
                resp: payload
            };
        case types.MANAGE_GET_ADMIN_SEARCH_ERROR:
            return {
                ...state,
                loading: false,
                error: payload
            };
        case types.MANAGE_GET_ADMIN_SEARCH_LOADING:
            return {
                ...state,
                loading: true
            };
        case types.MANAGE_GET_ADMIN_SEARCH_COUNT_SUCCESS:
            return {
                ...state,
                pagination: payload
            };
        default:
            return state;
    }
}

export const deleteAdmin = (state = initialState, { type, payload }) => {
    switch (type) {
        case types.MANAGE_DELETE_ADMIN_SUCCESS:
            return {
                ...state,
                loading: false,
                resp: payload
            };
        case types.MANAGE_DELETE_ADMIN_ERROR:
            return {
                ...state,
                loading: false,
                error: payload
            };
        case types.MANAGE_DELETE_ADMIN_LOADING:
            return {
                ...state,
                loading: true
            };
        default:
            return state;
    }
}

export const updateAdmin = (state = initialState, { type, payload }) => {
    switch (type) {
        case types.MANAGE_PUT_ADMIN_SUCCESS:
            return {
                ...state,
                loading: false,
                message: payload
            };
        case types.MANAGE_PUT_ADMIN_ERROR:
            return {
                ...state,
                loading: false,
                message: payload
            };
        case types.MANAGE_PUT_ADMIN_LOADING:
            return {
                ...state,
                loading: true
            };
        default:
            return state;
    }
}