import * as types from "../Action/actiontypes";

const initialState = {
    resp: [],
    loading: true,
    error: '',
    pagination: [],
    message: ''
};

export const AddWorkSpace = (state = initialState, { type, payload }) => {
    switch (type) {
        case types.WORKSPACE_ADD_SUCCESS:
            return {
                ...state,
                loading: false,
                message: payload
            };
        case types.WORKSPACE_ADD_ERROR:
            return {
                ...state,
                loading: false,
                message: payload
            };
        case types.WORKSPACE_ADD_LOADING:
            return {
                ...state,
                loading: true,
            };
        default:
            return state;
    }
}

export const GetWorkSpace = (state = initialState, { type, payload }) => {
    switch (type) {
        case types.WORKSPACE_GET_SUCCESS:
            return {
                ...state,
                loading: false,
                resp: payload
            };
        case types.WORKSPACE_GET_ERROR:
            return {
                ...state,
                loading: false,
                error: payload
            };
        case types.WORKSPACE_GET_LOADING:
            return {
                ...state,
                loading: true,
            };
        default:
            return state;
    }
}


export const PutWorkSpace = (state = initialState, { type, payload }) => {
    switch (type) {
        case types.WORKSPACE_PUT_SUCCESS:
            return {
                ...state,
                loading: false,
                resp: payload
            };
        case types.WORKSPACE_PUT_ERROR:
            return {
                ...state,
                loading: false,
                error: payload
            };
        case types.WORKSPACE_PUT_LOADING:
            return {
                ...state,
                loading: true,
            };
        default:
            return state;
    }
}

export const DeleteWorkSpace = (state = initialState, { type, payload }) => {
    switch (type) {
        case types.WORKSPACE_LIST_DELETE_SUCCESS:
            return {
                ...state,
                loading: false,
                resp: payload
            };
        case types.WORKSPACE_LIST_DELETE_ERROR:
            return {
                ...state,
                loading: false,
                error: payload
            };
        case types.WORKSPACE_LIST_DELETE_LOADING:
            return {
                ...state,
                loading: true,
            };
        default:
            return state;
    }
}