import React, {  useState } from 'react'
import { Link } from 'react-router-dom'
// import Delete from '../../Assets/img/delete-dark.svg'
// import DeleteLight from '../../Assets/img/delete-light.svg'
// import Archive from '../../Assets/img/archive-dark.svg'
// import ArchiveLight from '../../Assets/img/archive-light.svg'
// import Print from '../../Assets/img/print.svg'
import '../Assets/css/style.css'
// import Footer from '../../Container/Footer'
// import Axuis from 'axios'
// import { connect } from 'react-redux'
// import { CareersJobListing, CareersJobListingDelete, CareersFormfromUserListing, CareersFormdatafromUserDelete } from '../../Action/careers'
// import Deletemodal from '../../Component/Deletemodal'
// import Datatable from '../../Component/Datatable'
// import Quicknotification from '../../Component/Quicknotification'
// import { convertDatePickerTime, Commondateconvert } from '../../Const/function'
// import Search from '../../Component/Search'
import CareersEntriesRequest from '../../Container/Entries/CareersEntries/ViewCareersApplicationRequest';
import Careerformdata from '../../Container/Applicationrequest/Career'
import {
    BrowserRouter as Router, Switch, Route, 
    useRouteMatch
} from 'react-router-dom'
import CareersList from './Careerslist'

const Careers = () => {

    const [breadcrumb, setbreadcrumb] = useState('Career listings')
    let { path, url } = useRouteMatch();

    return (
        <>
            <section className="wrapper px-4 ">
                <div className="container">
                    <div className="page-title mb-4">
                        <div className="row align-items-center">
                            <div className="col-md-5">
                                <div className="page-title-inner">
                                    <h4>Careers</h4>
                                </div>
                            </div>
                            <div className="col-md-7">
                                <nav aria-label="breadcrumb">
                                    <ol className="breadcrumb mb-2 justify-content-end p-0 bg-transparent align-items-center">
                                        <li className="breadcrumb-item"><Link to="/">Dashboard</Link></li>
                                        <li className="breadcrumb-item"><Link to="#">Content manager</Link></li>
                                        <li className="breadcrumb-item active" aria-current="page">{/*this.state.breadcrumb*/}</li>
                                    </ol>
                                </nav>
                            </div>
                        </div>
                    </div>

                    <div className="nees-feeds">
                        <div className="row">
                            <div className="col-12 cus-tabs">
                                <ul className="nav nav-tabs">
                                    <Link to={url} className="nav-item cursor">
                                        <a onClick={() => {
                                            setbreadcrumb("Career listings")
                                        }} className={`nav-link ${breadcrumb === "Career listings" && "active"} `}>Career listings</a>
                                    </Link>
                                    <Link to={`${path}/carrear-applicant-tab`} className="nav-item cursor">
                                        <a onClick={() => {
                                            setbreadcrumb("Career application request")
                                        }} className={`nav-link ${breadcrumb === "Career application request" && "active"} `}>Career application request </a>
                                    </Link>
                                </ul>

                                <Switch>
                                    <Route exact path={url} component={CareersList} />
                                    <Route path={`${path}/carrear-applicant-tab`} component={Careerformdata} />
                                    <Route path={`${path}/view-career-request/:id`} component={CareersEntriesRequest} />
                                </Switch>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </>
    )
}
export default Careers
//     constructor(props) {
//         super(props)
//         this.state = {
//             breadcrumb: "Career listings",
//             deleteid: '',
//             currentPage: 1,
//             data: [],
//             dataLength: '',
//             firstIndex: '',
//             datalimit: 7,
//             searchvalue: '',
//             paginationDetail: '',


//         }
//     }

//     componentDidMount() {

//         let data = {
//             searchdata: this.state.searchvalue,
//             currentPage: this.state.currentPage,
//             contract: "All",
//             datalimit: this.state.datalimit
//         }
//         this.props.viewCareersDispatch(data)
//         this.props.carrerFormdataFromuser()
//     }

//     componentDidUpdate(prevProps, prevState) {
//         // if (prevState.currentPage !== this.state.currentPage) {
//         //     this.props.viewCareersDispatch(this.state.currentPage)
//         // }
//         // if (prevProps.CareerData.resp !== this.props.CareerData.resp) {
//         //     const lastCount = (this.state.currentPage * this.state.datalimit)
//         //     const initialCount = lastCount - this.state.datalimit
//         //     this.setState({
//         //         data: this.props.CareerData.resp,
//         //         dataLength: this.props.CareerData.resp.length,
//         //         firstIndex: initialCount
//         //     })
//         // }
//         if (prevState.searchvalue !== this.state.searchvalue) {

//             let data = {
//                 searchdata: this.state.searchvalue,
//                 currentPage: this.state.currentPage,
//                 contract: "All",
//                 datalimit: this.state.datalimit
//             }
//             this.props.viewCareersDispatch(data)
//         }

//         if (prevState.currentPage !== this.state.currentPage) {
//             let data = {
//                 searchdata: this.state.searchvalue,
//                 currentPage: this.state.currentPage,
//                 contract: "All",
//                 datalimit: this.state.datalimit
//             }
//             this.props.viewCareersDispatch(data)
//         }

//         if (prevState.datalimit !== this.state.datalimit) {
//             let data = {
//                 searchdata: this.state.searchvalue,
//                 currentPage: this.state.currentPage,
//                 contract: "All",
//                 datalimit: this.state.datalimit
//             }
//             this.props.viewCareersDispatch(data)
//         }



//         //------- ! Set State of Data on currentPage Change Without Search !
//         if (prevProps.CareerData !== this.props.CareerData) {
//             const Lastcount = (this.state.currentPage) * this.state.datalimit
//             const initialCount = Lastcount - this.state.datalimit
//             this.setState({
//                 data: this.props.CareerData.resp,
//                 paginationDetail: this.props.CareerData.pagination,
//                 dataLength: this.props.CareerData.resp.length,
//                 firstIndex: initialCount
//             })
//         }

//     }

//     handleOnSearch = (e) => {
//         this.setState({
//             searchvalue: e.target.value,
//             currentPage: 1
//         })
//     }


//     handlePrev = () => {
//         if (this.state.currentPage > 1) {
//             this.setState({
//                 currentPage: this.state.currentPage - 1
//             })
//         }
//     }

//     handleNext = (pageCount) => {
//         if (this.state.currentPage < pageCount) {
//             this.setState({
//                 currentPage: this.state.currentPage + 1
//             })

//         }
//     }

//     deleteUserInfo = () => {
//         this.setState({
//             snakbarnotification: true
//         })
//         let userid = this.state.deleteid
//         this.props.CarrerJobListDelete(userid)
//         setTimeout(() => {

//             if (this.state.dataLength > 1) {
//                 let data = {
//                     searchdata: this.state.searchvalue,
//                     currentPage: this.state.currentPage,
//                     contract: "All"
//                 }
//                 this.props.viewCareersDispatch(data)
//                 this.setState({ currentPage: this.state.currentPage })
//             }
//             else {
//                 if (this.state.currentPage > 1) {
//                     this.setState({ currentPage: this.state.currentPage - 1 })
//                 }

//             }

//         }, 1000);
//     }

//     deleteUserFormInfo = () => {
//         this.setState({
//             snakbarnotification: true
//         })
//         let userid = this.state.carrerFormid
//         this.props.CarrerFormdataFromUserDelete(userid)
//         setTimeout(() => {
//             this.props.carrerFormdataFromuser()
//         }, 500);
//     }


//     render() {

//         const { breadcrumb } = this.state
//         const { Carrearformdata } = this.props


//         let tabledata = []
//         {
//             (Carrearformdata.resp.length > 0) && (Carrearformdata.resp.map((user) => {
//                 let data = {
//                     name: user.first_name,
//                     date: convertDatePickerTime(user.rowcreateddatetime),
//                     title: user.jobpost_name,
//                     mobile: user.mobile,
//                     status: user.status,
//                     view: <Link to={`/view-career-request/${user.sr_no}`}><button className="btn btn-warning text-white px-4 py-1 viewentrybtn cursor">View Entry</button></Link>,
//                     delete: <> <span onClick={() => this.setState({ carrerFormid: user.sr_no })} className="mr-3"><img src={Delete} alt="Delete" title="Delete" data-toggle="modal" data-target="#formdelete" className="img-fluid" /></span>
//                         <span><img src={Archive} alt="Archive" title="Archive" className="img-fluid" /></span></>
//                 }
//                 tabledata.push(data)
//             }))
//         }
//         // const pageCount = this.props.CareerData.pagination.pageCount
//         // const itemCount = this.props.CareerData.pagination.itemCount
//         // if (this.props.CareerData.pagination.list == undefined) {
//         //     console.log("length", this.props.CareerData.pagination.list)
//         // } else {
//         //     console.log("length", this.props.CareerData.pagination.list.length)
//         // }
//         // const lastPage = this.state.currentPage * 6
//         // const firstPage = lastPage - 6 + 1
//         // let count = lastPage

//         const data = this.state.data
//         const { firstIndex, dataLength } = this.state
//         const pageCount = this.state.paginationDetail.pageCount
//         const itemCount = this.state.paginationDetail.itemCount
//         let count = firstIndex

//         return (
//             <>
//                 <section className="wrapper px-4 ">
//                     <div className="container">
//                         <div className="page-title mb-4">
//                             <div className="row align-items-center">
//                                 <div className="col-md-5">
//                                     <div className="page-title-inner">
//                                         <h4>Careers</h4>
//                                     </div>
//                                 </div>
//                                 <div className="col-md-7">
//                                     <nav aria-label="breadcrumb">
//                                         <ol className="breadcrumb mb-2 justify-content-end p-0 bg-transparent align-items-center">
//                                             <li className="breadcrumb-item"><Link to="/">Dashboard</Link></li>
//                                             <li className="breadcrumb-item"><Link to="#">Content manager</Link></li>
//                                             <li className="breadcrumb-item active" aria-current="page">{this.state.breadcrumb}</li>
//                                         </ol>
//                                     </nav>
//                                 </div>
//                             </div>
//                         </div>

//                         <div className="nees-feeds">
//                             <div className="row">
//                                 <div className="col-12 cus-tabs">
//                                     <ul className="nav nav-tabs">
//                                         {/* <li className="nav-item cursor">
//                                             <a onClick={() => {
//                                                 this.setState({
//                                                     breadcrumb: "Career listings"
//                                                 })
//                                             }} className={`nav-link ${breadcrumb === "Career listings" && "active"} `}>Career listings</a>
//                                         </li>
//                                         <li className="nav-item cursor">
//                                             <a onClick={() => {
//                                                 this.setState({
//                                                     breadcrumb: "Career application request"
//                                                 })
//                                             }} className={`nav-link ${breadcrumb === "Career application request" && "active"} `}>Career application request </a>
//                                         </li> */}
//                                         <Link to="/careers">Career Job POst</Link>
//                                         <Link to="/careers/Job-Aplications">Career job application</Link>
//                                     </ul>





//                                     <div className="tab-content" id="myTabContent">

//                                         {breadcrumb == "Career listings" &&

//                                             <div className="tab-inner bg-white  px-4 py-3">
//                                                 <div className="tab-inner-head pb-3">
//                                                     <div className="row align-items-center mb-3">
//                                                         <div className="col-md-6">
//                                                             <Search
//                                                                 onChange={this.handleOnSearch}
//                                                                 value={this.state.searchvalue}
//                                                             />
//                                                         </div>
//                                                         <div className="col-md-6">
//                                                             <div className="show-pages d-flex justify-content-end align-items-center">
//                                                                 <span className="mr-3 d-inline-block">
//                                                                     <select className="fs-14 form-control" onChange={(e) => this.setState({ datalimit: e.target.value, currentPage: 1 })}>
//                                                                         <option value="7">7 Rows Per Page</option>
//                                                                         <option value="10">10 Rows Per Page</option>
//                                                                         <option value="15">15 Rows Per Page</option>
//                                                                         <option value="20">20 Rows Per Page</option>
//                                                                         <option value="30">30 Rows Per Page</option>
//                                                                         <option value="50">50 Rows Per Page</option>
//                                                                     </select>
//                                                                 </span>
//                                                                 <p className="mb-0 mr-3">Showing <span>{firstIndex + 1}</span> - <span> {firstIndex + dataLength}</span> of <span>{itemCount}</span></p>
//                                                                 <div className="prev-next">
//                                                                     <span className="prev"><button className="fas fa-angle-left" onClick={this.handlePrev}></button></span>
//                                                                     <span className="next"><button className="fas fa-angle-right" onClick={() => this.handleNext(pageCount)}></button></span>
//                                                                 </div>
//                                                             </div>
//                                                         </div>
//                                                     </div>
//                                                 </div>
//                                                 <div className="tab-inner-body">
//                                                     <div className="add-events-btn text-right mb-3">
//                                                         <Link to="/post-career"><button className="btn btn-outline-warning px-4 rounded-50 fs-14">Add Careers</button></Link>
//                                                     </div>
//                                                     <div className="table-responsive">
//                                                         <table className="table border">
//                                                             <thead>
//                                                                 <tr className="bg-cusgray">
//                                                                     <td className="border-top-0">S/N</td>
//                                                                     <td className="border-top-0">Title</td>
//                                                                     <td className="border-top-0">Company</td>
//                                                                     <td className="border-top-0">Job type</td>
//                                                                     <td className="border-top-0">Location</td>
//                                                                     <td className="border-top-0 width-md-180 white-space-nowrap">Date created</td>
//                                                                     <td className="border-top-0">Action</td>
//                                                                 </tr>
//                                                             </thead>
//                                                             <tbody>

//                                                                 {data.map((user, i) => {
//                                                                     return (
//                                                                         <tr key={i}><td>{count = count + 1}</td>
//                                                                             <td>
//                                                                                 <p className="mb-0">{user.title}</p>
//                                                                             </td>
//                                                                             <td>
//                                                                                 <p className="mb-0">{user.company_name}</p>                                                                            </td>
//                                                                             <td>
//                                                                                 <p className="mb-0">{user.job_type}</p>
//                                                                             </td>
//                                                                             <td>
//                                                                                 <p className="mb-0">{user.location}</p>
//                                                                             </td>

//                                                                             <td>
//                                                                                 <p className="mb-0 date-time"><span>{Commondateconvert(user.rowcreateddatetime, 1)}</span></p>
//                                                                             </td>
//                                                                             <td className="all-action">
//                                                                                 <div className="dropdown ml-2">
//                                                                                     <span id="dropdownMenu" data-toggle="dropdown" aria-haspopup="true" className="cursor" aria-expanded="false"><i className="fa fa-ellipsis-v"></i></span>
//                                                                                     <div className="dropdown-menu " aria-labelledby="dropdownMenu">
//                                                                                         <Link className="dropdown-item" to={`/edit-careers/${user.sr_no}`}>Edit</Link>
//                                                                                         <span onClick={() => this.setState({ deleteid: user.sr_no })} className="dropdown-item" data-toggle="modal" data-target="#remove" >Remove</span>
//                                                                                     </div>
//                                                                                 </div>
//                                                                             </td>
//                                                                         </tr>

//                                                                     )
//                                                                 })}


//                                                             </tbody>
//                                                         </table>
//                                                     </div>
//                                                 </div>
//                                             </div>}
//                                         {breadcrumb == "Career application request" &&
//                                             <Careerformdata />
//                                         }

//                                     </div>
//                                 </div>
//                             </div>
//                         </div>
//                     </div>
//                 </section>
//                 <Deletemodal
//                     deleteid={this.deleteUserInfo}
//                     message={"Are you sure you want to remove this list"}
//                     buttonname={"Remove"}
//                 />

//                 <div className="modal fade" id="formdelete" role="dialog" aria-labelledby="removeLabel" aria-hidden="true">
//                     <div className="modal-dialog mt-0" role="document">
//                         <div className="modal-content border-0 shadow">
//                             <div className="modal-header border-0 px-md-4">
//                                 <div className="headings">
//                                     <h5 className="modal-title font-weight-bold pb-1" id="removeLabel">Delete entry</h5>
//                                     <p className="fs-14 mb-0">Are you sure you want to delete this list</p>
//                                 </div>
//                                 <button type="button" className="close outline-none" data-dismiss="modal" aria-label="Close">
//                                     <span aria-hidden="true">&times;</span>
//                                 </button>
//                             </div>
//                             <div className="modal-footer border-0 px-md-4 pt-2">
//                                 <span className="mb-0 text-secondary pr-3 text-decoration-none d-inline-block cursor" data-dismiss="modal">Cancel</span>
//                                 <span onClick={this.deleteUserFormInfo} className="mb-0 text-warning text-decoration-none d-inline-block cursor" data-dismiss="modal">Delete</span>
//                             </div>
//                         </div>
//                     </div>
//                 </div>
//                 <Switch>
//                     <Route exact to="/careers" />
//                     <Route to="/careers/Job-Aplications"><Careerformdata /> </Route>
//                 </Switch>                
//                 {/* <div className="alert-msg d-none">
//                     <div className="bg-overlay"></div>
//                     <div className="msg right-msg position-absolute">
//                         <div className="msg-inner d-flex align-items-center justify-content-between">
//                             <p className="mb-0 mr-4 fs-14">Career listings have been removed sucessfully</p>
//                             <p className="mb-0"><i className="fa fa-times"></i></p>
//                         </div>
//                     </div>
//                 </div> */}

//                 <Quicknotification
//                     show={this.state.snakbarnotification}
//                     message={"Career listings have been removed sucessfully"}
//                     closetab={() => this.setState({ snakbarnotification: false })}
//                     // bgblock={true}
//                     closesecond={8000}
//                     timeout={true}
//                 />
//                 <Footer />

//             </>
//         )
//     }
// }
// const mapStateToProps = state => {
//     return {
//         CareerData: state.CarrearJobListingData,
//         Carrearformdata: state.Carrerformlist
//     }
// }

// const mapDispatchToProps = (dispatch) => {
//     return {
//         viewCareersDispatch: (data) => { dispatch(CareersJobListing(data)) },
//         CarrerJobListDelete: (userid) => { dispatch(CareersJobListingDelete(userid)) },
//         carrerFormdataFromuser: () => { dispatch(CareersFormfromUserListing()) },
//         CarrerFormdataFromUserDelete: (userid) => { dispatch(CareersFormdatafromUserDelete(userid)) }
//     }
// }
