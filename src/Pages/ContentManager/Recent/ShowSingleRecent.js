import Axios from 'axios';
import React, { useState } from 'react';
import { withRouter } from 'react-router-dom';
import makeAnimated from "react-select/animated";
import Select from "react-select/creatable";
import TextEditor from '../../../Component/TextEditor';
import { Api } from '../../../Const/Api';
import Header from '../../../Container/CMSHeader';


const styles = {
  container: {
    width: "100%",
    backgroundColor: "white",
    marginBottom: 15,
    padding: "33px 29px 21px"
  },
  dataArea: {
    border: ".1px solid rgba(59,107,211,.2)",
    borderRadius: 6,
    rowGap: 30,
  },
  header: {
    display: "grid",
    gridTemplateColumns: "10% 20% 25% 25% 10% 10%",
    fontSize: 16,
    fontWeight: "bold",
    padding: "38px 40px 17px",
    borderBottom: ".1px solid rgba(5, 0, 56, 0.06)",
  },
  body: {
    display: "grid",
    fontSize: 16,
    gridTemplateColumns: "10% 20% 25% 25% 10% 10%",
    padding: "20px 40px 18px",
  },
  button: {
    width: 148,
    height: 44,
    border: "0.1px solid #050038",
    boxSizing: "border-box",
    display: "flex",
    alignItems: "center",
    boxShadow: "0px 2px 2px rgba(5, 0, 56, 0.08)",
    borderRadius: 6,
    cursor: "pointer",
    marginRight: 35
  },
  image: {
    width: "100%",
    marginBottom: 114
  },
  socialDivs: {
    width: 67,
    height: 50,
    padding: 2,
    marginRight: 42,
    border: "0.4px solid rgba(59, 107, 211, 0.8)",
    boxSizing: "border-box",
    borderRadius: "4px",
    display: "flex",
    flexDirection: "column"
  }
};

const animatedComponents = makeAnimated();

const ShowSingleRecent = (props) => {
  const [loading, setLoading] = useState(false);
  const [openFilter, setOpenFilter] = useState(false);
  const [viewMode, setViewMode] = useState("live");
  const [recentNewsData, setRecentNewsData] = useState({ ...props.location.state });

  console.log(recentNewsData);

  const switchViewMode = () => {
    if(viewMode === "live") {
      // setShowedRecents(totalRecents.filter(recent => archivedRecentIds.includes(recent.id)));
      setViewMode("archived");
    }else {
      // setShowedRecents(totalRecents.filter(recent => !archivedRecentIds.includes(recent.id)));
      setViewMode("live");
    }
    setOpenFilter(false)
  }

  const toggleArchive = async (id) => {
    setLoading(true)
    let url;
    if(recentNewsData.status === "live") {
      url = `${Api.BaseUrl}/api/recentNews/archiveNews`;
    }else {
      url = `${Api.BaseUrl}/api/recentNews/unarchiveNews`;
    }
    try{
        const result = await Axios.post(
          url,
          {id, section: recentNewsData.section},
          {
            'headers': { 'Authorization': `Bearer ${localStorage.getItem('token')}` }
          },
        );
        setLoading(false);
        if(result.status === 200) {
          setRecentNewsData({ ...recentNewsData, status: "archived"})
        }
    }catch(e) {
      setLoading(false);
      console.log(e)
    }
  }

  const toggleOpenFilter = (e) => {
    if(e.target.id === "filterArrow" || e.target.id === "filterArrow-img") {
      setOpenFilter(!openFilter)
    } else {
      if(openFilter) setOpenFilter(false)
    }
  }

  const togglePin = async (id) => {
    setLoading(true)
    let url;
    if(!recentNewsData.pined) {
      url = `${Api.BaseUrl}/api/recentNews/pin`;
    }else {
      url = `${Api.BaseUrl}/api/recentNews/unpin`;
    }
    try{
        const result = await Axios.patch(
          url,
          {ids: [id]},
          {
            'headers': { 'Authorization': `Bearer ${localStorage.getItem('token')}` }
          },
        )

        setLoading(false)
        
        if(result.status === 200) {
          setRecentNewsData({ ...recentNewsData, pined: !recentNewsData.pined})
        }
    }catch(e) {
      setLoading(false)
      console.log(e)
    }
  }
        return (
            <>
              <Header  />
              <div style={{
                marginBottom: 20,
                display: "flex",
                flexDirection: "row",
                justifyContent: "flex-end"
              }}>
                <div style={{ display: "flex", flexDirection: "row", justifyContent: "flex-end"}}>
              <div
                style={{
                  position: "relative"
                }}
              >
                <div
                id="filterArrow"
                  style={{
                    ...styles.button,
                    display: "flex",
                    flexDirection: "row",
                    color: "#050038",
                    background: "white",
                    paddingLeft: 12,
                    paddingRight: 5,
                    borderBottomLeftRadius: openFilter ? 0 : 6,
                    borderBottomRightRadius: openFilter ? 0 : 6,
                    border: ".1px solid rgba(59,107,211,.2)",
                    boxSizing: "border-box",
                    boxShadow: "0px 2px 2px rgba(5, 0, 56, 0.08)",
                    borderRadius: 6,
                    cursor: "pointer",
                  }}
                  onClick={(e) => toggleOpenFilter(e)}
                >
                  <img style={{ marginRight: 20 }} src={require("../../../Assets/img/filter.svg")} />
                  <span style={{ marginRight: 25 }}>Filter</span>
                  <img
                    style={{ marginTop: 2 }}
                    id="filterArrow-img"
                    src={require("../../../Assets/img/downwardArrow.svg")}
                  />
                </div>
                {openFilter && <div style={{
                  position: 'absolute',
                  width: 148,
                  height: 100,
                  color: "#050038",
                  fontSize: 14,
                  paddingLeft: 37,
                  paddingTop: 5,
                  background: "white",
                  zIndex: 100,
                  marginTop: -1,
                  borderLeft: ".1px solid rgba(59,107,211,.2)",
                  borderRight: ".1px solid rgba(59,107,211,.2)",
                  borderBottom: ".1px solid rgba(59,107,211,.2)",
                  borderBottomLeftRadius: 6,
                  borderBottomRightRadius: 6
                }}>
                  {/* <div style={{ marginBottom: 25, cursor: "pointer"}}>Date</div> */}
                  <div style={{ cursor: "pointer" }} onClick={switchViewMode}>
                      {viewMode === "live" ? "Archived" : "Unarchived"}
                    </div>
                </div>}
              </div>
            </div>
              </div>

                <section className="px-4">
                  <div style={styles.container}>
                    <div
                      style={{
                        ...styles.dataArea,
                        padding: "20px 85px",
                        display: "grid",
                        gridTemplateColumns: "80% 15%",
                        rowGap: "5%"
                      }}
                    >
                      <div style={styles.leftContent}>
                        <div style={styles.image}>
                          <img src={`${recentNewsData.picture?.slice(2, -2)}`} style={{ width: "85%", height: "auto"}} />
                        </div>
                        <div style={{ display: "flex", flexDirection: "row", marginBottom: 70 }}>
                          <div style={styles.socialDivs}>
                            <div
                              style={{
                                display: "flex",
                                flexDirection: "row",
                                marginBottom: 3,
                                justifyContent: "flex-end"
                              }}
                            >
                              <input type="checkbox" checked={recentNewsData.twitter_url} />
                            </div>
                            <div
                              style={{
                                display: "flex",
                                flexDirection: "row",
                                justifyContent: "center"
                              }}
                            >
                              <img
                                src={require("../../../Assets/img/twitter.svg")}
                                style={{ width: 20, height: 16}}
                              />
                            </div>
                          </div>
                          <div style={styles.socialDivs}>
                            <div
                              style={{
                                display: "flex",
                                flexDirection: "row",
                                marginBottom: 3,
                                justifyContent: "flex-end"
                              }}
                            >
                              <input type="checkbox" checked={recentNewsData.instagram_url} />
                            </div>
                            <div
                              style={{
                                display: "flex",
                                flexDirection: "row",
                                justifyContent: "center"
                              }}
                            >
                              <img
                                src={require("../../../Assets/img/instagram.svg")}
                                style={{ width: 20, height: 16}}
                              />
                            </div>
                          </div>
                          <div style={styles.socialDivs}>
                            <div
                              style={{
                                display: "flex",
                                flexDirection: "row",
                                marginBottom: 3,
                                justifyContent: "flex-end"
                              }}
                            >
                              <input type="checkbox" checked={recentNewsData.linkedin_url} />
                            </div>
                            <div
                              style={{
                                display: "flex",
                                flexDirection: "row",
                                justifyContent: "center"
                              }}
                            >
                              <img
                                src={require("../../../Assets/img/linkedin.svg")}
                                style={{ width: 20, height: 16}}
                              />
                            </div>
                          </div>
                        </div>
                        <div style={{ fontSize: 16, marginBottom: 70 }}>
                          <div>Insert link</div>
                          <input
                            type="text"
                            disabled
                            value={recentNewsData.inner_link}
                            style={{
                              width: "85%",
                              background: "rgba(207, 207, 207, 0.2)",
                              borderRadius: "4px",
                              padding: 10,
                              color: "#050038",
                              fontWeight: 700
                            }}
                          />
                        </div>
                        <div style={{ fontSize: 16, width: "85%", marginBottom: 70 }}>
                          <div>Content tag</div>
                          <Select
                            styles={{
                              width: "100%",
                              background: "rgba(207, 207, 207, 0.2)",
                              borderRadius: "4px",
                              padding: 10,
                              color: "#050038",
                              fontWeight: 700
                            }}
                            isMulti
                            placeholder="Type tags and separate them with tabs"
                            closeMenuOnSelect={true}
                            defaultValue={recentNewsData.tag.split(",").map(tag => ({ label: tag, value: tag }))}
                            isDisabled
                            components={animatedComponents}
                            name="tags"
                          />
                        </div>
                        <div style={{ fontSize: 16, marginBottom: 70 }}>
                          <div>Likes</div>
                          <input
                            type="text"
                            disabled
                            value={recentNewsData.likes}
                            style={{
                              width: "85%",
                              background: "rgba(207, 207, 207, 0.2)",
                              borderRadius: "4px",
                              padding: 10,
                              color: "#050038",
                              fontWeight: 700
                            }}
                          />
                        </div>
                        <div style={{ width: "85%", marginBottom: 70}}>
                          <div>Heading</div>
                          <TextEditor
                            defaultValue={recentNewsData.heading}
                            disabled
                          />
                        </div>
                        <div style={{ width: "85%", marginBottom: 70}}>
                          <div>Caption</div>
                          <TextEditor
                            defaultValue={recentNewsData.content}
                            disabled
                          />
                        </div>
                        <div style={{ display: "flex", flexDirection: "row", rowGap: 80, marginBottom: 70}}>
                          <div
                            style={{
                              ...styles.button,
                              color: "white",
                              justifyContent: "center",
                              marginBottom: 50,
                              backgroundColor: loading ? "rgba(5, 0, 56, 0.4)" : "#050038",
                              border: loading ? "" : ".1px solid rgba(5, 0, 56, 0.06)",
                              cursor: loading ? "not-allowed" : "pointer",
                            }}
                            onClick={() => toggleArchive(recentNewsData.id)}
                          >
                            Archive Post
                          </div>
                          <div
                            style={{
                              ...styles.button,
                              color: "#050038",
                              justifyContent: "center",
                              border: loading ? "" : ".1px solid rgba(5, 0, 56, 0.06)",
                              cursor: loading ? "not-allowed" : "pointer",
                            }}
                            onClick={() => togglePin(recentNewsData.recentId)}
                          >
                            {recentNewsData.pined ? "Unpin Post" : "Pin Post"}
                          </div>
                        </div>
                      </div>
                      <div style={styles.rightContent}>
                        <div
                          style={{
                            ...styles.button,
                            color: "white",
                            justifyContent: "center",
                            marginBottom: 50,
                            backgroundColor: loading ? "rgba(5, 0, 56, 0.4)" : "#050038",
                            border: loading ? "" : ".1px solid rgba(5, 0, 56, 0.06)",
                            cursor: loading ? "not-allowed" : "pointer",
                          }}
                          onClick={() => toggleArchive(recentNewsData.id)}
                        >
                          {/* {recentNewsData.pined ? "Unpin Post" : "Pin Post"} */}
                          Archive Post
                        </div>
                        <div
                          style={{
                            ...styles.button,
                            color: "#050038",
                            justifyContent: "center",
                            border: loading ? "" : ".1px solid rgba(5, 0, 56, 0.06)",
                            cursor: loading ? "not-allowed" : "pointer",
                          }}
                          onClick={() => togglePin(recentNewsData.recentId)}
                        >
                          {recentNewsData.pined ? "Unpin Post" : "Pin Post"}
                        </div>
                      </div>
                    </div>
                  </div>
                </section>
            </>
        )
}

export default withRouter(ShowSingleRecent)
