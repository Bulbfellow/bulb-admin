import Axios from 'axios';
import React, { useEffect, useState } from 'react';
import { withRouter } from 'react-router-dom';
import { Api } from '../../../Const/Api';
import Header from '../../../Container/CMSHeader';


const styles = {
  container: {
    width: "100%",
    backgroundColor: "white",
    marginBottom: 15,
    padding: "33px 29px 21px"
  },
  dataArea: {
    border: ".1px solid rgba(59,107,211,.2)",
    borderRadius: 6,
    rowGap: 30,
  },
  header: {
    display: "grid",
    gridTemplateColumns: "10% 20% 30% 30% 5% 5%",
    fontSize: 16,
    fontWeight: "bold",
    padding: "38px 40px 17px",
    borderBottom: ".1px solid rgba(5, 0, 56, 0.06)",
  },
  body: {
    display: "grid",
    fontSize: 16,
    gridTemplateColumns: "10% 20% 30% 30% 5% 5%",
    padding: "20px 40px 18px",
    cursor: "pointer"
  },
  button: {
    width: 148,
    height: 44,
    display: "flex",
    alignItems: "center",
    color: "white",
    marginRight: 35
  },
  ellipsisOptions: {
    paddingTop: 10,
    paddingLeft: 25,
    paddingBottom: 10,
    cursor: "pointer"
  }
};
const Recent = (props) => {
    const [openFilter, setOpenFilter] = useState(false);
    const [pinnedRecents, setPinnedRecents] = useState([]);
    const [totalRecents, setTotalRecents] = useState([]);
    const [showedRecents, setShowedRecents] = useState([]);
    const [viewMode, setViewMode] = useState("live");
    const [selectedRecents, setSelectedRecents] = useState([]);
    const [archivedRecentIds, setArchivedRecentIds] = useState([]);

    const [openEllipsisOptions, setOpenEllipsisOptions] = useState(null);

    const togglePin = async (id) => {
      try{
        const result = await Axios.post(
          `${Api.BaseUrl}/api/pin`,
            {
              'headers': { 'Authorization': localStorage.getItem('token') }
            },
        )
        if(result.status === 200) {
          let totalArchivedIds;
          if(viewMode === "live") {
            totalArchivedIds = [...archivedRecentIds, id]
            setArchivedRecentIds(totalArchivedIds);
            setShowedRecents(totalRecents.filter(recent => !totalArchivedIds.includes(recent.id)));
          } else {
            totalArchivedIds = archivedRecentIds.filter(archiveId => archiveId !== id)
            setArchivedRecentIds(totalArchivedIds);
            setShowedRecents(totalRecents.filter(post => totalArchivedIds.includes(post.id)));
          }
          setOpenEllipsisOptions(null);
          setSelectedRecents([]);
        }
    }catch(e) {
      console.log(e)
    }
    }

    const toggleArchive = async (id, status, section) => {
      const url = status === "live" ?
        `${Api.BaseUrl}/api/recentNews/archiveNews/`
        : `${Api.BaseUrl}/api/recentNews/unarchiveNews/`;

      try{
          const result = await Axios.post(
            url,
            {id, section},
            {
              'headers': { 'Authorization': `Bearer ${localStorage.getItem('token')}` }
            },
          );
          
          if(result.status === 200) {
            let totalArchivedIds;
            if(viewMode === "live") {
              totalArchivedIds = [...archivedRecentIds, id]
              setArchivedRecentIds(totalArchivedIds);
              setShowedRecents(totalRecents.filter(recent => !totalArchivedIds.includes(recent.id)));
            } else {
              totalArchivedIds = archivedRecentIds.filter(archiveId => archiveId !== id);
              setArchivedRecentIds(totalArchivedIds);
              setShowedRecents(totalRecents.filter(post => totalArchivedIds.includes(post.id)));
            }
            setOpenEllipsisOptions(null);
            setSelectedRecents([]);
          }
      }catch(e) {
        console.log(e)
      }
    }

    const switchViewMode = () => {
      if(viewMode === "live") {
        setShowedRecents(totalRecents.filter(recent => archivedRecentIds.includes(recent.id)));
        setViewMode("archived");
      }else {
        setShowedRecents(totalRecents.filter(recent => !archivedRecentIds.includes(recent.id)));
        setViewMode("live");
      }
      setOpenFilter(false)
    }

    const openEllipsis = (id) => {
      if(openEllipsisOptions === id) setOpenEllipsisOptions(null)
      else setOpenEllipsisOptions(id)
    }

    const toggleOpenFilter = (e) => {
      if(e.target.id === "filterArrow" || e.target.id === "filterArrow-img") {
        setOpenFilter(!openFilter)
      } else {
        if(openFilter) setOpenFilter(false)
      }
    }

    const handleSelectRecent = (recentId) => {
      if(selectedRecents.includes(recentId)) {
        setSelectedRecents(selectedRecents.filter(recent => recent !== recentId && recent !== "Select all"))
      }else {
        setSelectedRecents([...selectedRecents, recentId]);
      }
    }

    const handleSelectAll = () => {
      if(selectedRecents[0] === "Select all") {
        setSelectedRecents([]);
      }else {
        setSelectedRecents(["Select all", ...totalRecents.map(recent => recent.id)])
      }
    }

    useEffect(() =>{
      const getAllRecentNews = async () => {
        try {
          const result = await Axios.get(
            `${Api.BaseUrl}/api/recentNews/all`,
            {
              headers: { 'Authorization': `Bearer ${localStorage.getItem('token')}` }
            },
          );

          console.log(result);

          if(result.status === 200) {
            setTotalRecents(result.data.payload.message.map(recent => (
              {
                ...recent,
                section: recent.Blog ? "blog" : "press"
              }
            )));
            setShowedRecents(result.data.payload.message.map(recent => {
              if(recent.Blog?.status === "live" || recent.Press?.status === "live"){
                return {
                  ...recent,
                  section: recent.Blog ? "blog" : "press"
                }
              }
            }
            ));
            setArchivedRecentIds(result.data.payload.message
              .filter(post => post.Press?.status === "archived" || post.Blog?.status === "archived")
              .map(post => post.id));
          }
        } catch(e) {
          console.log(e)
        }
      };

      getAllRecentNews();
    }, [props]);

      return (
        <>
          <Header />
          <div style={{
            marginBottom: 20,
            paddingLeft: 29,
            paddingRight: 29,
            display: "flex",
            flexDirection: "row",
            justifyContent: "space-between"
          }}>
            <div>
              <div
                style={{
                  ...styles.button,
                  justifyContent: "center",
                  color: "#050038",
                  backgroundColor: selectedRecents.length > 0 ? "white" : "",
                  border: selectedRecents.length > 0 ? ".1px solid rgba(59,107,211,.2)" : "",
                  cursor: selectedRecents.length < 1 ? "default" : "pointer",
                  borderRadius: 6,
                  boxShadow: selectedRecents.length < 1 && "" 
                }}
                onClick={() => toggleArchive(selectedRecents)}
              >
                {selectedRecents.length > 0 && (viewMode === "live" ? "Archive" : "Unarchive")}
              </div>
            </div>
            <div style={{ display: "flex", flexDirection: "row", justifyContent: "flex-end"}}>
              <div
                style={{
                  position: "relative"
                }}
              >
                <div
                id="filterArrow"
                  style={{
                    ...styles.button,
                    display: "flex",
                    flexDirection: "row",
                    color: "#050038",
                    background: "white",
                    paddingLeft: 12,
                    paddingRight: 5,
                    borderBottomLeftRadius: openFilter ? 0 : 6,
                    borderBottomRightRadius: openFilter ? 0 : 6,
                    border: ".1px solid rgba(59,107,211,.2)",
                    boxSizing: "border-box",
                    boxShadow: "0px 2px 2px rgba(5, 0, 56, 0.08)",
                    borderRadius: 6,
                    cursor: "pointer",
                  }}
                  onClick={(e) => toggleOpenFilter(e)}
                >
                  <img style={{ marginRight: 20 }} src={require("../../../Assets/img/filter.svg")} />
                  <span style={{ marginRight: 25 }}>Filter</span>
                  <img
                    style={{ marginTop: 2 }}
                    id="filterArrow-img"
                    src={require("../../../Assets/img/downwardArrow.svg")}
                  />
                </div>
                {openFilter && <div style={{
                  position: 'absolute',
                  width: 148,
                  height: 100,
                  color: "#050038",
                  fontSize: 14,
                  paddingLeft: 37,
                  paddingTop: 5,
                  background: "white",
                  zIndex: 100,
                  marginTop: -1,
                  borderLeft: ".1px solid rgba(59,107,211,.2)",
                  borderRight: ".1px solid rgba(59,107,211,.2)",
                  borderBottom: ".1px solid rgba(59,107,211,.2)",
                  borderBottomLeftRadius: 6,
                  borderBottomRightRadius: 6
                }}>
                  {/* <div style={{ marginBottom: 25, cursor: "pointer"}}>Date</div> */}
                  <div style={{ cursor: "pointer" }} onClick={switchViewMode}>
                      {viewMode === "live" ? "Archived" : "Unarchived"}
                    </div>
                </div>}
              </div>
            </div>
          </div>

          <section className="px-4">
            <div style={styles.container}>
              <div style={styles.dataArea}>
                <div style={styles.header}>
                  <div>
                    <input
                      type="checkbox"
                      style={{ transform: "scale(1.5)", cursor: "pointer" }}
                      checked={selectedRecents.includes("Select all")}
                      onClick={handleSelectAll}
                    />
                  </div>
                  <div style={{fontWeight: "bold"}}>Date Posted</div>
                  <div style={{ fontWeight: "bold" }}>Heading</div>
                  <div style={{ fontWeight: "bold" }}>Content</div>
                  <div></div>
                  <div></div>
                </div>
                {showedRecents.length > 0 ? showedRecents.map((recent) =>
                  <div style={styles.body}>
                  <div>
                    <input
                      type="checkbox"
                      onClick={() => handleSelectRecent(recent.id)}
                      checked={selectedRecents.includes(recent.id) || selectedRecents[0] === "Select all"}
                      style={{ transform: "scale(1.5)", cursor: "pointer" }}
                    />
                  </div>
                  <div onClick={() => props.history.push('/content-manager/recent/single', {...recent.Press || recent.Blog, section: recent.section, pined: recent.pined, recentId: recent.id })}>
                    {
                      new Date(recent.created_at)
                        .toLocaleString("en-GB", { year: "numeric", month: "numeric", day: "numeric"})
                        .replaceAll("/", "-",)
                    }
                  </div>
                  <div
                    onClick={() => props.history.push('/content-manager/recent/single',  {...recent.Press || recent.Blog, section: recent.section, pined: recent.pined, recentId: recent.id })}
                    dangerouslySetInnerHTML={{ __html: recent.Press?.heading || recent.Blog?.heading }}
                  />
                  <div
                    onClick={() => props.history.push('/content-manager/recent/single',  {...recent.Press || recent.Blog, section: recent.section, pined: recent.pined, recentId: recent.id })}
                    dangerouslySetInnerHTML={{ __html: recent.Press?.content || recent.Blog?.content }}
                  />

                  <div style={{ position: "relative"}}>
                    <img
                      onClick={() => openEllipsis(recent.id)}
                      src={require("../../../Assets/img/verticalEllipsis.svg")}
                      style={{ cursor: "pointer"}}
                    />
                    {openEllipsisOptions === recent.id && <div
                      style={{
                        position: "absolute",
                        width: 150,
                        border: ".1px solid rgba(59,107,211,.2)",
                        marginLeft: -70,
                        backgroundColor: "white",
                        borderRadius: 3,
                        marginTop: 3,
                        zIndex: 700
                      }}
                    >
                      <div
                        style={styles.ellipsisOptions}
                        onMouseEnter={() => {}}
                      >
                        View Calendar
                      </div>
                      <div style={styles.ellipsisOptions}>Export</div>
                      <div
                        onClick={() => togglePin(recent.id)}
                        style={styles.ellipsisOptions}
                      >
                          {pinnedRecents.includes(recent.id) ? "Unpin" : "Pin"}
                      </div>
                      <div
                        onClick={() => toggleArchive(recent.id, (recent.Press?.status || recent.Blog?.status), (recent))}
                        style={styles.ellipsisOptions}
                      >
                          {archivedRecentIds.includes(recent.id) ? "Unarchive" : "Archive"}
                      </div>

                    </div>}
                  </div>
                  {pinnedRecents.includes(recent.id) && <div><img src={require("../../../Assets/img/pin.svg")} /></div>}
                </div>)
                : <div
                    style={{
                      textAlign: "center",
                      fontSize: 16,
                      color: "#050038",
                      marginBottom: 30,
                      marginTop: 30
                    }}
                  >
                    There are no recent news posts yet. Check back later.
                  </div>}
              </div>
            </div>
          </section>
        </>
      )
}

export default withRouter(Recent);
