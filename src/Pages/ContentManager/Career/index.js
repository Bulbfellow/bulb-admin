import React, { useState } from 'react';
import { withRouter } from 'react-router-dom';
import Header from '../../../Container/CMSHeader';
import { jobs } from "./jobOffers";


const styles = {
  container: {
    width: "100%",
    backgroundColor: "white",
    marginBottom: 15,
    padding: "33px 29px 21px"
  },
  dataArea: {
    borderRadius: 6,
    rowGap: 30,
    marginBottom: 50
  },
  header: {
    fontSize: 16,
    fontWeight: "bold",
    padding: "38px 40px 17px",
    borderBottom: ".1px solid rgba(5, 0, 56, 0.06)",
  },
  body: {
    display: "grid",
    fontSize: 16,
    gridTemplateColumns: "10% 20% 25% 25% 10% 10%",
    padding: "20px 40px 18px",
    cursor: "pointer"
  },
  button: {
    width: 148,
    height: 44,
    border: ".1px solid rgba(59,107,211,.2)",
    boxSizing: "border-box",
    display: "flex",
    alignItems: "center",
    color: "white",
    boxShadow: "0px 2px 2px rgba(5, 0, 56, 0.08)",
    borderRadius: 6,
    cursor: "pointer",
    marginRight: 35
  },
  ellipsisOptions: {
    paddingTop: 10,
    paddingLeft: 25,
    paddingBottom: 10,
    cursor: "pointer"
  }
};
const Careers = (props) => {
    const [openFilter, setOpenFilter] = useState(false);
    const [hoveredPicture, setHoveredPicture] = useState(false);
    const [pickedPictures, setPickedPictures] = useState([]);


    const handlePictureHover = (pictureOwner) => {
      setHoveredPicture(pictureOwner);
    }
  
    const toggleOpenFilter = (e) => {
      if(e.target.id === "filterArrow") {
        setOpenFilter(!openFilter)
      } else {
        if(openFilter) setOpenFilter(false)
      }
    }

    // useEffect(() =>{
    //   const getAllJobs = async () => {
    //     try {
    //       const result = await Axios.get(
    //         `${Api.BaseUrl}/api/blog/all`,
    //         {
    //           'headers': { 'Authorization': localStorage.getItem('token') }
    //         },
    //       );

    //       if(result.status === 200) {
    //         setTotalPosts(result.data.payload.message);
    //         setShowedPosts(result.data.payload.message.filter(post => post.status === "live"))
    //         setArchivedPostIds(result.data.payload.message
    //           .filter(post => post.status === "archived")
    //           .map(post => post.id))
    //       }
    //     } catch(e) {
    //       console.log(e)
    //     }
    //   };

    //   getAllBlogs();
    // }, [props]);

      return (
        <>
          <Header />
          <div style={{
            marginBottom: 20,
            paddingLeft: 29,
            paddingRight: 29,
            display: "flex",
            flexDirection: "row",
            justifyContent: "space-between"
          }}>
            <div>
              <div
                style={{
                  ...styles.button,
                  justifyContent: "center",
                  color: "#050038",
                  backgroundColor: pickedPictures.length > 0 ? "white" : "",
                  border: pickedPictures.length > 0 ? ".1px solid rgba(59,107,211,.2)" : "",
                  cursor: pickedPictures.length < 0 && "default",
                  boxShadow: pickedPictures.length < 0 && "",
                  borderRadius: 6,
                }}
              >
                {pickedPictures.length > 0 && "Archive" }
              </div>
            </div>
            <div style={{ display: "flex", flexDirection: "row", justifyContent: "space-between"}}>
              <div
                style={{
                  ...styles.button,
                  backgroundColor: "#050038",
                  justifyContent: "center",
                }}
                onClick={() => props.history.push('/content-manager/career/create')}
              >
                New Post
              </div>
              <div
                style={{
                  position: "relative"
                }}
              >
                <div style={{
                  ...styles.button,
                  display: "flex",
                  flexDirection: "row",
                  color: "#050038",
                  background: "white",
                  paddingLeft: 12,
                  paddingRight: 5,
                  borderBottomLeftRadius: openFilter ? 0 : 6,
                  borderBottomRightRadius: openFilter ? 0 : 6
                }}>
                  <img style={{ marginRight: 20 }} src={require("../../../Assets/img/filter.svg")} />
                  <span style={{ marginRight: 25 }}>Filter</span>
                  <img
                    onClick={(e) => toggleOpenFilter(e, "arrow")}
                    style={{ marginTop: 2 }}
                    id="filterArrow"
                    src={require("../../../Assets/img/downwardArrow.svg")}
                  />
                </div>
                {openFilter && <div style={{
                  position: 'absolute',
                  width: 148,
                  height: 100,
                  color: "#050038",
                  fontSize: 14,
                  paddingLeft: 37,
                  paddingTop: 5,
                  background: "white",
                  zIndex: 100,
                  marginTop: -1,
                  borderLeft: ".1px solid rgba(59,107,211,.2)",
                  borderRight: ".1px solid rgba(59,107,211,.2)",
                  borderBottom: ".1px solid rgba(59,107,211,.2)",
                  borderBottomLeftRadius: 6,
                  borderBottomRightRadius: 6
                }}>
                  <div style={{ marginBottom: 25, cursor: "pointer"}}>Date</div>
                  <div style={{ cursor: "pointer" }}>Archive</div>
                </div>}
              </div>
            </div>
          </div>

          <section className="px-4">
            <div style={styles.container}>
              <div style={styles.dataArea}>
                {jobs.map(job => 
                  <div
                    style={{
                      width: "100%",
                      borderRadius: 6,
                      marginBottom: 70,
                      border: ".1px solid rgba(59,107,211,.2)",
                      padding: "39px 282px 51px 31px",
                      cursor: "pointer"
                    }}
                    onclick={() => props.history.push("/content-manager/career/single", {...job})}
                  >
                    <div
                      style={{
                        color: "#050038",
                        fontSize: 22,
                        fontWeight: "bold",
                        marginBottom: 20
                      }}
                    >
                      {job.title}
                    </div>
                    <div
                      style={{
                        display: "flex",
                        justifyContent: "space-between",
                        marginBottom: 41,
                        fontSize: 16,
                        color: "#050038"
                      }}
                    >
                      <div style={{ display: "flex" }}>
                        <span>
                          <img
                            style={{ marginRight: 11, marginTop: -4 }}
                            src={require("../../../Assets/img/location.svg")}
                          />
                        </span>
                        Location: <span style={{ fontWeight: 700, marginLeft: 5 }}>{job.location}</span>
                      </div>
                      <div style={{ display: "flex" }}>
                        <span>
                          <img
                            style={{ marginRight: 11, marginTop: -4 }}
                            src={require("../../../Assets/img/briefcase.svg")}
                          />
                          </span>
                        Job Type: <span style={{ fontWeight: 700, marginLeft: 5 }}>{job.type}</span>
                      </div>
                      <div style={{ display: "flex" }}>
                        <span>
                          <img
                            style={{ marginRight: 11, marginTop: -4 }}
                            src={require("../../../Assets/img/calendar.svg")}
                          />
                          </span>
                        Application Deadline: <span style={{ fontWeight: 700, marginLeft: 5 }}>{job.deadline}</span>
                      </div>
                    </div>
                    <div style={{ fontSize: 16, color: "#050038"}}>
                      {job.description}
                    </div>
                  </div>  
                )}
              </div>
            </div>
          </section>
        </>
      )
}

export default withRouter(Careers);
