import React, { useCallback, useEffect, useRef, useState } from 'react';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import makeAnimated from "react-select/animated";
import TextEditor from '../../../Component/TextEditor';
import Header from '../../../Container/CMSHeader';


const styles = {
  container: {
    width: "100%",
    backgroundColor: "white",
    marginBottom: 15,
    padding: "33px 29px 21px"
  },
  dataArea: {
    border: ".1px solid rgba(59,107,211,.2)",
    borderRadius: 6,
    rowGap: 30,
  },
  header: {
    display: "grid",
    gridTemplateColumns: "10% 20% 25% 25% 10% 10%",
    fontSize: 16,
    fontWeight: "bold",
    padding: "38px 40px 17px",
    borderBottom: ".1px solid rgba(5, 0, 56, 0.06)",
  },
  body: {
    display: "grid",
    fontSize: 16,
    gridTemplateColumns: "10% 20% 25% 25% 10% 10%",
    padding: "20px 40px 18px",
  },
  button: {
    width: 148,
    height: 44,
    border: ".1px solid rgba(5, 0, 38, 1)",
    boxSizing: "border-box",
    display: "flex",
    alignItems: "center",
    boxShadow: "0px 2px 2px rgba(5, 0, 56, 0.08)",
    borderRadius: 6,
    cursor: "pointer",
    marginRight: 35
  },
  image: {
    width: "85%",
    marginBottom: 114
  },
  socialDivs: {
    width: 67,
    height: 50,
    padding: 2,
    marginRight: 42,
    border: "0.4px solid rgba(59, 107, 211, 0.8)",
    boxSizing: "border-box",
    borderRadius: "4px",
    display: "flex",
    flexDirection: "column"
  }
};

const animatedComponents = makeAnimated();

const ShowSingleBlog = (props) => {

  const [editMode, setEditMode] = useState(false);
  const [imageUploaded, setImageUploaded] = useState(false);
  const [loading, setLoading] = useState(false);

  const blogData = { ...props.location.state}

  const defaultFormValues = {
    files: [],
    author: "",
    twitterLink: false,
    linkedinLink: false,
    instagramLink: false,
    inner_link: "",
    tags: [],
    likes: "",
    heading: "",
    content: ""
  }
  const [formValues, setFormValues] = useState(defaultFormValues);

  const imageElement = useRef(null);

  const onDrop = useCallback(acceptedFiles => {
    handleImageUpload(acceptedFiles[0])
  }, []);

  const handleImageUpload = (file) => {
    const newImageSource = URL.createObjectURL(file);
    imageElement.current.src = newImageSource;
    setImageUploaded(true);
    setFormValues({ ...formValues, files: [...formValues.files, file]})
  }

  useEffect(() => {
    document.addEventListener("resize", () => {
      document.querySelector("#dropzone").style.height = `${imageElement.current.height}px`;
    });

    return () => {
      document.removeEventListener("resize", () => {
        document.querySelector("#dropzone").style.height = `${imageElement.current.height}px`;
      });
    }
  }, []);

  const adjustDropzone = () =>  {
    document.querySelector("#dropzone").style.height = `${imageElement.current.height}px`;
  }

        return (
            <>
              <Header />
              <div style={{
                marginBottom: 20,
                display: "flex",
                flexDirection: "row",
                paddingLeft: 29,
                paddingRight: 29,
                justifyContent: "space-between"
              }}>
                <div>
                  <div
                    style={{
                      ...styles.button,
                      backgroundColor: "",
                      justifyContent: "center",
                      color: "#050038",
                      border: "",
                      boxShadow: "",
                      cursor: "default"
                    }}
                  >
                  </div>
                </div>
                <div style={{ display: "flex", flexDirection: "row", justifyContent: "space-between"}}>
                  <div
                    style={{
                      ...styles.button,
                      backgroundColor: "#050038",
                      justifyContent: "center",
                      color: "white"
                    }}
                    onClick={() => props.history.push('/content-manager/blog/create')}
                  >
                    New Post
                  </div>
                  <div style={{
                    ...styles.button,
                    display: "flex",
                    flexDirection: "row",
                    color: "#050038",
                    paddingLeft: 12,
                    paddingRight: 5
                  }}>
                    <img style={{ marginRight: 20 }} src={require("../../../Assets/img/filter.svg")} />
                    <span style={{ marginRight: 25 }}>Filter</span>
                    <img style={{ marginTop: 2 }} src={require("../../../Assets/img/downwardArrow.svg")} />
                  </div>
                </div>
              </div>

                <section className="px-4">
                  <div style={styles.container}>
                    <div
                      style={{
                        ...styles.dataArea,
                        padding: "20px 85px",
                        display: "grid",
                        gridTemplateColumns: "80% 15%",
                        rowGap: "5%"
                      }}
                    >
                      <div style={styles.leftContent}>
                        <div style={{ fontSize: 16, marginBottom: 70 }}>
                          <div>Job Title</div>
                          <input
                            type="text"
                            disabled={editMode ? false : true}
                            value={blogData.author}
                            style={{
                              width: "85%",
                              background: editMode ? "" : "rgba(207, 207, 207, 0.2)",
                              borderRadius: "4px",
                              padding: 10,
                              color: "#050038",
                              fontWeight: 700
                            }}
                          />
                        </div>
                        <div style={{ fontSize: 16, marginBottom: 70 }}>
                          <div>Location</div>
                          <input
                            type="text"
                            disabled={editMode ? false : true}
                            value={blogData.innerLink}
                            style={{
                              width: "85%",
                              background: editMode ? "" : "rgba(207, 207, 207, 0.2)",
                              borderRadius: "4px",
                              padding: 10,
                              color: "#050038",
                              fontWeight: 700
                            }}
                          />
                        </div>
                        <div style={{ fontSize: 16, width: "85%", marginBottom: 70 }}>
                          <div>Job Type</div>
                          <input
                            type="text"
                            disabled={editMode ? false : true}
                            value={blogData.innerLink}
                            style={{
                              width: "85%",
                              background: editMode ? "" : "rgba(207, 207, 207, 0.2)",
                              borderRadius: "4px",
                              padding: 10,
                              color: "#050038",
                              fontWeight: 700
                            }}
                          />
                        </div>
                        <div style={{ fontSize: 16, marginBottom: 70 }}>
                          <div>Likes</div>
                          <input
                            type="text"
                            disabled={editMode ? false : true}
                            value={blogData.likes}
                            style={{
                              width: "85%",
                              background: editMode ? "" : "rgba(207, 207, 207, 0.2)",
                              borderRadius: "4px",
                              padding: 10,
                              color: "#050038",
                              fontWeight: 700
                            }}
                          />
                        </div>
                        <div style={{ width: "85%", marginBottom: 70}}>
                          <div>Heading</div>
                          <TextEditor
                            defaultValue={blogData.heading}
                            disabled={editMode ? false : true}
                          />
                        </div>
                        <div style={{ width: "85%", marginBottom: 70}}>
                          <div>Caption</div>
                          <TextEditor
                            defaultValue={blogData.content}
                            disabled={editMode ? false : true}
                          />
                        </div>
                        {editMode && <div style={{ display: "flex", flexDirection: "row", rowGap: 80, marginBottom: 70}}>
                          <div
                            style={{
                              ...styles.button,
                              backgroundColor: "#050038",
                              color: "white",
                              justifyContent: "center",
                              marginBottom: 50
                            }}
                            onClick={() => setEditMode(true)}
                          >
                            Save changes {loading && (
                            <div className="spinner-border spinner-border-white spinner-border-sm ml-2"></div>
                          )}
                          </div>
                          <div
                            style={{
                              ...styles.button,
                              color: "#050038",
                              justifyContent: "center",
                            }}
                          >
                            Archive Post
                          </div>
                        </div>}
                      </div>
                      <div style={styles.rightContent}>
                        <div
                          style={{
                            ...styles.button,
                            backgroundColor: editMode ? "rgba(5, 0, 56, 0.4)" : "#050038",
                            border: editMode ? "" : ".1px solid rgba(5, 0, 56, 0.06)",
                            cursor: editMode ? "not-allowed" : "pointer",
                            color: "white",
                            justifyContent: "center",
                            marginBottom: 50
                          }}
                          onClick={() => setEditMode(true)}
                        >
                          Edit {loading && (
                            <div className="spinner-border spinner-border-white spinner-border-sm ml-2"></div>
                          )}
                        </div>
                      </div>
                    </div>
                  </div>
                </section>
            </>
        )
}
const mapStateToProps = state => {
    return {
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
    }
}
export default withRouter(connect(mapStateToProps, mapDispatchToProps)(ShowSingleBlog))
