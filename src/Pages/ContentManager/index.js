import React, { useState } from 'react';
import { connect } from 'react-redux';
import Header from '../../Container/CMSHeader';

const styles = {
  container: {
    width: "100%",
    backgroundColor: "white",
    height: "67vh",
    overflowY: "scroll",
    marginBottom: 15,
    padding: "33px 29px 21px"
  },
  dataArea: {
    border: ".1px solid rgba(59,107,211,.2)",
    borderRadius: 6,
    rowGap: 30,
  },
  header: {
    display: "grid",
    gridTemplateColumns: "10% 20% 25% 25% 10% 10%",
    fontSize: 16,
    fontWeight: "bold",
    padding: "38px 40px 17px",
    borderBottom: ".1px solid rgba(5, 0, 56, 0.06)",
  },
  body: {
    display: "grid",
    fontSize: 16,
    gridTemplateColumns: "10% 20% 25% 25% 10% 10%",
    padding: "20px 40px 18px",
  },
  button: {
    width: 148,
    height: 44,
    border: ".1px solid rgba(59,107,211,.2)",
    boxSizing: "border-box",
    display: "flex",
    alignItems: "center",
    color: "white",
    boxShadow: "0px 2px 2px rgba(5, 0, 56, 0.08)",
    borderRadius: 6,
    cursor: "pointer",
    marginRight: 35
  },
  ellipsisOptions: {
    paddingTop: 10,
    paddingLeft: 25,
    paddingBottom: 10,
    cursor: "pointer"
    // background: "rgba(59,107,211,.08)"
  }
};
const Recent = () => {
    const [openFilter, setOpenFilter] = useState(false);
    const [openEllipsisOptions, setOpenEllipsisOptions] = useState({
      0: false,
      1: false
    });

    const openEllipsis = (index) => {
      setOpenEllipsisOptions({ ...openEllipsisOptions, [index]: !openEllipsisOptions[index] })
    }

    const toggleOpenFilter = (e) => {
      if(e.target.id === "filterArrow") {
        setOpenFilter(!openFilter)
      } else {
        if(openFilter) setOpenFilter(false)
      }
    }

      return (
        <>
          <Header />
          <div style={{
            marginLeft: "70%",
            marginBottom: 20,
            display: "flex",
            flexDirection: "row",
          }}>
            <div
              style={{
                ...styles.button,
                backgroundColor: "#050038",
                justifyContent: "center",
              }}
              onClick={() => window.location.href = '/content-manager/recent/create'}
            >
              New Post
            </div>
            <div
              style={{
                position: "relative"
              }}
            >
              <div style={{
                ...styles.button,
                display: "flex",
                flexDirection: "row",
                color: "#050038",
                background: "white",
                paddingLeft: 12,
                paddingRight: 5,
                borderBottomLeftRadius: openFilter ? 0 : 6,
                borderBottomRightRadius: openFilter ? 0 : 6
              }}>
                <img style={{ marginRight: 20 }} src={require("../../Assets/img/filter.svg")} />
                <span style={{ marginRight: 25 }}>Filter</span>
                <img
                  onClick={(e) => toggleOpenFilter(e, "arrow")}
                  style={{ marginTop: 2 }}
                  id="filterArrow"
                  src={require("../../Assets/img/downwardArrow.svg")}
                />
              </div>
              {openFilter && <div style={{
                position: 'absolute',
                width: 148,
                height: 100,
                color: "#050038",
                fontSize: 14,
                paddingLeft: 37,
                paddingTop: 5,
                background: "white",
                zIndex: 100,
                marginTop: -1,
                borderLeft: ".1px solid rgba(59,107,211,.2)",
                borderRight: ".1px solid rgba(59,107,211,.2)",
                borderBottom: ".1px solid rgba(59,107,211,.2)",
                borderBottomLeftRadius: 6,
                borderBottomRightRadius: 6
              }}>
                <div style={{ marginBottom: 25, cursor: "pointer"}}>Date</div>
                <div style={{ cursor: "pointer" }}>Archive</div>
              </div>}
            </div>
          </div>

          <section className="px-4">
            <div style={styles.container}>
              <div style={styles.dataArea}>
                <div style={styles.header}>
                  <div>
                    <input type="checkbox" style={{ transform: "scale(1.5)" }} />
                  </div>
                  <div style={{fontWeight: "bold"}}>Date Posted</div>
                  <div style={{ fontWeight: "bold" }}>Heading</div>
                  <div style={{ fontWeight: "bold" }}>Content</div>
                  <div></div>
                  <div></div>
                </div>
                <div style={styles.body}>
                  <div>
                    <input type="checkbox" style={{ transform: "scale(1.5)" }} />
                  </div>
                  <div>15-05-2021</div>
                  <div>The Bulb Africa gets pre-seed...</div>
                  <div>The quick brown fox jumped...</div>
                  <div style={{ position: "relative"}}>
                    <img onClick={() => openEllipsis(0)} src={require("../../Assets/img/verticalEllipsis.svg")} style={{ cursor: "pointer"}} />
                    {openEllipsisOptions[0] && <div
                      style={{
                        position: "absolute",
                        width: 105,
                        border: ".1px solid rgba(59,107,211,.2)",
                        marginLeft: -50,
                        backgroundColor: "white",
                        borderRadius: 3,
                        marginTop: 3
                      }}
                    >
                      <div
                        style={styles.ellipsisOptions}
                        onMouseEnter={() => {}}
                      >
                        View
                      </div>
                      <div style={styles.ellipsisOptions}>Export</div>
                      <div style={styles.ellipsisOptions}>Unpin</div>
                      <div style={styles.ellipsisOptions}>Archive</div>

                    </div>}
                  </div>
                  <div><img src={require("../../Assets/img/pin.svg")} /></div>
                </div>
              </div>
            </div>
          </section>
        </>
      )
}
const mapStateToProps = state => {
    return {

    }
}

const mapDispatchToProps = (dispatch) => {
    return {
    }
}
export default connect(mapStateToProps, mapDispatchToProps)(Recent)
