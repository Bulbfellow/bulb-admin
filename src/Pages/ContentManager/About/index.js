import Axios from 'axios';
import React, { useEffect, useRef, useState } from 'react';
import { withRouter } from 'react-router-dom';
import StaffProfile from "../../../Component/StaffProfile";
import { Api } from '../../../Const/Api';
import Header from '../../../Container/CMSHeader';
import staff from "./staff";

const styles = {
  container: {
    width: "100%",
    backgroundColor: "white",
    marginBottom: 15,
    padding: "33px 29px 21px"
  },
  dataArea: {
    border: ".1px solid rgba(59,107,211,.2)",
    borderRadius: 6,
    rowGap: 30,
    marginBottom: 50
  },
  header: {
    fontSize: 16,
    fontWeight: "bold",
    padding: "38px 40px 17px",
    borderBottom: ".1px solid rgba(5, 0, 56, 0.06)",
  },
  body: {
    display: "grid",
    fontSize: 16,
    gridTemplateColumns: "10% 20% 25% 25% 10% 10%",
    padding: "20px 40px 18px",
    cursor: "pointer"
  },
  button: {
    width: 148,
    height: 44,
    border: ".1px solid rgba(59,107,211,.2)",
    boxSizing: "border-box",
    display: "flex",
    alignItems: "center",
    color: "white",
    boxShadow: "0px 2px 2px rgba(5, 0, 56, 0.08)",
    borderRadius: 6,
    cursor: "pointer",
    marginRight: 35
  },
  ellipsisOptions: {
    paddingTop: 10,
    paddingLeft: 25,
    paddingBottom: 10,
    cursor: "pointer"
  }
};
const About = (props) => {
    const [openFilter, setOpenFilter] = useState(false);
    const [showAddForm, setShowAddForm] = useState(false);
    const [departments, setDepartments] = useState([]);
    const [departmentInputValue, setDepartmentInputValue] = useState("");
    const [openDepartment, setOpenDepartment] = useState(false);
    const [hoveredPicture, setHoveredPicture] = useState(false);
    const [pickedPictures, setPickedPictures] = useState([]);
    const [businessUnit, setBusinessUnit] = useState("Executive");

    const addDepartmentInput = useRef(null);

    const handlePictureHover = (pictureOwner) => {
      setHoveredPicture(pictureOwner);
    }

    const handleAddDepartmentInput = async (e) => {
      if(e.key === "Enter") {
        addDepartmentInput.current.disabled = true;
        addDepartmentInput.current.style.cursor = "not-allowed";
        await addDepartmentHandler(departmentInputValue);
      } else {
        setDepartmentInputValue(e.target.value);
      }
    }

    const addDepartmentHandler = async(name) => {
      try {
        const result = await Axios.post(
          `${Api.BaseUrl}/api/department/save`,
          { name },
          {
            'headers': { 'Authorization': `Bearer ${localStorage.getItem('token')}` }
          },
        );

        if(result.status === 200) {
          setDepartments([...departments, { name }]);
          setDepartmentInputValue("")
          addDepartmentInput.current.disabled = false;
          addDepartmentInput.current.value = "";
          addDepartmentInput.current.style.cursor = "text";
        }
      } catch(e) {
        console.log(e)
      }
    }

    const deleteDepartment = async(id) => {
      try {
        const result = await Axios.delete(
          `${Api.BaseUrl}/api/department/delete/${id}`,
          {
            'headers': { 'Authorization': `Bearer ${localStorage.getItem('token')}` }
          },
        );

        console.log(result);

        if(result.status === 200) {
          setDepartments(departments.filter(department => department.id !== id));
        }
      } catch(e) {
        console.log(e)
      }
    }

    useEffect(() =>{
      const getAllDepartments = async () => {
        try {
          const result = await Axios.get(
            `${Api.BaseUrl}/api/department/all`,
            {
              'headers': { 'Authorization': `Bearer ${localStorage.getItem('token')}` }
            },
          );

          console.log(result)

          if(result.status === 200) {
            setDepartments(result.data.payload.message);
          }
        } catch(e) {
          console.log(e)
        }
      };

      getAllDepartments();
    }, [props]);

    const handlePickedPicture = (pictureOwner) => {
      if(pickedPictures.includes(pictureOwner)) {
        setPickedPictures(pickedPictures.filter(pickedPicture => pickedPicture !== pictureOwner));
      }else {
        setPickedPictures([...pickedPictures, pictureOwner])
      }
    }

    const handleAddDepartment = () => {
      setShowAddForm(true);
    }
  
    const handleBusinessUnit = (unit) => {
      setPickedPictures([]);
      setBusinessUnit(unit);
      setOpenDepartment(false);
    }

    const toggleOpenFilter = (e) => {
      if(e.target.id === "filterArrow" || e.target.id === "filterArrow-img") {
        setOpenFilter(!openFilter)
      } else {
        if(openFilter) setOpenFilter(false)
      }
    }

    const toggleOpenDepartment = (e) => {
      if(e.target.id === "department-div" || e.target.id === "department-div-img") {
        setOpenDepartment(!openDepartment)
      } else {
        if(openDepartment) setOpenDepartment(false)
      }
      setShowAddForm(false);
    }

      return (
        <>
          <Header />
          <div style={{
            marginBottom: 20,
            paddingLeft: 29,
            paddingRight: 29,
            display: "flex",
            flexDirection: "row",
            justifyContent: "space-between"
          }}>
            <div>
              <div
                style={{
                  ...styles.button,
                  justifyContent: "center",
                  color: "#050038",
                  backgroundColor: pickedPictures.length > 0 ? "white" : "",
                  border: pickedPictures.length > 0 ? "0.2px solid #3B6BD3" : "",
                  cursor: pickedPictures.length < 0 && "default",
                  boxShadow: pickedPictures.length < 0 && "" 
                }}
              >
                {pickedPictures.length > 0 && "Archive" }
              </div>
            </div>
            <div style={{ display: "flex", flexDirection: "row", justifyContent: "space-between"}}>
              <div
                style={{
                  ...styles.button,
                  backgroundColor: "#050038",
                  justifyContent: "center",
                }}
                onClick={() => props.history.push('/content-manager/about/create')}
              >
                New Upload
              </div>
              <div
                style={{
                  position: "relative"
                }}
              >
                <div style={{
                  ...styles.button,
                  display: "flex",
                  flexDirection: "row",
                  color: "#050038",
                  background: "white",
                  paddingLeft: 12,
                  paddingRight: 5,
                  borderBottomLeftRadius: openFilter ? 0 : 6,
                  borderBottomRightRadius: openFilter ? 0 : 6
                }}>
                  <img style={{ marginRight: 20 }} src={require("../../../Assets/img/filter.svg")} />
                  <span style={{ marginRight: 25 }}>Filter</span>
                  <img
                    onClick={(e) => toggleOpenFilter(e, "arrow")}
                    style={{ marginTop: 2 }}
                    id="filterArrow"
                    src={require("../../../Assets/img/downwardArrow.svg")}
                  />
                </div>
                {openFilter && <div style={{
                  position: 'absolute',
                  width: 148,
                  height: 100,
                  color: "#050038",
                  fontSize: 14,
                  paddingLeft: 37,
                  paddingTop: 5,
                  background: "white",
                  zIndex: 100,
                  marginTop: -1,
                  borderLeft: ".1px solid rgba(59,107,211,.2)",
                  borderRight: ".1px solid rgba(59,107,211,.2)",
                  borderBottom: ".1px solid rgba(59,107,211,.2)",
                  borderBottomLeftRadius: 6,
                  borderBottomRightRadius: 6
                }}>
                  <div style={{ marginBottom: 25, cursor: "pointer"}}>Date</div>
                  <div style={{ cursor: "pointer" }}>Archive</div>
                </div>}
              </div>
            </div>
          </div>

          <section className="px-4">
            <div style={styles.container}>
              <div style={styles.dataArea}>
                <div style={styles.header}>
                  <div
                    style={{
                      width: 300,
                      height: 50,
                      borderRadius: 4,
                      marginBottom: 80,
                      position: "relative",
                    }}
                  >
                      <div
                        id="department-div"
                        style={{
                          ...styles.button,
                          display: "flex",
                          flexDirection: "row",
                          justifyContent: "space-between",
                          color: "#050038",
                          background: "white",
                          width: "100%",
                          height: "100%",
                          paddingLeft: 15,
                          paddingRight: 20,
                          borderBottomLeftRadius: openDepartment ? 0 : 6,
                          borderBottomRightRadius: openDepartment ? 0 : 6,
                          border: ".1px solid rgba(59,107,211,.2)",
                          boxSizing: "border-box",
                          boxShadow: "0px 2px 2px rgba(5, 0, 56, 0.08)",
                          borderRadius: 6,
                          cursor: "pointer",
                        }}
                        onClick={(e) => toggleOpenDepartment(e)}
                      >
                        <span>Select Department</span>
                        <img
                          id="department-div-img"
                          style={{ marginTop: 2 }}
                          src={require("../../../Assets/img/downwardArrow.svg")}
                        />
                      </div>
                      {openDepartment && <div style={{
                        position: 'absolute',
                        width: "100%",
                        minHeight: 100,
                        color: "#050038",
                        fontSize: 14,
                        paddingTop: 5,
                        background: "white",
                        zIndex: 100,
                        marginTop: -1,
                        borderLeft: ".1px solid rgba(59,107,211,.2)",
                        borderRight: ".1px solid rgba(59,107,211,.2)",
                        borderBottom: ".1px solid rgba(59,107,211,.2)",
                        borderBottomLeftRadius: 6,
                        borderBottomRightRadius: 6
                      }}>
                        {departments.map(department => <div
                          style={{
                            display: "flex",
                            flexDirection: "row",
                            paddingLeft: 15,
                            paddingRight: 20,
                            justifyContent: "space-between",
                            marginBottom: 25,
                            cursor: "pointer"
                          }}
                        >
                          <span onClick={() => handleBusinessUnit('Executive')}>{`${department.name} Team`}</span>
                          <img
                            onClick={() => deleteDepartment(department.id)}
                            src={require("../../../Assets/img/delete-red.svg")}
                          />
                        </div>)}                    
                        {!showAddForm ? <div
                          style={{
                            marginBottom: 25,
                            cursor: "pointer",
                            display: "flex",
                            paddingLeft: 15,
                            paddingRight: 20,
                            flexDirection: "row"
                          }}
                        >
                          <div style={{ marginRight: 15 }}>
                            <img
                              src={require("../../../Assets/img/add-department.svg")}
                            />
                          </div>
                          <div 
                            style={{
                              fontSize: 16,
                              fontWeight: "normal",
                              color: "#707070",
                              marginTop: 2
                            }}
                            onClick={() => handleAddDepartment()}
                          >
                            Add department
                          </div>
                        </div> : <div>
                            <div>
                              <input
                                type="text"
                                autoFocus
                                ref={addDepartmentInput}
                                // value={departmentInputValue}
                                style={{
                                  padding: "10px 10px 10px 18px",
                                  width: "100%",
                                  fontSize: 16,
                                  color: "#707070",
                                  backgroundColor: "rgba(196, 196, 196, 0.1)",
                                  outline: "none",
                                  border: "none"
                                }}
                                onKeyUp={handleAddDepartmentInput}
                              />
                            </div>
                        </div>}                   
                      </div>}
                  </div>
                  <div
                    style={{
                      marginBottom: 50,
                      display: "grid",
                      gridTemplateColumns: "30% 30% 30%",
                      columnGap: "5%",
                      rowGap: 50
                      }}
                    >
                    {staff[businessUnit].map(({ name, role }) => 
                      <StaffProfile
                        name={name}
                        role={role}
                        imageStyle={{width: 248, height: "auto"}}
                        setHoveredPicture={handlePictureHover}
                        hoveredPicture={hoveredPicture}
                        pickedPictures={pickedPictures}
                        setPickedPictures={handlePickedPicture}
                        businessUnit={businessUnit}
                      />
                    )}
                  </div>
                </div>
              </div>
            </div>
          </section>
        </>
      )
}

export default withRouter(About);
