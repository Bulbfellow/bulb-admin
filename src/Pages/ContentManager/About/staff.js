export default {
  Executive: [
    {
      name:"Tosin Osunkoya",
      role:"President & Founder"
    },
    {
      name:"Wande Adams",
      role:"Managing Director"
    },
    {
      name:"Modupe Durosinmi-Etti",
      role:"Programs Manager"
    }
  ],
  Operations: [
    { name: "Bukola Adelanwo", role: "Operations/Admin Lead"},
    { name: "Seun Adedokun", role: "Finance Officer"},
    { name: "Oluwaseyi Oyefeso", role: "Business Development"},
    { name: "Majemu Olowodola", role: "Business Analyst"},
    { name: "Bibilomo Dada", role: "HR/Admin Officer"},
    { name: "Adeyemi Vincent", role: "System & Network Admin"},
    { name: "Abimbola Fisher", role: "Human Resource"},
  ],
  Engineering: [
    { name: "Lateefat Amuda", role: "Backend Developer"},
    { name: "Rufus Abioritsegbemi", role: "QA Engineer"},
    { name: "Juwon Fagbohungbe", role: "Fullstack Developer"},
    { name: "Emmanuel Aboderin", role: "Backend Developer"},
    { name: "Taiwo Olajide", role: "Software Tester"},
    { name: "Jesse Omoefe", role: "Fullstack Developer"},
    { name: "Isaiah Olaifa", role: "Fullstack Developer"},
    { name: "Gbenga Olowogbayi", role: "Fullstack Developer"},
    { name: "Babatunde Ogedengbe", role: "Backend Developer"},
    { name: "Anthony Odu", role: "Mobile Developer"},
    { name: "Amani Kanu", role: "Frontend Developer"},
    { name: "Osagie Eloghosa", role: "Backend Developer"},
    { name: "Tolulope Adetula", role: "Frontend Developer"},
    { name: "Chukwuemeka Igbokwe", role: "Fullstack Developer"},
    { name: "Bunmi Balogun", role: "Data Scientist"},
    { name: "David Igbigbi", role: "Fullstack Developer"},
    { name: "Timothy Fadayini", role: "Fullstack Developer"},
    { name: "Oluwadamiloju Yusuf", role: "Fullstack Developer"},
    { name: "Olamilekan Thanni", role: "Web Developer"},
    { name: "Jahfaith Irokanulo", role: "Data Scientist"},
    { name: "Olaniyi Ajayi", role: "Machine Learning Engineer"},
    { name: "Austin Ekpene", role: "Software Data Analyst"},
    { name: "Daniel Ayodele", role: "Software Tester"},
    { name: "Nnenna Kalu-Odo", role: "Scrum Master"},
    { name: "Oluyemi Adebayo", role: "Software Developer"},
  ],
  Projects: [
    { name: "Desire Ugwumba", role: "Projects Manager"},
    { name: "Isabella Igbojionu", role: "Product Management Intern"},
  ],
  Design: [
    { name: "Tamarabibi Ofuya", role: "Product Designer"},
    { name: "Tega Odiavbara", role: "Product Designer"},
    { name: "Edmond Benson", role: "Product Designer"},
  ],
  Content: [
    { name: "Muhammad Eyinfunjowo", role: "Content Developer"},
    { name: "Olumide Onakoya", role: "Marketting Intern"},
  ]
}