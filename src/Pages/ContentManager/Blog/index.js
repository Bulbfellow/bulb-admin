import Axios from 'axios';
import React, { useEffect, useState } from 'react';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import { Api } from '../../../Const/Api';
import Header from '../../../Container/CMSHeader';

const styles = {
  container: {
    width: "100%",
    backgroundColor: "white",
    marginBottom: 15,
    padding: "33px 29px 21px"
  },
  dataArea: {
    border: ".1px solid rgba(59,107,211,.2)",
    borderRadius: 6,
    rowGap: 30,
  },
  header: {
    display: "grid",
    gridTemplateColumns: "10% 20% 30% 30% 5% 5%",
    fontSize: 16,
    fontWeight: "bold",
    padding: "38px 40px 17px",
    borderBottom: ".1px solid rgba(5, 0, 56, 0.06)",
  },
  body: {
    display: "grid",
    fontSize: 16,
    gridTemplateColumns: "10% 20% 30% 30% 5% 5%",
    padding: "20px 40px 18px",
    cursor: "pointer"
  },
  button: {
    width: 148,
    height: 44,
    border: ".1px solid rgba(59,107,211,.2)",
    boxSizing: "border-box",
    display: "flex",
    alignItems: "center",
    color: "white",
    boxShadow: "0px 2px 2px rgba(5, 0, 56, 0.08)",
    borderRadius: 6,
    cursor: "pointer",
    marginRight: 35
  },
  ellipsisOptions: {
    paddingTop: 10,
    paddingLeft: 25,
    paddingBottom: 10,
    cursor: "pointer"
  }
};
const Blog = (props) => {
    const [openFilter, setOpenFilter] = useState(false);
    const [viewMode, setViewMode] = useState("live");
    const [archivedPostIds, setArchivedPostIds] = useState([]);
    const [totalPosts, setTotalPosts] = useState([]);
    const [selectedPosts, setSelectedPosts] = useState([]);
    const [showedPosts, setShowedPosts] = useState([]);
    const [openEllipsisOptions, setOpenEllipsisOptions] = useState(null);

    const openEllipsis = (index) => {
      setOpenEllipsisOptions({ ...openEllipsisOptions, [index]: !openEllipsisOptions[index] })
    }

    const handleSelectPost = (blogId) => {
      if(selectedPosts.includes(blogId)) {
        setSelectedPosts(selectedPosts.filter(post => post !== blogId && post !== "Select all"))
      }else {
        setSelectedPosts([...selectedPosts, blogId]);
      }
    }

    const handleSelectAll = () => {
      if(selectedPosts[0] === "Select all") {
        setSelectedPosts([]);
      }else {
        setSelectedPosts(["Select all", ...totalPosts.map(post => post.id)])
      }
    }

    const switchViewMode = () => {
      if(viewMode === "live") {
        setShowedPosts(totalPosts.filter(post => archivedPostIds.includes(post.id)));
        setViewMode("archived")
      }else {
        setShowedPosts(totalPosts.filter(post => !archivedPostIds.includes(post.id)));
        setViewMode("live")
      }
      setOpenFilter(false)
    }

    const toggleArchive = async (ids) => {
      const url = viewMode === "live" ?
        `${Api.BaseUrl}/api/blog/archiveBlog/`
        : `${Api.BaseUrl}/api/blog/unarchiveBlog/`;

      try{
          const result = await Axios.patch(
            url,
            { ids: ids[0] === 'Select all' ? ids.slice(1) : ids },
            {
              'headers': { 'Authorization': `Bearer ${localStorage.getItem('token')}` }
            },
          )
          if(result.status === 200) {
            let totalArchivedIds;
            if(viewMode === "live") {
              totalArchivedIds = [...archivedPostIds, ...ids]
              setArchivedPostIds(totalArchivedIds);
              setShowedPosts(totalPosts.filter(post => !totalArchivedIds.includes(post.id)));
            } else {
              totalArchivedIds = archivedPostIds.filter(id => !ids.includes(id))
              setArchivedPostIds(totalArchivedIds);
              setShowedPosts(totalPosts.filter(post => totalArchivedIds.includes(post.id)));
            }
            setOpenEllipsisOptions(null);
            setSelectedPosts([]);
          }
      }catch(e) {
        console.log(e)
      }
    }

    const toggleOpenFilter = (e) => {
      if(e.target.id === "filterArrow" || e.target.id === "filterArrow-img") {
        setOpenFilter(!openFilter)
      } else {
        if(openFilter) setOpenFilter(false)
      }
    }

    useEffect(() =>{
      const getAllBlogs = async () => {
        try {
          const result = await Axios.get(
            `${Api.BaseUrl}/api/blog/all`,
            {
              'headers': { 'Authorization': `Bearer ${localStorage.getItem('token')}` }
            },
          );

          if(result.status === 200) {
            setTotalPosts(result.data.payload.message);
            setShowedPosts(result.data.payload.message.filter(post => post.status === "live"))
            setArchivedPostIds(result.data.payload.message
              .filter(post => post.status === "archived")
              .map(post => post.id))
          }
        } catch(e) {
          console.log(e)
        }
      };

      getAllBlogs();
    }, [props]);
    

      return (
        <>
          <Header />
          <div style={{
            marginBottom: 20,
            paddingLeft: 29,
            paddingRight: 29,
            display: "flex",
            flexDirection: "row",
            justifyContent: "space-between"
          }}>
            <div>
              <div
                style={{
                  ...styles.button,
                  justifyContent: "center",
                  color: "#050038",
                  backgroundColor: selectedPosts.length > 0 ? "white" : "",
                  border: selectedPosts.length > 0 ? ".1px solid rgba(59,107,211,.2)" : "",
                  cursor: selectedPosts.length < 1 ? "default" : "pointer",
                  boxShadow: selectedPosts.length < 1 && "",
                  borderRadius: 6,

                }}
                onClick={() => toggleArchive(selectedPosts)}
              >
                {selectedPosts.length > 0 && (viewMode === "live" ? "Archive" : "Unarchive")}
              </div>
            </div>
            <div style={{ display: "flex", flexDirection: "row", justifyContent: "space-between"}}>
              <div
                style={{
                  ...styles.button,
                  backgroundColor: "#050038",
                  justifyContent: "center",
                }}
                onClick={() => props.history.push('/content-manager/blog/create')}
              >
                New Post
              </div>
              <div
                style={{
                  position: "relative"
                }}
              >
                <div
                  id="filterArrow"
                  style={{
                    ...styles.button,
                    display: "flex",
                    flexDirection: "row",
                    color: "#050038",
                    background: "white",
                    paddingLeft: 12,
                    paddingRight: 5,
                    borderBottomLeftRadius: openFilter ? 0 : 6,
                    borderBottomRightRadius: openFilter ? 0 : 6,
                    border: ".1px solid rgba(59,107,211,.2)",
                    boxSizing: "border-box",
                    boxShadow: "0px 2px 2px rgba(5, 0, 56, 0.08)",
                    borderRadius: 6,
                    cursor: "pointer",
                  }}
                  onClick={(e) => toggleOpenFilter(e)}
                >
                  <img style={{ marginRight: 20 }} src={require("../../../Assets/img/filter.svg")} />
                  <span style={{ marginRight: 25 }}>Filter</span>
                  <img
                    id="filterArrow-img"
                    style={{ marginTop: 2 }}
                    src={require("../../../Assets/img/downwardArrow.svg")}
                  />
                </div>
                {openFilter && <div style={{
                  position: 'absolute',
                  width: 148,
                  height: 100,
                  color: "#050038",
                  fontSize: 14,
                  paddingLeft: 37,
                  paddingTop: 5,
                  background: "white",
                  zIndex: 100,
                  marginTop: -1,
                  borderLeft: ".1px solid rgba(59,107,211,.2)",
                  borderRight: ".1px solid rgba(59,107,211,.2)",
                  borderBottom: ".1px solid rgba(59,107,211,.2)",
                  borderBottomLeftRadius: 6,
                  borderBottomRightRadius: 6
                }}>
                  <div style={{ marginBottom: 25, cursor: "pointer"}}>Date</div>
                  <div style={{ cursor: "pointer" }} onClick={switchViewMode}>
                    {viewMode === "live" ? "Archived" : "Unarchived"}
                  </div>
                </div>}
              </div>
            </div>
          </div>

          <section className="px-4">
            <div style={styles.container}>
              <div style={styles.dataArea}>
                <div style={styles.header}>
                  <div>
                    <input
                      type="checkbox"
                      style={{ transform: "scale(1.5)", cursor: "pointer" }}
                      checked={selectedPosts.includes("Select all")}
                      onChange={() => handleSelectAll()}
                    />
                  </div>
                  <div style={{fontWeight: "bold"}}>Date Posted</div>
                  <div style={{ fontWeight: "bold" }}>Heading</div>
                  <div style={{ fontWeight: "bold" }}>Content</div>
                  <div></div>
                  <div></div>
                </div>
                {showedPosts.length > 0 ? showedPosts.map(blog =>
                  <div style={styles.body}>
                    <div>
                      <input
                        type="checkbox"
                        style={{ transform: "scale(1.5)", cursor: "pointer" }}
                        checked={selectedPosts.includes(blog.id) || selectedPosts[0] === "Select all"}
                        onChange={() => handleSelectPost(blog.id)}
                      />
                    </div>
                    <div
                      onClick={() => props.history.push('/content-manager/blog/single', {...blog})}
                    >
                      {
                      new Date(blog.created_at)
                        .toLocaleString("en-GB", { year: "numeric", month: "numeric", day: "numeric"})
                        .replaceAll("/", "-",)}
                    </div>
                    <div
                      dangerouslySetInnerHTML={{ __html: blog.heading }}
                      onClick={() => props.history.push('/content-manager/blog/single', {...blog})}
                    />
                    <div
                      dangerouslySetInnerHTML={{ __html: blog.content }}
                      onClick={() => props.history.push('/content-manager/blog/single', {...blog})}
                    />
                    <div style={{ position: "relative"}}>
                      <img
                        onClick={() => openEllipsis(blog.id)}
                        src={require("../../../Assets/img/verticalEllipsis.svg")}
                        style={{ cursor: "pointer"}}
                      />
                      {openEllipsisOptions === blog.id && <div
                        style={{
                          position: "absolute",
                          width: 105,
                          border: ".1px solid rgba(59,107,211,.2)",
                          marginLeft: -50,
                          backgroundColor: "white",
                          borderRadius: 3,
                          marginTop: 3,
                          zIndex: 700
                        }}
                      >
                        <div
                          style={styles.ellipsisOptions}
                          onMouseEnter={() => {}}
                        >
                          View
                        </div>
                        <div style={styles.ellipsisOptions}>Export</div>
                        <div
                          onClick={() => toggleArchive([blog.id])}
                          style={styles.ellipsisOptions}
                        >
                            {archivedPostIds.includes(blog.id) ? "Unarchive" : "Archive"}
                        </div>
                      </div>}
                    </div>
                  </div>
                ): (
                  <div
                    style={{
                      textAlign: "center",
                      fontSize: 16,
                      color: "#050038",
                      marginBottom: 30,
                      marginTop: 30
                    }}
                  >
                    {
                      `There are no ${viewMode === "archived" ? "archived" : "unarchived"} blog posts yet. Check back later.`
                    }
                  </div>
                )}
              </div>
            </div>
          </section>
        </>
      )
}
const mapStateToProps = state => {
    return {

    }
}

const mapDispatchToProps = (dispatch) => {
    return {
    }
}
export default withRouter(connect(mapStateToProps, mapDispatchToProps)(Blog));
