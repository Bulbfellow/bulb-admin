import Axios from 'axios';
import React, { useEffect, useState } from 'react';
import { withRouter } from 'react-router-dom';
import ExtendedModal from '../../../Component/ExtendedModal';
import { Api } from '../../../Const/Api';
import Header from '../../../Container/CMSHeader';


const styles = {
  container: {
    width: "100%",
    backgroundColor: "white",
    marginBottom: 15,
    padding: "33px 29px 21px"
  },
  dataArea: {
    border: ".1px solid rgba(59,107,211,.2)",
    borderRadius: 6,
    rowGap: 30,
  },
  header: {
    display: "grid",
    gridTemplateColumns: "10% 20% 25% 25% 10% 10%",
    fontSize: 16,
    fontWeight: "bold",
    padding: "38px 40px 17px",
    borderBottom: ".1px solid rgba(5, 0, 56, 0.06)",
  },
  body: {
    display: "grid",
    fontSize: 16,
    gridTemplateColumns: "10% 20% 25% 25% 10% 10%",
    padding: "20px 40px 18px",
    cursor: "pointer"
  },
  button: {
    width: 148,
    height: 44,
    border: ".1px solid rgba(59,107,211,.2)",
    boxSizing: "border-box",
    display: "flex",
    alignItems: "center",
    color: "white",
    boxShadow: "0px 2px 2px rgba(5, 0, 56, 0.08)",
    borderRadius: 6,
    cursor: "pointer",
    marginRight: 35
  },
  ellipsisOptions: {
    paddingTop: 10,
    paddingLeft: 25,
    paddingBottom: 10,
    cursor: "pointer"
  }
};
const Community = (props) => {
    const [openFilter, setOpenFilter] = useState(false);
    const [showModal, setShowModal] = useState(false);
    const [viewMode, setViewMode] = useState("live");
    const [hoveredLogo, setHoveredLogo] = useState(null);
    const [pickedLogos, setPickedLogos] = useState([]);
    const [totalCommunities, setTotalCommunities] = useState([]);
    const [showedCommunities, setShowedCommunities] = useState([]);
    const [archivedCommunitiesIds, setArchivedCommunitiesIds] = useState([]);
    const [modalDetails, setModalDetails] = useState({
      title: "",
      style: {},
      onClickAction: null
    });

    const handleLogoHover = (logo) => {
      setHoveredLogo(logo);
    }

    const handlePickedLogos = (logo) => {
      if(pickedLogos.includes(logo)) {
        setPickedLogos(pickedLogos.filter(pickedLogo => pickedLogo !== logo));
      }else {
        setPickedLogos([...pickedLogos, logo])
      }
    }

    const closeModal = () => {
      setShowModal(false);
      setModalDetails({});
    }

    const toggleArchive = () => {
      if(viewMode === "live") {
        setShowedCommunities(totalCommunities.filter(community => community.status !== "live"));
        setViewMode("archived");
      }else {
        setShowedCommunities(totalCommunities.filter(community => community.status === "live"));
        setViewMode("live");
      }
      setPickedLogos([]);
      setOpenFilter(false)
    }

    const toggleOpenFilter = (e) => {
      if(e.target.id === "filterArrow" || e.target.id === "filterArrow-img") {
        setOpenFilter(!openFilter)
      } else {
        if(openFilter) setOpenFilter(false)
      }
    }

    const handleButtonsClick = (buttonTitle) => {
      const namesOfPickedCommunities = pickedLogos.map(logo => {
        const theCommunity = totalCommunities.find(community => community.id === logo)
        if(theCommunity) {
          return theCommunity.heading
        }
      })
      .reduce((communities, community) => community && `${communities}, ${community}`)

      if(buttonTitle === "Archive") {
        setModalDetails({
          title: `Are you sure you want to archive `,
          style: { backgroundColor: "#050038", color: "white", border: "0.2px solid #3B6BD3"},
          onClickAction: () => archiveCommunity(pickedLogos),
          buttonTitle : viewMode === "live" ? buttonTitle : `Un${buttonTitle.toLowerCase()}`,
          namesOfPickedCommunities
        });
      }else if(buttonTitle === "Delete") {
        setModalDetails({
          title: `Are you sure you want to delete `,
          style: { backgroundColor: "#BD1919", color: "#fff"},
          onClickAction: () => deleteCommunity(pickedLogos),
          buttonTitle,
          namesOfPickedCommunities
        });
      }
      setShowModal(true);
    }

    const archiveCommunity = async (ids) => {
      const url = viewMode === "live" ?
        `${Api.BaseUrl}/api/community/archive`
        : `${Api.BaseUrl}/api/community/unarchive/`;

      try {
        const result = await Axios.patch(
          url,
          { ids },
          {
            'headers': { 'Authorization': `Bearer ${localStorage.getItem('token')}` }
          },
        );
        if(result.status === 200) {
          setPickedLogos([]);
          setModalDetails({
            title: `Communities ${viewMode === "live"? "archived" : "unarchived"} successfully.`,
            status: result.status
          });
        }
        console.log(result);
      }catch(e) {
        console.log(e);
      }
    }

    const deleteCommunity = async (ids) => {
      const url = `${Api.BaseUrl}/api/community/delete`;
      try {
        const result = await Axios.patch(
          url,
          { ids },
          {
            'headers': { 'Authorization': `Bearer ${localStorage.getItem('token')}` }
          },
        );

        if(result.status === 200) {
          setPickedLogos([]);
          setModalDetails({
            title: `Communities deleted successfully.`,
            status: result.status
          });
        }
      }catch(e) {
        console.log(e);
      }
    }


    useEffect(() =>{
      const getAllCommunities = async () => {
        try {
          const result = await Axios.get(
            `${Api.BaseUrl}/api/community/all`,
            {
              'headers': { 'Authorization': localStorage.getItem('token') }
            },
          );

          if(result.status === 200) {
            setTotalCommunities(result.data.payload.message);
            setShowedCommunities(result.data.payload.message.filter(post => post.status === "live"))
            setArchivedCommunitiesIds(result.data.payload.message
              .filter(post => post.status !== "live")
              .map(post => post.id))
          }
        } catch(e) {
          console.log(e)
        }
      };

      getAllCommunities();
    }, [props, showModal]);

      return (
        <>
          <Header />
          <div style={{
            marginBottom: 20,
            paddingLeft: 29,
            paddingRight: 29,
            display: "flex",
            flexDirection: "row",
            justifyContent: "space-between"
          }}>
            <div style={{ display: "flex", flexDirection: "row", justifyContent: "space-between"}}>
              <div
                style={{
                  ...styles.button,
                  justifyContent: "center",
                  color: "#050038",
                  backgroundColor: pickedLogos.length > 0 ? "white" : "",
                  border: pickedLogos.length > 0 ? "0.2px solid #3B6BD3" : "",
                  cursor: pickedLogos.length === 0 ? "default" : "pointer",
                  boxShadow: pickedLogos.length < 0 && "" 
                }}
                onClick={() => handleButtonsClick("Archive")}
              >
                {pickedLogos.length > 0 && (
                    viewMode === "live" ? 'Archive' : 'Unarchive' 
                )}
              </div>
              <div
                style={{
                  ...styles.button,
                  justifyContent: "center",
                  color: "#BD1919",
                  fontSize: 15,
                  fontWeight: "bold",
                  backgroundColor: pickedLogos.length > 0 ? "white" : "",
                  border: pickedLogos.length > 0 ? "0.2px solid #3B6BD3" : "",
                  cursor: pickedLogos.length === 0 ? "default" : "pointer",
                  boxShadow: pickedLogos.length < 0 && "" 
                }}
                onClick={() => pickedLogos.length > 0 && handleButtonsClick("Delete")}
              >
                {pickedLogos.length > 0 && <span style={{marginRight: 13, marginTop: -3}}><img src={require("../../../Assets/img/trash-red.svg")} /></span>} {pickedLogos.length > 0 && "Delete"}
              </div>
            </div>
            <div style={{ display: "flex", flexDirection: "row", justifyContent: "space-between"}}>
              <div
                style={{
                  ...styles.button,
                  backgroundColor: "#050038",
                  justifyContent: "center",
                }}
                onClick={() => props.history.push('/content-manager/community/create')}
              >
                New Upload
              </div>
              <div
                style={{
                  position: "relative"
                }}
              >
                <div 
                  id="filterArrow"
                  style={{
                    ...styles.button,
                    display: "flex",
                    flexDirection: "row",
                    color: "#050038",
                    background: "white",
                    paddingLeft: 12,
                    paddingRight: 5,
                    borderBottomLeftRadius: openFilter ? 0 : 6,
                    borderBottomRightRadius: openFilter ? 0 : 6
                  }}
                  onClick={(e) => toggleOpenFilter(e)}
                >
                  <img style={{ marginRight: 20 }} src={require("../../../Assets/img/filter.svg")} />
                  <span style={{ marginRight: 25 }}>Filter</span>
                  <img
                    style={{ marginTop: 2 }}
                    id="filterArrow-img"
                    src={require("../../../Assets/img/downwardArrow.svg")}
                  />
                </div>
                {openFilter && <div style={{
                  position: 'absolute',
                  width: 148,
                  height: 100,
                  color: "#050038",
                  fontSize: 14,
                  paddingLeft: 37,
                  paddingTop: 5,
                  background: "white",
                  zIndex: 100,
                  marginTop: -1,
                  borderLeft: ".1px solid rgba(59,107,211,.2)",
                  borderRight: ".1px solid rgba(59,107,211,.2)",
                  borderBottom: ".1px solid rgba(59,107,211,.2)",
                  borderBottomLeftRadius: 6,
                  borderBottomRightRadius: 6
                }}>
                  <div style={{ marginBottom: 25, cursor: "pointer"}}>Date</div>
                  <div style={{ cursor: "pointer" }} onClick={toggleArchive}>
                    {viewMode === "live" ? 'Archived' : 'Unarchived' }
                  </div>
                </div>}
              </div>
            </div>
          </div>

          <section className="px-4">
            <div style={styles.container}>
              <div style={{ ...styles.dataArea, padding: 30, minHeight: "100vh" }}>
                <div style={{
                  display: "grid",
                  gridTemplateColumns: "repeat(3, 30%)",
                  columnGap: "5%",
                  rowGap: 30
                }}>
                  {showedCommunities.map(community => <div
                    style={{
                      border: "0.1px solid #3B6BD3",
                      boxSizing: "border-box",
                      boxShadow: "0px 2px 2px rgba(59, 107, 211, 0.08)",
                      borderRadius: 4,
                      display: "flex",
                      flexDirection: "column",
                      alignItems: "center",
                      padding: "30px 25px",
                      color: "#050038",
                      fontSize: 16,
                      position: "relative"
                    }}
                    onMouseEnter={() => handleLogoHover(community.id)}
                    onMouseLeave={() => handleLogoHover("")}
                  >
                    <img style={{ marginBottom: 12, width: 90, height: 90 }} src={`${community.logo.slice(2, -2)}`} />
                    <p style={{ fontWeight: 700, marginBottom: 26 }}>{community.heading}</p>
                    <p>{community.description}</p>
                    {(hoveredLogo === community.id || pickedLogos.length > 0) && <div
                      style={{
                        position: "absolute",
                        width: "100%",
                        height: "100%",
                        padding: "14px 25px",
                        backgroundColor: "rgba(0, 0, 0, .4)",
                        top: 0,
                        left: 0,
                        display: "flex",
                        flexDirection: "column",
                      }}
                    >
                      <div
                        style={{
                          display: "flex",
                          flexDirection: "row",
                          marginBottom: "35%",
                          justifyContent: "flex-end"
                        }}
                      >
                        <input
                          type="checkbox"
                          style={{ transform: "scale(1.5)"}}
                          onChange={() => handlePickedLogos(community.id)}
                        />
                      </div>
                      <div
                        style={{
                          display: "flex",
                          flexDirection: "row",
                          justifyContent: "center"
                        }}
                      >
                        <div
                          style={{
                            width: 111,
                            height: 48,
                            background: "white",
                            display: "flex",
                            justifyContent: "center",
                            alignItems: "center",
                            borderRadius: 4,
                            fontSize: 15,
                            fontWeight: "bold",
                            color: "#050038",
                            cursor: "pointer"
                          }}
                          onClick={() => props.history.push("/content-manager/community/single", {...community})}
                        >
                          View
                        </div>
                      </div>

                    </div>}
                  </div>)}
                </div>
              </div>
            </div>
          </section>
          <ExtendedModal showModal={showModal} closeModal={closeModal}>
            {modalDetails.style ? <div style={{
              display: "flex",
              flexDirection: "column",
              alignItems: "center",
              justifyContent: "center",
              marginTop: 70
            }}>
              <p style={{ marginBottom: 20 }}>
                {modalDetails.title}
                <span style={{ fontWeight: "bold"}}>{`${modalDetails.namesOfPickedCommunities}?`}</span>
              </p>

              <div
                style={{
                  ...styles.button,
                  backgroundColor: modalDetails.style.backgroundColor,
                  color: modalDetails.style.color,
                  fontSize: 15,
                  fontWeight: "bold",
                  justifyContent: "center",
                }}
                onClick={modalDetails.onClickAction}
              >
                {modalDetails.buttonTitle}
              </div>
            </div>  :
            <div style={{
              display: "flex",
              flexDirection: "column",
              alignItems: "center"
            }}>
              <img style={{
                  marginBottom: 30,
                  width: 56,
                  height: 56
                }}
                src={ modalDetails.status === 200 ? 
                  require("../../../Assets/img/success.svg")
                  : require("../../../Assets/img/error.svg")
                }
              />
              <p>{modalDetails.title}</p>
              </div>}
          </ExtendedModal>
        </>
      )
}

export default withRouter(Community);
