import axios from "axios";
import React, { useRef, useState } from 'react';
import { withRouter } from 'react-router-dom';
import ExtendedModal from "../../../Component/ExtendedModal";
import { Api } from "../../../Const/Api";
import Header from '../../../Container/CMSHeader';



const styles = {
  container: {
    width: "100%",
    backgroundColor: "white",
    marginBottom: 15,
    padding: "33px 29px 21px"
  },
  dataArea: {
    border: ".1px solid rgba(59,107,211,.2)",
    borderRadius: 6,
    rowGap: 30,
  },
  header: {
    display: "grid",
    gridTemplateColumns: "10% 20% 25% 25% 10% 10%",
    fontSize: 16,
    fontWeight: "bold",
    padding: "38px 40px 17px",
    borderBottom: ".1px solid rgba(5, 0, 56, 0.06)",
  },
  body: {
    display: "grid",
    fontSize: 16,
    gridTemplateColumns: "10% 20% 25% 25% 10% 10%",
    padding: "20px 40px 18px",
  },
  button: {
    width: 148,
    height: 44,
    border: ".1px solid rgba(5, 0, 38, 1)",
    boxSizing: "border-box",
    display: "flex",
    alignItems: "center",
    boxShadow: "0px 2px 2px rgba(5, 0, 56, 0.08)",
    borderRadius: 6,
    cursor: "pointer",
    marginRight: 35
  },
  image: {
    width: "85%",
    marginBottom: 114
  },
  socialDivs: {
    width: 67,
    height: 50,
    padding: 2,
    marginRight: 42,
    border: "0.4px solid rgba(59, 107, 211, 0.8)",
    boxSizing: "border-box",
    borderRadius: "4px",
    display: "flex",
    flexDirection: "column"
  }
};

const ShowSingleBlog = (props) => {

  const [loading, setLoading] = useState(false);
  const [imageUploaded, setImageUploaded] = useState(false);
  const [showModal, setShowModal] = useState(false);
  const [serverResponse, setServerResponse] = useState({ status: "", response: ""});


  const defaultFormValues = {
    files: [],
    heading: "",
    link: "",
    description: ""
  }
  const [formValues, setFormValues] = useState(defaultFormValues);

  const imageElement = useRef(null);
  const fileInput = useRef(null);

  const handleChange = (e) => {
    const newImageSource = URL.createObjectURL(e.target.files[0]);
    imageElement.current.style.width = "100px";
    imageElement.current.style.height = "100px";
    imageElement.current.src = newImageSource;
    setImageUploaded(true);
    setFormValues({ ...formValues, files: [...formValues.files, e.target.files[0]]})
  }

  const handleInput = (key, value) => {
    let processedValue = value;
    if(Array.isArray(formValues[key]) && key !== "tag") {
      processedValue = [...formValues[key], value]
    }
    setFormValues({ ...formValues, [key]: processedValue });
  }

  const closeModal = () => {
    setShowModal(false);
    if(serverResponse.status === 200) {
      props.history.push("/content-manager/community")
    }
  }

  const saveChanges = async () => {
    const finalFormInput = {
      ...formValues,
    }

    setLoading(true)
    const payload = new FormData();

    if(finalFormInput.files && finalFormInput.files.length !== 0) {
        payload.append("myFiles", finalFormInput.files[0], finalFormInput.files[0].name);
    }

    for (const key in finalFormInput) {
      if (Object.hasOwnProperty.call(finalFormInput, key) && key !== "files") {
        payload.append(key, finalFormInput[key]);
      }
    }

    try {
      const result = await axios.post(
        `${Api.BaseUrl}/api/community/save`,
        payload,
        {
          'headers': { 'Authorization': `Bearer ${localStorage.getItem('token')}` }
        },
      );

      setLoading(false);

      imageElement.current.src = null;

      setServerResponse({
        status: result.status,
        response: result.status === 200 ? "Community created successfully" : "There was an error. Please try again."
      })
      setShowModal(true);
    } catch(e) {
      setServerResponse({
        status: -1,
        response: "There was an error. Please try again."
      })
      setShowModal(true);
      setLoading(false)
      console.log(e)
    }
  }

  const handleClick = () => {
    fileInput.current.click();
  }

        return (
            <>
              <Header />
              <div style={{
                marginBottom: 20,
                display: "flex",
                flexDirection: "row",
                paddingLeft: 29,
                paddingRight: 29,
                justifyContent: "space-between"
              }}>
                <div>
                  <div
                    style={{
                      ...styles.button,
                      backgroundColor: "",
                      justifyContent: "center",
                      color: "#050038",
                      border: "",
                      boxShadow: "",
                      cursor: "default"
                    }}
                  >
                  </div>
                </div>
                <div style={{ display: "flex", flexDirection: "row", justifyContent: "space-between"}}>
                  <div
                    style={{
                      ...styles.button,
                      backgroundColor: "rgba(5, 0, 56, 0.4)",
                      justifyContent: "center",
                      color: "white",
                      cursor: "not-allowed"
                    }}
                  >
                    New Upload
                  </div>
                  <div style={{
                    ...styles.button,
                    display: "flex",
                    flexDirection: "row",
                    color: "#050038",
                    paddingLeft: 12,
                    paddingRight: 5
                  }}>
                    <img style={{ marginRight: 20 }} src={require("../../../Assets/img/filter.svg")} />
                    <span style={{ marginRight: 25 }}>Filter</span>
                    <img style={{ marginTop: 2 }} src={require("../../../Assets/img/downwardArrow.svg")} />
                  </div>
                </div>
              </div>

                <section className="px-4">
                  <div style={styles.container}>
                    <div
                      style={{
                        ...styles.dataArea,
                        padding: "20px 85px",
                        display: "grid",
                        gridTemplateColumns: "80% 15%",
                        rowGap: "5%"
                      }}
                    >
                      <div style={styles.leftContent}>
                        <div
                          style={{
                            ...styles.image,
                            height: 255,
                            width: 294,
                            border: "0.1px solid rgba(59, 107, 211, 0.5)",
                            borderRadius: 6,
                            display: "flex",
                            flexDirection: "column",
                            justifyContent: !imageUploaded && "center",
                            alignItems: "center",
                          }}
                        >
                            <div
                              style={{
                                background: "rgba(207, 207, 207, 0.2)",
                                borderRadius: 6,
                                display: "flex",
                                border: "1px dashed #3B6BD3",
                                width: 167,
                                height: 52,
                                justifyContent: "center",
                                alignItems: "center",
                                marginBottom: imageUploaded && 57,
                                cursor: "pointer",
                              }}
                              onClick={handleClick}
                            >
                              <span style={{ marginRight: 15}}><img src={require("../../../Assets/img/upload.svg")} /></span>{imageUploaded ? "Replace logo" : "Upload logo" }
                            </div>
                          <img
                            ref={imageElement}
                          />
                          <input accept="image/*" type="file" style={{ display: "none" }} ref={fileInput} onChange={handleChange} />
                        </div>
                        <div style={{ fontSize: 16, marginBottom: 70 }}>
                          <div>Heading</div>
                          <input
                            type="text"
                            value={formValues.heading}
                            onChange={(e) => handleInput("heading", e.target.value)}
                            style={{
                              width: "85%",
                              border: "0.1px solid rgba(59, 107, 211, 0.5)",
                              outline: "none",
                              borderRadius: "4px",
                              padding: 10,
                              color: "#050038",
                              fontWeight: 700
                            }}
                          />
                        </div>
                        <div style={{ fontSize: 16, marginBottom: 70 }}>
                          <div>Insert link</div>
                          <input
                            type="text"
                            value={formValues.link}
                            onChange={(e) => handleInput("link", e.target.value)}
                            style={{
                              width: "85%",
                              border: "0.1px solid rgba(59, 107, 211, 0.5)",
                              outline: "none",
                              borderRadius: "4px",
                              padding: 10,
                              color: "#050038",
                              fontWeight: 700
                            }}
                          />
                        </div>
                        <div style={{ fontSize: 16, marginBottom: 70 }}>
                          <div>Description</div>
                          <textarea
                            value={formValues.description}
                            onChange={(e) => handleInput("description", e.target.value)}
                            style={{
                              width: "85%",
                              height: 280,
                              border: "0.1px solid rgba(59, 107, 211, 0.5)",
                              outline: "none",
                              borderRadius: "4px",
                              padding: 10,
                              color: "#050038",
                              fontWeight: 700,
                              resize: "none"
                            }}
                          />
                        </div>
                          <div
                            style={{
                              ...styles.button,
                              color: "white",
                              justifyContent: "center",
                              marginBottom: 50,
                              fontSize: 15,
                              width: 184,
                              backgroundColor: loading ? "rgba(5, 0, 56, 0.4)" : "#050038",
                              border: loading ? "" : ".1px solid rgba(5, 0, 56, 0.06)",
                              cursor: loading ? "not-allowed" : "pointer",
                            }}
                            onClick={() => saveChanges()}
                          >
                            Save new community {loading && (
                            <div className="spinner-border spinner-border-white spinner-border-sm ml-2"></div>
                          )}
                          </div>
                      </div>
                      <div style={styles.rightContent}>
                      </div>
                    </div>
                  </div>
                </section>
                <ExtendedModal showModal={showModal} closeModal={closeModal}>
                  <div style={{
                    display: "flex",
                    flexDirection: "column",
                    alignItems: "center"
                  }}>
                    <img style={{
                        marginBottom: 30,
                        width: 56,
                        height: 56
                      }}
                      src={ serverResponse.status === 200 ? 
                        require("../../../Assets/img/success.svg")
                        : require("../../../Assets/img/error.svg")
                      }
                    />
                    <p>{serverResponse.response}</p>
                  </div>  
                </ExtendedModal>
            </>
        )
}

export default withRouter(ShowSingleBlog)
