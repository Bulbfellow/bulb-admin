import Axios from 'axios';
import React, { useEffect, useRef, useState } from 'react';
import { withRouter } from 'react-router-dom';
import ExtendedModal from '../../../Component/ExtendedModal';
import { Api } from '../../../Const/Api';
import Header from '../../../Container/CMSHeader';

const styles = {
  container: {
    width: "100%",
    backgroundColor: "white",
    marginBottom: 15,
    padding: "33px 29px 21px"
  },
  dataArea: {
    border: ".1px solid rgba(59,107,211,.2)",
    borderRadius: 6,
    rowGap: 30,
    marginBottom: 50
  },
  header: {
    fontSize: 16,
    fontWeight: "bold",
    padding: "38px 40px 17px",
    borderBottom: ".1px solid rgba(5, 0, 56, 0.06)",
  },
  body: {
    display: "grid",
    fontSize: 16,
    gridTemplateColumns: "10% 20% 25% 25% 10% 10%",
    padding: "20px 40px 18px",
    cursor: "pointer"
  },
  button: {
    width: 148,
    height: 44,
    border: ".1px solid rgba(59,107,211,.2)",
    boxSizing: "border-box",
    display: "flex",
    alignItems: "center",
    color: "white",
    boxShadow: "0px 2px 2px rgba(5, 0, 56, 0.08)",
    borderRadius: 6,
    cursor: "pointer",
    marginRight: 35
  },
  ellipsisOptions: {
    paddingTop: 10,
    paddingLeft: 25,
    paddingBottom: 10,
    cursor: "pointer"
  }
};
const Gallery = (props) => {
    const [openFilter, setOpenFilter] = useState(false);
    const [viewMode, setViewMode] = useState("active");
    const [hoveredPicture, setHoveredPicture] = useState(false);
    const [pickedPictures, setPickedPictures] = useState([]);
    const [totalPictures, setTotalPictures] = useState([]);
    const [showedPictures, setShowedPictures] = useState([]);
    const [showModal, setShowModal] = useState(false);
    const [serverResponse, setServerResponse] = useState({ status: "", response: ""});
    const [loading, setLoading] = useState(false);

    const newUpload = useRef(null)

    const handlePictureHover = (pictureOwner) => {
      setHoveredPicture(pictureOwner);
    }

    const triggerNewUpload = () => {
      newUpload.current.click()
    }

    const closeModal = () => {
      setShowModal(false)
    }

    const handleImageUpload = async (file) => {
      setLoading(true)
      const payload = new FormData();

      payload.append("myFiles", file, file.name);

      try {
        const result = await Axios.post(
          `${Api.BaseUrl}/api/gallery/save`,
          payload,
          {
            'headers': { 'Authorization': `Bearer ${localStorage.getItem('token')}` }
          },
        );

        setServerResponse({
          status: result.status,
          response: result.status === 200 ? "Picture uploaded successfully" : "There was an error. Please try again."
        })
        setShowModal(true);
  
        setLoading(false);
  
      } catch(e) {
        setServerResponse({
          status: -1,
          response: "There was an error. Please try again."
        })
        setShowModal(true);
        setLoading(false)
        console.log(e)
      }

    }
  
    const toggleOpenFilter = (e) => {
      if(e.target.id === "filterArrow") {
        setOpenFilter(!openFilter)
      } else {
        if(openFilter) setOpenFilter(false)
      }
    }

    const archivePicture = async () => {
      const url = viewMode === "active" ?
        `${Api.BaseUrl}/api/gallery/archive/`
        : `${Api.BaseUrl}/api/gallery/unarchive/`;

      try {
        const result = await Axios.patch(
          url,
          {ids: pickedPictures},
          {
            'headers': { 'Authorization': `Bearer ${localStorage.getItem('token')}` }
          },
        );

        setServerResponse({
          status: result.status,
          response: result.status === 200 ? "Changes saved successfully" : "There was an error. Please try again."
        })
        setShowModal(true);
        setPickedPictures([]);
        setLoading(false);

      }catch(e) {
        setServerResponse({
          status: -1,
          response: "There was an error. Please try again."
        })
        setShowModal(true);
        setPickedPictures([]);
        setLoading(false)
        console.log(e)
      }
    }

    const pickPicture = (id, status) => {
      setPickedPictures([...pickedPictures, id]);
      setTotalPictures(totalPictures.map(picture => {
        if(picture.id === id) {
          picture.status = status === "active" ? "archived" : "active";
        }
        return picture;
      }));
      setShowedPictures(showedPictures.filter(picture => picture.id !== id));
    }

    const toggleArchive = () => {
      if(viewMode === "active") {
        setShowedPictures(totalPictures.filter(picture => picture.status !== "active"));
        setViewMode("archived");
      }else {
        setShowedPictures(totalPictures.filter(picture => picture.status === "active"));
        setViewMode("active");
      }
      setOpenFilter(false)
    }

    useEffect(() =>{
      const getAllPictures = async () => {
        try {
          const result = await Axios.get(
            `${Api.BaseUrl}/api/gallery/allPictures`,
            {
              'headers': { 'Authorization': `Bearer ${localStorage.getItem('token')}` }
            },
          );

          if(result.status === 200) {
            setTotalPictures(result.data.payload.message);
            setShowedPictures(result.data.payload.message.filter(post => post.status === "active"));
          }
        } catch(e) {
          console.log(e)
        }
      };

      getAllPictures();
    }, [props, showModal]);

      return (
        <>
          <Header />
          <div style={{
            marginBottom: 20,
            paddingLeft: 29,
            paddingRight: 29,
            display: "flex",
            flexDirection: "row",
            justifyContent: "space-between"
          }}>
            <div>
              
            </div>
            <div style={{ display: "flex", flexDirection: "row", justifyContent: "space-between"}}>
              <div
                style={{
                  ...styles.button,
                  backgroundColor: "#050038",
                  justifyContent: "center",
                }}
                onClick={triggerNewUpload}
              >
                New Upload
              </div>
              <div
                style={{
                  position: "relative"
                }}
              >
                <div style={{
                  ...styles.button,
                  display: "flex",
                  flexDirection: "row",
                  color: "#050038",
                  background: "white",
                  paddingLeft: 12,
                  paddingRight: 5,
                  borderBottomLeftRadius: openFilter ? 0 : 6,
                  borderBottomRightRadius: openFilter ? 0 : 6
                }}>
                  <img style={{ marginRight: 20 }} src={require("../../../Assets/img/filter.svg")} />
                  <span style={{ marginRight: 25 }}>Filter</span>
                  <img
                    onClick={(e) => toggleOpenFilter(e, "arrow")}
                    style={{ marginTop: 2 }}
                    id="filterArrow"
                    src={require("../../../Assets/img/downwardArrow.svg")}
                  />
                </div>
                {openFilter && <div style={{
                  position: 'absolute',
                  width: 148,
                  height: 100,
                  color: "#050038",
                  fontSize: 14,
                  paddingLeft: 37,
                  paddingTop: 5,
                  background: "white",
                  zIndex: 100,
                  marginTop: -1,
                  borderLeft: ".1px solid rgba(59,107,211,.2)",
                  borderRight: ".1px solid rgba(59,107,211,.2)",
                  borderBottom: ".1px solid rgba(59,107,211,.2)",
                  borderBottomLeftRadius: 6,
                  borderBottomRightRadius: 6
                }}>
                  <div style={{ marginBottom: 25, cursor: "pointer"}}>Date</div>
                  <div style={{ cursor: "pointer" }} onClick={toggleArchive}>
                    {viewMode === "active" ? 'Archived' : 'Unarchived' }
                  </div>
                </div>}
              </div>
            </div>
          </div>

          <section className="px-4">
            <div style={styles.container}>
              <div style={styles.dataArea}>
                <div style={styles.header}>
                  <input type="file" style={{ display: "none"}} ref={newUpload} onChange={(e) => handleImageUpload(e.target.files[0])} accept="image/*" />
                  {showedPictures.length > 0 ? <div
                    style={{
                      marginBottom: 50,
                      display: "grid",
                      gridTemplateColumns: "30% 30% 30%",
                      columnGap: "5%",
                      rowGap: 50
                      }}
                    >
                    {showedPictures.map(image => 
                      <div 
                        style={{
                          position: "relative",
                        }}
                        onMouseEnter={() => setHoveredPicture(image)}
                        onMouseLeave={() => setHoveredPicture("")}
                      >
                        <img src={image.picture} style={{ width: "100%", height: 300, maxHeight: 300 }} />
                        {hoveredPicture === image && <div style={{
                          width: 300,
                          height: 300,
                          backgroundColor: "rgba(0, 0, 0, .2)",
                          position: "absolute",
                          display: "flex",
                          color: "#fff",
                          flexDirection: "column",
                          justifyContent: "center",
                          padding: 15,
                          top: 0,
                          left: 0
                          }}
                        >
                            <div
                              style={{
                                display: "flex",
                                justifyContent: "center",
                                alignItems: "center"
                              }}
                            >
                              <div
                                style={{
                                  width: 182,
                                  height: 52,
                                  background: "white",
                                  borderRadius: 6,
                                  border: ".1px solid rgba(59,107,211,.2)",
                                  color: "#050038",
                                  fontSize: 15,
                                  fontWeight: "bold",
                                  display: "flex",
                                  justifyContent: "center",
                                  alignItems: "center",
                                  cursor: "pointer"
                                }}
                                onClick={() => pickPicture(image.id, image.status)}
                              >
                                {image.status !== "active" ? "Unarchive picture" : "Archive picture"}
                              </div>
                            </div>
                        </div>}
                      </div>
                    )}
                  </div> : (
                      <div
                        style={{
                          textAlign: "center",
                          fontSize: 16,
                          color: "#050038",
                          marginBottom: 30,
                          marginTop: pickedPictures.length > 0 ? 150 : 30
                        }}
                      >
                        {`There are no ${viewMode === "archived" ? "archived" : "unarchived"} pictures here yet. Check back later.`}
                      </div>
                    )}
                    {pickedPictures.length > 0 &&
                      <div
                      style={{
                        ...styles.button,
                        backgroundColor: "#050038",
                        justifyContent: "center",
                        marginTop: showedPictures.length === 0 && 150,
                      }}
                      onClick={archivePicture}
                    >
                      Save changes
                    </div>
                    }
                  </div>
                </div>
              </div>
          </section>
          <ExtendedModal showModal={showModal} closeModal={closeModal}>
            <div style={{
              display: "flex",
              flexDirection: "column",
              alignItems: "center"
            }}>
              <img style={{
                  marginBottom: 30,
                  width: 56,
                  height: 56
                }}
                src={ serverResponse.status === 200 ? 
                  require("../../../Assets/img/success.svg")
                  : require("../../../Assets/img/error.svg")
                }
              />
              <p>{serverResponse.response}</p>
            </div>  
          </ExtendedModal>
        </>
      )
}

export default withRouter(Gallery);
