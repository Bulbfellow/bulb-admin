import Axios from 'axios';
import React, { useEffect, useState } from 'react';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import { Api } from '../../../Const/Api';
import Header from '../../../Container/CMSHeader';

const styles = {
  container: {
    width: "100%",
    backgroundColor: "white",
    marginBottom: 15,
    padding: "33px 29px 21px"
  },
  dataArea: {
    border: ".1px solid rgba(59,107,211,.2)",
    borderRadius: 6,
    rowGap: 30,
  },
  header: {
    display: "grid",
    gridTemplateColumns: "10% 20% 30% 30% 5% 5%",
    fontSize: 16,
    fontWeight: "bold",
    padding: "38px 40px 17px",
    borderBottom: ".1px solid rgba(5, 0, 56, 0.06)",
  },
  body: {
    display: "grid",
    fontSize: 16,
    gridTemplateColumns: "10% 20% 30% 30% 5% 5%",
    padding: "20px 40px 18px",
    cursor: "pointer"
  },
  button: {
    width: 148,
    height: 44,
    border: ".1px solid rgba(59,107,211,.2)",
    boxSizing: "border-box",
    display: "flex",
    alignItems: "center",
    color: "white",
    boxShadow: "0px 2px 2px rgba(5, 0, 56, 0.08)",
    borderRadius: 6,
    cursor: "pointer",
    marginRight: 35
  },
  ellipsisOptions: {
    paddingTop: 10,
    paddingLeft: 25,
    paddingBottom: 10,
    cursor: "pointer"
  }
};
const Press = (props) => {
    const [openFilter, setOpenFilter] = useState(false);
    const [viewMode, setViewMode] = useState("live");
    const [selectedPress, setSelectedPress] = useState([]);
    const [archivedPressIds, setArchivedPressIds] = useState([]);
    const [showedPress, setShowedPress] = useState([]);
    const [totalPress, setTotalPress] = useState([]);
    const [openEllipsisOptions, setOpenEllipsisOptions] = useState(null);

    const openEllipsis = (index) => {
      setOpenEllipsisOptions({ ...openEllipsisOptions, [index]: !openEllipsisOptions[index] })
    }

    const handleSelectPress = (pressId) => {
      if(selectedPress.includes(pressId)) {
        setSelectedPress(selectedPress.filter(press => press !== pressId && press !== "Select all"))
      }else {
        setSelectedPress([...selectedPress, pressId]);
      }
    }

    const switchViewMode = () => {
      if(viewMode === "live") {
        setShowedPress(totalPress.filter(post => archivedPressIds.includes(post.id)));
        setViewMode("archived")
      }else {
        setShowedPress(totalPress.filter(post => !archivedPressIds.includes(post.id)));
        setViewMode("live")
      }
      setOpenFilter(false)
    }

    const handleSelectAll = () => {
      if(selectedPress[0] === "Select all") {
        setSelectedPress([]);
      }else {
        setSelectedPress(["Select all", ...totalPress.map(post => post.id)])
      }
    }

    const toggleArchive = async (ids) => {
      const url = viewMode === "live" ?
        `${Api.BaseUrl}/api/press/archivePress/`
        : `${Api.BaseUrl}/api/press/unArchivePress/`;

      try{
        const result = await Axios.post(
          url,
          { ids: ids[0] === 'Select all' ? ids.slice(1) : ids },
          {
            'headers': { 'Authorization': `Bearer ${localStorage.getItem('token')}` }
          },
        );
        if(result.status === 200) {
          let totalArchivedIds;
          if(viewMode === "live") {
            totalArchivedIds = [...archivedPressIds, ...ids]
            setArchivedPressIds(totalArchivedIds);
            setShowedPress(totalPress.filter(post => !totalArchivedIds.includes(post.id)));
          } else {
            totalArchivedIds = archivedPressIds.filter(id => !ids.includes(id))
            setArchivedPressIds(totalArchivedIds);
            setShowedPress(totalPress.filter(post => totalArchivedIds.includes(post.id)));
          }
          setOpenEllipsisOptions(null);
          setSelectedPress([]);
        }    
      }catch(e) {
        console.log(e)
      }
    }

    const toggleOpenFilter = (e) => {
      if(e.target.id === "filterArrow" || e.target.id === "filterArrow-img") {
        setOpenFilter(!openFilter)
      } else {
        if(openFilter) setOpenFilter(false)
      }
    }

    useEffect(() =>{
      const getAllPress = async () => {
        try {
          const result = await Axios.get(
            `${Api.BaseUrl}/api/press/all`,
            {
              'headers': { 'Authorization': localStorage.getItem('token') }
            },
          );

          if(result.status === 200) {
            setTotalPress(result.data.payload.message);
            setShowedPress(result.data.payload.message.filter(press => press.status === "live"))
            setArchivedPressIds(result.data.payload.message
              .filter(press => press.status === "archived")
              .map(press => press.id))
          }
        } catch(e) {
          console.log(e)
        }
      };

      getAllPress();
    }, [props]);

      return (
        <>
          <Header />
          <div style={{
            marginBottom: 20,
            paddingLeft: 29,
            paddingRight: 29,
            display: "flex",
            flexDirection: "row",
            justifyContent: "space-between"
          }}>
            <div>
              <div
                style={{
                  ...styles.button,
                  justifyContent: "center",
                  color: "#050038",
                  backgroundColor: selectedPress.length > 0 ? "white" : "",
                  border: selectedPress.length > 0 ? "0.2px solid #3B6BD3" : "",
                  cursor: selectedPress.length < 1 ? "default" : "pointer",
                  boxShadow: selectedPress.length < 1 && "" 
                }}
                onClick={() => toggleArchive(selectedPress)}
              >
                {selectedPress.length > 0 && (viewMode === "live" ? "Archive" : "Unarchive")}
              </div>
            </div>
            <div style={{ display: "flex", flexDirection: "row", justifyContent: "space-between"}}>
              <div
                style={{
                  ...styles.button,
                  backgroundColor: "#050038",
                  justifyContent: "center",
                }}
                onClick={() => props.history.push('/content-manager/press/create')}
              >
                New Post
              </div>
              <div
                style={{
                  position: "relative"
                }}
              >
                <div 
                  id="filterArrow"
                  style = {{
                    ...styles.button,
                    display: "flex",
                    flexDirection: "row",
                    color: "#050038",
                    background: "white",
                    paddingLeft: 12,
                    paddingRight: 5,
                    borderBottomLeftRadius: openFilter ? 0 : 6,
                    borderBottomRightRadius: openFilter ? 0 : 6,
                    border: ".1px solid rgba(59,107,211,.2)",
                    boxSizing: "border-box",
                    boxShadow: "0px 2px 2px rgba(5, 0, 56, 0.08)",
                    borderRadius: 6,
                    cursor: "pointer",
                  }}
                  onClick={(e) => toggleOpenFilter(e)}
                >
                  <img style={{ marginRight: 20 }} src={require("../../../Assets/img/filter.svg")} />
                  <span style={{ marginRight: 25 }}>Filter</span>
                  <img
                    id="filterArrow-img"
                    style={{ marginTop: 2 }}
                    src={require("../../../Assets/img/downwardArrow.svg")}
                  />
                </div>
                {openFilter && <div style={{
                  position: 'absolute',
                  width: 148,
                  height: 100,
                  color: "#050038",
                  fontSize: 14,
                  paddingLeft: 37,
                  paddingTop: 5,
                  background: "white",
                  zIndex: 100,
                  marginTop: -1,
                  borderLeft: ".1px solid rgba(59,107,211,.2)",
                  borderRight: ".1px solid rgba(59,107,211,.2)",
                  borderBottom: ".1px solid rgba(59,107,211,.2)",
                  borderBottomLeftRadius: 6,
                  borderBottomRightRadius: 6
                }}>
                  <div style={{ marginBottom: 25, cursor: "pointer"}}>Date</div>
                  <div style={{ cursor: "pointer" }} onClick={switchViewMode}>
                    {viewMode === "live" ? "Archived" : "Unarchived"}
                  </div>
                </div>}
              </div>
            </div>
          </div>

          <section className="px-4">
            <div style={styles.container}>
              <div style={styles.dataArea}>
                <div style={styles.header}>
                  <div>
                    <input
                      type="checkbox"
                      style={{ transform: "scale(1.5)", cursor: "pointer" }}
                      checked={selectedPress.includes("Select all")}
                      onChange={() => handleSelectAll()}
                    />
                  </div>
                  <div style={{fontWeight: "bold"}}>Date Posted</div>
                  <div style={{ fontWeight: "bold" }}>Heading</div>
                  <div style={{ fontWeight: "bold" }}>Content</div>
                  <div></div>
                  <div></div>
                </div>
                {showedPress.length > 0 ?  showedPress.map(press =>
                  <div style={styles.body}>
                    <div>
                      <input
                        type="checkbox"
                        style={{ transform: "scale(1.5)", cursor: "pointer" }}
                        checked={selectedPress.includes(press.id) || selectedPress[0] === "Select all"}
                        onChange={() => handleSelectPress(press.id)}
                      />
                    </div>
                    <div
                      onClick={() => props.history.push('/content-manager/press/single', {...press})}
                    >
                      {
                        new Date(press.created_at)
                          .toLocaleString("en-GB", { year: "numeric", month: "numeric", day: "numeric"})
                          .replaceAll("/", "-",)
                      }
                    </div>
                    <div
                      dangerouslySetInnerHTML={{ __html: press.heading }}
                      onClick={() => props.history.push('/content-manager/press/single', {...press})}
                    />
                    <div
                      dangerouslySetInnerHTML={{ __html: press.content }}
                      onClick={() => props.history.push('/content-manager/press/single', {...press})}
                    />
                    <div style={{ position: "relative"}}>
                      <img
                        onClick={() => openEllipsis(press.id)}
                        src={require("../../../Assets/img/verticalEllipsis.svg")}
                        style={{ cursor: "pointer"}}
                      />
                      {openEllipsisOptions === press.id && <div
                        style={{
                          position: "absolute",
                          width: 105,
                          border: ".1px solid rgba(59,107,211,.2)",
                          marginLeft: -50,
                          backgroundColor: "white",
                          borderRadius: 3,
                          marginTop: 3,
                          zIndex: 700
                        }}
                      >
                        <div
                          style={styles.ellipsisOptions}
                          onMouseEnter={() => {}}
                        >
                          View
                        </div>
                        <div style={styles.ellipsisOptions}>Export</div>
                        <div
                          onClick={() => toggleArchive(press.id)}
                          style={styles.ellipsisOptions}
                        >
                          {archivedPressIds.includes(press.id) ? "Unarchive" : "Archive"}
                        </div>
                      </div>}
                    </div>
                  </div>
                ) : (
                  <div
                    style={{
                      textAlign: "center",
                      fontSize: 16,
                      color: "#050038",
                      marginBottom: 30,
                      marginTop: 30
                    }}
                  >
                    {`There are no ${viewMode === "archived" ? "archived" : "unarchived"} press articles yet. Check back later.`}
                  </div>
                )}
              </div>
            </div>
          </section>
        </>
      )
}
const mapStateToProps = state => {
    return {

    }
}

const mapDispatchToProps = (dispatch) => {
    return {
    }
}
export default withRouter(connect(mapStateToProps, mapDispatchToProps)(Press));
