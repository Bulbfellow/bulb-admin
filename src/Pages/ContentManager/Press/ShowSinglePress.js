import axios from 'axios';
import React, { useCallback, useEffect, useRef, useState } from 'react';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import makeAnimated from "react-select/animated";
import Select from "react-select/creatable";
import Dropzone from '../../../Component/Dropzone';
import ExtendedModal from '../../../Component/ExtendedModal';
import TextEditor from '../../../Component/TextEditor';
import { Api } from '../../../Const/Api';
import Header from '../../../Container/CMSHeader';



const styles = {
  container: {
    width: "100%",
    backgroundColor: "white",
    marginBottom: 15,
    padding: "33px 29px 21px"
  },
  dataArea: {
    border: ".1px solid rgba(59,107,211,.2)",
    borderRadius: 6,
    rowGap: 30,
  },
  header: {
    display: "grid",
    gridTemplateColumns: "10% 20% 25% 25% 10% 10%",
    fontSize: 16,
    fontWeight: "bold",
    padding: "38px 40px 17px",
    borderBottom: ".1px solid rgba(5, 0, 56, 0.06)",
  },
  body: {
    display: "grid",
    fontSize: 16,
    gridTemplateColumns: "10% 20% 25% 25% 10% 10%",
    padding: "20px 40px 18px",
  },
  button: {
    width: 148,
    height: 44,
    border: ".1px solid rgba(5, 0, 38, 1)",
    boxSizing: "border-box",
    display: "flex",
    alignItems: "center",
    boxShadow: "0px 2px 2px rgba(5, 0, 56, 0.08)",
    borderRadius: 6,
    cursor: "pointer",
    marginRight: 35
  },
  image: {
    width: "85%",
    marginBottom: 114
  },
  socialDivs: {
    width: 67,
    height: 50,
    padding: 2,
    marginRight: 42,
    border: "0.4px solid rgba(59, 107, 211, 0.8)",
    boxSizing: "border-box",
    borderRadius: "4px",
    display: "flex",
    flexDirection: "column"
  }
};

const animatedComponents = makeAnimated();

const ShowSinglePress = (props) => {
  const pressData = { ...props.location.state}

  const defaultFormValues = {
    files: [],
    twitter_url: pressData.twitter_url.length > 0,
    linkedin_url: pressData.linkedin_url.length > 0,
    instagram_url: pressData.instagram_url.length > 0,
    inner_link: pressData.inner_link,
    tag: pressData.tag.split(",").map(tag => ({ label: tag, value: tag })),
    heading: pressData.heading,
    content: pressData.content
  }
  
  const [editMode, setEditMode] = useState(false)
  const [loading, setLoading] = useState(false)
  const [imageUploaded, setImageUploaded] = useState(false);
  const [likes, setLikes] = useState(0);
  const imageElement = useRef(null);
  const [formValues, setFormValues] = useState(defaultFormValues);
  const [serverResponse, setServerResponse] = useState({ status: "", response: ""});
  const [showModal, setShowModal] = useState(false);




  const onDrop = useCallback(acceptedFiles => {
    handleImageUpload(acceptedFiles[0])
  }, []);

  const closeModal = () => {
    setShowModal(false)
    if(serverResponse.status === 200) {
      props.history.push("/content-manager/press")
    }
  }

  const handleInput = (key, value) => {
    let processedValue = value;
    if(Array.isArray(formValues[key]) && key !== "tag") {
      processedValue = [...formValues[key], value]
    }
    setFormValues({ ...formValues, [key]: processedValue });
  }

  const handleImageUpload = (file) => {
    const newImageSource = URL.createObjectURL(file);
    imageElement.current.src = newImageSource;
    setImageUploaded(true);
    setFormValues({ ...formValues, files: [...formValues.files, file]})
  }

  useEffect(() => {
    document.addEventListener("resize", () => {
      document.querySelector("#dropzone").style.height = `${imageElement.current.height}px`;
    });

    return () => {
      document.removeEventListener("resize", () => {
        document.querySelector("#dropzone").style.height = `${imageElement.current.height}px`;
      });
    }
  }, []);

  useEffect(() =>{
    const getPressLikes = async () => {
      let result;
      try {
        result = await axios.post(
          `${Api.BaseUrl}/api/like/article`,
          { id: pressData.id, section: "press" },
          {
            'headers': { 'Authorization': localStorage.getItem('token') }
          },
        );

        if(result.status === 200) {
          setLikes(result.data.payload.message.total_likes);
        }
      } catch(e) {
        console.log(e)
      }
    };

    getPressLikes();
  }, [props]);

  const editPress = async () => {
    const finalFormInput = {
      ...formValues,
      instagram_url: formValues.instagram_url ? "https://www.instagram.com/thebulb.africa/" : "",
      twitter_url: formValues.twitter_url ? "https://www.twitter.com/thebulbafrica/" : "",
      linkedin_url: formValues.linkedin_url ? "https://www.linkedin.com/the-bulb-africa/" : "",
      tag: formValues.tag.map(tagObject => tagObject.value)
    }

    setLoading(true)
    const payload = new FormData();

    if(finalFormInput.files && finalFormInput.files.length !== 0) {
        payload.append("myFiles", finalFormInput.files[0], finalFormInput.files[0].name);
    }

    for (const key in finalFormInput) {
      if (Object.hasOwnProperty.call(finalFormInput, key) && key !== "files") {
        payload.append(key, finalFormInput[key]);
      }
    }

    try {
      const result = await axios.patch(
        `${Api.BaseUrl}/api/press/update/${pressData.id}`,
        payload,
        {
          'headers': { 'Authorization': `Bearer ${localStorage.getItem('token')}` }
        },
      );

      setLoading(false);

      imageElement.current.src = null;

      setServerResponse({
        status: result.status,
        response: result.status === 200 ? "Artice edited successfully" : "There was an error. Please try again."
      })
      setShowModal(true);

    } catch(e) {
      setServerResponse({
        status: -1,
        response: "There was an error. Please try again."
      })
      setShowModal(true);
      setLoading(false)
      console.log(e)
    }
  }

  const adjustDropzone = () =>  {
    document.querySelector("#dropzone").style.height = `${imageElement.current.height}px`;
  }

        return (
            <>
              <Header />
              <div style={{
                marginBottom: 20,
                display: "flex",
                flexDirection: "row",
                paddingLeft: 29,
                paddingRight: 29,
                justifyContent: "space-between"
              }}>
                <div>
                  <div
                    style={{
                      ...styles.button,
                      backgroundColor: "",
                      justifyContent: "center",
                      color: "#050038",
                      border: "",
                      boxShadow: "",
                      cursor: "default"
                    }}
                  >
                  </div>
                </div>
                <div style={{ display: "flex", flexDirection: "row", justifyContent: "space-between"}}>
                  <div
                    style={{
                      ...styles.button,
                      backgroundColor: "#050038",
                      justifyContent: "center",
                      color: "white"
                    }}
                    onClick={() => props.history.push('/content-manager/press/create')}
                  >
                    New Post
                  </div>
                  <div style={{
                    ...styles.button,
                    display: "flex",
                    flexDirection: "row",
                    color: "#050038",
                    paddingLeft: 12,
                    paddingRight: 5
                  }}>
                    <img style={{ marginRight: 20 }} src={require("../../../Assets/img/filter.svg")} />
                    <span style={{ marginRight: 25 }}>Filter</span>
                    <img style={{ marginTop: 2 }} src={require("../../../Assets/img/downwardArrow.svg")} />
                  </div>
                </div>
              </div>

                <section className="px-4">
                  <div style={styles.container}>
                    <div
                      style={{
                        ...styles.dataArea,
                        padding: "20px 85px",
                        display: "grid",
                        gridTemplateColumns: "80% 15%",
                        rowGap: "5%"
                      }}
                    >
                      <div style={styles.leftContent}>
                        <div id="dropzone"
                          style={{
                            ...styles.image,
                            height: 406,
                            border: "0.4px solid rgba(59, 107, 211, 0.8)",
                            borderRadius: 6,
                            position: "relative"
                          }}
                        >
                          <img
                            src={`${pressData.picture?.slice(2, -2)}`}
                            ref={imageElement}
                            style={{ width: "100%", height: "auto", maxHeight: 406}}
                            onLoad={() => adjustDropzone()}
                          />
                          {editMode && <Dropzone onDrop={onDrop} accept="image/*" style={{
                              display: "flex",
                              justifyContent: "center",
                              alignItems: "center",
                              position: "absolute",
                              top: 0,
                              left: 0,
                              width: "100%",
                              height: "100%"
                            }}>
                              <div
                                style={{
                                  background: "#FFFFFF",
                                  border: "1px dashed #3B6BD3",
                                  boxSizing: "border-box",
                                  borderRadius: 6,
                                  display: "flex",
                                  width: 182,
                                  height: 52,
                                  justifyContent: "center",
                                  alignItems: "center",
                                  cursor: "pointer",
                                }}
                              >
                                Replace picture
                              </div>
                          </Dropzone>}
                        </div>
                        <div style={{ display: "flex", flexDirection: "row", marginBottom: 70 }}>
                          <div style={styles.socialDivs}>
                            <div
                              style={{
                                display: "flex",
                                flexDirection: "row",
                                marginBottom: 3,
                                justifyContent: "flex-end"
                              }}
                            >
                              <input
                                type="checkbox"
                                checked={formValues.twitter_url}
                                onChange={() => handleInput("twitter_url", !formValues.twitter_url)}
                              />
                            </div>
                            <div
                              style={{
                                display: "flex",
                                flexDirection: "row",
                                justifyContent: "center"
                              }}
                            >
                              <img
                                src={require("../../../Assets/img/twitter.svg")}
                                style={{ width: 20, height: 16}}
                              />
                            </div>
                          </div>
                          <div style={styles.socialDivs}>
                            <div
                              style={{
                                display: "flex",
                                flexDirection: "row",
                                marginBottom: 3,
                                justifyContent: "flex-end"
                              }}
                            >
                              <input
                                type="checkbox"
                                checked={formValues.twitter_url}
                                onChange={() => handleInput("instagram_url", !formValues.instagram_url)}
                              />
                            </div>
                            <div
                              style={{
                                display: "flex",
                                flexDirection: "row",
                                justifyContent: "center"
                              }}
                            >
                              <img
                                src={require("../../../Assets/img/instagram.svg")}
                                style={{ width: 20, height: 16}}
                              />
                            </div>
                          </div>
                          <div style={styles.socialDivs}>
                            <div
                              style={{
                                display: "flex",
                                flexDirection: "row",
                                marginBottom: 3,
                                justifyContent: "flex-end"
                              }}
                            >
                              <input
                                type="checkbox"
                                checked={formValues.twitter_url}
                                onChange={() => handleInput("linkedin_url", !formValues.linkedin_url)}
                              />
                            </div>
                            <div
                              style={{
                                display: "flex",
                                flexDirection: "row",
                                justifyContent: "center"
                              }}
                            >
                              <img
                                src={require("../../../Assets/img/linkedin.svg")}
                                style={{ width: 20, height: 16}}
                              />
                            </div>
                          </div>
                        </div>
                        <div style={{ fontSize: 16, marginBottom: 70 }}>
                          <div>Written by</div>
                          <input
                            type="text"
                            disabled
                            value={pressData.User[0].username}
                            style={{
                              width: "85%",
                              background: editMode ? "" : "rgba(207, 207, 207, 0.2)",
                              borderRadius: "4px",
                              padding: 10,
                              color: "#050038",
                              fontWeight: 700
                            }}
                          />
                        </div>
                        <div style={{ fontSize: 16, marginBottom: 70 }}>
                          <div>Insert link</div>
                          <input
                            type="text"
                            disabled={editMode ? false : true}
                            value={formValues.inner_link}
                            onChange={(e) => handleInput("inner_link", e.target.value)}
                            style={{
                              width: "85%",
                              background: editMode ? "" : "rgba(207, 207, 207, 0.2)",
                              borderRadius: "4px",
                              padding: 10,
                              color: "#050038",
                              fontWeight: 700
                            }}
                          />
                        </div>
                        <div style={{ fontSize: 16, width: "85%", marginBottom: 70 }}>
                          <div>Content tag</div>
                          <Select
                            styles={{
                              width: "100%",
                              background: editMode ? "" : "rgba(207, 207, 207, 0.2)",
                              borderRadius: "4px",
                              padding: 10,
                              color: "#050038",
                              fontWeight: 700
                            }}
                            isMulti
                            placeholder="Type tags and separate them with tabs"
                            closeMenuOnSelect={true}
                            defaultValue={formValues.tag}
                            isDisabled={editMode ? false : true}
                            components={animatedComponents}
                            name="tags"
                            onChange={(e) => handleInput("tag", e )}
                          />
                        </div>
                        <div style={{ fontSize: 16, marginBottom: 70 }}>
                          <div>Likes</div>
                          <input
                            type="text"
                            disabled={editMode}
                            value={likes}
                            style={{
                              width: "85%",
                              background: "rgba(207, 207, 207, 0.2)",
                              borderRadius: "4px",
                              padding: 10,
                              color: "#050038",
                              fontWeight: 700
                            }}
                          />
                        </div>
                        <div style={{ width: "85%", marginBottom: 70}}>
                          <div>Heading</div>
                          <TextEditor
                            defaultValue={formValues.heading}
                            disabled={editMode ? false : true}
                            onChange={heading => handleInput("heading", heading)}
                          />
                        </div>
                        <div style={{ width: "85%", marginBottom: 70}}>
                          <div>Caption</div>
                          <TextEditor
                            defaultValue={formValues.content}
                            disabled={editMode ? false : true}
                            onChange={content => handleInput("content", content)}
                          />
                        </div>
                        {editMode && <div style={{ display: "flex", flexDirection: "row", rowGap: 80, marginBottom: 70}}>
                          <div
                            style={{
                              ...styles.button,
                              backgroundColor: loading ? "rgba(5, 0, 56, 0.4)" : "#050038",
                              color: "white",
                              justifyContent: "center",
                              marginBottom: 50
                            }}
                            onClick={editPress}
                          >
                            Save changes {loading && (
                            <div className="spinner-border spinner-border-white spinner-border-sm ml-2"></div>
                          )}
                          </div>
                          <div
                            style={{
                              ...styles.button,
                              color: "#050038",
                              justifyContent: "center",
                            }}
                          >
                            Archive Post
                          </div>
                        </div>}
                      </div>
                      <div style={styles.rightContent}>
                        <div
                          style={{
                            ...styles.button,
                            backgroundColor: editMode ? "rgba(5, 0, 56, 0.4)" : "#050038",
                            border: editMode ? "" : ".1px solid rgba(5, 0, 56, 0.06)",
                            cursor: editMode ? "not-allowed" : "pointer",
                            color: "white",
                            justifyContent: "center",
                            marginBottom: 50
                          }}
                          onClick={() => setEditMode(true)}
                        >
                          Edit
                        </div>
                      </div>
                    </div>
                  </div>
                </section>
                <ExtendedModal showModal={showModal} closeModal={closeModal}>
                  <div style={{
                    display: "flex",
                    flexDirection: "column",
                    alignItems: "center"
                  }}>
                    <img style={{
                        marginBottom: 30,
                        width: 56,
                        height: 56
                      }}
                      src={ serverResponse.status === 200 ? 
                        require("../../../Assets/img/success.svg")
                        : require("../../../Assets/img/error.svg")
                      }
                    />
                    <p>{serverResponse.response}</p>
                  </div>  
                </ExtendedModal>
            </>
        )
}
const mapStateToProps = state => {
    return {
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
    }
}
export default withRouter(connect(mapStateToProps, mapDispatchToProps)(ShowSinglePress))
