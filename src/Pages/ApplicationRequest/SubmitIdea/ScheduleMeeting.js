import React, { useState } from 'react';
import { withRouter } from 'react-router-dom';
import Header from '../../../Container/Header';

const styles = {
  container: {
    width: "100%",
    backgroundColor: "white",
    marginBottom: 15,
    padding: "33px 20px 21px"
  },
  dataArea: {
    border: ".1px solid rgba(59,107,211,.2)",
    borderRadius: 6,
    rowGap: 30,
  },
  header: {
    display: "grid",
    gridTemplateColumns: "10% 20% 25% 25% 10% 10%",
    fontSize: 16,
    fontWeight: "bold",
    padding: "38px 40px 17px",
    borderBottom: ".1px solid rgba(5, 0, 56, 0.06)",
  },
  body: {
    display: "grid",
    fontSize: 16,
    gridTemplateColumns: "10% 20% 25% 25% 10% 10%",
    padding: "20px 40px 18px",
  },
  button: {
    width: 148,
    height: 44,
    border: ".1px solid rgba(5, 0, 38, 1)",
    boxSizing: "border-box",
    display: "flex",
    alignItems: "center",
    boxShadow: "0px 2px 2px rgba(5, 0, 56, 0.08)",
    borderRadius: 6,
    cursor: "pointer",
    marginRight: 35
  },
  image: {
    width: "85%",
    marginBottom: 114
  },
  socialDivs: {
    width: 67,
    height: 50,
    padding: 2,
    marginRight: 42,
    border: "0.4px solid rgba(59, 107, 211, 0.8)",
    boxSizing: "border-box",
    borderRadius: "4px",
    display: "flex",
    flexDirection: "column"
  }
};

const Schedule = (props) => {

  const [editMode, setEditMode] = useState(false);
  const [loading, setLoading] = useState(false);



  const [serverResponse, setServerResponse] = useState({ status: "", response: ""});

        return (
            <>
              <Header />
              <div style={{
                marginBottom: 20,
                display: "flex",
                flexDirection: "row",
                paddingLeft: 29,
                paddingRight: 29,
                justifyContent: "space-between"
              }}>
                <div>
                  <div
                    style={{
                      ...styles.button,
                      backgroundColor: "",
                      justifyContent: "center",
                      color: "#050038",
                      border: "",
                      boxShadow: "",
                      cursor: "default"
                    }}
                  >
                  </div>
                </div>
                <div style={{ display: "flex", flexDirection: "row", justifyContent: "space-between"}}>
                  <div
                    style={{
                      ...styles.button,
                      backgroundColor: "#050038",
                      justifyContent: "center",
                      color: "white"
                    }}
                    onClick={() => props.history.push('/content-manager/blog/create')}
                  >
                    New Post
                  </div>
                  <div style={{
                    ...styles.button,
                    display: "flex",
                    flexDirection: "row",
                    color: "#050038",
                    paddingLeft: 12,
                    paddingRight: 5
                  }}>
                    <img style={{ marginRight: 20 }} src={require("../../../Assets/img/filter.svg")} />
                    <span style={{ marginRight: 25 }}>Filter</span>
                    <img style={{ marginTop: 2 }} src={require("../../../Assets/img/downwardArrow.svg")} />
                  </div>
                </div>
              </div>
                <section className="px-4">
                  <div style={styles.container}>
                    <div
                      style={{
                        ...styles.dataArea,
                        padding: 20,
                        display: "grid",
                        gridTemplateColumns: "40% 55%",
                        rowGap: "5%"
                      }}
                    >
                      <div style={styles.leftContent}>
                        
                      </div>
                      <div style={styles.rightContent}>
                        <div style={{
                          background: "white",
                          border: ".1px solid rgba(59,107,211,.2)",
                          borderRadius: 4
                        }}>
                          <div
                            style={{
                              height: 70,
                              width: "100%",
                              background: "#050038",
                              borderRadius: "4px 4px 0 0",
                              display: "flex",
                              alignItems: "center",
                              justifyContent: "flex-end",
                              paddingRight: 56
                            }}
                          >
                            <div
                              style={{
                                background: "white",
                                width: 208,
                                height: 46,
                                border: ".1px solid rgba(59,107,211,.2)",
                                borderRadius: 4,
                                marginRight: 91,
                                display: "flex",
                                alignItems: "center",
                                justifyContent: "center",
                              }}
                            >
                              <img style={{ marginRight: 27, cursor: "pointer"}} src={require("../../../Assets/img/back-arrow.svg")} />
                              <span style={{ color: "#050038", fontSize: 14 }}>May 2021</span>
                              <img style={{ marginLeft: 27, cursor: "pointer"}} src={require("../../../Assets/img/forward-arrow.svg")} />
                            </div>
                            <div
                              style={{
                                display: "flex",
                                alignItems: "center",
                                justifyContent: "center",
                                fontSize: 15,
                                color: "white",
                                fontWeight: "bold",
                                cursor: "pointer"
                              }}
                            >
                              View all
                            </div>
                          </div>
                          <div style={{ overflowY: "scroll", maxHeight: "50vh"}}>
                            <div style={{
                              borderBottom: ".1px solid rgba(59,107,211,.2)",
                              display: "grid",
                              gridTemplateColumns: "25% 75%"
                            }}>
                              <div
                                style={{
                                  paddingTop: 12,
                                  paddingLeft: 22,
                                  border: ".1px solid rgba(59,107,211,.2)",
                                }}
                              >
                                <div style={{ fontSize: 15, color: "rgba(0, 0, 0, 0.7)", marginBottom: 47}}>9th Saturday</div>
                                <div style={{ fontSize: 15, color: "#050038"}}>08:00 AM</div>
                              </div>
                              <div
                                style={{
                                  padding: "30px 41px 36px 21px"
                                }}
                              >
                                <div
                                  style={{
                                    border: ".1px solid rgba(59,107,211,.2)",
                                    borderRadius: 4,
                                    borderLeft: "5px solid #3B6BD3",
                                    padding: "17px 31px",
                                    fontSize: 15
                                  }} 
                                >
                                  <p>Zoom meeting with <span style={{ color: "#123070", opacity: .93, fontWeight: "bold"}}>James Stone</span></p>
                                  <p style={{ color: "#12278C" }}>https://zoommeeting.com</p>
                                </div>
                              </div>
                            </div>
                            <div style={{
                              borderBottom: ".1px solid rgba(59,107,211,.2)",
                              display: "grid",
                              gridTemplateColumns: "25% 75%"
                            }}>
                              <div
                                style={{
                                  paddingTop: 12,
                                  paddingLeft: 22,
                                  borderRight: ".1px solid rgba(59,107,211,.2)",
                                }}
                              >
                                <div style={{ fontSize: 15, color: "rgba(0, 0, 0, 0.7)", marginBottom: 47}}>9th Saturday</div>
                                <div style={{ fontSize: 15, color: "#050038"}}>08:00 AM</div>
                              </div>
                              <div
                                style={{
                                  padding: "30px 41px 36px 21px"
                                }}
                              >
                                <div
                                  style={{
                                    border: ".1px solid rgba(59,107,211,.2)",
                                    borderRadius: 4,
                                    borderLeft: "5px solid #35935A",
                                    padding: "17px 31px",
                                    fontSize: 15
                                  }} 
                                >
                                  <p>Zoom meeting with <span style={{ color: "#123070", opacity: .93, fontWeight: "bold"}}>Onyeka and sons</span></p>
                                  <p style={{ color: "#12278C" }}>https://zoommeeting.com</p>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </section>
            </>
        )
}

export default withRouter(Schedule)
