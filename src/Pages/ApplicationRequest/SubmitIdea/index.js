import Axios from 'axios';
import React, { useEffect, useState } from 'react';
import { withRouter } from "react-router-dom";
import { Api } from '../../../Const/Api';
import Header from '../../../Container/Header';


const styles = {
  container: {
    width: "100%",
    backgroundColor: "white",
    height: "67vh",
    overflowY: "scroll",
    marginBottom: 15,
    padding: "33px 29px 21px"
  },
  dataArea: {
    border: ".1px solid rgba(59,107,211,.2)",
    borderRadius: 6,
    rowGap: 30
  },
  header: {
    display: "grid",
    gridTemplateColumns: "1% 5% 10% 12% 15% 12% 10px",
    columnGap: "7%",
    fontSize: 16,
    fontWeight: "bold",
    padding: "38px 40px 17px",
    borderBottom: ".1px solid rgba(5, 0, 56, 0.06)",
  },
  button: {
    width: 148,
    height: 44,
    display: "flex",
    alignItems: "center",
    color: "white",
    marginRight: 35
  },
  body: {
    display: "grid",
    gridTemplateColumns: "1% 5% 10% 12% 15% 12% 10px",
    columnGap: "7%",
    padding: "20px 40px 18px",
  },
  centre: {
    textAlign: "center"
  }
};
const SubmitIdea = (props) => {

  const [viewMode, setViewMode] = useState("approved");
  const [openFilter, setOpenFilter] = useState(false);
  const [allIdeas, setAllIdeas] = useState({});
  const [openEllipsisOptions, setOpenEllipsisOptions] = useState(false);

  const switchViewMode = (status) => {
    setViewMode(status);
    setOpenFilter(false);
  }

  const toggleOpenFilter = (e) => {
    if(e.target.id === "filterArrow" || e.target.id === "filterArrow-img") {
      setOpenFilter(!openFilter)
    } else {
      if(openFilter) setOpenFilter(false)
    }
  }

  useEffect(() => {
    const getAllIdeas = async () => {
      try{
        const result = await Axios.get(
          `${Api.BaseUrl}/api/idea/all`,
          {'headers': { 'Authorization': `Bearer ${localStorage.getItem("token")}` } }
        );

        console.log(result);

        if(result.status === 200) {
          const data = result.data.payload.message;
          setAllIdeas({
            approved: data.filter(request => request.status === "approved"),
            rejected: data.filter(request => request.status === "rejected"),
            archived: data.filter(request => request.status === "archived")
          });
        }
      }catch(e) {
        console.log(e)
      }
    };

    getAllIdeas();
  }, [props]);

        return (
            <>
              <Header  />
              <div style={{
                marginBottom: 20,
                paddingLeft: 29,
                paddingRight: 29,
                display: "flex",
                flexDirection: "row",
                justifyContent: "flex-end"
              }}>
                <div style={{ display: "flex", flexDirection: "row", justifyContent: "space-between"}}>
                  <div
                    style={{
                      ...styles.button,
                      backgroundColor: "#050038",
                      borderRadius: 6,
                      justifyContent: "center",
                    }}
                    // onClick={() => props.history.push('/content-manager/press/create')}
                  >
                    View Calendar
                  </div>
                  <div
                    style={{
                      position: "relative"
                    }}
                  >
                    <div 
                      id="filterArrow"
                      style = {{
                        ...styles.button,
                        display: "flex",
                        flexDirection: "row",
                        color: "#050038",
                        background: "white",
                        paddingLeft: 12,
                        paddingRight: 5,
                        borderBottomLeftRadius: openFilter ? 0 : 6,
                        borderBottomRightRadius: openFilter ? 0 : 6,
                        border: ".1px solid rgba(59,107,211,.2)",
                        boxSizing: "border-box",
                        boxShadow: "0px 2px 2px rgba(5, 0, 56, 0.08)",
                        borderRadius: 6,
                        cursor: "pointer",
                      }}
                      onClick={(e) => toggleOpenFilter(e)}
                    >
                      <img style={{ marginRight: 20 }} src={require("../../../Assets/img/filter.svg")} />
                      <span style={{ marginRight: 25 }}>Filter</span>
                      <img
                        id="filterArrow-img"
                        style={{ marginTop: 2 }}
                        src={require("../../../Assets/img/downwardArrow.svg")}
                      />
                    </div>
                    {openFilter && <div style={{
                      position: 'absolute',
                      width: 148,
                      minHeight: 100,
                      color: "#050038",
                      fontSize: 14,
                      paddingLeft: 37,
                      paddingTop: 5,
                      background: "white",
                      zIndex: 100,
                      marginTop: -1,
                      borderLeft: ".1px solid rgba(59,107,211,.2)",
                      borderRight: ".1px solid rgba(59,107,211,.2)",
                      borderBottom: ".1px solid rgba(59,107,211,.2)",
                      borderBottomLeftRadius: 6,
                      borderBottomRightRadius: 6
                    }}>
                      <div style={{ cursor: "pointer", marginBottom: 25 }} onClick={() => switchViewMode("approved")}>
                        Approved
                      </div>
                      <div style={{ cursor: "pointer", marginBottom: 25 }} onClick={() => switchViewMode("rejected")}>
                        Rejected
                      </div>
                      <div style={{ cursor: "pointer", marginBottom: 25 }} onClick={() => switchViewMode("archived")}>
                        Archived
                      </div>
                    </div>}
                  </div>
                </div>
              </div>

                <section className="px-4">
                  <div style={styles.container}>
                    <div style={styles.dataArea}>
                      <div style={styles.header}>
                        <div>
                          <input type="checkbox" style={{ transform: "scale(1.5)" }} />
                        </div>
                        <div style={{fontWeight: "bold"}}>Title</div>
                        <div style={{ ...styles.centre, fontWeight: "bold" }}>Name</div>
                        <div style={{ ...styles.centre, fontWeight: "bold" }}>Phone Number</div>
                        <div style={{ ...styles.centre, fontWeight: "bold" }}>Email Address</div>
                        <div style={{ ...styles.centre, fontWeight: "bold" }}>Status</div>
                        <div></div>
                      </div>
                      <div>
                        {allIdeas[viewMode]?.length > 0 ? allIdeas[viewMode].map(request => (
                          <div style={styles.body}>
                            <div>
                              <input type="checkbox" style={{ transform: "scale(1.5)" }} />
                            </div>
                            <div>{request.title}</div>
                            <div style={styles.centre}>{`${request.firstname} ${request.lastname}`}</div>
                            <div style={styles.centre}>{request.phone_number}</div>
                            <div style={styles.centre}>{request.email}</div>
                            <div style={styles.centre}>{request.status}</div>
                            <div
                              style={{
                                ...styles.centre,
                                cursor: "pointer",
                                position: "relative"
                              }}
                              onClick={() => setOpenEllipsisOptions(true)}
                            >
                              ...
                              {openEllipsisOptions && <div
                                style={{
                                  position: "absolute",
                                  width: 105,
                                  border: ".1px solid rgba(59,107,211,.2)",
                                  marginLeft: -50,
                                  backgroundColor: "white",
                                  borderRadius: 3,
                                  marginTop: 3,
                                  zIndex: 700
                                }}
                              >
                                <div
                                  style={styles.ellipsisOptions}
                                  onMouseEnter={() => {}}
                                >
                                  View
                                </div>
                                <div
                                  // onClick={() => toggleArchive(recent.id)}
                                  style={styles.ellipsisOptions}
                                >
                                  {[].includes(1) ? "Unarchive" : "Archive"}
                                </div>

                              </div>}
                            </div>
                          </div>)
                        ) : (
                          <div
                            style={{
                              textAlign: "center",
                              fontSize: 16,
                              color: "#050038",
                              marginBottom: 30,
                              marginTop: 30
                            }}
                          >
                            {
                              `There are no ${viewMode} requests yet. Check back later.`
                            }
                          </div>
                        )}
                      </div>
                    </div>
                  </div>
                </section>
            </>
        )
}

export default withRouter(SubmitIdea)
