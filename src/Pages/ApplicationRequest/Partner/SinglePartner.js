import React, { useState } from 'react';
import { withRouter } from 'react-router-dom';
import makeAnimated from "react-select/animated";
import Select from "react-select/creatable";
import ExtendedModal from '../../../Component/ExtendedModal';
import TextEditor from '../../../Component/TextEditor';
import Header from '../../../Container/Header';

const styles = {
  container: {
    width: "100%",
    backgroundColor: "white",
    marginBottom: 15,
    padding: "33px 29px 21px"
  },
  dataArea: {
    border: ".1px solid rgba(59,107,211,.2)",
    borderRadius: 6,
    rowGap: 30,
  },
  header: {
    display: "grid",
    gridTemplateColumns: "10% 20% 25% 25% 10% 10%",
    fontSize: 16,
    fontWeight: "bold",
    padding: "38px 40px 17px",
    borderBottom: ".1px solid rgba(5, 0, 56, 0.06)",
  },
  body: {
    display: "grid",
    fontSize: 16,
    gridTemplateColumns: "10% 20% 25% 25% 10% 10%",
    padding: "20px 40px 18px",
  },
  button: {
    width: 148,
    height: 44,
    border: ".1px solid rgba(5, 0, 38, 1)",
    boxSizing: "border-box",
    display: "flex",
    alignItems: "center",
    boxShadow: "0px 2px 2px rgba(5, 0, 56, 0.08)",
    borderRadius: 6,
    cursor: "pointer",
    marginRight: 35
  },
  image: {
    width: "85%",
    marginBottom: 114
  },
  socialDivs: {
    width: 67,
    height: 50,
    padding: 2,
    marginRight: 42,
    border: "0.4px solid rgba(59, 107, 211, 0.8)",
    boxSizing: "border-box",
    borderRadius: "4px",
    display: "flex",
    flexDirection: "column"
  }
};

const animatedComponents = makeAnimated();

const SinglePartner = (props) => {

  const [editMode, setEditMode] = useState(false);
  const [loading, setLoading] = useState(false);



  const [serverResponse, setServerResponse] = useState({ status: "", response: ""});
  const [showModal, setShowModal] = useState(false);

  const closeModal = () => {
    setShowModal(false)
    if(serverResponse.status === 200) {
      props.history.push("/content-manager/blog")
    }
  }

        return (
            <>
              <Header />
              <div style={{
                marginBottom: 20,
                display: "flex",
                flexDirection: "row",
                paddingLeft: 29,
                paddingRight: 29,
                justifyContent: "space-between"
              }}>
                <div>
                  <div
                    style={{
                      ...styles.button,
                      backgroundColor: "",
                      justifyContent: "center",
                      color: "#050038",
                      border: "",
                      boxShadow: "",
                      cursor: "default"
                    }}
                  >
                  </div>
                </div>
                <div style={{ display: "flex", flexDirection: "row", justifyContent: "space-between"}}>
                  <div
                    style={{
                      ...styles.button,
                      backgroundColor: "#050038",
                      justifyContent: "center",
                      color: "white"
                    }}
                    onClick={() => props.history.push('/content-manager/blog/create')}
                  >
                    New Post
                  </div>
                  <div style={{
                    ...styles.button,
                    display: "flex",
                    flexDirection: "row",
                    color: "#050038",
                    paddingLeft: 12,
                    paddingRight: 5
                  }}>
                    <img style={{ marginRight: 20 }} src={require("../../../Assets/img/filter.svg")} />
                    <span style={{ marginRight: 25 }}>Filter</span>
                    <img style={{ marginTop: 2 }} src={require("../../../Assets/img/downwardArrow.svg")} />
                  </div>
                </div>
              </div>
                <section className="px-4">
                  <div style={styles.container}>
                    <div
                      style={{
                        ...styles.dataArea,
                        padding: "20px 85px",
                        display: "grid",
                        gridTemplateColumns: "80% 15%",
                        rowGap: "5%"
                      }}
                    >
                      <div style={styles.leftContent}>
                        <div style={{ fontSize: 16, marginBottom: 70 }}>
                          <div>First name</div>
                          <input
                            type="text"
                            disabled
                            defaultValue={'James'}
                            style={{
                              width: "85%",
                              background:"rgba(207, 207, 207, 0.2)",
                              borderRadius: "4px",
                              padding: 10,
                              border: "0.4px solid rgba(59, 107, 211, 0.8)",
                              color: "#050038",
                              fontWeight: 700
                            }}
                          />
                        </div>
                        <div style={{ fontSize: 16, marginBottom: 70 }}>
                          <div>Last name</div>
                          <input
                            type="text"
                            disabled
                            defaultValue={'Stone'}
                            style={{
                              width: "85%",
                              background:"rgba(207, 207, 207, 0.2)",
                              borderRadius: "4px",
                              padding: 10,
                              border: "0.4px solid rgba(59, 107, 211, 0.8)",
                              color: "#050038",
                              fontWeight: 700
                            }}
                          />
                        </div>
                        <div style={{ fontSize: 16, marginBottom: 70 }}>
                          <div>Phone Number</div>
                          <input
                            type="text"
                            disabled
                            defaultValue={'090123456789'}
                            style={{
                              width: "85%",
                              background: "rgba(207, 207, 207, 0.2)",
                              borderRadius: "4px",
                              padding: 10,
                              border: "0.4px solid rgba(59, 107, 211, 0.8)",
                              color: "#050038",
                              fontWeight: 700
                            }}
                          />
                        </div>
                        <div style={{ fontSize: 16, marginBottom: 70 }}>
                          <div>Email Address</div>
                          <input
                            type="text"
                            disabled
                            defaultValue={'jamesholden@gmail.com'}
                            style={{
                              width: "85%",
                              background: "rgba(207, 207, 207, 0.2)",
                              borderRadius: "4px",
                              padding: 10,
                              border: "0.4px solid rgba(59, 107, 211, 0.8)",
                              color: "#050038",
                              fontWeight: 700
                            }}
                          />
                        </div>
                        <div style={{ fontSize: 16, width: "85%", marginBottom: 70 }}>
                          <div>Partnership Type</div>
                          <Select
                            styles={{
                              width: "100%",
                              background: "rgba(207, 207, 207, 0.2)",
                              borderRadius: "4px",
                              padding: 10,
                              border: "0.4px solid rgba(59, 107, 211, 0.8)",
                              color: "#050038",
                              fontWeight: 700
                            }}
                            isMulti
                            placeholder="Type tags and separate them with tabs"
                            closeMenuOnSelect={true}
                            defaultValue={"Sponsorship"}
                            isDisabled
                            components={animatedComponents}
                            name="tag"
                          />
                        </div>
                        <div style={{ fontSize: 16, marginBottom: 70 }}>
                          <div>Company Website</div>
                          <input
                            type="text"
                            disabled
                            value={"https://mybigtechcompany.com"}
                            style={{
                              width: "85%",
                              background: "rgba(207, 207, 207, 0.2)",
                              borderRadius: "4px",
                              padding: 10,
                              border: "0.4px solid rgba(59, 107, 211, 0.8)",
                              color: "#050038",
                              fontWeight: 700
                            }}
                          />
                        </div>
                        <div style={{ fontSize: 16, marginBottom: 70 }}>
                          <div>Linkedin URL</div>
                          <input
                            type="text"
                            disabled
                            value={"https://mybigtechcompany.com/linkedin"}
                            style={{
                              width: "85%",
                              background: "rgba(207, 207, 207, 0.2)",
                              borderRadius: "4px",
                              padding: 10,
                              color: "#050038",
                              border: "0.4px solid rgba(59, 107, 211, 0.8)",
                              fontWeight: 700
                            }}
                          />
                        </div>
                        <div style={{ fontSize: 16, marginBottom: 70 }}>
                          <div>Twitter URL</div>
                          <input
                            type="text"
                            disabled
                            value={"https://mybigtechcompany.com/twitter"}
                            style={{
                              width: "85%",
                              background: "rgba(207, 207, 207, 0.2)",
                              borderRadius: "4px",
                              padding: 10,
                              color: "#050038",
                              fontWeight: 700,
                              border: "0.4px solid rgba(59, 107, 211, 0.8)",
                            }}
                          />
                        </div>
                        <div style={{ width: "85%", marginBottom: 70}}>
                          <div>Brief on type of partnership</div>
                          <TextEditor
                            defaultValue={"Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.--"}
                            disabled
                          />
                        </div>
                        <div style={{ fontSize: 16, marginBottom: 70 }}>
                          <div>Attached Presentation</div>
                          <div
                            style={{
                              width: "85%",
                              background: "rgba(207, 207, 207, 0.2)",
                              borderRadius: 6,
                              padding: 10,
                              paddingLeft: 20,
                              color: "#050038",
                              fontWeight: 700,
                              border: "0.4px solid rgba(59, 107, 211, 0.8)",
                            }}
                          >
                            <span>
                              <img
                                src={require("../../../Assets/img/attachment-blue.svg")} 
                                style={{ marginRight: 10 }}
                              /> My great plan (PDF) 8mb
                            </span>
                          </div>
                        </div>
                        <div style={{ display: "flex", flexDirection: "row", rowGap: 80, marginBottom: 70}}>
                          <div
                            style={{
                              ...styles.button,
                              backgroundColor: loading ? "rgba(5, 0, 56, 0.4)" : "#050038",
                              border: loading ? "" : ".1px solid rgba(5, 0, 56, 0.06)",
                              cursor: loading ? "not-allowed" : "pointer",
                              color: "white",
                              justifyContent: "center",
                              marginBottom: 50
                            }}
                          >
                            Approve {loading && (
                            <div className="spinner-border spinner-border-white spinner-border-sm ml-2"></div>
                          )}
                          </div>
                          <div
                            style={{
                              ...styles.button,
                              color: "#050038",
                              justifyContent: "center",
                            }}
                          >
                            Reject
                          </div>
                        </div>
                      </div>
                      <div style={styles.rightContent}>
                        <div
                          style={{
                            ...styles.button,
                            backgroundColor: editMode ? "rgba(5, 0, 56, 0.4)" : "#050038",
                            border: editMode ? "" : ".1px solid rgba(5, 0, 56, 0.06)",
                            cursor: editMode ? "not-allowed" : "pointer",
                            color: "white",
                            justifyContent: "center",
                            marginBottom: 50
                          }}
                        >
                          Approve {loading && (
                            <div className="spinner-border spinner-border-white spinner-border-sm ml-2"></div>
                          )}
                        </div>
                        <div
                          style={{
                            ...styles.button,
                            color: "#050038",
                            justifyContent: "center",
                            cursor: loading ? "not-allowed" : "pointer",
                          }}
                        >
                          Reject
                        </div>
                      </div>
                    </div>
                  </div>
                </section>
                <ExtendedModal showModal={showModal} closeModal={closeModal}>
                  <div style={{
                    display: "flex",
                    flexDirection: "column",
                    alignItems: "center"
                  }}>
                    <img style={{
                        marginBottom: 30,
                        width: 56,
                        height: 56
                      }}
                      src={ serverResponse.status === 200 ? 
                        require("../../../Assets/img/success.svg")
                        : require("../../../Assets/img/error.svg")
                      }
                    />
                    <p>{serverResponse.response}</p>
                  </div>  
                </ExtendedModal>
            </>
        )
}

export default withRouter(SinglePartner)
