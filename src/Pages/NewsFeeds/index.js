import React, { Component } from 'react'
import { Link } from 'react-router-dom'
import '../Assets/css/style.css'
import Footer from '../../Container/Footer'
import NewsfeedPress from '../../Container/NewsfeedContainer/NewsfeedPressContainer'
import NewsfeedEvent from '../../Container/NewsfeedContainer/NewsfeedEventContainer'
import  NewsfeedTags  from '../../Container/NewsfeedContainer/NewsfeedTagsContainer'
import NewsfeedGallary  from '../../Container/NewsfeedContainer/NewsfeedGallaryContainer'

class NewsFeeds extends Component {
	constructor() {
		super()
		this.state = {
			breadcrumb: 'Events',
			eventData: [],
			deleteid: ''
		}
	}


	handleEventDelete = (e) => {
		this.setState({ deleteid: e.target.value })
		// console.log('delete', deleteid)
		// this.props.eventDeleteDispatch(deleteid)
		// setTimeout(() => {
		//     this.props.getEventDispatch()
		// }, 500);
		// <Deletemodal
		//             deleteid={deleteid}
		//             message={"Are you sure you want to remove this event"}
		//             buttonname={"Remove"}
		//         />
	}




	render()
	//   const ={newsfeedEventdata}  this.props
	{
		const { breadcrumb } = this.state
		


		return (

			<>
				<section className="wrapper px-4 ">
					<div className="container">
						<div className="page-title mb-4">
							<div className="row align-items-center">
								<div className="col-md-5">
									<div className="page-title-inner">
										<h4>Content manager</h4>
									</div>
								</div>
								<div className="col-md-7">
									<nav aria-label="breadcrumb">
										<ol className="breadcrumb mb-2 justify-content-end p-0 bg-transparent align-items-center">
											<li className="breadcrumb-item"><Link to="/">Dashboard</Link></li>
											<li className="breadcrumb-item"><Link to="#">Content manager</Link></li>
											<li className="breadcrumb-item"><Link to="/news_feeds">News Feeds</Link></li>
											<li className="breadcrumb-item active" aria-current="page">{this.state.breadcrumb}</li>
										</ol>
									</nav>
								</div>
							</div>
						</div>

						<div className="nees-feeds">
							<div className="row">
								<div className="col-12 cus-tabs">
									<ul className="nav nav-tabs" id="myTab" role="tablist">
										<li className="nav-item">
											<a onClick={() => {
												this.setState({
													breadcrumb: "Events"

												})
											}} className="nav-link active" id="events-tab" data-toggle="tab" href="#events" role="tab" aria-controls="events" aria-selected="true">Events</a>
										</li>
										<li className="nav-item">
											<a onClick={() => {
												this.setState({
													breadcrumb: "Press"
												})
											}} className="nav-link" id="press-tab" data-toggle="tab" href="#press" role="tab" aria-controls="press" aria-selected="false">Press </a>
										</li>
										<li className="nav-item">
											<a onClick={() => {
												this.setState({
													breadcrumb: "Tags"
												})
											}} className="nav-link" id="tags-tab" data-toggle="tab" href="#tags" role="tab" aria-controls="tags" aria-selected="false">Tags</a>
										</li>
										<li className="nav-item">
											<a onClick={() => {
												this.setState({
													breadcrumb: "Gallery"
												})
											}} className="nav-link" id="gallery-tab" data-toggle="tab" href="#gallery" role="tab" aria-controls="gallery" aria-selected="false">Gallery</a>
										</li>
									</ul>

									<div className="tab-content" id="myTabContent">
										{breadcrumb === "Events" && <NewsfeedEvent />}
										{breadcrumb === "Press" && <NewsfeedPress />}
										{breadcrumb === "Tags" && <NewsfeedTags />}
										{breadcrumb === "Gallery" && <NewsfeedGallary />}
									</div>
								</div>
							</div>
						</div>
					</div>
				</section>


				<Footer />
			</>
		)
	}
}
// const mapStateToProps = state => {
// 	return {
// 		newsfeedEventdata: state.NewsfeedEvent,
// 		newsfeedPressdata: state.NewsfeedPress

// 	}
// }

// const mapDispatchToProps = (dispatch) => {
// 	return {
// 		getEventDispatch: () => { dispatch(getEvent()) },
// 		eventDeleteDispatch: (deleteid) => { dispatch(eventDelete(deleteid)) },
// 		getPressDispatch: () => { dispatch(getPress()) }
// 	}
// }
export default (NewsFeeds)
