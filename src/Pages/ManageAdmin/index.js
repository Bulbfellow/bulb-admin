import React, { Component } from 'react'
import { Link } from 'react-router-dom'
import Footer from '../../Container/Footer'
import '../Assets/css/style.css'
import { GetAdminList, DeleteAdminList, SearchAdminList } from '../../Action/manageAdmin'
import { connect } from 'react-redux'
import Deletemodal from '../../Component/Deletemodal'
import Search from '../../Component/Search'
import Quicknotification from '../../Component/Quicknotification'
import CircularProgress from '@material-ui/core/CircularProgress';

class ManageAdmin extends Component {
    constructor(props) {
        super(props);
        this.state = {
            deleteID: '',
            page: 1,
            data: [],
            paginationDetail: [],
            searchvalue: '',
            dataLength: '',
            firstIndex: '',
            showpopup : false,
            loading : true
        }
    }

    componentDidMount() {
        let slice = this.state.page
        this.props.GetAdminAction(slice)
    }

    componentDidUpdate(prevProps, prevState) {
        //-----Checking Coundition on Next and Privious Page---------
        if (prevProps.AdminList.loading !== this.props.AdminList.loading) {
            this.setState({
                loading : this.props.AdminList.loading
            })
        }

        if (prevState.page !== this.state.page) {
            if (prevState.searchvalue === '') {
                this.props.GetAdminAction(this.state.page)
            } else {
                const filter = {
                    Search: this.state.searchvalue,
                    page: this.state.page
                }
                this.props.SearchAdminListAction(filter)
            }
        }
        //------- ! Set State of Data on Page Change Without Search !
        if (prevProps.AdminList !== this.props.AdminList) {
            const Lastcount = (this.state.page) * 7
            const initialCount = Lastcount - 7
            this.setState({
                data: this.props.AdminList.resp,
                paginationDetail: this.props.AdminList.pagination,
                dataLength: this.props.AdminList.resp.length,
                firstIndex: initialCount
            })
        }
        //--------!  Hiting Search Api/NOrmal Get Api on Change of Search Value!
        if (prevState.searchvalue !== this.state.searchvalue) {
            if (this.state.searchvalue !== '') {
                const filter = {
                    Search: this.state.searchvalue,
                    page: this.state.page
                }
                this.props.SearchAdminListAction(filter)
            } else {
                this.props.GetAdminAction(this.state.page)
            }
        }

        // ---- Set State of Data on Searching of Value | ----------
        if (prevProps.AdminListBySearch !== this.props.AdminListBySearch) {
            const Lastcount = (this.state.page) * 7
            const initialCount = Lastcount - 7
            this.setState({
                data: this.props.AdminListBySearch.resp,
                paginationDetail: this.props.AdminListBySearch.pagination,
                dataLength: this.props.AdminListBySearch.resp.length,
                firstIndex: initialCount
            })
        }
    }


    handleDelete = () => {
        this.props.DeleteAdminAction(this.state.deleteID)
        setTimeout(() => {
        
            if (this.state.dataLength > 1 ) {
                this.props.GetAdminAction(this.state.page)
            this.setState({ page : this.state.page })
            }
            else{ 
                if (this.state.page > 1){
                this.setState({ page : this.state.page -1 })
            }
               
            }
    
        }, 1000);
        
        
    }

    handleIncrement = (pagecount) => {
        if (this.state.page < pagecount) {
            this.setState({ page: this.state.page + 1 })
        }
    }

    handleDecrement = () => {
        if (this.state.page > 1) {
            this.setState({ page: this.state.page - 1 })
        }
    }

    handleOnSearch = (e) => {
        this.setState({
            searchvalue: e.target.value,
            page: 1
        })
    }

    render() {
        const data = this.state.data
        const pagecount = this.state.paginationDetail.pageCount
        const itemCount = this.state.paginationDetail.itemCount
        const firstIndex = this.state.firstIndex
        const dataLength = this.state.dataLength
        let count = firstIndex
        let lastIndex = firstIndex + dataLength


        return (
            <>
                <section className="wrapper px-4 ">
                    <div className="container">
                        <div className="page-title mb-4">
                            <div className="row align-items-center">
                                <div className="col-md-5">
                                    <div className="page-title-inner">
                                        <h4>Manage Admin</h4>
                                    </div>
                                </div>
                                <div className="col-md-7">
                                    <nav aria-label="breadcrumb">
                                        <ol className="breadcrumb mb-2 justify-content-end p-0 bg-transparent align-items-center">
                                            <li className="breadcrumb-item"><Link to="/">Dashboard</Link></li>
                                            <li className="breadcrumb-item active" aria-current="page">manage admin</li>
                                        </ol>
                                    </nav>
                                </div>
                            </div>
                        </div>

                        <div className="nees-feeds activity">
                            <div className="tab-inner bg-white px-4 py-4">
                                <div className="tab-inner-head pb-3">
                                    <div className="row align-items-center mb-3">
                                        <div className="col-md-6">
                                            <Search
                                                onChange={this.handleOnSearch}
                                                value={this.state.searchvalue}
                                            />
                                        </div>
                                        <div className="col-md-6">
                                            <div className="show-pages d-flex justify-content-end align-items-center">
                                                <p className="mb-0 mr-3">Showing <span>{firstIndex + 1}</span> - <span>{lastIndex}</span> of <span>{itemCount}</span></p>
                                                <div className="prev-next">
                                                    <span className="prev"><button className="fas fa-angle-left" onClick={this.handleDecrement}></button></span>
                                                    <span className="next"><button className="fas fa-angle-right" onClick={() => this.handleIncrement(pagecount)}></button></span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div className="tab-inner-body">
                                    <div className="add-events-btn text-right mb-3">
                                        <Link to="/add-admin"><button className="btn btn-outline-warning px-4 rounded-50 fs-14">Add admin</button></Link>
                                    </div>
                                    <div className="table-responsive">
                                        <table className="table border">
                                            <thead>
                                                <tr className="bg-cusgray">
                                                    <td className="border-top-0">S/N</td>
                                                    <td className="border-top-0">Name</td>
                                                    <td className="border-top-0">Email</td>
                                                    <td className="border-top-0 width-md-180 white-space-nowrap">Date created</td>
                                                    <td className="border-top-0">Role</td>
                                                    <td className="border-top-0">Action</td>
                                                </tr>
                                            </thead>
                                            {this.state.loader && this.state.loader && <CircularProgress />}
                                            {data.length > 0 && data.map((list,i) => {
                                                return (
                                                    <tr className={list.status === "0" && "admin-pending"} key={i}>

                                                        <td>{count = count + 1}</td>
                                                        <td>
                                                            <p className="mb-0">{list.first_name} {list.last_name}</p>
                                                        </td>
                                                        <td>
                                                            <p className="mb-0 white-space-nowrap">{list.email}</p>
                                                        </td>
                                                        <td>
                                                            <p className="mb-0 date-time"><span>{list.rawdate}</span> <span>{list.rawtime}</span></p>
                                                        </td>
                                                        <td>
                                                            <p className="mb-0 date-time"><span>{list.role}</span> <span>{list.rawtime}</span></p>
                                                        </td>
                                                        <td className="all-action">
                                                            <div className="dropdown ml-2" className="cursor">
                                                                <span id="dropdownMenu" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i className="fa fa-ellipsis-v"></i></span>
                                                                <div className="dropdown-menu" aria-labelledby="dropdownMenu">
                                                                    <Link className="dropdown-item" to={`/edit-admin/${list.admin_id}`}>Edit</Link>
                                                                    <button className="dropdown-item" data-toggle="modal" data-target="#remove" onClick={() => this.setState({ deleteID: list.admin_id })}>Remove</button>
                                                                </div>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                )
                                            })}
                                        </table>
                                    </div>
                                </div>
                                <Deletemodal
                                    deleteid={this.handleDelete}
                                    message={"Are you sure you want to remove this admin account"}
                                    buttonname={"Delete"}
                                />
                                <Quicknotification
                                    show={this.state.showpopup}
                                    message={"Admin account deleted sucessfully"}
                                    closetab={() => this.setState({ showpopup: false })}
                                    closesecond={4000}
                                    timeout={true}
                                />
                            </div>
                        </div>
                    </div>
                </section>
                <Footer />
            </>
        )
    }
}
const mapStateToProps = state => {
    return {
        AdminList: state.getAdminList,
        AdminListBySearch: state.getAdminListBySearch
    }
}
const mapDispatchToProps = (dispatch) => {
    return {
        GetAdminAction: (slice) => dispatch(GetAdminList(slice)),
        DeleteAdminAction: (deleteID) => { dispatch(DeleteAdminList(deleteID)) },
        SearchAdminListAction: (filter) => { dispatch(SearchAdminList(filter)) }
    }
}
export default connect(mapStateToProps, mapDispatchToProps)(ManageAdmin)
