import React, { useState } from 'react'
import '../Assets/css/style.css'
import Footer from '../../Container/Footer'
import WorkspaceRequest from '../../Container/Desk/Workspacerequest'
import Enterprisedesk from '../../Container/Desk/Enterpriserequest'
import Newsletter from '../../Container/Desk/Newslatter'
import {
    BrowserRouter as  Switch, Link, Route, 
    useRouteMatch
} from 'react-router-dom'
import WorkspaceRequestview from '../../Container/Entries/WorkspaceRequest';
import EnterpriseRequestview from '../../Container/Entries/EnterpriseRequest'; 

const TheDesk = () => {

    const [breadcrumb, setBreadcrumb] = useState('workspace-request')
    let { path } = useRouteMatch();

    return (
        <>
            <section className="wrapper px-4 ">
                <div className="container">
                    <div className="page-title mb-4">
                        <div className="row align-items-center">
                            <div className="col-md-5">
                                <div className="page-title-inner">
                                    <h4>The desk</h4>
                                </div>
                            </div>
                            <div className="col-md-7">
                                <nav aria-label="breadcrumb">
                                    <ol className="breadcrumb mb-2 justify-content-end p-0 bg-transparent align-items-center">
                                        <li className="breadcrumb-item"><Link to="/">Dashboard</Link></li>
                                        <li className="breadcrumb-item"><Link to="/the-desk">the desk</Link></li>
                                        <li className="breadcrumb-item active" aria-current="page">{breadcrumb}</li>
                                    </ol>
                                </nav>
                            </div>
                        </div>
                    </div>

                    <div className="nees-feeds">
                        <div className="row">
                            <div className="col-12 cus-tabs">
                                <ul className="nav nav-tabs" id="myTab" role="tablist">
                                    <Link to={path} className="nav-item">
                                        <a onClick={() => {
                                            setBreadcrumb("workspace-request")
                                        }} className={`nav-link ${breadcrumb === "workspace-request" && "active"}`} >Workspace request</a>
                                    </Link>
                                    <Link to={`${path}/enterprise-tab`} className="nav-item">
                                        <a onClick={() => {
                                            setBreadcrumb("Enterprise-request")
                                        }} className={`nav-link ${breadcrumb === "Enterprise-request" && "active"}`} >Enterprise request</a>
                                    </Link>
                                    <Link to={`${path}/Newsletter-tab`} className="nav-item">
                                        <a onClick={() => {
                                            setBreadcrumb("Newsletter")
                                        }} className={`nav-link ${breadcrumb === "Newsletter" && "active"}`} >Newsletter</a>
                                    </Link>
                                </ul>

                                <Switch>
                                    <Route exact path={path} component={WorkspaceRequest} />
                                    <Route path={`${path}/enterprise-tab`} component={Enterprisedesk} />
                                    <Route path={`${path}/Newsletter-tab`} component={Newsletter} />
                                    <Route path={`${path}/view-workspace/:id`} component={WorkspaceRequestview} />
                                    <Route path={`${path}/view-enterprise/:id`} component={EnterpriseRequestview} />
                                </Switch>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <Footer />
        </>
    )
}
export default TheDesk
