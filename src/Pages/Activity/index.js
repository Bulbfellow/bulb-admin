import React, { Component } from 'react'
import { Link } from 'react-router-dom'
import Footer from '../../Container/Footer'
import { connect } from 'react-redux'
import { Adminactvity, AdminactvityDelete } from '../../Action/activity'
import Deletemodal from '../../Component/Deletemodal'
import { convertDatePickerTime } from '../../Const/function'
import '../Assets/css/style.css'
import Search from '../../Component/Search'
import Quicknotification from '../../Component/Quicknotification'

class Activity extends Component {
    constructor() {
        super()
        this.state = {
            currentPage: 1,
            postPerPage: 7,
            data: [],
            searchvalue: '',
            showpopup: false
        }
    }

    componentDidMount() {
        this.props.AdminActivity()
    }
    // componentDidUpdate(prevProps, prevState){
    //     if(prevProps.AdminActivityData.loading !== this.props.AdminActivityData.loading){
    //        this.setState({
    //         showpopup : true
    //        })
    //     }
    // }
    handleSearch = (e) => {
        this.setState({
            searchvalue: e.target.value,
            currentPage: 1
        })
    }


    deleteActvity = (id) => {
        this.props.AdminActivityDelete(id)
        this.props.AdminActivity()
        // this.setState({
        //     showpopup: true
        // })
    }

    increment = (pageNumbers) => {
        if (this.state.currentPage < pageNumbers.length)
            this.setState({
                currentPage: this.state.currentPage + 1,
            })
    }

    decrement = (event) => {
        if (this.state.currentPage > 1) {
            this.setState({
                currentPage: this.state.currentPage - 1,
            });
        }
    }

    render() {
        const data = this.props.AdminActivityData.resp.filter(
            data => data.admin_activity.toString().toLowerCase().includes(
                this.state.searchvalue.toString().toLowerCase()));
        const indexOfLastPost = this.state.currentPage * this.state.postPerPage;
        const indexOfFirstPost = indexOfLastPost - this.state.postPerPage;
        const currentPosts = data.slice(indexOfFirstPost, indexOfLastPost);
        const lastindex = currentPosts.length

        let count = indexOfFirstPost
        const pageNumbers = [];
        for (let i = 1; i <= Math.ceil(data.length / this.state.postPerPage); i++) {
            pageNumbers.push(i);
        }

        return (
            <>
                <section className="wrapper px-4 ">
                    <div className="container">
                        <div className="page-title mb-4">
                            <div className="row align-items-center">
                                <div className="col-md-5">
                                    <div className="page-title-inner">
                                        <h4>Activity</h4>
                                    </div>
                                </div>
                                <div className="col-md-7">
                                    <nav aria-label="breadcrumb">
                                        <ol className="breadcrumb mb-2 justify-content-end p-0 bg-transparent align-items-center">
                                            <li className="breadcrumb-item"><Link to="/">Dashboard</Link></li>
                                            <li className="breadcrumb-item active" aria-current="page">Activity</li>
                                        </ol>
                                    </nav>
                                </div>
                            </div>
                        </div>

                        <div className="nees-feeds activity">
                            <div className="tab-inner bg-white px-4 py-4">
                                <div className="tab-inner-head pb-3">
                                    <div className="row align-items-center mb-3">
                                        <div className="col-md-6">

                                            <Search
                                                onChange={this.handleSearch}
                                                value={this.state.searchvalue}
                                            />
                                        </div>
                                        <div className="col-md-6">
                                            


                                            <div className="show-pages d-flex justify-content-end align-items-center">
                                                <span className="mr-3 d-inline-block">
                                                    <select className="form-control fs-14" onChange={(e) => this.setState({ postPerPage: e.target.value, currentPage: 1 })}>
                                                        <option value="7">7 Rows Per Page</option>
                                                        <option value="10">10 Rows Per Page</option>
                                                        <option value="15">15 Rows Per Page</option>
                                                        <option value="20">20 Rows Per Page</option>
                                                        <option value="30">30 Rows Per Page</option>
                                                        <option value="50">50 Rows Per Page</option>
                                                    </select>
                                                </span>
                                                <p className="mb-0 mr-3">Showing <span>{indexOfFirstPost + 1}</span> - <span>{indexOfFirstPost + lastindex}</span> of <span>{data.length}</span></p>
                                                <div className="prev-next">
                                                    <span className="prev"><button className="fas fa-angle-left" onClick={this.decrement} ></button></span>
                                                    <span className="next"><button className="fas fa-angle-right" onClick={() => this.increment(pageNumbers)}></button></span>

                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                                <div className="tab-inner-body">
                                    <div className="table-responsive">
                                        <table className="table border">
                                            <thead>
                                                <tr className="bg-cusgray">
                                                    <td className="border-top-0"><span className="pl-md-3">S/N</span></td>
                                                    <td className="border-top-0"><span className="pl-md-3">Activity</span></td>
                                                    <td className="border-top-0 width-md-180 white-space-nowrap">Date</td>
                                                    <td className="border-top-0">Action</td>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                {currentPosts.length > 0 && currentPosts.map((user,i) => {
                                                    return (
                                                        <tr key={i}>
                                                            <td>
                                                                <p className="mb-0 pl-md-3">{count = count + 1}</p>
                                                            </td>
                                                            <td>
                                                                <p className="mb-0 pl-md-3">{user.admin_activity}</p>
                                                            </td>
                                                            <td>
                                                                <p className="mb-0 date-time"><span>{convertDatePickerTime(user.rowcreateddatetime)}</span></p>
                                                            </td>
                                                            <td className="all-action">
                                                                <div className="dropdown ml-2 cursor">
                                                                    <span id="dropdownMenu" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i className="fa fa-ellipsis-v"></i></span>
                                                                    <div className="dropdown-menu" aria-labelledby="dropdownMenu">
                                                                        <span onClick={() => this.deleteActvity(user.sr_no)} className="dropdown-item" data-toggle="modal" data-target="#remove" to="#">Remove</span>
                                                                    </div>
                                                                </div>
                                                            </td>
                                                        </tr>
                                                    )
                                                })}
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
                <Deletemodal
                    deleteid={this.deleteActvity}
                    message={"Are you sure you want to remove this activity"}
                    buttonname={"Remove"}
                />
                <Quicknotification
                    show={this.state.showpopup}
                    message={"Activity sucessfully removed"}
                    closetab={() => this.setState({ showpopup: false })}
                    closesecond={4000}
                    timeout={true}
                />

                <div className="alert-msg d-none">
                    <div className="bg-overlay"></div>
                    <div className="msg right-msg position-absolute">
                        <div className="msg-inner d-flex align-items-center justify-content-between">
                            <p className="mb-0 mr-4 fs-14">Activity sucessfully removed</p>
                            <p className="mb-0"><i className="fa fa-times"></i></p>
                        </div>
                    </div>
                </div>
                <Footer />
            </>
        )
    }
}
const mapStateToProps = state => {
    return {
        AdminActivityData: state.AdminActivity,
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        AdminActivity: () => { dispatch(Adminactvity()) },
        AdminActivityDelete: (deleteid) => { dispatch(AdminactvityDelete(deleteid)) },
    }
}
export default connect(mapStateToProps, mapDispatchToProps)(Activity)
