import React, { Component } from 'react'
import { Link } from 'react-router-dom'
import Footer from '../../Container/Footer'
import Deletemodal from '../../Component/Deletemodal'
import { WorkspaceListing, deleteWorkspace } from '../../Action/workspace'
import { connect } from 'react-redux'

class Workspace extends Component {
    constructor() {
        super();
        this.state = {
            deleteid: ''
        }
    }
    componentDidMount() {
        this.props.DispatchWorkspaceListing()
    }


    handleDelete = () => {
        this.props.ActiondeleteWorkspace(this.state.deleteid)
        setTimeout(() => {
            this.props.DispatchWorkspaceListing()
        }, 1000)
    }

    render() {
        const data = this.props.WorkspaceData.resp
        return (
            <>
                <section className="wrapper px-4 ">
                    <div className="container">
                        <div className="page-title mb-4">
                            <div className="row align-items-center">
                                <div className="col-md-5">
                                    <div className="page-title-inner">
                                        <h4>Workspace</h4>
                                    </div>
                                </div>
                                <div className="col-md-7">
                                    <nav aria-label="breadcrumb">
                                        <ol className="breadcrumb mb-2 justify-content-end p-0 bg-transparent align-items-center">
                                            <li className="breadcrumb-item"><Link to="/">Dashboard</Link></li>
                                            <li className="breadcrumb-item"><Link to="#">Content Mangement</Link></li>
                                            <li className="breadcrumb-item active" aria-current="page">Workspace</li>
                                        </ol>
                                    </nav>
                                </div>
                            </div>
                        </div>

                        <div className="workspace">
                            <div className="row mb-3 align-items-center">
                                <div className="col-md-12 text-right">
                                    <Link to="/add-workspace"><button className="btn btn-outline-warning btn-warning-cus px-4 fs-14 rounded-50">Add workspace</button></Link>
                                </div>
                            </div>
                            <div className="row">
                                {data.length > 0 && data.map((list,i) => {
                                    return (
                                        <div className="col-sm-6 col-lg-4" key={i}>
                                            <div className="card border-0 overflow-hidden shadow-sm mb-4">
                                                <div className="card-header position-relative p-0 border-0">
                                                    <img src={list.image_url} alt="Workspace" className="img-fluid" />
                                                    <div className="workspace-overlay position-absolute">
                                                        <Link to={`/edit-workspace/${list.sr_no}`} className="d-inline-block text-decoration-none text-white rounded-circle"><i className="fa fa-pencil-alt"></i></Link>
                                                        <span className="mb-0 text-warning text-decoration-none" data-toggle="modal" data-target="#remove" onClick={(e) => this.setState({
                                                            deleteid: list.sr_no
                                                        })}><i className="fa fa-trash"></i></span>

                                                    </div>
                                                </div>
                                                <div className="card-body">
                                                    <h5>{list.title}</h5>
                                                    <p className="fs-14 mb-0" dangerouslySetInnerHTML={{ __html: list.description }}></p>
                                                </div>
                                            </div>
                                        </div>)
                                })}

                            </div>
                        </div>
                        <Deletemodal
                            deleteid={this.handleDelete}
                            message={"Do you Want to delete Work space"}
                            buttonname={"Delete"}
                        />
                    </div>
                </section>
                <Footer />
            </>
        )
    }
}
const mapStateToProps = state => {
    return {
        WorkspaceData: state.GetWorkSpace
    }
}
const mapDispatchToProps = (dispatch) => {
    return {
        DispatchWorkspaceListing: () => { dispatch(WorkspaceListing()) },
        ActiondeleteWorkspace: (deleteid) => { dispatch(deleteWorkspace(deleteid)) },
    }
}
export default connect(mapStateToProps, mapDispatchToProps)(Workspace)
