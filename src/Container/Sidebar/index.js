import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import { unReadCount } from '../../Action/unReadCount';
import Activity from '../../Assets/img/activity.svg';
import ApplicationReq from '../../Assets/img/application-reqest.svg';
import ContentManger from '../../Assets/img/content.svg';
import Logo from '../../Assets/img/logo.png';
import TheDesk from '../../Assets/img/thebulbicon.png';
import DashboardIcon from '../../Assets/img/ticket.svg';
import ManageAdmin from '../../Assets/img/user.svg';
import './style.css';

class Sidebar extends Component {
    constructor() {
        super()
      const currentPage = window.location.pathname.split("/")[1];
        this.state = {
            currentselecttab: currentPage.length > 0 ? currentPage : "Dashboard",
            currentlisttab: ''
        }
    }
    componentDidMount() {
      const currentPage = window.location.pathname.split("/")[1];
      this.setState({ ...this.state, currentselecttab: currentPage })
      if(window.location.pathname.includes("content-manager")) {
        this.selectTab("Content manager")
      }
    }

    selectTab = (name) => {
        this.setState({
            currentselecttab: name
        })
    }
    selectSubtab = name => {
        this.setState({
            currentlisttab: name
        })
    }
    render() {
        const { admininfo } = this.props
        let adminrole = admininfo.adminRole
        const { currentselecttab } = this.state
        const countData = this.props.UnReadcountdata.resp
        const totalCount = countData.sumbit_an_idea + countData.request_tour + countData.incubator + countData.hire_dev + countData.schedule_visit + countData.partnership
        return (
            <>
                <aside className="side-left">
                    <div className="logo-left mb-3">
                        <Link className="text-decoration-none d-inline-block" to="/"><img src={Logo} alt="The Bulb" className="img-fluid w-75" /></Link>
                    </div>
                    <div className="navlinks">
                        <ul className="list-unstyled list-line">
                            <li onClick={() => this.selectTab("Dashboard")} className={`nav-items ${currentselecttab === "Dashboard" && "active"}`}><Link to="/" className="nav-link"><img src={DashboardIcon} alt="Dashboard" className="img-fluid" /> <span>Dashboard</span></Link></li>
                            <li onClick={() => this.selectTab("Application Request")} className={`nav-items ${currentselecttab === "Application Request" && "active"}`}><Link to="/application-request/submitIdea" className="nav-link"><img src={ApplicationReq} alt="application-request" className="img-fluid" /> <span>Application Request</span> {(totalCount) > 0 && (<span className="badge badge-danger ml-2 rounded-circle font-weight-normal">{totalCount}</span>)}</Link></li>
                            <li className={`nav-items ${currentselecttab === "Content manager" && "active"}`} onClick={() => this.selectTab("Content manager")}>
                                <Link to="/content-manager/recent" className="nav-link"><img src={ContentManger} alt="Content manager" className="img-fluid" /> <span>Content manager</span></Link>
                            </li>
                            <li onClick={() => this.selectTab("the desk")} className={`nav-items ${currentselecttab === "the desk" && "active"}`}><Link to="the-desk" className="nav-link"><img src={TheDesk} alt="the desk" className="img-fluid" /> <span>the desk</span></Link></li>
                            <li onClick={() => this.selectTab("Activity")} className={`nav-items ${currentselecttab === "Activity" && "active"}`}><Link to="activity" className="nav-link"><img src={Activity} alt="activity" className="img-fluid" /> <span>Activity</span></Link></li>
                            {adminrole === "Admin" ? '' :
                                <li onClick={() => this.selectTab("Manage Admin")} className={`nav-items ${currentselecttab === "Manage Admin" && "active"}`}><Link to="manage-admin" className="nav-link"><img src={ManageAdmin} alt="manage admin" className="img-fluid" /> <span>Manage Admin</span></Link></li>
                            }
                        </ul>
                    </div>
                </aside>
            </>
        )
    }
}

const mapStateToProps = (state) => {
    return {
        admininfo: state.login,
        UnReadcountdata: state.UnReadcount
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        GetunReadCount: () => { dispatch(unReadCount()) },
    }
}
export default connect(mapStateToProps, mapDispatchToProps)(Sidebar);
