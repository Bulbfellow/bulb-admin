import React, { useEffect, useState } from 'react'
import { connect } from 'react-redux'
import { newsletterGet } from '../../../Action/applicationRequestAction'
import { convertDatePickerTime } from '../../../Const/function'

const Newsletter = ({ newsletterdata, newsletterDispatch, call }) => {
    const [currentPage, setCurrentPage] = useState(1)
    const [postPerPage, setPostPerPage] = useState(7)


    useEffect(() => {
        const type= "bulb"
        newsletterDispatch(type)
    }, [])

    const increment = (pageNumbers) => {
        
        if (currentPage < pageNumbers.length)

            setCurrentPage(currentPage + 1)
        // this.setState({
        //         currentPage: this.state.currentPage + 1,
        //     })
    }

    const decrement = (event) => {
        
        if (currentPage > 1) {
            setCurrentPage(currentPage - 1)
        }
        // this.setState({
        //     currentPage: this.state.currentPage - 1,
        // });
    }


    let data = newsletterdata.resp
    const indexOfLastPost = currentPage * postPerPage;
    const indexOfFirstPost = indexOfLastPost - postPerPage;
    const currentPosts = data.slice(indexOfFirstPost, indexOfLastPost);

    const lastindex = currentPosts.length

    let count = indexOfFirstPost
    const pageNumbers = [];
    for (let i = 1; i <= Math.ceil(data.length / postPerPage); i++) {
        pageNumbers.push(i);
    }

    return (
        <>
            <div className="row align-items-center py-3">
                <div className="col-md-6">
                    <h5>Newsletter</h5>
                </div>
                <div className="col-md-6">
                    <div className="show-pages d-flex justify-content-end align-items-center">
                        <p className="mb-0 mr-3">Showing <span>{indexOfFirstPost + 1}</span> - <span>{indexOfFirstPost + lastindex}</span> of <span>{data.length}</span></p>
                        <div className="prev-next">
                            <div className="prev d-inline-block mr-2"><button className="btn btn-warning py-1" onClick={decrement} ><i class="fas fa-angle-left"></i></button></div>
                            <div className="next d-inline-block"><button className="btn btn-warning py-1" onClick={()=>increment(pageNumbers)}><i class="fas fa-angle-right"></i></button></div>

                        </div>
                    </div>

                </div>
            </div>
            <div className="tab-inner-body">
                <div className="table-responsive">
                    <table className="table border">
                        <thead>
                            <tr className="bg-cusgray">
                                <td className="border-top-0">S/N</td>
                                <td className="border-top-0">Email</td>
                                <td className="border-top-0">Date created</td>
                            </tr>
                        </thead>
                        {currentPosts.length > 0 && currentPosts.map((user,i) => {
                            return (
                                <tr key={i}>
                                    <td>{count = count + 1}</td>
                                    <td>
                                        <p className="mb-0">{user.email}</p>
                                    </td>
                                    <td>
                                        <p className="mb-0 date-time"><span>{convertDatePickerTime(user.rowcreateddatetime)}</span></p>
                                    </td>
                                </tr>
                            )
                        })}
                    </table>
                </div>
            </div>
        </>
    )
}
const mapStateToProps = state => {
    return {
        newsletterdata: state.newsletterGetData
    }
}
const mapDispatchToProps = (dispatch) => {
    return {
        newsletterDispatch: (type) => { dispatch(newsletterGet(type)) },
        // scheduledelete: (userid) => { dispatch(schedulevisitDelete(userid)) }
    }
}
export default connect(mapStateToProps, mapDispatchToProps)(Newsletter)