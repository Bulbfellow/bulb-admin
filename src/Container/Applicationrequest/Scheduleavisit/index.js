import React, { useEffect, useState } from 'react'
import { connect } from 'react-redux'
import { scheduleavisit, schedulevisitDelete, scheduleavisitmultiple } from '../../../Action/applicationRequestAction'
import Datatable from '../../../Component/Datatable'
import { Link } from 'react-router-dom'
import Deletemodal from '../../../Component/Deletemodal'
import Delete from '../../../Assets/img/delete-dark.svg'
import Archive from '../../../Assets/img/archive-dark.svg'
import {convertDatePickerTime} from '../../../Const/function'

const Scheduleavisit = ({ scheduleavisit, scheduleavisitdata, scheduleavisitdelete,scheduleavisitMultiplerow, call }) => {
 
    let datas = scheduleavisitdata.resp
    const [deleteid, setDeleteid] = useState('')
	const [datatype, setDatatype] = useState(99)
	const [finaldata, setFinaldata] = useState([])
	const [deleteids, setdeleteids] = useState([])
	const [singlearchive, setSinglearchive] = useState('')
	const [toggleCleared, setToggleCleared] = React.useState(false);
	const [storedeleteis, setStoredeleteis] = useState([])

	let tabledata = []
    let idss = []
    
    const singleArchive = (id) => {
        let userid = singlearchive
		let apidata = {
			ids: [userid],
			status: "3"
		}
        scheduleavisitMultiplerow(apidata)
        scheduleavisit(datatype)
        updatedData()
    }

//     sr_no: 14
// title: "Miss"
// first_name: "Onyinyechi"
// last_name: "Nwadinobi"
// mobile: "08090557104"
// email: "victoria.n@philsprints.com"
// organization: "Philsprints Limited"
// group_organization: "Philsprints Limited"
// group_size: "4"
// participant_names: "Mr Deji Oguntuga↵Mr Lanre Balogun↵Mr Babatunde Olotu↵Miss Onyinyechi Nwadinobi"
// reason: "Business networking, development and partnership."
// info: "Philsprints Limited has been in existence for almost 2 decades and corporate companies like Enyo retails supply, Ecrisson, Nokia solution network, The Hook agency just to mention a few, have been our trusted clients due to our understanding for our customer's needs, good quality and sprint service delivery"
// view_status: "1"
// status: "1"
// rowcreateddatetime: "2020-02-27T12:41:52.000Z"
// rowmodifieddatetime: "2020-02-27T12:41:52.000Z"


    const updatedData = () => {
        (datas.length > 0) && (datas.map((user) => {
            let data = {
                title : user.title,
                name: user.first_name,
                lname : user.last_name,
                date: convertDatePickerTime(user.rowcreateddatetime),
                mobile: user.mobile,
                email : user.email,
                status: user.view_status,
                Organization_name : user.group_organization,
                group_size : user.group_size,
                Participant_Names : user.participant_names, 
                Reason_for_the_tour : user.reason,
                Additional_Info : user.info,
                deleteid: user.sr_no,
                view: <Link to={`/application-request/view-schedule-visit/${user.sr_no}`}><button className="btn btn-warning text-white px-4 py-1 viewentrybtn cursor">View Entry</button></Link>,
                delete: <> <span onClick={() => setDeleteid(user.sr_no)} className="mr-3"><img src={Delete} alt="Delete" title="Delete" data-toggle="modal" data-target="#remove" className="img-fluid" /></span>
                    <span onClick={() => setSinglearchive(user.sr_no)}><img src={Archive} alt="Archive" title="Archive" data-toggle="modal" data-target="#archive" className="img-fluid" /></span></>
            }
            tabledata.push(data)
        }))
        setFinaldata(tabledata)
    }


    useEffect(() => {
        updatedData()
    }, [scheduleavisitdata.resp])

    useEffect(() => {
		idss = deleteids
	}, [deleteids])

    useEffect(() => {
        scheduleavisit(datatype)
    }, [])

    useEffect(() => {
        scheduleavisit(datatype)
    }, [datatype])

    useEffect(() => {
		var arr = []
		storedeleteis.map((user) => {
			arr.push(user.deleteid)
		})
		setdeleteids(arr)
	}, [storedeleteis])

    const setDataType = (e) => {
        setDatatype(e)
        scheduleavisit(e)
        updatedData()
    }

    const archive = (e) => {
        setDatatype(e)
        scheduleavisit(e)
        updatedData()
    }

    const deleteUserInfo = () => {
        let userid = deleteid
        scheduleavisitdelete(userid)
        setTimeout(() => {
            scheduleavisit(datatype)
        }, 500);
    }

    const handleChangeRow = (e) => {
        setStoredeleteis(e.selectedRows)
    }

    const deleteDataRow = () => {
		let apidata = {
			ids: deleteids,
			status: "0"
		}
        scheduleavisitMultiplerow(apidata)
        scheduleavisit(datatype)
        updatedData()
        setdeleteids([])
		setToggleCleared(!toggleCleared)
    }

    const archiveDataRow = () => {
		let apidata = {
			ids: deleteids,
			status: "3"
		}
        scheduleavisitMultiplerow(apidata)
        scheduleavisit(datatype)
        updatedData()
        setdeleteids([])
		setToggleCleared(!toggleCleared)
    }

    const backData = (e) => {
        setDatatype(e)
        scheduleavisit(e)
        updatedData()
    }


    return (
        <>
            {/* <p>Scheduleavisit</p>
        {userid}
        <MaterialTableDemo tabledata={tabledata} /> */}
            <Datatable
            tabledata={finaldata}
            archiveDataList={() => archive(3)}
            changeHandle={(e) => setDataType(e.target.value)}
            value={datatype}
            deleteids={idss}
            handleChangeRow={handleChangeRow}
            deleteDataRow={deleteDataRow}
            archiveDataRow={archiveDataRow}
            backData={() => backData(99)}
            toggleCleared={toggleCleared}
            datas={storedeleteis}


        />
            <Deletemodal
                deleteid={deleteUserInfo}
                message={"Are you sure you want to delete this entry"}
                buttonname={"Delete"}
            />
            <div className="modal fade" id="archive" role="dialog" aria-labelledby="removeLabel" aria-hidden="true">
			<div className="modal-dialog mt-0" role="document">
				<div className="modal-content border-0 shadow">
					<div className="modal-header border-0 px-md-4">
						<div className="headings">
							<h5 className="modal-title font-weight-bold pb-1" id="removeLabel">Archive entry</h5>
							<p className="fs-14 mb-0">Are you sure you want to archive this entry</p>
						</div>
						<button type="button" className="close outline-none" data-dismiss="modal" aria-label="Close">
							<span aria-hidden="true">&times;</span>
						</button>
					</div>
					<div className="modal-footer border-0 px-md-4 pt-2">
						<span className="mb-0 text-secondary pr-3 text-decoration-none d-inline-block cursor" data-dismiss="modal">Cancel</span>
						<span onClick={singleArchive} className="mb-0 text-warning text-decoration-none d-inline-block cursor" data-dismiss="modal">Archive</span>
					</div>
				</div>
			</div>
		</div>
        </>
    )
}
const mapStateToProps = state => {
    return {
        scheduleavisitdata: state.scheduleavisit
    }
}
const mapDispatchToProps = (dispatch) => {
    return {
        scheduleavisit: (datatype) => { dispatch(scheduleavisit(datatype)) },
        scheduleavisitdelete: (userid) => { dispatch(schedulevisitDelete(userid)) },
        scheduleavisitMultiplerow: (data) => { dispatch(scheduleavisitmultiple(data)) }
    }
}
export default connect(mapStateToProps, mapDispatchToProps)(Scheduleavisit)