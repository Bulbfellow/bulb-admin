import React, { useEffect, useState } from 'react'
import { connect } from 'react-redux'
import { applyforincubation, applyincubatorDelete, Applyincubatormultiple } from '../../../Action/applicationRequestAction'
import Datatable from '../../../Component/Datatable'
import { Link } from 'react-router-dom'
import Deletemodal from '../../../Component/Deletemodal'
import Delete from '../../../Assets/img/delete-dark.svg'
import Archive from '../../../Assets/img/archive-dark.svg'
import { convertDatePickerTime } from '../../../Const/function'

const Applyforincubation = ({ applyforincubation, ApplyincubatorMultiplerow, applyforincubationdata, call, deleteapplyincubator }) => {
  
    let datas = applyforincubationdata.resp
    const [deleteid, setDeleteid] = useState('')
	const [datatype, setDatatype] = useState(99)
	const [finaldata, setFinaldata] = useState([])
	const [deleteids, setdeleteids] = useState([])
	const [singlearchive, setSinglearchive] = useState('')
	const [toggleCleared, setToggleCleared] = React.useState(false);
    const [storedeleteis, setStoredeleteis] = useState([])
    
    useEffect(() => {
        applyforincubation(datatype)
    }, [])

    let tabledata = []
	let idss = []

    const singleArchive = () => {
        let userid = singlearchive
		let apidata = {
			ids: [userid],
			status: "3"
		}
        ApplyincubatorMultiplerow(apidata)
        applyforincubation(datatype)
        updatedData()
    }



    const updatedData = () => {
        (datas.length > 0) && (datas.map((user) => {
            let data = {
                title : user.title,
                name: user.first_name,
                lname : user.last_name,
                date: convertDatePickerTime(user.rowcreateddatetime),
                mobile: user.mobile,
                email : user.email,
                status: user.view_status,
                city : user.city,
                state : user.state,
                deleteid: user.sr_no,
                fileurl : user.file_url,
                aboutself: user.about_self,
                aboutproduct : user.about_product,                
                view: <Link to={`/application-request/view-incubation/${user.sr_no}`}><button className="btn btn-warning text-white px-4 py-1 viewentrybtn">View Entry</button></Link>,
                delete: <> <span onClick={() => setDeleteid(user.sr_no)} className="mr-3"><img src={Delete} alt="Delete" title="Delete" data-toggle="modal" data-target="#remove" className="img-fluid" /></span>
                    <span onClick={() => setSinglearchive(user.sr_no)}><img src={Archive} alt="Archive" data-toggle="modal" data-target="#archive" title="Archive" className="img-fluid" /></span></>
            }
            tabledata.push(data)
        }))
        setFinaldata(tabledata)
    }

    useEffect(() => {
        updatedData()
    }, [applyforincubationdata.resp])

    useEffect(() => {
		idss = deleteids
	}, [deleteids])


    


    useEffect(() => {
        applyforincubation(datatype)
    }, [datatype])


    useEffect(() => {
		var arr = []
		storedeleteis.map((user) => {
			arr.push(user.deleteid)
		})
		setdeleteids(arr)
	}, [storedeleteis])

    const setDataType = (e) => {
        setDatatype(e)
        applyforincubation(e)
        updatedData()
    }


    const archive = (e) => {
        setDatatype(e)
        applyforincubation(e)
        updatedData()
    }

    const deleteUserInfo = () => {
        let userid = deleteid
        deleteapplyincubator(userid)
        setTimeout(() => {
            applyforincubation(datatype)
        }, 500);
    }

    const handleChangeRow = (e) => {
        setStoredeleteis(e.selectedRows)
    }

    const deleteDataRow = () => {
		let apidata = {
			ids: deleteids,
			status: "0"
		}
        ApplyincubatorMultiplerow(apidata)
        applyforincubation(datatype)
        updatedData()
        setdeleteids([])
		setToggleCleared(!toggleCleared)
    }

    const archiveDataRow = () => {
 
		let apidata = {
			ids: deleteids,
			status: "3"
		}
        ApplyincubatorMultiplerow(apidata)
        applyforincubation(datatype)
        updatedData()
        setdeleteids([])
		setToggleCleared(!toggleCleared)
    }

    const backData = (e) => {
        setDatatype(e)
        applyforincubation(e)
        updatedData()
    }

    return (
        <>
            <Datatable
                tabledata={finaldata}
                archiveDataList={() => archive(3)}
                changeHandle={(e) => setDataType(e.target.value)}
                value={datatype}
                deleteids={idss}
                handleChangeRow={handleChangeRow}
                deleteDataRow={deleteDataRow}
                archiveDataRow={archiveDataRow}
                backData={() => backData(99)}
                toggleCleared={toggleCleared}
                datas={storedeleteis}

            />
            <Deletemodal
                deleteid={deleteUserInfo}
                message={"Are you sure you want to delete this entry"}
                buttonname={"Delete"}
            />
            	<div className="modal fade" id="archive" role="dialog" aria-labelledby="removeLabel" aria-hidden="true">
			<div className="modal-dialog mt-0" role="document">
				<div className="modal-content border-0 shadow">
					<div className="modal-header border-0 px-md-4">
						<div className="headings">
							<h5 className="modal-title font-weight-bold pb-1" id="removeLabel">Archive entry</h5>
							<p className="fs-14 mb-0">Are you sure you want to archive this entry</p>
						</div>
						<button type="button" className="close outline-none" data-dismiss="modal" aria-label="Close">
							<span aria-hidden="true">&times;</span>
						</button>
					</div>
					<div className="modal-footer border-0 px-md-4 pt-2">
						<span className="mb-0 text-secondary pr-3 text-decoration-none d-inline-block cursor" data-dismiss="modal">Cancel</span>
						<span onClick={singleArchive} className="mb-0 text-warning text-decoration-none d-inline-block cursor" data-dismiss="modal">Archive</span>
					</div>
				</div>
			</div>
		</div>
        </>
    )
}
const mapStateToProps = state => {
    return {
        applyforincubationdata: state.applyforincubation
    }
}
const mapDispatchToProps = (dispatch) => {
    return {
        applyforincubation: (datatype) => { dispatch(applyforincubation(datatype)) },
        deleteapplyincubator: (userid) => { dispatch(applyincubatorDelete(userid)) },
        ApplyincubatorMultiplerow: (data) => { dispatch(Applyincubatormultiple(data)) }
    }
}
export default connect(mapStateToProps, mapDispatchToProps)(Applyforincubation)

