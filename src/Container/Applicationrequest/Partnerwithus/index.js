import React, { useEffect, useState } from 'react'
import { connect } from 'react-redux'
import { partnership, partnershipDelete,Partnershipmultiple } from '../../../Action/applicationRequestAction'
import Datatable from '../../../Component/Datatable'
import { Link } from 'react-router-dom'
import Deletemodal from '../../../Component/Deletemodal'
import Delete from '../../../Assets/img/delete-dark.svg'
import Archive from '../../../Assets/img/archive-dark.svg'
import {convertDatePickerTime} from '../../../Const/function'

const Partnership = ({ partnership, partnershipdata, partnershipdelete, call,partnershipMultiplerow }) => {
    let datas = partnershipdata.resp
	const [deleteid, setDeleteid] = useState('')
	const [datatype, setDatatype] = useState(99)
	const [finaldata, setFinaldata] = useState([])
	const [deleteids, setdeleteids] = useState([])
	const [singlearchive, setSinglearchive] = useState('')
	const [toggleCleared, setToggleCleared] = React.useState(false);
	const [storedeleteis, setStoredeleteis] = useState([])

	let tabledata = []
    let idss = []
    
    const singleArchive = () => {
        let userid = singlearchive
		let apidata = {
			ids: [userid],
			status: "3"
		}
        partnershipMultiplerow(apidata)
        partnership(datatype)
        updatedData()
    }

//     sr_no: 4
// first_name: "Testas"
// last_name: "Jha"
// company_name: "asas"
// mobile: "1234567890"
// notes: "vasc"
// type: "NGO"
// file_url: "http://melodymantra.com/files/2020357211318-181725_new-wallpaper-hd-nature-love-beautiful-images-of.jpg"
// status: "1"
// view_status: "1"
// rowcreateddatetime: "2020-03-05T06:21:13.000Z"
// rowmodifieddatetime: "2020-03-05T06:21:13.000Z"

    const updatedData = () => {
        (datas.length > 0) && (datas.map((user) => {
            let data = {
                name: user.first_name,
                lastname: user.last_name,
                date: convertDatePickerTime(user.rowcreateddatetime),
                companyname: user.company_name,
                mobile: user.mobile,
                status: user.view_status,
                deleteid: user.sr_no,
                notes:user.notes,
                file_url:user.file_url,
                view: <Link to={`/application-request/view-partnership/${user.sr_no}`}><button className="btn btn-warning text-white px-4 py-1 viewentrybtn cursor">View Entry</button></Link>,
                delete: <> <span onClick={() => setDeleteid(user.sr_no)} className="mr-3"><img src={Delete} alt="Delete" title="Delete" data-toggle="modal" data-target="#remove" className="img-fluid" /></span>
                    <span onClick={() => setSinglearchive(user.sr_no)}><img src={Archive} alt="Archive" data-toggle="modal" data-target="#archive" title="Archive" className="img-fluid" /></span></>
            }
            tabledata.push(data)
        }))
        setFinaldata(tabledata)
    }

    useEffect(() => {
        updatedData()
    }, [partnershipdata.resp])

    useEffect(() => {
		idss = deleteids
	}, [deleteids])

    useEffect(() => {
        partnership(datatype)
    }, [])

    useEffect(() => {
        partnership(datatype)
    }, [datatype])

    useEffect(() => {
		var arr = []
		storedeleteis.map((user) => {
			arr.push(user.deleteid)
		})
		setdeleteids(arr)
	}, [storedeleteis])
    
    const setDataType = (e) => {
        setDatatype(e)
        partnership(e)
        updatedData()
    }

    const archive = (e) => {
        setDatatype(e)
        partnership(e)
        updatedData()
    }
 
    const deleteUserInfo = () => {
        let userid = deleteid
        partnershipdelete(userid)
        setTimeout(() => {
            partnership(datatype)
        }, 500);
    }
  
    const handleChangeRow = (e) => {
        setStoredeleteis(e.selectedRows)
    }

    const deleteDataRow = () => {
        alert("deleteDataRow")
       
		let apidata = {
			ids: deleteids,
			status: "0"
		}
        partnershipMultiplerow(apidata)
        partnership(datatype)
        updatedData()
        setdeleteids([])
		setToggleCleared(!toggleCleared)
    }

    const archiveDataRow = () => {
        alert("archiveDataRow")
		let apidata = {
			ids: deleteids,
			status: "3"
		}
        partnershipMultiplerow(apidata)
        partnership(datatype)
        updatedData()
        setdeleteids([])
		setToggleCleared(!toggleCleared)
    }

    const backData = (e) => {
        setDatatype(e)
        partnership(e)
        updatedData()
    }
    return <> 
     <Datatable
            tabledata={finaldata}
            archiveDataList={() => archive(3)}
            changeHandle={(e) => setDataType(e.target.value)}
            value={datatype}
            handleChangeRow={handleChangeRow}
            deleteDataRow={deleteDataRow}
            archiveDataRow={archiveDataRow}
            backData={() => backData(99)}
            toggleCleared={toggleCleared}
            deleteids={idss}
            datas={storedeleteis}

        />
        <Deletemodal
            deleteid={deleteUserInfo}
            message={"Are you sure you want to delete this entry"}
            buttonname={"Delete"}
        />
        <div className="modal fade" id="archive" role="dialog" aria-labelledby="removeLabel" aria-hidden="true">
			<div className="modal-dialog mt-0" role="document">
				<div className="modal-content border-0 shadow">
					<div className="modal-header border-0 px-md-4">
						<div className="headings">
							<h5 className="modal-title font-weight-bold pb-1" id="removeLabel">Archive entry</h5>
							<p className="fs-14 mb-0">Are you sure you want to archive this entry</p>
						</div>
						<button type="button" className="close outline-none" data-dismiss="modal" aria-label="Close">
							<span aria-hidden="true">&times;</span>
						</button>
					</div>
					<div className="modal-footer border-0 px-md-4 pt-2">
						<span className="mb-0 text-secondary pr-3 text-decoration-none d-inline-block cursor" data-dismiss="modal">Cancel</span>
						<span onClick={singleArchive} className="mb-0 text-warning text-decoration-none d-inline-block cursor" data-dismiss="modal">Archive</span>
					</div>
				</div>
			</div>
		</div>
    </>

}
const mapStateToProps = state => {
    return {
        partnershipdata: state.partnership
    }
}
const mapDispatchToProps = (dispatch) => {
    return {
        partnership: (datatype) => { dispatch(partnership(datatype)) },
        partnershipdelete: (userid) => { dispatch(partnershipDelete(userid)) },
        partnershipMultiplerow: (data) => { dispatch(Partnershipmultiple(data)) }
    }
}
export default connect(mapStateToProps, mapDispatchToProps)(Partnership)
