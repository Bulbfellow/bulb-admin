import React, { useEffect, useState } from 'react'
import { connect } from 'react-redux'
import { CareersFormfromUserListing, CareersFormdatafromUserDelete, Careermultiple } from '../../../Action/careers'
import Datatable from '../../../Component/Datatable'
import { Link } from 'react-router-dom'
import Deletemodal from '../../../Component/Deletemodal'
import Delete from '../../../Assets/img/delete-dark.svg'
import Archive from '../../../Assets/img/archive-dark.svg'
import { convertDatePickerTime } from '../../../Const/function'

const Careerformdata = ({ Carrearformdata, carrerFormdataFromuser, CarrerFormdataFromUserDelete, CarrerFormdataMultiple }) => {
	let datas = Carrearformdata.resp
	console.log("datas", datas)
	const [deleteid, setDeleteid] = useState('')
	const [datatype, setDatatype] = useState(99)
	const [finaldata, setFinaldata] = useState([])
	const [deleteids, setdeleteids] = useState([])
	const [singlearchive, setSinglearchive] = useState('')
	const [toggleCleared, setToggleCleared] = React.useState(false);
	const [storedeleteis, setStoredeleteis] = useState([])

	let tabledata = []
	let idss = []

	const singleArchive = () => {
		let userid = singlearchive
		let apidata = {
			ids: [userid],
			status: "3"
		}
		CarrerFormdataMultiple(apidata)
		carrerFormdataFromuser(datatype)
		updatedData()
	}

	// sr_no: 1453
	// jobpost_name: "FRONTEND REACT DEVELOPER"
	// title: "Mr."
	// first_name: "Oseagwina"
	// last_name: "Akhenisi"
	// mobile: "08084253886"
	// email: "Akhenisio@yahoo.com"
	// info: "Dear sir/madam↵My career as a software developer started at my room in college, but has grown to reach a global extent.↵I have always had a passion to be surrounded by innovative and creative people which is what your company fosters in its employees.↵I love turning tech ideas into profitable reality. I love coding but that is my aim."
	// cv_url: "http://melodymantra.com/files/20203201828osecvv copy.pdf"
	// website: "https://peaceful-goldwasser-3f2cda.netlify.com"
	// linkedin: "https://www.linkedin.com/in/ose-akhenisi-698956196/"
	// twitter: null
	// github: "https://github.com/primeos1"
	// behance: null
	// dribbble: null
	// status: "1"
	// view_status: "1"
	// rowcreateddatetime: "2020-03-20T00:13:47.000Z"
	// rowmodifieddatetime: "2020-03-20T00:13:47.000Z"

	const updatedData = () => {
		(datas.length > 0) && (datas.map((user) => {
			let data = {
				jobpost_name: user.jobpost_name,
				title: user.jobpost_name,
				name: user.first_name,
				nametitle:user.title,
				lname: user.last_name,
				email: user.email,
				date: convertDatePickerTime(user.rowcreateddatetime),
				mobile: user.mobile,
				status: user.view_status,
				deleteid: user.sr_no,
				cv_url: user.cv_url,
				Tell_us_why_you_want_to_join_our_team: user.info,
				website: user.website,
				linkedin: user.linkedin,
				twitter: user.twitter,
				github: user.github,
				behance: user.behance,
				dribbble: user.dribbble,
				view: <Link to={`/careers/view-career-request/${user.sr_no}`}><button className="btn btn-warning text-white px-4 py-1 viewentrybtn cursor">View Entry</button></Link>,
				delete: <> <span onClick={() => setDeleteid(user.sr_no)} className="mr-3"><img src={Delete} alt="Delete" title="Delete" data-toggle="modal" data-target="#remove" className="img-fluid" /></span>
					<span onClick={() => setSinglearchive(user.sr_no)}><img src={Archive} alt="Archive" title="Archive" data-toggle="modal" data-target="#archive" className="img-fluid" /></span></>
			}
			tabledata.push(data)
		}))
		setFinaldata(tabledata)
	}

	useEffect(() => {
		updatedData()
	}, [Carrearformdata.resp])

	useEffect(() => {
		idss = deleteids
	}, [deleteids])

	useEffect(() => {
		carrerFormdataFromuser(datatype)
	}, [])

	useEffect(() => {
		carrerFormdataFromuser(datatype)
	}, [datatype])

	useEffect(() => {
		var arr = []
		storedeleteis.map((user) => {
			arr.push(user.deleteid)
		})
		setdeleteids(arr)
	}, [storedeleteis])

	const setDataType = (e) => {
		setDatatype(e)
		carrerFormdataFromuser(e)
		updatedData()
	}

	const archive = (e) => {
		setDatatype(e)
		carrerFormdataFromuser(e)
		updatedData()
	}

	const deleteUserInfo = () => {
		let userid = deleteid
		CarrerFormdataFromUserDelete(userid)
		setTimeout(() => {
			carrerFormdataFromuser(datatype)
		}, 500);
	}

	const handleChangeRow = (e) => {
		setStoredeleteis(e.selectedRows)
	}

	function deleteDataRow() {
		let apidata = {
			ids: deleteids,
			status: "0"
		}

		CarrerFormdataMultiple(apidata)
		carrerFormdataFromuser(datatype)
		updatedData()
		setdeleteids([])
		setToggleCleared(!toggleCleared)
	}

	const archiveData = () => {
		let apidata = {
			ids: deleteids,
			status: "3"
		}

		CarrerFormdataMultiple(apidata)
		carrerFormdataFromuser(datatype)
		updatedData()
		setdeleteids([])
		setToggleCleared(!toggleCleared)

	}

	const backData = (e) => {
		setDatatype(e)
		carrerFormdataFromuser(e)
		updatedData()
	}

	return <>

		<Datatable
			tabledata={finaldata}
			archiveDataList={() => archive(3)}
			changeHandle={(e) => setDataType(e.target.value)}
			value={datatype}
			deleteids={idss}
			handleChangeRow={handleChangeRow}
			deleteDataRow={deleteDataRow}
			archiveDataRow={archiveData}
			backData={() => backData(99)}
			toggleCleared={toggleCleared}
			datas={storedeleteis}

		/>
		<Deletemodal
			deleteid={deleteUserInfo}
			message={"Are you sure you want to delete this entry"}
			buttonname={"Delete"}
		/>
		<div className="modal fade" id="archive" role="dialog" aria-labelledby="removeLabel" aria-hidden="true">
			<div className="modal-dialog mt-0" role="document">
				<div className="modal-content border-0 shadow">
					<div className="modal-header border-0 px-md-4">
						<div className="headings">
							<h5 className="modal-title font-weight-bold pb-1" id="removeLabel">Archive entry</h5>
							<p className="fs-14 mb-0">Are you sure you want to archive this entry</p>
						</div>
						<button type="button" className="close outline-none" data-dismiss="modal" aria-label="Close">
							<span aria-hidden="true">&times;</span>
						</button>
					</div>
					<div className="modal-footer border-0 px-md-4 pt-2">
						<span className="mb-0 text-secondary pr-3 text-decoration-none d-inline-block cursor" data-dismiss="modal">Cancel</span>
						<span onClick={singleArchive} className="mb-0 text-warning text-decoration-none d-inline-block cursor" data-dismiss="modal">Archive</span>
					</div>
				</div>
			</div>
		</div>
	</>

}
const mapStateToProps = state => {
	return {
		Carrearformdata: state.Carrerformlist
	}
}
const mapDispatchToProps = (dispatch) => {
	return {
		carrerFormdataFromuser: (datatype) => { dispatch(CareersFormfromUserListing(datatype)) },
		CarrerFormdataFromUserDelete: (userid) => { dispatch(CareersFormdatafromUserDelete(userid)) },
		CarrerFormdataMultiple: (data) => { dispatch(Careermultiple(data)) }

	}
}
export default connect(mapStateToProps, mapDispatchToProps)(Careerformdata)