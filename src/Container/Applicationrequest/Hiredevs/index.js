import React, { useEffect, useState } from 'react'
import { connect } from 'react-redux'
import { hireadeveloper, hireadeveloperDelete, HireaDevelopermultiple } from '../../../Action/applicationRequestAction'
import Datatable from '../../../Component/Datatable'
import Deletemodal from '../../../Component/Deletemodal'
import Delete from '../../../Assets/img/delete-dark.svg'
import Archive from '../../../Assets/img/archive-dark.svg'
import { convertDatePickerTime } from '../../../Const/function'

import {
	BrowserRouter as Router,
	
	Link,
	useRouteMatch
} from "react-router-dom";

const Hiredev = ({ hireadeveloper, hireadeveloperdata, hiredeveloperdelete, hiredeveloperMultiplerow }) => {
	let datas = hireadeveloperdata.resp
	const [deleteid, setDeleteid] = useState('')
	const [datatype, setDatatype] = useState(99)
	const [finaldata, setFinaldata] = useState([])
	const [deleteids, setdeleteids] = useState([])
	const [singlearchive, setSinglearchive] = useState('')
	const [toggleCleared, setToggleCleared] = React.useState(false);
	const [storedeleteis, setStoredeleteis] = useState([])

	let tabledata = []
	let idss = []


	const singleArchive = () => {
		let userid = singlearchive
		let apidata = {
			ids: [userid],
			status: "3"
		}
		hiredeveloperMultiplerow(apidata)
		hireadeveloper(datatype)
		updatedData()
	}

	const updatedData = () => {
		(datas.length > 0) && (datas.map((user) => {
			let data = {
				name: user.first_name,
				lastname: user.last_name,
				mobile: user.mobile,
				companyname: user.company_name,
				email: user.work_email,
				date: convertDatePickerTime(user.rowcreateddatetime),
				mobile: user.mobile,
				status: user.view_status,
				deleteid: user.sr_no,
				state: user.state,
				country: user.country,
				// view: <Link to={`/view-hiredeveloper/${user.sr_no}`}><button className="btn btn-warning text-white px-4 py-1 viewentrybtn cursor">View Entry</button></Link>,
				view: <Link to={`/application-request/view-hiredeveloper/${user.sr_no}`}><button className="btn btn-warning text-white px-4 py-1 viewentrybtn cursor">View Entry</button></Link>,

				delete: <> <span onClick={() => setDeleteid(user.sr_no)} className="mr-3"><img src={Delete} alt="Delete" title="Delete" data-toggle="modal" data-target="#remove" className="img-fluid" /></span>
					<span onClick={() => setSinglearchive(user.sr_no)}><img src={Archive} alt="Archive" title="Archive" data-toggle="modal" data-target="#archive" className="img-fluid" /></span></>
			}
			tabledata.push(data)
		}))
		setFinaldata(tabledata)
	}
	// <Link to={`${url}/rendering`}>Rendering with React</Link>

	useEffect(() => {
		updatedData()
	}, [hireadeveloperdata.resp])

	useEffect(() => {
		idss = deleteids
	}, [deleteids])

	useEffect(() => {
		hireadeveloper(datatype)
	}, [])

	useEffect(() => {
		hireadeveloper(datatype)
	}, [datatype])

	useEffect(() => {
		var arr = []
		storedeleteis.map((user) => {
			arr.push(user.deleteid)
		})
		setdeleteids(arr)
	}, [storedeleteis])


	const setDataType = (e) => {
		setDatatype(e)
		hireadeveloper(e)
		updatedData()
	}

	const archive = (e) => {
		setDatatype(e)
		hireadeveloper(e)
		updatedData()
	}

	const deleteUserInfo = () => {
		let userid = deleteid
		hiredeveloperdelete(userid)
		setTimeout(() => {
			hireadeveloper(datatype)
		}, 500);
	}

	const handleChangeRow = (e) => {
		setStoredeleteis(e.selectedRows)
	}

	function deleteDataRow() {
		let apidata = {
			ids: deleteids,
			status: "0"
		}
		hiredeveloperMultiplerow(apidata)
		hireadeveloper(datatype)
		updatedData()
		setdeleteids([])
		setToggleCleared(!toggleCleared)
	}

	const archiveData = () => {
		let apidata = {
			ids: deleteids,
			status: "3"
		}

		hiredeveloperMultiplerow(apidata)
		hireadeveloper(datatype)
		updatedData()
		setdeleteids([])
		setToggleCleared(!toggleCleared)

	}

	const backData = (e) => {
		setDatatype(e)
		hireadeveloper(e)
		updatedData()
	}

	return <>
		{/* <Router>
			<Switch>
				<Route exact path={path}>
				{console.log("dddddddd")}llo
			</Route>
				<Route exact path={`${path}/view-hiredeveloper/:id`} component={HireDevs}/>
			</Switch>
		</Router> */}

		<Datatable
			tabledata={finaldata}
			archiveDataList={() => archive(3)}
			changeHandle={(e) => setDataType(e.target.value)}
			value={datatype}
			deleteids={idss}
			handleChangeRow={handleChangeRow}
			deleteDataRow={deleteDataRow}
			archiveDataRow={archiveData}
			backData={() => backData(99)}
			toggleCleared={toggleCleared}
			datas={storedeleteis}
		/>
		<Deletemodal
			deleteid={deleteUserInfo}
			message={"Are you sure you want to delete this entry"}
			buttonname={"Delete"}
		/>
		<div className="modal fade" id="archive" role="dialog" aria-labelledby="removeLabel" aria-hidden="true">
			<div className="modal-dialog mt-0" role="document">
				<div className="modal-content border-0 shadow">
					<div className="modal-header border-0 px-md-4">
						<div className="headings">
							<h5 className="modal-title font-weight-bold pb-1" id="removeLabel">Archive entry</h5>
							<p className="fs-14 mb-0">Are you sure you want to archive this entry</p>
						</div>
						<button type="button" className="close outline-none" data-dismiss="modal" aria-label="Close">
							<span aria-hidden="true">&times;</span>
						</button>
					</div>
					<div className="modal-footer border-0 px-md-4 pt-2">
						<span className="mb-0 text-secondary pr-3 text-decoration-none d-inline-block cursor" data-dismiss="modal">Cancel</span>
						<span onClick={singleArchive} className="mb-0 text-warning text-decoration-none d-inline-block cursor" data-dismiss="modal">Archive</span>
					</div>
				</div>
			</div>
		</div>
	</>

}
const mapStateToProps = state => {
	return {
		hireadeveloperdata: state.hireadeveloper
	}
}
const mapDispatchToProps = (dispatch) => {
	return {
		hireadeveloper: (datatype) => { dispatch(hireadeveloper(datatype)) },
		hiredeveloperdelete: (userid) => { dispatch(hireadeveloperDelete(userid)) },
		hiredeveloperMultiplerow: (data) => { dispatch(HireaDevelopermultiple(data)) }
	}
}
export default connect(mapStateToProps, mapDispatchToProps)(Hiredev)