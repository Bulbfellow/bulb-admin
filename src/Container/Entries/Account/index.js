import React, { Component } from 'react'
import { Link } from 'react-router-dom'
import Footer from '../../../Container/Footer'
import '../style.css'
import Axios from 'axios'
import {Api} from '../../../Const/Api'

export default class Account extends Component {
    constructor() {
        super();
        this.state = {
            first_name: '',
            last_name: '',
            email: '',
            mobile: '',
            role: ''
        }
    }
    componentDidMount() {
        Axios.get(Api.BaseUrl+`/api/admin/${this.props.match.params.id}`, {
            'headers': { 'Authorization': localStorage.getItem('adminToken') },

        }).then((resp) => {
            const adminData = resp.data.data.list
            this.setState(adminData)

        })
    }

    render() {
        const adminData = this.state
        return (
            <>
                <section className="wrapper px-4 ">
                    <div className="container">
                        <div className="page-title mb-4">
                            <div className="row align-items-center">
                                <div className="col-md-5">
                                    <div className="page-title-inner">
                                        <h4>Account</h4>
                                    </div>
                                </div>
                                <div className="col-md-7">
                                    <nav aria-label="breadcrumb">
                                        <ol className="breadcrumb mb-2 justify-content-end p-0 bg-transparent align-items-center">
                                            <li className="breadcrumb-item"><Link to="/">Dashboard</Link></li>
                                            <li className="breadcrumb-item active" aria-current="page">Account</li>
                                        </ol>
                                    </nav>
                                </div>
                            </div>
                        </div>
                        
                        <div className="view-entry">
                            <div className="row mb-3 align-items-center">
                                <div className="col-md-12 text-right">
                                    <Link to="/change-password"><button className="btn btn-outline-warning btn-warning-cus px-4 fs-14 rounded-50">Change password</button></Link>
                                </div>
                            </div>
                            <div className="card border-0  px-md-4 shadow-sm">
                                <div className="card-body hire-devs px-md-2">
                                    <div className="row pt-3">
                                        <div className="col-md-5">
                                            <div className="form-group">
                                                <label for="fname">First name</label>
                                                <input type="text" className="form-control" id="fname" value={adminData.first_name}/>
                                            </div>
                                        </div>
                                        <div className="col-md-5">
                                            <div className="form-group">
                                                <label for="lname">Last name</label>
                                                <input type="text" className="form-control" id="lname" value={adminData.last_name} />
                                            </div>
                                        </div>
                                    </div>
                                    <div className="row">
                                        <div className="col-md-5">
                                            <div className="form-group">
                                                <label for="emailaddress">Email address</label>
                                                <input type="email" className="form-control" id="emailaddress" value={adminData.email} />
                                            </div>
                                        </div>
                                        <div className="col-md-5">
                                            <div className="form-group">
                                                <label for="mobilenumber">Mobile number</label>
                                                <input type="number" className="form-control" id="mobilenumber" value={adminData.mobile} />
                                            </div>
                                        </div>
                                    </div>
                                    <div className="row mb-5">
                                        <div className="col-md-5">
                                            <div className="form-group">
                                                <label for="role">Role</label>
                                                <input type="text" className="form-control" id="role" value={adminData.role} />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
                <Footer />
            </>
        )
    }
}
