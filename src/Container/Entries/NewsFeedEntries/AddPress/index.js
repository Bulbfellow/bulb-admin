import React from 'react'
import { Link } from 'react-router-dom'
import Footer from '../../../Footer'
import AddImage from '../../../../Assets/img/addimage-icon.png'
import '../../style.css'
import Datepicker from '../../../../Component/Datepicker'
// import { addPress } from '../../../../Action/newsfeed'
import { connect } from 'react-redux'
import Axios from 'axios'
import { Uploadfile } from '../../../../Action/commonAction'
import { Api } from '../../../../Const/Api'
import { convertdate } from '../../../../Const/function'
import { Editor } from 'react-draft-wysiwyg';
import { EditorState } from 'draft-js';
import 'react-draft-wysiwyg/dist/react-draft-wysiwyg.css';
import { stateToHTML } from "draft-js-export-html";
import Quicknotification from '../../../../Component/Quicknotification'

class AddPress extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            title: '',
            desc: '',
            imgUrl: '',
            image: '',
            selectedDate: new Date(),
            editorState: EditorState.createEmpty(),
            status: null,
            showpopup: false,
            errors: {},
            respMessage: ''
        }
    }


    handleDateChange = date => {
        this.setState({ selectedDate: date });
       
    };



    imageupload = (e) => {
        // this.setState({ image: e.target.files[0] })
        // const formData = new FormData()
        // formData.append(
        //     'file',
        //     e.target.files[0]
        // )
        // Axios.post(Api.BaseUrl+'/api/file-upload', formData,
        //     console.log("Image ", formData))
        //     .then(res => {
        //         const Url = res.data.data.image_url
        //         this.setState({ imgUrl: Url })
        //     })
        this.setState({
            fileuploadloader: true
        })
        this.props.FileUploadDispatch(e.target.files[0])
    }

    componentDidUpdate(prevProp) {
        if (prevProp.FileUploadState.loading !== this.props.FileUploadState.loading) {
            this.setState({
                imgUrl: this.props.FileUploadState.resp,
                fileuploadloader: this.props.FileUploadState.loading,
                fileuploadloadermsg: this.props.FileUploadState.error
            })
        }
    }

    submitInfo = (e) => {
        e.preventDefault()
        if (this.handleValidation()) {
            let date = convertdate(this.state.selectedDate)
            const data = {
                title: this.state.title,
                description: this.state.editorContentHtml,
                image_url: this.state.imgUrl,
                date: date
            }
            Axios.post(Api.BaseUrl + "/api/presslist", data,
                { 'headers': { 'Authorization': localStorage.getItem('adminToken') } })
                .then(resp => {
                    this.setState({
                        status: resp.data.status,
                        respMessage: resp.data.message,
                        showpopup: true
                    }, () => {
                        if (this.state.status === 1) {
                            this.setState({
                                title: '',
                                desc: '',
                                imgUrl: '',
                                image: '',
                                selectedDate: new Date(),
                                editorState: EditorState.createEmpty(),
                            })
                        }
                    })
                })
        }
    }

    onEditorStateChange = (editorState) => {
        this.setState({
            editorState: editorState,
            editorContentHtml: stateToHTML(editorState.getCurrentContent())
        });
    };

    handleValidation = () => {
        let imgUrl = this.state.imgUrl;
        let editorContentHtml = this.state.editorContentHtml;
        let errors = {};
        let formIsValid = true;

        if (imgUrl === '') {
            formIsValid = false;
            errors["imgUrl"] = "Please Upload Image"
        }

        if (editorContentHtml === null) {
            formIsValid = false;
            errors["editorState"] = "This Field require"
        }
        this.setState({ errors: errors });
        return formIsValid;
    }

    render() {
        const {  showpopup, respMessage } = this.state;
        return (
            <>
                <section className="wrapper px-4 ">
                    <div className="container">
                        <div className="page-title mb-4">
                            <div className="row align-items-center">
                                <div className="col-md-5">
                                    <div className="page-title-inner">
                                        <h4>Add Press</h4>
                                    </div>
                                </div>
                                <div className="col-md-7">
                                    <nav aria-label="breadcrumb">
                                        <ol className="breadcrumb mb-2 justify-content-end p-0 bg-transparent align-items-center">
                                            <li className="breadcrumb-item"><Link to="/">Dashboard</Link></li>
                                            <li className="breadcrumb-item"><Link to="#">Content Mangement</Link></li>
                                            <li className="breadcrumb-item"><Link to="/news-feeds">Newsfeed</Link></li>
                                            <li className="breadcrumb-item"><Link to="/news-feeds/#press-tab">Press</Link></li>
                                            <li className="breadcrumb-item active" aria-current="page">Add Press</li>
                                        </ol>
                                    </nav>
                                </div>
                            </div>
                        </div>

                        <div className="view-entry">
                            <div className="row mb-3 align-items-center">
                                <div className="col-md-6">

                                </div>
                                <div className="col-md-6 text-center text-md-right">
                                    <Link to="/news-feeds" ><button className="btn btn-outline-warning px-4 fs-14 rounded-50">Back to press</button></Link>
                                </div>
                            </div>
                            <div className="card border-0  px-md-4 shadow-sm">
                                <form onSubmit={this.submitInfo}>
                                    <div className="card-body hire-devs px-md-2">
                                        <div className="row">
                                            <div className="col-md-8">
                                                <div className="form-group pb-4">
                                                    <label for="pressTitle">Title</label>
                                                    <input type="text" className="form-control" id="pressTitle" name={"title"} value={this.state.title}
                                                        onChange={(e) => this.setState({ title: e.target.value })} required />
                                                </div>
                                            </div>
                                            <div className="col-md-8">
                                                <div className="form-group">
                                                    <label>Image</label>
                                                    <input type="file" className="form-control d-none" id="addpressimage" onChange={this.imageupload} />

                                                    <div className="add-image">
                                                        {this.state.imgUrl === '' ?
                                                            <label className="text-center w-100" for="addpressimage">
                                                                <img src={AddImage} alt="Add Image icon" className="img-fluid icon" />
                                                                <h5 className="fs-18 pt-2">Click to add image</h5>
                                                                <p className="text-secondary mb-0 fs-14">Jpeg or PNG not larger than 10MB</p>
                                                            </label>
                                                            :
                                                            <label className="text-center w-100" for="addpressimage">
                                                                <div className="uplaodedImage">
                                                                    <img src={this.state.imgUrl} alt="Events image" className="img-fluid" />
                                                                </div>

                                                            </label>

                                                        }
                                                        <span style={{ color: "red" }}>{this.state.errors["imgUrl"]}</span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div className="row">
                                            <div className="col-md-8">
                                                <div className="form-group">
                                                    <label for="pressdescription">Description</label>
                                                    <Editor
                                                        editorState={this.state.editorState}
                                                        wrapperClassName="demo-wrapper"
                                                        editorClassName="demo-editor"
                                                        onEditorStateChange={this.onEditorStateChange}
                                                        ref="content"
                                                    />
                                                    <span style={{ color: "red" }}>{this.state.errors["editorState"]}</span>
                                                </div>
                                            </div>
                                        </div>
                                        <div className="row mb-5">
                                            <div className="col-md-8">
                                                <div className="form-group">
                                                    <label for="pressdescription">Date</label>
                                                    <Datepicker handleDateChange={this.handleDateChange}
                                                        value={this.state.selectedDate}
                                                    />
                                                </div>
                                            </div>
                                        </div>
                                        <div className="row">
                                            <div className="col-md-12">
                                                <div className="form-group text-center">
                                                    <button type="submit" className="btn btn-warning px-4 text-white" >Post Press</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </section>
                <Quicknotification
                    show={showpopup}
                    message={respMessage}
                    closetab={() => this.setState({ showpopup: false })}
                    closesecond={10000}
                    timeout={true} />
                <Footer />
            </>
        )
    }
}


const mapStateToProps = state => {
    return {
        // AddPressState: state.NewsfeedPressAdd,
        FileUploadState: state.FileuploadReducer,
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        // AddPressDispatch: (data) => { dispatch(addPress(data)) },
        FileUploadDispatch: (file) => { dispatch(Uploadfile(file)) }
    }
}
export default connect(mapStateToProps, mapDispatchToProps)(AddPress)