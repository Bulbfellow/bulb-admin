import React, { Component } from 'react'
import { Link } from 'react-router-dom'
import Footer from '../../../Footer'
import AddImage from '../../../../Assets/img/addimage-icon.png'
import '../../style.css'
// import { addEvent } from '../../../../Action/newsfeed'
import Timepicker from '../../../../Component/Timepicker'
import Datepicker from '../../../../Component/Datepicker'
import Axios from 'axios'
import { Uploadfile } from '../../../../Action/commonAction'
import { Api } from '../../../../Const/Api'
import { connect } from 'react-redux'
import { Editor } from 'react-draft-wysiwyg';
import { EditorState } from 'draft-js';
import 'react-draft-wysiwyg/dist/react-draft-wysiwyg.css';
import { stateToHTML } from "draft-js-export-html";
import CircularProgress from '@material-ui/core/CircularProgress';
import Quicknotification from '../../../../Component/Quicknotification'

class AddEvents extends Component {
    _isMounted = false;
    constructor(props) {
        super(props);
        this.state = {
            title: '',
            image: '',
            imgUrl: '',
            description: '',
            venue: '',
            link: '',
            postvenue: 'ALL',
            eventtype: 'All',
            selectedDate: new Date(),
            selectedTime: new Date(),
            editorState: EditorState.createEmpty(),
            typelist: [],
            status: null,
            showpopup: false,
            errors : {}
        }
    }

    componentDidMount() {
        this._isMounted = true;
        if(this._isMounted){
        Axios.get(Api.BaseUrl +"/api/eventtype-list")
            .then(res => {
                const typelistData = res.data.data.list
                this.setState({ typelist: typelistData })
               
            })
        }
    }



    componentDidUpdate(prevPrps) {
        // if (prevPrps.AddEventDispatchState.resp !== this.props.AddEventDispatchState.resp) {
        //     this.setState({
        //         status: this.props.AddEventDispatchState.resp,
        //         showpopup: true,
        //         respMessage : this.props.AddEventDispatchState.resp.data.message,
        //     })
        // }
        if (prevPrps.FileUploadState.loading !== this.props.FileUploadState.loading) {
            this.setState({
                imgUrl: this.props.FileUploadState.resp,
                fileuploadloader: this.props.FileUploadState.loading,
                fileuploadloadermsg: this.props.FileUploadState.error
            })
        }
    }
   
    handleChange = (e) => {
        this.setState({
            [e.target.name]: e.target.value
        })
    }

    handleDateChange = date => {
        this.setState({ selectedDate: date });
    };
    handleTimeChange = time => {
        this.setState({ selectedTime: time });
    };
    onEditorStateChange = (editorState) => {
        this.setState({
            editorState: editorState,
            editorContentHtml: stateToHTML(editorState.getCurrentContent()),
        });
    };

    submitInfo = (e) => {
        e.preventDefault();
        if(this.handleValidation()){
        let data = {
            title: this.state.title,
            image_url: this.state.imgUrl,
            description: this.state.editorContentHtml,
            venue: this.state.venue,
            link: this.state.link,
            event_post: this.state.postvenue,
            event_type: this.state.eventtype,
            date: this.state.selectedDate,
            time: this.state.selectedTime
        }
        // this.props.AddEventDispatch(data)
        Axios.post(Api.BaseUrl + "/api/events", data,
            { 'headers': { 'Authorization': localStorage.getItem('adminToken') } })
            .then(resp => {
                console.log(resp.data.status)
                this.setState({
                    status: resp.data.status,
                    respMessage : resp.data.message,
                    showpopup : true
                }, () => {
                    if (this.state.status == 1) {
                        this.setState({
                            title: '',
                            image: '',
                            imgUrl: '',
                            description: '',
                            venue: '',
                            link: '',
                            postvenue: " ",
                            eventtype: '',
                            selectedDate: new Date(),
                            selectedTime: new Date(),
                            editorState: EditorState.createEmpty(),
                        })
                    }
                })
            })
        }
    }

    imageUpload = (e) => {
        this.setState({
            fileuploadloader: true
        })
        this.props.FileUploadDispatch(e.target.files[0])
    }


    handleValidation=()=>{
        let imgUrl = this.state.imgUrl;
        let editorContentHtml= this.state.editorContentHtml;
        let errors = {};
        let formIsValid = true;
        
        if(imgUrl === ''){
            formIsValid = false;
            errors["imgUrl"] = "Please Upload Image"
        }

        if(editorContentHtml === null){
            formIsValid = false;
            errors["editorState"] = "This Field require"
        }
        this.setState({errors: errors});
       return formIsValid;
    }

    render() {
        const { typelist, showpopup,respMessage } = this.state;
        return (
            <>
                <section className="wrapper px-4 ">
                    <div className="container">
                        <div className="page-title mb-4">
                            <div className="row align-items-center">
                                <div className="col-md-5">
                                    <div className="page-title-inner">
                                        <h4>Add Event</h4>
                                    </div>
                                </div>
                                <div className="col-md-7">
                                    <nav aria-label="breadcrumb">
                                        <ol className="breadcrumb mb-2 justify-content-end p-0 bg-transparent align-items-center">
                                            <li className="breadcrumb-item"><Link to="/">Dashboard</Link></li>
                                            <li className="breadcrumb-item"><Link to="#">Content Mangement</Link></li>
                                            <li className="breadcrumb-item"><Link to="/news-feeds">Newsfeed</Link></li>
                                            <li className="breadcrumb-item"><Link to="#">Event</Link></li>
                                            <li className="breadcrumb-item active" aria-current="page">Add Events</li>
                                        </ol>
                                    </nav>
                                </div>
                            </div>
                        </div>

                        <div className="view-entry">
                            <div className="row mb-3 align-items-center">
                                <div className="col-md-6">

                                </div>
                                <div className="col-md-6 text-center text-md-right">
                                    <Link to="/news-feeds"><button className="btn btn-outline-warning px-4 fs-14 rounded-50">Back to Events</button></Link>
                                </div>
                            </div>
                            <div className="card border-0  px-md-4 shadow-sm">
                            <form onSubmit={this.submitInfo}>
                                <div className="card-body hire-devs px-md-2">
                                    <div className="row">
                                        <div className="col-md-8">
                                            <div className="form-group pb-4">
                                                <label for="eventTitle">Event Title</label>
                                                <input
                                                    type={"text"}
                                                    className={"form-control"}
                                                    name="title"
                                                    onChange={this.handleChange}
                                                    value={this.state.title}
                                                    required/>
                                            </div>
                                        </div>
                                        <div className="col-md-8">
                                            <div className="form-group">
                                                <label>Event Image</label>
                                                <input
                                                    type="file"
                                                    className="form-control d-none"
                                                    id="addeventimage"
                                                    name="file"
                                                    value={this.state.file}
                                                    onChange={this.imageUpload}
                                                    />
                                                <div className="add-image">
                                                    {this.state.imgUrl === '' ?
                                                        <label className="text-center w-100" for="addeventimage">
                                                            <img src={AddImage} alt="Add Image icon" className="img-fluid icon" />
                                                            <h5 className="fs-18 pt-2">Click to add image</h5>
                                                            <p className="text-secondary mb-0 fs-14">Jpeg or PNG not larger than 10MB</p>
                                                            {this.state.fileuploaderLoader && this.state.fileuploaderLoader &&
                                                                <CircularProgress />}
                                                        </label>
                                                        :
                                                        <label className="text-center w-100" for="addeventimage">
                                                            <div className="uplaodedImage">
                                                                <img src={this.state.imgUrl} alt="Events image" className="img-fluid" />
                                                            </div>
                                                        </label>
                                                    }
                                                    <span style={{color: "red"}}>{this.state.errors["imgUrl"]}</span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="row">
                                        <div className="col-md-8">
                                            <div className="form-group">
                                                <label htmlFor="eventdescription">Event Description</label>
                                                <Editor
                                                    editorState={this.state.editorState}
                                                    wrapperClassName="demo-wrapper"
                                                    editorClassName="demo-editor"
                                                    onEditorStateChange={this.onEditorStateChange}
                                                    ref="content"
                                                    />
                                                     <span style={{color: "red"}}>{this.state.errors["editorState"]}</span>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="row">
                                        <div className="col-md-3">
                                            <div className="form-group">
                                                <label htmlFor="eventdate">Event Date</label>
                                                <Datepicker
                                                    handleDateChange={this.handleDateChange}
                                                    value={this.state.selectedDate}
                                                    />
                                            </div>
                                        </div>
                                        <div className="col-md-2"></div>
                                        <div className="col-md-3">
                                            <div className="form-group">
                                                <label htmlFor="eventtime">Event Time</label>
                                                <Timepicker
                                                    handleTimeChange={this.handleTimeChange}
                                                    value={this.state.selectedTime}
                                                />
                                            </div>
                                        </div>
                                    </div>
                                    <div className="row">
                                        <div className="col-md-8">
                                            <div className="form-group">
                                                <label htmlFor="eventvenue">Event venue</label>
                                                <input
                                                    type={"text"}
                                                    className={"form-control"}
                                                    name="venue"
                                                    value={this.state.venue}
                                                    onChange={this.handleChange}
                                                    required/>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="row">
                                        <div className="col-md-8">
                                            <div className="form-group">
                                                <label htmlFor="eventlink">Event link</label>
                                                <input
                                                    type={"text"}
                                                    className={"form-control"}
                                                    name="link"
                                                    value={this.state.link}
                                                    onChange={this.handleChange}
                                                    required/>
                                            </div>
                                        </div>
                                    </div>

                                    <div className="row">
                                        <div className="col-md-3">
                                            <div className="form-group">
                                                <label htmlFor="postto">Event post</label>
                                                <select name="EventTime" id="eventtime" className="form-control" value={this.state.postvenue} onChange={(e) => this.setState({ postvenue: e.target.value })}>
                                                    <option defaultValue="ALL" >All</option>
                                                    <option value="HUB">The hub page</option>
                                                    <option value="EVENT">The event page </option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="row mb-5">
                                        <div className="col-md-3">
                                            <div className="form-group">
                                                <label htmlFor="postto">Event type filter</label>
                                                <select name="EventTime" id="eventtime" className="form-control" value={this.state.eventtype} onChange={(e) => this.setState({ eventtype: e.target.value })}>
                                                    {typelist.length > 0 && typelist.map(user => {
                                                        return (
                                                            <option key={user.id} value={user.event_type}>{user.event_type}</option>
                                                        )
                                                    })}

                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="row">
                                        <div className="col-md-12">
                                            <div className="form-group text-center">
                                                <button type="submit" className="btn btn-warning px-4 text-white" onClick={this.submitInfo}  >Post events</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </section>
                <Quicknotification
                    show={showpopup}
                    message={respMessage}
                    closetab={() => this.setState({ showpopup: false })}
                    closesecond={4000}
                    timeout={true}
                />
                <Footer />
            </>
        )
    }
}

const mapStateToProps = state => {
    return {
        // AddEventDispatchState: state.NewsfeedEventAdd,
        FileUploadState: state.FileuploadReducer,
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        // AddEventDispatch: (data) => { dispatch(addEvent(data)) },
        FileUploadDispatch: (file) => { dispatch(Uploadfile(file)) }
    }
}
export default connect(mapStateToProps, mapDispatchToProps)(AddEvents)
