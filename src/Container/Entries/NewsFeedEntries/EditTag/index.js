import React, { Component } from 'react'
import { Link } from 'react-router-dom'
import Footer from '../../../Footer'
import Datepicker from '../../../../Component/Datepicker'
import '../../style.css'
import { Api } from '../../../../Const/Api'
import Axios from 'axios'
import { connect } from 'react-redux'
// import { updateTag } from '../../../../Action/newsfeed'
import { logout } from '../../../../Action/applicationRequestAction'
import { Uploadfile } from '../../../../Action/commonAction'
import { Editor } from 'react-draft-wysiwyg';
import { EditorState, convertFromHTML, ContentState } from 'draft-js';
import 'react-draft-wysiwyg/dist/react-draft-wysiwyg.css';
import { stateToHTML } from "draft-js-export-html";
import Quicknotification from '../../../../Component/Quicknotification'


class EditTag extends Component {
    constructor(props) {
        super(props);
        this.state = {
            title: '',
            image_url: '',
            description: '',
            date: '',
            tag_type: '',
            selectedDate: new Date(),
            selecttag: '',
            selectedFile: null,
            dedvalue: '',
            showpopup: false,
            respMessage : ''
        }
    }

    componentDidMount() {
        let id = this.props.match.params.id

 

        Axios.get(Api.BaseUrl + "/api/tag/" + id)
            .then(resp => {
                const getData = resp.data.data.list
                this.setState({
                    title: getData.title,
                    image_url: getData.image_url,
                    editorState: EditorState.createWithContent(ContentState.createFromBlockArray(convertFromHTML(getData.description))),
                    selectedDate: getData.date,
                    tag_type: getData.tag_type
                })
                if (resp.data.status === "2") {
                    logout()
                    return false
                }
            })
            .catch(err => {
                console.log("error", err)
            });
        Axios.get(Api.BaseUrl+'/api/tag-list')
            .then(res => {
                // console.log("select tag Respose", res)
                const data = res.data.data.list
                this.setState({ selecttag: data })

            })
            .catch(error => {
                console.log(error.res);
            });

    }

    handleDateChange = (date) => {
        // this.setState({ selectedDate : convertdate(date) })
        this.setState({ selectedDate: date })
    };

    onEditorStateChange = (editorState) => {
        this.setState({
            editorState,
            editorContentHtml: stateToHTML(editorState.getCurrentContent())
        });
    };

    handleChange = (e) => {
        this.setState({ [e.target.name]: e.target.value })
    }

   

    imageDelete = (e) => {
        this.setState({
            image_url: null
        })
    }

    HandleSubmit = (e) => {
        e.preventDefault()
        this.setState({
            editorContentHtml: stateToHTML(this.state.editorState.getCurrentContent())
        }, () => {
            let data = {
                id: this.props.match.params.id,
                title: this.state.title,
                image_url: this.state.image_url,
                description: this.state.editorContentHtml,
                date: this.state.selectedDate,
                tag_type: this.state.tag_type
            }
            Axios.put(Api.BaseUrl + "/api/tag/" + data.id, {
                title: data.title,
                image_url: data.image_url,
                description: data.description,
                date: data.date,
                tag_type: data.tag_type
            },
                { 'headers': { 'Authorization': localStorage.getItem('adminToken') } })
                .then(resp => {
                    this.setState({
                        respMessage: resp.data.message,
                        showpopup : true
                    })
                })
        })
    }

    uploadOnClick = (e) => {
        this.setState({
            fileuploadloader: true
        })
        this.props.FileUploadDispatch(e.target.files[0])
    }

    componentDidUpdate(prevProp) {
        if (prevProp.FileUploadState.loading !== this.props.FileUploadState.loading) {
            this.setState({
                image_url: this.props.FileUploadState.resp,
                fileuploadloader: this.props.FileUploadState.loading,
                fileuploadloadermsg: this.props.FileUploadState.error
            })
        }
    }



    render() {
        const selecttag = this.state.selecttag
        const { showpopup, respMessage } = this.state;

        return (
            <>
                <section className="wrapper px-4 ">
                    <div className="container">
                        <div className="page-title mb-4">
                            <div className="row align-items-center">
                                <div className="col-md-5">
                                    <div className="page-title-inner">
                                        <h4>Edit tags</h4>
                                    </div>
                                </div>
                                <div className="col-md-7">
                                    <nav aria-label="breadcrumb">
                                        <ol className="breadcrumb mb-2 justify-content-end p-0 bg-transparent align-items-center">
                                            <li className="breadcrumb-item"><Link to="/">Dashboard</Link></li>
                                            <li className="breadcrumb-item"><Link to="#">Content Mangement</Link></li>
                                            <li className="breadcrumb-item"><Link to="/news-feeds">Newsfeed</Link></li>
                                            <li className="breadcrumb-item active" aria-current="page">Tags</li>
                                        </ol>
                                    </nav>
                                </div>
                            </div>
                        </div>

                        <div className="view-entry">
                            <div className="row mb-3 align-items-center">
                                <div className="col-md-6">

                                </div>
                                <div className="col-md-6 text-center text-md-right">
                                    <Link to="/news-feeds"><button className="btn btn-outline-warning px-4 fs-14 rounded-50">Back to tags</button></Link>
                                </div>
                            </div>
                            <div className="card border-0  px-md-4 shadow-sm">
                                <div className="card-body hire-devs px-md-2">
                                    <div className="row">
                                        <div className="col-md-8">
                                            <div className="form-group pb-4">
                                                <label for="tagTitle">Title</label>
                                                <input type="text" className="form-control" id="tagTitle" name="title" value={this.state.title} onChange={this.handleChange} />
                                            </div>
                                        </div>
                                        <div className="col-md-8">
                                            <div className="form-group">
                                                <label>Image</label>
                                                <input type="file" className="form-control d-none" id="editpressimage" onChange={this.uploadOnClick} />
                                                <div className="add-image">
                                                    <label className="text-center w-100 parallax" for="editpressimage">
                                                        <div className="uplaodedImage" >
                                                            <img src={this.state.image_url} alt="Events image" className="img-fluid" />
                                                            <div className="overlay-remove position-absolute">
                                                                <i className="fas fa-lg fa-trash"></i>
                                                            </div>
                                                        </div>
                                                    </label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="row">
                                        <div className="col-md-8">
                                            <div className="form-group">
                                                <label for="tagdescription">Description</label>
                                                <Editor
                                                    editorState={this.state.editorState}
                                                    wrapperClassName="demo-wrapper"
                                                    editorClassName="demo-editor"
                                                    onEditorStateChange={this.onEditorStateChange}
                                                    ref="content"
                                                />

                                            </div>
                                        </div>
                                    </div>
                                    <div className="row mb-5">
                                        <div className="col-md-3">
                                            <div className="form-group">
                                                <label for="selecttag">Select tag</label>
                                                <select className="form-control" id="selecttag" onChange={(e) => this.setState({ tag_type: e.target.value })}>
                                                    <option selected value={this.state.tag_type}>{this.state.tag_type}</option>
                                                    {selecttag.length > 0 && selecttag.map((Tlist) => {
                                                        return (
                                                            <option value={Tlist.tag}>{Tlist.tag}</option>
                                                        )
                                                    })}
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="row mb-5">
                                        <div className="col-md-3">
                                            <div className="form-group">
                                                <label for="selecttag">Date</label>
                                                <Datepicker
                                                    handleDateChange={this.handleDateChange}
                                                    value={this.state.selectedDate}
                                                />
                                            </div>
                                        </div>
                                    </div>
                                    <div className="row">
                                        <div className="col-md-12">
                                            <div className="form-group text-center">
                                                <button className="btn btn-warning px-4 text-white" onClick={this.HandleSubmit}>Save edit</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
                <Quicknotification
                    show={showpopup}
                    message={respMessage}
                    closetab={() => this.setState({ showpopup: false })}
                    closesecond={10000}
                    timeout={true} />
                <Footer />
            </>
        )
    }
}

const mapStateToProps = state => {
    return {
        FileUploadState: state.FileuploadReducer,
    }
}
const mapDispatchToProps = (dispatch) => {
    return {
        // NewsfeedTagUpdate: (data) => { dispatch(updateTag(data)) },
        FileUploadDispatch: (file) => { dispatch(Uploadfile(file)) },
    }
}



export default connect(mapStateToProps, mapDispatchToProps)(EditTag)
