import React, { Component } from 'react'
import { Link } from 'react-router-dom'
import Footer from '../../../Footer'
import AddGalleryIcon from '../../../../Assets/img/addgallery-img.png'
import '../../style.css'
// import { newsfeedaddGallery } from '../../../../Action/newsfeed'
import Axios from 'axios'
import { Api } from '../../../../Const/Api'
import Timepicker from '../../../../Component/Timepicker'
import Datepicker from '../../../../Component/Datepicker'
import { connect } from 'react-redux'
import { Editor } from 'react-draft-wysiwyg';
import { EditorState } from 'draft-js';
import 'react-draft-wysiwyg/dist/react-draft-wysiwyg.css';
import { stateToHTML } from "draft-js-export-html";
import Quicknotification from '../../../../Component/Quicknotification'



class AddGallery extends Component {
    constructor() {
        super();
        this.state = {
            files: [],
            title: '',
            imgUrl: [],
            description: '',
            venue: '',
            selectedDate: new Date(),
            selectedTime: new Date(),
            editorState: EditorState.createEmpty(),
            selectedFile: '',
            imageurls: [],
            errors: {},
            status: '',
            showpopup: false,

        }
    }

    handleDateChange = date => {
        this.setState({ selectedDate: date });
    };
    handleTimeChange = date => {
        this.setState({ selectedTime: date });
    };

    onEditorStateChange = (editorState) => {
        this.setState({
            editorState: editorState,
            editorContentHtml: stateToHTML(editorState.getCurrentContent())
        });
    };

    fileSelectedHandler = (e) => {
        this.setState({ selectedFile: e.target.files[0] })
        const formData = new FormData()
        formData.append(
            'file',
            e.target.files[0]
        )
        Axios.post(Api.BaseUrl+'/api/file-upload', formData,)
            .then(res => {
                const Url = res.data.data.image_url
                this.setState({ imageurls: [...this.state.imageurls, Url] })
            })

    }

    deleteImage = (d) => {
        this.setState(
            this.state.imageurls.splice(d, 1)
        )
    }

    handleSubmit = (e) => {
        e.preventDefault();
        if (this.handleValidation()) {
            let data = {
                title: this.state.title,
                image_urls: this.state.imageurls,
                description: this.state.editorContentHtml,
                date: this.state.selectedDate,
                time: this.state.selectedTime,
                venue: this.state.venue
            }
            Axios.post(Api.BaseUrl + "/api/gallerylist", data,
                { 'headers': { 'Authorization': localStorage.getItem('adminToken') } })
                .then(resp => {
                    this.setState({
                        status: resp.data.status,
                        respMessage: resp.data.message,
                        showpopup : true

                    }, () => {
                        if (this.state.status === 1) {
                            this.setState({
                                files: [],
                                title: '',
                                imgUrl: [],
                                description: '',
                                venue: '',
                                selectedDate: new Date(),
                                selectedTime: new Date(),
                                editorState: EditorState.createEmpty(),
                                selectedFile: '',
                                imageurls: []
                            })
                        }
                    })
                })
        }
    }


    handleValidation = () => {

        let imageurls = this.state.imageurls;
        let editorContentHtml = this.state.editorContentHtml;
        let errors = {};
        let formIsValid = true;

        if (imageurls ==='') {
            formIsValid = false;
            errors["imageurls"] = "Please Upload Image"
        }

        if (editorContentHtml === null) {
            formIsValid = false;
            errors["editorState"] = "This Field require"
        }
        this.setState({ errors: errors });
        return formIsValid;
    }

    render() {
        const {  showpopup, respMessage } = this.state;

        return (
            <>
                <section className="wrapper px-4 ">
                    <div className="container">
                        <div className="page-title mb-4">
                            <div className="row align-items-center">
                                <div className="col-md-5">
                                    <div className="page-title-inner">
                                        <h4>Add gallery</h4>
                                    </div>
                                </div>
                                <div className="col-md-7">
                                    <nav aria-label="breadcrumb">
                                        <ol className="breadcrumb mb-2 justify-content-end p-0 bg-transparent align-items-center">
                                            <li className="breadcrumb-item"><Link to="/">Dashboard</Link></li>
                                            <li className="breadcrumb-item"><Link to="#">Content Mangement</Link></li>
                                            <li className="breadcrumb-item"><Link to="/news-feeds">Newsfeed</Link></li>
                                            <li className="breadcrumb-item"><Link to="/news-feeds">Gallery</Link></li>
                                            <li className="breadcrumb-item active" aria-current="page">Add Gallery</li>
                                        </ol>
                                    </nav>
                                </div>
                            </div>
                        </div>

                        <div className="view-entry">
                            <div className="row mb-3 align-items-center">
                                <div className="col-md-6">

                                </div>
                                <div className="col-md-6 text-center text-md-right">
                                    <Link to="/news-feeds/#gallery-tab"><button className="btn btn-outline-warning px-4 fs-14 rounded-50">Back to gallery</button></Link>
                                </div>
                            </div>
                            <div className="card border-0  px-md-4 shadow-sm">
                                <form onSubmit={this.handleSubmit}>
                                    <div className="card-body hire-devs px-md-2">
                                        <div className="row">
                                            <div className="col-md-8">
                                                <div className="form-group pb-4">
                                                    <label for="galleryTitle">Title</label>
                                                    <input type="text" className="form-control" id="galleryTitle" name="title" onChange={(e) => this.setState({ title: e.target.value })} value={this.state.title} required />
                                                </div>
                                            </div>
                                            <div className="col-md-8">
                                                <div className="form-group">
                                                    <label>Add Photos</label>
                                                    <p className="mb-1 text-secondary fs-12">Add at least 5 photos for this category.</p>
                                                    <p className="text-secondary fs-12">First picture - is the title picture. You can change the order of photos: just grab your photos and drag</p>
                                                    <input type="file" className="form-control d-none" name="file" id="addgalleryimage" onChange={this.fileSelectedHandler} multiple value={this.state.file} />
                                                    <div className="add-gallery my-4 d-flex align-items-center">
                                                        <label className="text-center" for="addgalleryimage">
                                                            <img src={AddGalleryIcon} alt="Add Image icon" className="img-fluid icon" />
                                                        </label>
                                                        <div className="viewImage d-flex align-items-center">
                                                            {this.state.imageurls.length > 0 && this.state.imageurls.map((img, i) => {
                                                                return (
                                                                    <div className="newsfeedGallery position-relative" key={i}>
                                                                        <span><i className="fa fa-trash position-absolute" onClick={() => this.deleteImage(i)}></i></span>
                                                                        <img src={img} className="img-fluid" />
                                                                    </div>
                                                                )
                                                            })}

                                                        </div>
                                                    </div>
                                                    <p className="text-secondary fs-12">Jpeg or PNG not larger than 5MB</p>
                                                    <span style={{ color: "red" }}>{this.state.errors["imageurls"]}</span>
                                                </div>
                                            </div>
                                        </div>
                                        <div className="row">
                                            <div className="col-md-8">
                                                <div className="form-group">
                                                    <label for="gallerydescription">Description</label>
                                                    {/* <textarea className="form-control" rows="7" id="gallerydescription"></textarea> */}
                                                    <Editor
                                                        editorState={this.state.editorState}
                                                        wrapperClassName="demo-wrapper"
                                                        editorClassName="demo-editor"
                                                        onEditorStateChange={this.onEditorStateChange}
                                                        ref="content"
                                                    />
                                                    <span style={{ color: "red" }}>{this.state.errors["editorState"]}</span>
                                                </div>
                                            </div>
                                        </div>
                                        <div className="row">
                                            <div className="col-md-3">
                                                <div className="form-group pb-4">
                                                    <label for="eventdate">Event date</label>
                                                    <Datepicker
                                                        handleDateChange={this.handleDateChange}
                                                        value={this.state.selectedDate}
                                                    />
                                                </div>
                                            </div>
                                            <div className="col-md-2"></div>
                                            <div className="col-md-3">
                                                <div className="form-group">
                                                    <label for="eventtime">Event time</label>
                                                    <Timepicker
                                                        handleTimeChange={this.handleTimeChange}
                                                        value={this.state.selectedTime}
                                                    />
                                                </div>
                                            </div>
                                        </div>
                                        <div className="row mb-5">
                                            <div className="col-md-8">
                                                <div className="form-group pb-4">
                                                    <label for="eventvenue">Event venue</label>
                                                    <input type="text" className="form-control" id="eventvenue" name="vanue" value={this.state.venue} onChange={(e) => this.setState({ venue: e.target.value })} required />
                                                </div>
                                            </div>
                                        </div>
                                        <div className="row">
                                            <div className="col-md-12">
                                                <div className="form-group text-center">
                                                    <button type="submit" className="btn btn-warning px-4 text-white">Post gallery</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </section>
                <Quicknotification
                    show={showpopup}
                    message={respMessage}
                    closetab={() => this.setState({ showpopup: false })}
                    closesecond={10000}
                    timeout={true} />
                <Footer />
            </>
        )
    }
}
const mapStateToProps = state => {
    return {
        state
    }
}


const mapDispatchToProps = (dispatch) => {
    return {
        // AddGalleryDispatch: (data) => { dispatch(newsfeedaddGallery(data)) },
        // partnershipdelete: (userid) => { dispatch(partnershipDelete(userid)) }
    }
}
export default connect(mapStateToProps, mapDispatchToProps)(AddGallery)