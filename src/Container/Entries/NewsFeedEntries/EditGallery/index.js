import React, { Component } from 'react'
import { Link } from 'react-router-dom'
import Footer from '../../../Footer'
import AddGalleryIcon from '../../../../Assets/img/addgallery-img.png'
import '../../style.css'
import Timepicker from '../../../../Component/Timepicker'
import Datepicker from '../../../../Component/Datepicker'
import { connect } from 'react-redux'
import { Editor } from 'react-draft-wysiwyg';
import { EditorState, convertFromHTML, ContentState } from 'draft-js';
import { logout } from '../../../../Action/applicationRequestAction'
import 'react-draft-wysiwyg/dist/react-draft-wysiwyg.css';
import { stateToHTML } from "draft-js-export-html";
import Axios from 'axios';
import { newsfeedUpdateGallery } from '../../../../Action/newsfeed'
import { Api } from '../../../../Const/Api'
import Quicknotification from '../../../../Component/Quicknotification'




class EditGallery extends Component {
    constructor() {
        super();
        this.state = {
            files: [],
            title: '',
            image_ids: '',
            description: '',
            venue: '',
            selectedDate: new Date(),
            selectedTime: new Date(),
            imageurls: []
        }
    }


    componentDidMount() {
        let id = this.props.match.params.id
        Axios.get(Api.BaseUrl + "/api/gallery/" + id)
            .then(resp => {
                const getData = resp.data.data.list
                this.setState({
                    title: getData.title,
                    imageurls: getData.image_urls,
                    editorState: EditorState.createWithContent(ContentState.createFromBlockArray(convertFromHTML(getData.description))),
                    selectedDate: getData.date,
                    selectedTime: getData.time,
                    venue: getData.venue
                })
                if (resp.data.status === "2") {
                    logout()
                    return false
                }
            })
            .catch(err => {
                console.log("error", err)
            });
    }

    handleChange = (e) => {
        this.setState({ [e.target.name]: e.target.value })
    }

    handleDateChange = date => {
        this.setState({ selectedDate: date });
    };
    handleTimeChange = date => {
        this.setState({ selectedTime: date });
    };


    onEditorStateChange = (editorState) => {
        this.setState({
            editorState: editorState,
            editorContentHtml: stateToHTML(editorState.getCurrentContent())
        });
    };

    deleteImage = (d) => {
        this.setState(
            this.state.imageurls.splice(d, 1)
        )
    }

    fileSelectedHandler = (e) => {
        this.setState({ selectedFile: e.target.files[0] })
        const formData = new FormData()
        formData.append(
            'file',
            e.target.files[0]
        )
        Axios.post(Api.BaseUrl+'/api/file-upload', formData,)
            .then(res => {
                const Url = res.data.data.image_url
                this.setState({
                    imageurls: [...this.state.imageurls, Url]
                })
            })

    }

    handleSubmit = (e) => {
        this.setState({
            editorContentHtml: stateToHTML(this.state.editorState.getCurrentContent())
        }, () => {
            let data = {
                id: this.props.match.params.id,
                title: this.state.title,
                image_urls: this.state.imageurls,
                description: this.state.editorContentHtml,
                date: this.state.selectedDate,
                time: this.state.selectedTime,
                venue: this.state.venue
            }
            this.props.updateGalleryDispatch(data)
            this.setState({
                showpopup: true
            })
        })
    }

    render() {
        const {  showpopup  } = this.state;

        return (
            <>
                <section className="wrapper px-4 ">
                    <div className="container">
                        <div className="page-title mb-4">
                            <div className="row align-items-center">
                                <div className="col-md-5">
                                    <div className="page-title-inner">
                                        <h4>Edit gallery</h4>
                                    </div>
                                </div>
                                <div className="col-md-7">
                                    <nav aria-label="breadcrumb">
                                        <ol className="breadcrumb mb-2 justify-content-end p-0 bg-transparent align-items-center">
                                            <li className="breadcrumb-item"><Link to="/">Dashboard</Link></li>
                                            <li className="breadcrumb-item"><Link to="#">Content Mangement</Link></li>
                                            <li className="breadcrumb-item"><Link to="/news-feeds">Newsfeed</Link></li>
                                            <li className="breadcrumb-item"><Link to="/news-feeds/#gallery-tab">Gallery</Link></li>
                                            <li className="breadcrumb-item active" aria-current="page">Edit Gallery</li>
                                        </ol>
                                    </nav>
                                </div>
                            </div>
                        </div>

                        <div className="view-entry">
                            <div className="row mb-3 align-items-center">
                                <div className="col-md-6">

                                </div>
                                <div className="col-md-6 text-center text-md-right">
                                    <Link to="/news-feeds/#gallery-tab"><button className="btn btn-outline-warning px-4 fs-14 rounded-50">Back to gallery</button></Link>
                                </div>
                            </div>
                            <div className="card border-0  px-md-4 shadow-sm">
                                <div className="card-body hire-devs px-md-2">
                                    <div className="row">
                                        <div className="col-md-8">
                                            <div className="form-group pb-4">
                                                <label for="galleryTitle">Title</label>
                                                <input type="text" className="form-control" id="galleryTitle" name="title" value={this.state.title} onChange={this.handleChange} />
                                            </div>
                                        </div>
                                        <div className="col-md-8">
                                            <div className="form-group">
                                                <label>Add Photos</label>
                                                <p className="mb-1 text-secondary fs-12">Add at least 5 photos for this category.</p>
                                                <p className="text-secondary fs-12">First picture - is the title picture. You can change the order of photos: just grab your photos and drag</p>
                                                <input type="file" className="form-control d-none" id="addgalleryimage" value={this.state.image_ids} onChange={this.fileSelectedHandler} />
                                                <div className="add-gallery my-4 d-flex align-items-center">
                                                    <label className="text-center" for="addgalleryimage">
                                                        <img src={AddGalleryIcon} alt="Add Image icon" className="img-fluid icon" />
                                                    </label>
                                                    <div className="viewImage d-flex align-items-center">
                                                        {this.state.imageurls === undefined ? this.state.imageurls : this.state.imageurls.length > 0 && this.state.imageurls.map((img, i) => {
                                                            return (
                                                                <div className="newsfeedGallery position-relative">
                                                                    <span><i className="fa fa-trash position-absolute" onClick={() => this.deleteImage(i)}></i></span>
                                                                    <img src={img} className="img-fluid" />
                                                                </div>
                                                            )
                                                        })}
                                                    </div>
                                                </div>
                                                <p className="text-secondary fs-12">Jpeg or PNG not larger than 5MB</p>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="row">
                                        <div className="col-md-8">
                                            <div className="form-group">
                                                <label for="gallerydescription">Description</label>
                                                {/* <textarea className="form-control" rows="7" id="gallerydescription"></textarea> */}
                                                <Editor
                                                    editorState={this.state.editorState}
                                                    wrapperClassName="demo-wrapper"
                                                    editorClassName="demo-editor"
                                                    onEditorStateChange={this.onEditorStateChange}
                                                />

                                            </div>
                                        </div>
                                    </div>
                                    <div className="row">
                                        <div className="col-md-3">
                                            <div className="form-group pb-4">
                                                <label for="eventdate">Event date</label>
                                                <Datepicker
                                                    handleDateChange={this.handleDateChange}
                                                    value={this.state.selectedDate}
                                                />
                                            </div>
                                        </div>
                                        <div className="col-md-2"></div>
                                        <div className="col-md-3">
                                            <div className="form-group">
                                                <label for="eventtime">Event time</label>
                                                <Timepicker
                                                    handleTimeChange={this.handleTimeChange}
                                                    value={this.state.selectedTime}
                                                />
                                            </div>
                                        </div>
                                    </div>
                                    <div className="row mb-5">
                                        <div className="col-md-8">
                                            <div className="form-group pb-4">
                                                <label for="eventvenue">Event venue</label>
                                                <input type="text" className="form-control" id="eventvenue" name="venue" value={this.state.venue} onChange={this.handleChange} />
                                            </div>
                                        </div>
                                    </div>
                                    <div className="row">
                                        <div className="col-md-12">
                                            <div className="form-group text-center">
                                                <button className="btn btn-warning px-4 text-white" onClick={this.handleSubmit}>Save edit</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
                <Quicknotification
                    show={showpopup}
                    message={"edit succes"}
                    closetab={() => this.setState({ showpopup: false })}
                    closesecond={10000}
                    timeout={true} />
                <Footer />
            </>
        )
    }
}
const mapStateToProps = state => {
    return {
        state
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        updateGalleryDispatch: (data) => { dispatch(newsfeedUpdateGallery(data)) },
    }
}
export default connect(mapStateToProps, mapDispatchToProps)(EditGallery)