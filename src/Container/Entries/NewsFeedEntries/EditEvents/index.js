import React, { Component } from 'react'
import { Link } from 'react-router-dom'
import Footer from '../../../Footer'
import '../../style.css'
import { connect } from 'react-redux'
import { logout } from '../../../../Action/applicationRequestAction'
import Timepicker from '../../../../Component/Timepicker'
import Datepicker from '../../../../Component/Datepicker'
import { Api } from '../../../../Const/Api'
import Axios from 'axios'
import { Editor } from 'react-draft-wysiwyg';
import { EditorState, convertFromHTML, ContentState } from 'draft-js';
import 'react-draft-wysiwyg/dist/react-draft-wysiwyg.css';
import { stateToHTML } from "draft-js-export-html";
import Quicknotification from '../../../../Component/Quicknotification'
import { Uploadfile } from '../../../../Action/commonAction'



class EditEvents extends Component {
    constructor(props) {
        super(props);
        this.state = {
            title: '',
            image_url: '',
            description: '',
            date: '',
            time: '',
            venue: '',
            link: '',
            event_post: '',
            event_type: '',
            eventList: [],
            selectedTime: '',
            selectedDate: '',
            statusofapi: null,
            showpopup: false
        }
    }

    componentDidMount() {
        let id = this.props.match.params.id
        Axios.get(Api.BaseUrl + "/api/event/" + id)
            .then(resp => {
                const getData = resp.data.data.list
                this.setState({
                    title: getData.title,
                    image_url: getData.image_url,
                    editorState: EditorState.createWithContent(ContentState.createFromBlockArray(convertFromHTML(getData.description))),
                    selectedDate: getData.date,
                    selectedTime: getData.time,
                    venue: getData.venue,
                    link: getData.link,
                    event_post: getData.event_post,
                    event_type: getData.event_type
                })
                if (resp.data.status === "2") {
                    logout()
                    return false
                }
            })
            .catch(err => {
                console.log("error", err)
            });

        Axios.get(Api.BaseUrl + "/api/eventtype-list")
            .then(res => {
                const tagData = res.data.data.list
                this.setState({ eventList: tagData })
            })
            .catch((error) => {
                console.log("error", error)
            })
    }

    componentDidUpdate(prevProp) {
        if (prevProp.FileUploadState.loading !== this.props.FileUploadState.loading) {
            this.setState({
                image_url: this.props.FileUploadState.resp,
                fileuploadloader: this.props.FileUploadState.loading,
                fileuploadloadermsg: this.props.FileUploadState.error
            })
        }
    }

    handleChange = (e) => {
        this.setState({
            [e.target.name]: e.target.value
        })
    }

    handleTimeChange = date => {
        this.setState({ selectedTime: date });
    };
    handleDateChange = date => {
        this.setState({ selectedDate: date });
    };

    imageUpload = (e) => {
        this.setState({
            fileuploadloader: true
        })
        this.props.FileUploadDispatch(e.target.files[0])
    }

    imageDelete = (e) => {
        this.setState({
            image_url: null
        })
    }

    onEditorStateChange = (editorState) => {
        this.setState({
            editorState,
            editorContentHtml: stateToHTML(editorState.getCurrentContent())
        });
    };


    handleSubmit = (e) => {
        e.preventDefault()
        this.setState({
            editorContentHtml: stateToHTML(this.state.editorState.getCurrentContent())
        }, () => {
            const data = {
                id: this.props.match.params.id,
                title: this.state.title,
                image_url: this.state.image_url,
                description: this.state.editorContentHtml,
                date: this.state.selectedDate,
                time: this.state.selectedTime,
                venue: this.state.venue,
                link: this.state.link,
                event_post: this.state.event_post,
                event_type: this.state.event_type
            }
            Axios.put(Api.BaseUrl + "/api/event/" + data.id, {
                title: data.title,
                image_url: data.image_url,
                description: data.description,
                date: data.date,
                time: data.time,
                venue: data.venue,
                link: data.link,
                event_post: data.event_post,
                event_type: data.event_type
            },
                { 'headers': { 'Authorization': localStorage.getItem('adminToken') } })
                .then(resp => {
                    this.setState({
                        respMessage: resp.data.message,
                        showpopup : true
                    })
                })
        })
    
}

render() {
    const { eventList, showpopup, respMessage } = this.state
    return (
        <>
            <section className="wrapper px-4 ">
                <div className="container">
                    <div className="page-title mb-4">
                        <div className="row align-items-center">
                            <div className="col-md-5">
                                <div className="page-title-inner">
                                    <h4>Edit Event</h4>
                                </div>
                            </div>
                            <div className="col-md-7">
                                <nav aria-label="breadcrumb">
                                    <ol className="breadcrumb mb-2 justify-content-end p-0 bg-transparent align-items-center">
                                        <li className="breadcrumb-item"><Link to="/">Dashboard</Link></li>
                                        <li className="breadcrumb-item"><Link to="#">Content Mangement</Link></li>
                                        <li className="breadcrumb-item"><Link to="/news-feeds">Newsfeed</Link></li>
                                        <li className="breadcrumb-item"><Link to="#">Event</Link></li>
                                        <li className="breadcrumb-item active" aria-current="page">Edit Events</li>
                                    </ol>
                                </nav>
                            </div>
                        </div>
                    </div>

                    <div className="view-entry">
                        <div className="row mb-3 align-items-center">
                            <div className="col-md-6">

                            </div>
                            <div className="col-md-6 text-center text-md-right">
                                <Link to="/news-feeds"><button className="btn btn-outline-warning px-4 fs-14 rounded-50">Back to Events</button></Link>
                            </div>
                        </div>
                        <div className="card border-0  px-md-4 shadow-sm">
                            <div className="card-body hire-devs px-md-2">
                                <div className="row">
                                    <div className="col-md-8">
                                        <div className="form-group pb-4">
                                            <label for="eventTitle">Title</label>
                                            <input type="text" className="form-control" id="eventTitle" name="title" value={this.state.title} onChange={this.handleChange} />
                                        </div>
                                    </div>
                                    <div className="col-md-8">
                                        <div className="form-group">
                                            <label>Events Image</label>
                                            <input type="file" className="form-control d-none" id="addeventimage" onChange={this.imageUpload} />
                                            <div className="add-image">
                                                <label className="text-center w-100 parallax" for="addeventimage">
                                                    <div className="uplaodedImage" >
                                                        <img src={this.state.image_url} alt="Events image" className="img-fluid" />
                                                        <div className="overlay-remove position-absolute">
                                                            <i className="fas fa-lg fa-trash"></i>
                                                        </div>
                                                    </div>
                                                </label>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                                <div className="row">
                                    <div className="col-md-8">
                                        <div className="form-group">
                                            <label for="eventdescription">Event Description</label>
                                            <Editor
                                                editorState={this.state.editorState}
                                                wrapperClassName="demo-wrapper"
                                                editorClassName="demo-editor"
                                                onEditorStateChange={this.onEditorStateChange}
                                                ref="content"
                                            />

                                        </div>
                                    </div>
                                </div>
                                <div className="row">
                                    <div className="col-md-3">
                                        <div className="form-group">
                                            <label for="eventdate">Event Date</label>
                                            <Datepicker
                                                handleDateChange={this.handleDateChange}
                                                value={this.state.selectedDate}
                                            />
                                        </div>
                                    </div>
                                    <div className="col-md-2"></div>
                                    <div className="col-md-3">
                                        <div className="form-group">
                                            <label for="eventtime">Event Time</label>
                                            <Timepicker
                                                handleTimeChange={this.handleTimeChange}
                                                value={this.state.selectedTime}
                                            />
                                        </div>
                                    </div>
                                </div>
                                <div className="row">
                                    <div className="col-md-8">
                                        <div className="form-group">
                                            <label for="eventvenue">Event venue</label>
                                            <input type="text" className="form-control" id="eventvenue" name="venue" onChange={this.handleChange} value={this.state.venue} />
                                        </div>
                                    </div>
                                </div>
                                <div className="row">
                                    <div className="col-md-8">
                                        <div className="form-group">
                                            <label for="eventlink">Event link</label>
                                            <input type="url" className="form-control" id="eventlink" name="link" onChange={this.handleChange} value={this.state.link} />
                                        </div>
                                    </div>
                                </div>
                                <div className="row mb-5">
                                    <div className="col-md-3">
                                        <div className="form-group">
                                            <label for="postto">Event type filter</label>
                                            <select name="EventTime" id="eventtime" className="form-control" name="event_type" onChange={(e) => this.setState({ event_type: e.target.value })} value={this.state.event_type} >
                                                {/* <option selected value={this.state.event_type}>{this.state.event_type}</option> */}
                                                {eventList.length > 0 && eventList.map((user,i) => {
                                                    return (
                                                        <option key={i} value={user.event_type}>{user.event_type}</option>
                                                    )
                                                })}
                                            </select>
                                        </div>
                                    </div>
                                    <div className="col-md-3">
                                        <div className="form-group">
                                            <label for="postto">Event Post</label>
                                            <select name="EventTime" id="eventtime" className="form-control" name="event_post" onChange={(e) => this.setState({ event_post: e.target.value })} value={this.state.event_post}>
                                                {/* <option selected value={this.state.event_post}>{this.state.event_post}</option> */}
                                                <option value="ALL">All</option>
                                                <option value="HUB">The hub page</option>
                                                <option value="EVENT">The event page </option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div className="row">
                                    <div className="col-md-12">
                                        <div className="form-group text-center">
                                            <button className="btn btn-warning px-4 text-white" onClick={this.handleSubmit}>Save edit</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <Quicknotification
                show={showpopup}
                message={respMessage}
                closetab={() => this.setState({ showpopup: false })}
                closesecond={4000}
                timeout={true}
            />
            <Footer />
        </>
    )
}
}

const mapStateToProps = state => {
    return {
        // NewsfeedEventState: state.NewsfeedEventUpdateReducer,
        FileUploadState: state.FileuploadReducer,
    }
}
const mapDispatchToProps = (dispatch) => {
    return {
        // NewsfeedEventUpdate: (data) => { dispatch(updateEvent(data)) },
        FileUploadDispatch: (file) => { dispatch(Uploadfile(file)) }
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(EditEvents)