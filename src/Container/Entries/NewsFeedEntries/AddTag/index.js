import React, { useState } from 'react'
import { Link } from 'react-router-dom'
import CKEditor from '@ckeditor/ckeditor5-react';
import ClassicEditor from '@ckeditor/ckeditor5-build-classic';
import Footer from '../../../Footer'
import AddImage from '../../../../Assets/img/addimage-icon.png'
import '../../style.css'
import { Uploadfile } from '../../../../Action/commonAction'
import { Api } from '../../../../Const/Api'
import Datepicker from '../../../../Component/Datepicker'
import { connect } from 'react-redux'
// import { addTag } from '../../../../Action/newsfeed'
import Axios from 'axios'
import { convertdate, convertDatePickerTime } from '../../../../Const/function'
import { Editor } from 'react-draft-wysiwyg';
import { EditorState } from 'draft-js';
import 'react-draft-wysiwyg/dist/react-draft-wysiwyg.css';
import { stateToHTML } from "draft-js-export-html";
import Quicknotification from '../../../../Component/Quicknotification'



class AddTag extends React.Component {
    constructor() {
        super();
        this.state = {
            title: '',
            imgUrl: '',
            desc: '',
            date: '',
            tagSelected: 'AI',
            selecttag: [],
            editorState: EditorState.createEmpty(),
            selectedDate: new Date(),
            errors: {},
            status: '',
            showpopup: false,
            respMessage : ''
        }
    }


    componentDidMount() {
        Axios.get(Api.BaseUrl+'/api/tag-list')
            .then(res => {
                const data = res.data.data.list
                this.setState({ selecttag: data })
              
            })
            .catch(error => {
                console.log(error.res);
            });
    }



    handleDateChange = (date) => {
        // this.setState({ selectedDate : convertdate(date) })
        this.setState({ selectedDate: date })
    };

    handleChange = (e) => {
        this.setState({ [e.target.name]: e.target.value })
    }

    imageupload = (e) => {
        this.setState({
            fileuploadloader: true
        })
        this.props.FileUploadDispatch(e.target.files[0])
    }

    componentDidUpdate(prevProp) {
        if (prevProp.FileUploadState.loading !== this.props.FileUploadState.loading) {
            this.setState({
                imgUrl: this.props.FileUploadState.resp,
                fileuploadloader: this.props.FileUploadState.loading,
                fileuploadloadermsg: this.props.FileUploadState.error
            })
        }
    }

    onEditorStateChange = (editorState) => {
        this.setState({
            editorState: editorState,
            editorContentHtml: stateToHTML(editorState.getCurrentContent())
        });
    };

    submitInfo = (e) => {
        e.preventDefault();
        if (this.handleValidation()) {
            let data = {
                title: this.state.title,
                image_url: this.state.imgUrl,
                description: this.state.editorContentHtml,
                date: this.state.selectedDate,
                tag_type: this.state.tagSelected
            }
            Axios.post(Api.BaseUrl + "/api/tags", data,
                { 'headers': { 'Authorization': localStorage.getItem('adminToken') } })
                .then(resp => {
                    this.setState({
                        status: resp.data.status,
                        respMessage: resp.data.message,
                        showpopup: true
                    }, () => {
                        if (this.state.status === 1) {
                            this.setState({
                                title: '',
                                imgUrl: '',
                                desc: '',
                                date: '',
                                selecttag: [],
                                editorState: EditorState.createEmpty(),
                                selectedDate: new Date()
                            })
                        }
                    })
                })
        }
    }

    handleValidation = () => {
        let imgUrl = this.state.imgUrl;
        let editorContentHtml = this.state.editorContentHtml;
        let errors = {};
        let formIsValid = true;

        if (imgUrl === '') {
            formIsValid = false;
            errors["imgUrl"] = "Please Upload Image"
        }

        if (editorContentHtml === null) {
            formIsValid = false;
            errors["editorState"] = "This Field require"
        }
        this.setState({ errors: errors });
        return formIsValid;
    }


    render() {
        const { selecttag, showpopup, respMessage } = this.state
        return (
            <>
                <section className="wrapper px-4 ">
                    <div className="container">
                        <div className="page-title mb-4">
                            <div className="row align-items-center">
                                <div className="col-md-5">
                                    <div className="page-title-inner">
                                        <h4>Add Tags</h4>
                                    </div>
                                </div>
                                <div className="col-md-7">
                                    <nav aria-label="breadcrumb">
                                        <ol className="breadcrumb mb-2 justify-content-end p-0 bg-transparent align-items-center">
                                            <li className="breadcrumb-item"><Link to="/">Dashboard</Link></li>
                                            <li className="breadcrumb-item"><Link to="#">Content Mangement</Link></li>
                                            <li className="breadcrumb-item"><Link to="/news-feeds">Newsfeed</Link></li>
                                            <li className="breadcrumb-item"><Link to="/news-feeds/#tags-tab">Tags</Link></li>
                                            <li className="breadcrumb-item active" aria-current="page">Add Tags</li>
                                        </ol>
                                    </nav>
                                </div>
                            </div>
                        </div>

                        <div className="view-entry">
                            <div className="row mb-3 align-items-center">
                                <div className="col-md-6">

                                </div>
                                <div className="col-md-6 text-center text-md-right">
                                    <Link to="/news-feeds"><button className="btn btn-outline-warning px-4 fs-14 rounded-50">Back to tags</button></Link>
                                </div>
                            </div>
                            <div className="card border-0  px-md-4 shadow-sm">
                                <form onSubmit={this.submitInfo}>
                                    <div className="card-body hire-devs px-md-2">
                                        <div className="row">
                                            <div className="col-md-8">
                                                <div className="form-group pb-4">
                                                    <label for="tagTitle">Title</label>
                                                    <input type="text" className="form-control" id="tagTitle" name="title"
                                                        value={this.state.title}
                                                        onChange={this.handleChange} required />
                                                </div>
                                            </div>
                                            <div className="col-md-8">
                                                <div className="form-group">
                                                    <label>Image</label>
                                                    <input type="file" className="form-control d-none" id="addtagimage" onChange={this.imageupload} value={this.state.file} />
                                                    <div className="add-image">
                                                        {this.state.imgUrl === '' ?
                                                            <label className="text-center w-100" for="addtagimage">
                                                                <img src={AddImage} alt="Add Image icon" className="img-fluid icon" />
                                                                <h5 className="fs-18 pt-2">Click to add image</h5>
                                                                <p className="text-secondary mb-0 fs-14">Jpeg or PNG not larger than 10MB</p>
                                                            </label>
                                                            :
                                                            <label className="text-center w-100" for="addtagimage">
                                                                <div className="uplaodedImage">
                                                                    <img src={this.state.imgUrl} alt="Events image" className="img-fluid" />
                                                                </div>
                                                            </label>
                                                        }
                                                        <span style={{ color: "red" }}>{this.state.errors["imgUrl"]}</span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div className="row ">
                                            <div className="col-md-8">
                                                <div className="form-group">
                                                    <label for="tagdescription">Description</label>
                                                    {/* <textarea className="form-control" rows="7" id="tagdescription"></textarea> */}
                                                    {/* <CKEditor
                                                    editor={ ClassicEditor } 
                                                   
                                                /> */}
                                                    <Editor
                                                        editorState={this.state.editorState}
                                                        wrapperClassName="demo-wrapper"
                                                        editorClassName="demo-editor"
                                                        onEditorStateChange={this.onEditorStateChange}
                                                        ref="content"
                                                    />
                                                    <span style={{ color: "red" }}>{this.state.errors["editorState"]}</span>
                                                </div>
                                            </div>
                                        </div>
                                        <div className="row">
                                            <div className="col-md-3">
                                                <div className="form-group">
                                                    <label for="selecttag">Select tag</label>
                                                    <select className="form-control" id="selecttag" name="selecttag" value={this.state.tagSelected} onChange={(e) => this.setState({ tagSelected: e.target.value })} >

                                                        {selecttag.length > 0 && selecttag.map((selecttag,i) => {
                                                            return (
                                                                <option key={i} value={selecttag.tag}>{selecttag.tag}</option>
                                                            )
                                                        })}

                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                        <div className="row mb-5">
                                            <div className="col-md-3">
                                                <div className="form-group">
                                                    <label for="selecttag">Date</label>
                                                    <Datepicker
                                                        handleDateChange={this.handleDateChange}
                                                        value={this.state.selectedDate}
                                                    />
                                                </div>
                                            </div>
                                        </div>
                                        <div className="row">
                                            <div className="col-md-12">
                                                <div className="form-group text-center">
                                                    <button type="submit" className="btn btn-warning px-4 text-white">Post tag</button>
                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </section>
                <Quicknotification
                    show={showpopup}
                    message={respMessage}
                    closetab={() => this.setState({ showpopup: false })}
                    closesecond={10000}
                    timeout={true} 
                    />
                <Footer />
            </>
        )
    }
}
const mapStateToProps = state => {
    return {
        FileUploadState: state.FileuploadReducer,
    }
}


const mapDispatchToProps = (dispatch) => {
    return {
        // addTagDispatch: (data) => { dispatch(addTag(data)) },
        FileUploadDispatch: (file) => { dispatch(Uploadfile(file)) }
    }
}
export default connect(mapStateToProps, mapDispatchToProps)(AddTag)


