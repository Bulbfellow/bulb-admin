import React, { Component } from 'react'
import { Link } from 'react-router-dom'
import Footer from '../../../Footer'
import '../../style.css'
import Axios from 'axios'
import {Api} from '../../../../Const/Api'
export default class ViewCareersEntriesRequest extends Component {
    constructor() {
        super()
        this.state = {
            title: '',
            first_name: '',
            last_name: '',
            mobile: "",
            email: "",
            info: '',
            cv_url: '',
            website: '',
            linkedin: '',
            twitter: '',
            github: '',
            behance: '',
            dribbble: '',
            jobtitle: ''
        }
    }

    componentDidMount() {
        Axios.get(Api.BaseUrl+`/api/career/${this.props.match.params.id}`,
            {
                'headers': { 'Authorization': localStorage.getItem('adminToken') }
            })
            .then(res => {
                this.setState({
                    title: res.data.data.list.title,
                    first_name: res.data.data.list.first_name,
                    last_name: res.data.data.list.last_name,
                    mobile: res.data.data.list.mobile,
                    email: res.data.data.list.email,
                    info: res.data.data.list.info,
                    cv_url: res.data.data.list.cv_url,
                    website: res.data.data.list.website,
                    linkedin: res.data.data.list.linkedin,
                    twitter: res.data.data.list.twitter,
                    github: res.data.data.list.github,
                    behance: res.data.data.list.behance,
                    dribbble: res.data.data.list.dribbble,
                    jobtitle: res.data.data.list.jobpost_name

                })
            })
    }

    render() {
        return (
            <>
                <section className="wrapper px-4 ">
                    <div className="container">
                        <div className="page-title mb-4">
                            <div className="row align-items-center">
                                <div className="col-md-5">
                                    <div className="page-title-inner">
                                        <h4>View Entry</h4>
                                    </div>
                                </div>
                                <div className="col-md-7">
                                    <nav aria-label="breadcrumb">
                                        <ol className="breadcrumb mb-2 justify-content-end p-0 bg-transparent align-items-center">
                                            <li className="breadcrumb-item"><Link to="/">Dashboard</Link></li>
                                            <li className="breadcrumb-item"><Link to="#">Content Manager</Link></li>
                                            <li className="breadcrumb-item active" aria-current="page">Careers</li>
                                        </ol>
                                    </nav>
                                </div>
                            </div>
                        </div>

                        <div className="view-entry">
                            <div className="row mb-3 align-items-center">
                                <div className="col-md-6">
                                    <div className="form-name text-center text-md-left">
                                        <h5 className="mb-md-0">Careers application request For <span> <h4> <b>{this.state.jobtitle}</b></h4></span></h5>
                                    </div>
                                </div>
                                <div className="col-md-6 text-center text-md-right">
                                    <Link to="/careers/carrear-applicant-tab"><button className="btn btn-outline-warning btn-warning-cus px-4 fs-14 rounded-50">Back to entries</button></Link>
                                </div>
                            </div>
                            <div className="card border-0  px-md-4 shadow-sm mb-5">
                                <div className="card-body px-md-2">
                                    <div className="headings mb-4">
                                        <h4 className="text-warning border-bottom pb-2">Individual Information</h4>
                                    </div>
                                    <div className="row">
                                        <div className="col-md-4">
                                            <div className="form-group">
                                                <label for="title">Title</label>
                                                <input type="text" className="form-control" id="title" value={this.state.title} />
                                            </div>
                                        </div>
                                        <div className="col-md-4">
                                            <div className="form-group">
                                                <label for="fname">First Name :</label>
                                                <input type="text" className="form-control" id="fname" value={this.state.first_name} />
                                            </div>
                                        </div>
                                        <div className="col-md-4">
                                            <div className="form-group">
                                                <label for="lname">Last Name :</label>
                                                <input type="text" className="form-control" id="lname" value={this.state.last_name} />
                                            </div>
                                        </div>

                                        <div className="col-md-6">
                                            <div className="form-group">
                                                <label for="phonenumber">Phone Number:</label>
                                                <input type="number" className="form-control" id="phonenumber" value={this.state.mobile} />
                                            </div>
                                        </div>
                                        <div className="col-md-6">
                                            <div className="form-group">
                                                <label for="email">Email Address:</label>
                                                <input type="email" className="form-control" id="email" value={this.state.email} />
                                            </div>
                                        </div>
                                    </div>

                                    <div className="headings mt-4 mb-4">
                                        <h4 className="text-warning border-bottom pb-2">Cover Letter</h4>
                                    </div>
                                    <div className="row">
                                        <div className="col-md-12">
                                            <div className="form-group">
                                                <label for="tellwhyjointeam">Tell us why you want to join our team!</label>
                                                <textarea className="form-control" rows="7" id="tellwhyjointeam" value={this.state.info}></textarea>
                                            </div>
                                        </div>
                                    </div>

                                    <div className="headings mt-4 mb-4">
                                        <h4 className="text-warning border-bottom pb-2">Your Profile</h4>
                                    </div>
                                    <div className="row">
                                        <div className="col-md-12">
                                            <div className="form-group">
                                                <p className="text-lightgray">Resume/CV:
                                                <a href={this.state.cv_url} download target="_blank" >Click to download</a>

                                                </p>
                                            </div>
                                        </div>
                                    </div>

                                    <div className="headings mt-4 mb-4">
                                        <h4 className="text-warning border-bottom pb-2">Your Web Presence</h4>
                                    </div>
                                    <div className="row ">
                                        <div className="col-md-12">
                                            <div className="form-group row align-items-center">
                                                <div className="col-sm-3 col-md-2 text-sm-right">
                                                    <label for="website" className="mb-sm-0">Website</label>
                                                </div>
                                                <div className="col-sm-9 col-md-6">
                                                    <input className="form-control" id="website" value={this.state.website} />
                                                </div>
                                            </div>
                                            <div className="form-group row align-items-center">
                                                <div className="col-sm-3 col-md-2 text-sm-right">
                                                    <label for="linkedin" className="mb-sm-0">Linkedin</label>
                                                </div>
                                                <div className="col-sm-9 col-md-6">
                                                    <input className="form-control" id="linkedin" value={this.state.linkedin} />
                                                </div>
                                            </div>
                                            <div className="form-group row align-items-center">
                                                <div className="col-sm-3 col-md-2 text-sm-right">
                                                    <label for="twitter" className="mb-sm-0">Twitter</label>
                                                </div>
                                                <div className="col-sm-9 col-md-6">
                                                    <input className="form-control" id="twitter" value={this.state.twitter} />
                                                </div>
                                            </div>
                                            <div className="form-group row align-items-center">
                                                <div className="col-sm-3 col-md-2 text-sm-right">
                                                    <label for="github" className="mb-sm-0">Github </label>
                                                </div>
                                                <div className="col-sm-9 col-md-6">
                                                    <input className="form-control" id="github" value={this.state.github} />
                                                </div>
                                            </div>
                                            <div className="form-group row align-items-center">
                                                <div className="col-sm-3 col-md-2 text-sm-right">
                                                    <label for="behance" className="mb-sm-0">Behance</label>
                                                </div>
                                                <div className="col-sm-9 col-md-6">
                                                    <input className="form-control" id="behance" value={this.state.behance} />
                                                </div>
                                            </div>
                                            <div className="form-group row align-items-center">
                                                <div className="col-sm-3 col-md-2 text-sm-right">
                                                    <label for="dribble" className="mb-sm-0">Dribble</label>
                                                </div>
                                                <div className="col-sm-9 col-md-6">
                                                    <input className="form-control" id="dribble" value={this.state.dribbble} />
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <Footer />
                            </div>
                        </div>
                    </div>
                </section>
            </>
        )
    }
}
