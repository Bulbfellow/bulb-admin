import React, { Component } from 'react'
import { Link } from 'react-router-dom'
import Footer from '../../../Footer'
import AddGalleryIcon from '../../../../Assets/img/addgallery-img.png'
import '../../style.css'
import { Api } from '../../../../Const/Api'
import Axios from 'axios'
import { connect } from 'react-redux'
import { addCareers } from '../../../../Action/careers'
import { Editor } from 'react-draft-wysiwyg';
import {  EditorState } from 'draft-js';
import 'react-draft-wysiwyg/dist/react-draft-wysiwyg.css';
import { stateToHTML } from "draft-js-export-html";
import Quicknotification from '../../../../Component/Quicknotification'


class AddCareers extends Component {
    constructor(props) {
        super(props);
        this.state = ({
            title: '',
            addlogo: '',
            jobtype: '',
            description: '',
            entryCloser: '',
            company_name: '',
            location: [],
            job: [],
            selectedFile: null,
            openmodal: false,
            editorState: EditorState.createEmpty()
        })


    }

    onEditorStateChange = (editorState) => {
        this.setState({ editorState :editorState,
            editorContentHtml: stateToHTML(editorState.getCurrentContent())
        });        
    };

    componentDidMount() {
        Axios.get(Api.BaseUrl + "/api/joblocation-list")
            .then(res => {
             
                const Dropdown = res.data.data.list
                this.setState({ location: Dropdown })
            })
            .catch(error => {
                console.log(error.res);
            });
        Axios.get(Api.BaseUrl + "/api/contracttype-list")
            .then(res => {
             
                const Dropdown2 = res.data.data.list
                this.setState({ job: Dropdown2 })
            })
            .catch(error => {
                console.log(error.res);
            });

    }

    handleFileUpload = (e) => {
        this.setState({ selectedFile: e.target.files[0] },
            () => {
                this.uploadHandler()
            })
    }

    uploadHandler = () => {
        const formData = new FormData()
        formData.append(
            'file',
            this.state.selectedFile
        )
        setTimeout(() => {
            Axios.post(Api.BaseUrl + "/api/file-upload", formData)
                .then(res => {
                  
                    const Url = res.data.data.image_url
                    this.setState({ Url })
                })
                .catch((error) => {
                    console.log("error", error)
                })
        }, 500)
    }

    postCareer = (e) => {
        e.preventDefault();
        let data = {
            title: this.state.title,
            Url: this.state.Url,
            jobtype: this.state.jobtype,
            description: this.state.editorContentHtml,
            entryCloser: this.state.entryCloser,
            location_id: this.state.location_id,
            company_name: this.state.company_name
        }
        this.setState({
            openmodal: true
        })
        this.props.addCareersDispatch(data)
        this.setState({
            // title: "",
            // Url: "",
            // jobtype: "",
            // description: "",
            // entryCloser: "",
            location_id: "",
            // company_name: ""
            title: '',
            addlogo: '',
            jobtype: '',
            description: '',
            entryCloser: '',
            company_name: '',
            Url : '',
            selectedFile: null,
            editorState: EditorState.createEmpty()
        })

    }

    handleClose = () => {
        this.setState({ openmodal: false });
    };


    render() {
        const { location ,job} = this.state;
        return (
            <>
                <section className="wrapper px-4">
                    <form onSubmit={this.postCareer}>
                        <div className="container">
                            <div className="page-title mb-4">
                                <div className="row align-items-center">
                                    <div className="col-md-5">
                                        <div className="page-title-inner">
                                            <h4>Add Careers</h4>
                                        </div>
                                    </div>
                                    <div className="col-md-7">
                                        <nav aria-label="breadcrumb">
                                            <ol className="breadcrumb mb-2 justify-content-end p-0 bg-transparent align-items-center">
                                                <li className="breadcrumb-item"><Link to="/">Dashboard</Link></li>
                                                <li className="breadcrumb-item"><Link to="#">Content Mangement</Link></li>
                                                <li className="breadcrumb-item"><Link to="/careers">Careers</Link></li>
                                                <li className="breadcrumb-item active" aria-current="page">Add careers</li>
                                            </ol>
                                        </nav>
                                    </div>
                                </div>
                            </div>



                            <div className="view-entry">
                                <div className="row mb-3 align-items-center">
                                    <div className="col-md-6">

                                    </div>
                                    <div className="col-md-6 text-center text-md-right">
                                        <Link to="/careers"><button className="btn btn-outline-warning btn-warning-cus px-4 fs-14 rounded-50">Back to careers</button></Link>
                                    </div>
                                </div>
                                <div className="card border-0  px-md-4 shadow-sm">
                                    <div className="card-body hire-devs px-md-2">
                                        <div className="row">
                                            <div className="col-md-8 mb-3">
                                                <div className="form-group ">
                                                    <label for="jobTitle">Job title</label>
                                                    <input type="text" className="form-control" id="jobTitle" name="title" onChange={(e) => this.setState({ title: e.target.value })} value={this.state.title}/>
                                                </div>
                                            </div>
                                            <div className="col-md-8 mb-3">
                                                <div className="form-group ">
                                                    <label for="jobTitle">Company</label>
                                                    <input type="text" className="form-control" id="company_name" name="company_name" onChange={(e) => this.setState({ company_name: e.target.value })} value={this.state.company_name}/>
                                                </div>
                                            </div>
                                            <div className="col-md-8 mb-3">
                                                <div className="form-group add-logo-career">
                                                    <label className="mb-0">Add logo</label>
                                                    <input type="file" className="form-control d-none" id="addlogo" onChange={this.handleFileUpload} value={this.state.addlogo}/>
                                                    <div className="add-gallery my-3 d-flex align-items-center">
                                                        <label className="text-center" for="addlogo">
                                                            <img src={AddGalleryIcon} alt="Add Image icon" className="img-fluid icon" />
                                                        </label>
                                                        <div className="viewImage d-flex align-items-center">
                                                            {!this.state.Url === "" ?
                                                                <img src={this.state.Url} alt="Logo" className="img-fluid" />
                                                                : ''}
                                                        </div>
                                                    </div>
                                                    <p className="text-secondary fs-12">Jpeg or PNG not larger than 5MB</p>
                                                </div>
                                            </div>

                                            <div className="col-md-8 mb-3">
                                                <div className="form-group">
                                                    <label for="jobtype">Job type</label>
                                                    <select className="form-control" id="jobtype" onChange={(e) => this.setState({ jobtype: e.target.value })}>
                                                        <option defaultValue=" ">Select</option>

                                                        {job.map(job => {
                                                            return (
                                                                <option value={job.contract_name}>{job.contract_name}</option>
                                                            )
                                                        })}

                                                    </select>
                                                </div>
                                            </div>

                                            <div className="col-md-8 mb-3">
                                                <div className="form-group">
                                                    <label for="description">Description</label>
                                                    <div className="ckeditor-cus">    

                                                        <Editor
                                                            editorState={this.state.editorState}
                                                            wrapperClassName="demo-wrapper"
                                                            editorClassName="demo-editor"
                                                            onEditorStateChange={this.onEditorStateChange}
                                                            ref="content"
                                                        />
                                                    </div>
                                                </div>
                                            </div>

                                            <div className="col-md-8 mb-3">
                                                <div className="form-group">
                                                    <label for="entryclosure">Entry closure</label>
                                                    <input type="date" className="form-control text-uppercase" id="entryclosure" onChange={(e) => this.setState({ entryCloser: e.target.value })} value={this.state.entryCloser}/>
                                                </div>
                                            </div>

                                            <div className="col-md-8 mb-3">
                                                <div className="form-group">
                                                    <label for="location">Location</label>
                                                    <select className="form-control" id="location" value={this.state.location_id} onChange={(e) => this.setState({ location_id: e.target.value })}>
                                                        <option defaultValue="">Select</option>
                                                        {location.map(location => {
                                                            return (
                                                                <option value={location.location_name}>{location.location_name}</option>
                                                            )
                                                        })}

                                                    </select>
                                                </div>
                                            </div>
                                        </div>

                                        <div className="row">
                                            <div className="col-md-12">
                                                <div className="form-group text-center">
                                                    <button className="btn btn-warning px-4 text-white">Post career</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </section>
                <Footer />
                <Quicknotification
                        show={this.state.openmodal}
                        message={"Career listing have been posted sucessfully"}
                        closetab={() => this.setState({openmodal : false})}
                        // bgblock={true}
                        closesecond={8000}
                        timeout={true}
                    />
                {/* <Snackbar
                    open={this.state.openmodal}
                    onClose={this.handleClose}
                    message="Your Data is submitted"
                /> */}
            </>
        )
    }
}
const mapStateToProps = state => {
    return {
        state
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        addCareersDispatch: (data) => { dispatch(addCareers(data)) }
    }
}
export default connect(mapStateToProps, mapDispatchToProps)(AddCareers)

