import React, { Component } from 'react'
import { Link } from 'react-router-dom'
import Footer from '../../../Footer'
import AddGalleryIcon from '../../../../Assets/img/addgallery-img.png'
import '../../style.css'
import Axios from 'axios'
import { CareersJobListingUpdate } from '../../../../Action/careers'
import { connect } from 'react-redux'
import { Api } from '../../../../Const/Api'
import { Editor } from 'react-draft-wysiwyg';
import { EditorState, convertFromHTML, ContentState, CompositeDecorator, ContentBlock, convertToRaw, } from 'draft-js';
import 'react-draft-wysiwyg/dist/react-draft-wysiwyg.css';
import { stateToHTML } from "draft-js-export-html";

class EditCareers extends Component {
  constructor(props) {
    super(props);
    this.state = {
      editList: '',
      title: '',
      logo_url: '',
      description: '',
      job_type: '',
      entry_closure: '',
      location: '',
      selectedFile: null,
      sr_no: '',
      company_name: '',
      locationList: [],
      jobList: [],
      // editorState: EditorState.createEmpty(),
    }
  }

  handleChange = (e) => {
    this.setState({
      [e.target.name]: e.target.value
    })
  }
  handleDateChange = date => {
    this.setState({ selectedDate: date });
};
handleTimeChange = date => {
    this.setState({ selectedTime: date });
};

  onEditorStateChange = (editorState) => {
    this.setState({
      editorState,
      editorContentHtml: stateToHTML(editorState.getCurrentContent())
    });
  };

  handleSubmit = (e) => {
    e.preventDefault()
    this.setState({
      editorContentHtml: stateToHTML(this.state.editorState.getCurrentContent())
    }, () => {
      let data = {
        sr_no: this.state.sr_no,
        title: this.state.title,
        company_name: this.state.company_name,
        logo_url: this.state.logo_url,
        description: this.state.editorContentHtml,
        job_type: this.state.job_type,
        entry_closure: this.state.entry_closure,
        location: this.state.location
      }
      this.props.CarearjobUpdateDispatch(data)
      // this.props.history.push('/careers')
    })
  }
 
  handlefileUpload = (e) => {
    this.setState({ selectedFile: e.target.files[0] })
    const formData = new FormData()
    formData.append(
      'file',
      e.target.files[0]
    )
    Axios.post(Api.BaseUrl+'/api/file-upload', formData,)
      .then(res => {
        const Url = res.data.data.image_url
        this.setState({ logo_url: Url })

      })
  }



  componentDidMount() {
    Axios.get(Api.BaseUrl+`/api/carrerpost/${this.props.match.params.id}`)

      .then(res => {
        const getData = res.data.data.list
        this.setState({ sr_no: getData.sr_no })
        this.setState({ title: getData.title })
        this.setState({ company_name: getData.company_name })
        this.setState({ logo_url: getData.logo_url })
        this.setState({ editorState: EditorState.createWithContent(ContentState.createFromBlockArray(convertFromHTML(getData.description))), })
        this.setState({ job_type: getData.job_type })
        this.setState({ entry_closure: getData.entry_closure })
        this.setState({ location: getData.location })
      })
    Axios.get(Api.BaseUrl + `/api/contracttype-list`)
      .then(res => {
        const contractData = res.data.data.list
        this.setState({ jobList: contractData })
      })
    Axios.get(Api.BaseUrl + `/api/joblocation-list`)
      .then(res => {
        const locationtData = res.data.data.list
        this.setState({ locationList: locationtData })
      })

    // const blocksFromHTML = convertFromHTML(this.state.description);
    // const htmlData = ContentState.createFromBlockArray(
    //     blocksFromHTML.contentBlocks,
    //     blocksFromHTML.entityMap,
    // );
    //     const blocksFromHTML = convertFromHTML(this.state.description);
    // const state = ContentState.createFromBlockArray(
    //     blocksFromHTML.contentBlocks,
    //     blocksFromHTML.entityMap,
    // );
    // this.state({
    //     editorState: EditorState.createWithContent(state)
    // })
  }


  render() {
    const { locationList } = this.state;
    const { jobList } = this.state;
    // const blocksFromHtml = htmlToDraft(this.props.description);
    // const { contentBlocks, entityMap } = blocksFromHtml;
    // const contentState = ContentState.createFromBlockArray(contentBlocks, entityMap);
    // const editorState = EditorState.createWithContent(contentState);
    return (
      <>
        <section className="wrapper px-4 ">
          <div className="container">
            <div className="page-title mb-4">
              <div className="row align-items-center">
                <div className="col-md-5">
                  <div className="page-title-inner">
                    <h4>Edit Careers</h4>
                  </div>
                </div>
                <div className="col-md-7">
                  <nav aria-label="breadcrumb">
                    <ol className="breadcrumb mb-2 justify-content-end p-0 bg-transparent align-items-center">
                      <li className="breadcrumb-item"><Link to="/">Dashboard</Link></li>
                      <li className="breadcrumb-item"><Link to="#">Content Mangement</Link></li>
                      <li className="breadcrumb-item"><Link to="/careers">Careers</Link></li>
                      <li className="breadcrumb-item active" aria-current="page">Edit careers</li>
                    </ol>
                  </nav>
                </div>
              </div>
            </div>

            <div className="view-entry">
              <div className="row mb-3 align-items-center">
                <div className="col-md-6">

                </div>
                <div className="col-md-6 text-center text-md-right">
                  <Link to="/careers"><button className="btn btn-outline-warning btn-warning-cus px-4 fs-14 rounded-50">Back to careers</button></Link>
                </div>
              </div>
              <form onSubmit={this.handleSubmit}>
                <div className="card border-0  px-md-4 shadow-sm">
                  <div className="card-body hire-devs px-md-2">
                    <div className="row">
                      <div className="col-md-8 mb-3">
                        <div className="form-group">
                          <label for="jobTitle">Job title</label>
                          <input type="text" className="form-control" id="jobTitle" value={this.state.title} name="title" onChange={this.handleChange} />
                        </div>
                      </div>
                      <div className="col-md-8 mb-3">
                        <div className="form-group">
                          <label for="jobTitle">Company</label>
                          <input type="text" className="form-control" id="company_name" value={this.state.company_name} name="company_name" onChange={this.handleChange} />
                        </div>
                      </div>
                      <div className="col-md-8 mb-3">
                        <div className="form-group add-logo-career">
                          <label className="mb-0">Add logo</label>
                          <input type="file" className="form-control d-none" id="addlogo" onChange={this.handlefileUpload} />
                          <div className="add-gallery my-3 d-flex align-items-center">
                            <label className="text-center" for="addlogo">
                              <img src={AddGalleryIcon} alt="Add Image icon" className="img-fluid icon" />
                            </label>
                            <div className="viewImage d-flex align-items-center">
                              <img src={this.state.logo_url} alt="Logo" className="img-fluid" />
                            </div>
                          </div>
                          <p className="text-secondary fs-12">Jpeg or PNG not larger than 5MB</p>
                        </div>
                      </div>
                    </div>
                    <div className="row">
                      <div className="col-md-8 mb-3">
                        <div className="form-group">
                          <label for="jobtype">Job type</label>
                          <select className="form-control" id="jobtype" name="jobtype" onChange={this.handleChange}>
                            <option value={this.state.job_type} selected>{this.state.job_type}</option>
                            {jobList.map(jobList => {
                              return (
                                <option value={jobList.contract_name}>{jobList.contract_name}</option>
                              )
                            })}
                          </select>
                        </div>
                      </div>
                    </div>
                    <div className="row">
                      <div className="col-md-8 mb-3">
                        <div className="form-group">
                          <label for="description">Description</label>
                          <div className="ckeditor-cus">

                            <Editor
                              editorState={this.state.editorState}
                              wrapperClassName="demo-wrapper"
                              editorClassName="demo-editor"
                              onEditorStateChange={this.onEditorStateChange}
                            />                                                    </div>
                        </div>
                      </div>
                    </div>
                    <div className="row mb-5">
                      <div className="col-md-3">
                        <div className="form-group">
                          <label for="entryclosure">Entry closure</label>
                          <input type="date" className="form-control text-uppercase" id="entryclosure" name="entry_closure" value={this.state.entry_closure} onChange={this.handleChange} />
                        </div>
                      </div>
                      <div className="col-md-2"></div>
                      <div className="col-md-3">
                        <div className="form-group">
                          <label for="location">Location</label>
                          <select className="form-control" id="location" name="location" onChange={this.handleChange}>
                            <option Value={this.state.location}>{this.state.location}</option>
                            {locationList.map(locationList => {
                              return (
                                <option value={locationList.location_name}>{locationList.location_name}</option>
                              )
                            })}

                          </select>
                        </div>
                      </div>
                    </div>

                    <div className="row">
                      <div className="col-md-12">
                        <div className="form-group text-center">
                          <button className="btn btn-warning px-4 text-white" name="submit">Save edit</button>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </form>
            </div>
          </div>
        </section>
        <Footer />
      </>
    )
  }
}

const mapStateToProps = state => {
  return {
    state
  }
}
const mapDispatchToProps = (dispatch) => {
  return {
    CarearjobUpdateDispatch: (data) => { dispatch(CareersJobListingUpdate(data)) }
  }
}
export default connect(mapStateToProps, mapDispatchToProps)(EditCareers)
