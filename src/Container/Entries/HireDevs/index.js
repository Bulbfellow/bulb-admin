import React, { Component } from 'react'
import { Link } from 'react-router-dom'
import Footer from '../../Footer'
import '../style.css'
import Axios from 'axios'
import { Api } from '../../../Const/Api'

export default class HireDevs extends Component {
    constructor(props) {
        super(props);
        this.state = {
            sr_no: " ",
            first_name: "",
            last_name: '',
            company_name: '',
            work_email: '',
            country: '',
            state: '',
            mobile: ''
        }
    }
    componentDidMount() {
        Axios.get(Api.BaseUrl+`/api/hire/${this.props.match.params.id}`, {
            'headers': { 'Authorization': localStorage.getItem('adminToken') }
        })
            .then(resp => {
                const data = resp.data.data.list
                this.setState({
                    sr_no: data.sr_no,
                    first_name: data.first_name,
                    last_name: data.last_name,
                    company_name: data.company_name,
                    work_email: data.work_email,
                    country: data.country,
                    state: data.state,
                    mobile: data.mobile  
                })
            })
            .catch(err => {
                console.log('errrr-------messgae', err)

            });

    }

    render() {
        // console.log('hite', this.props.this.state)
        // const { userinfo } = this.props
        return ( 
            <>
                <section className="wrapper px-4 ">
                    <div className="container">
                        <div className="page-title mb-4">
                            <div className="row align-items-center">
                                <div className="col-md-5">
                                    <div className="page-title-inner">
                                        <h4>View Entry</h4>
                                    </div>
                                </div>
                                <div className="col-md-7">
                                    <nav aria-label="breadcrumb">
                                        <ol className="breadcrumb mb-2 justify-content-end p-0 bg-transparent align-items-center">
                                            <li className="breadcrumb-item"><Link to="/">Dashboard</Link></li>
                                            <li className="breadcrumb-item"><Link to="/application-request">Application request</Link></li>
                                            <li className="breadcrumb-item"><Link to="/application-request">Hire Devs</Link></li>
                                            <li className="breadcrumb-item active" aria-current="page">View Entry</li>
                                        </ol>
                                    </nav>
                                </div>
                            </div>
                        </div>

                        <div className="view-entry">
                            <div className="row mb-3 align-items-center">
                                <div className="col-md-6">
                                    <div className="form-name text-center text-md-left">
                                        <h5 className="mb-md-0">Hire Devs</h5>
                                    </div>
                                </div>
                                <div className="col-md-6 text-center text-md-right">
                                    <Link to="/application-request/hiredevs-tab"><button className="btn btn-outline-warning px-4 fs-14 rounded-50">Back to entries</button></Link>
                                </div>
                            </div>
                            <div className="card border-0  px-md-4 shadow-sm">
                                <div className="card-body hire-devs px-md-2">
                                    <div className="row">
                                        <div className="col-md-5">
                                            <div className="form-group">
                                                <label for="fname">First Name</label>
                                                <input type="text" className="form-control" id="fname" value={this.state.first_name} />
                                            </div>
                                        </div>
                                        <div className="col-md-5">
                                            <div className="form-group">
                                                <label for="lname">Last Name</label>
                                                <input type="text" className="form-control" id="lname" value={this.state.last_name} />
                                            </div>
                                        </div>
                                    </div>
                                    <div className="row">
                                        <div className="col-md-5">
                                            <div className="form-group">
                                                <label for="companyname">Company name</label>
                                                <input type="text" className="form-control" id="companyname" value={this.state.company_name} />
                                            </div>
                                        </div>
                                        <div className="col-md-5">
                                            <div className="form-group">
                                                <label for="workemail">Work email</label>
                                                <input type="email" className="form-control" id="workemail" value={this.state.work_email} />
                                            </div>
                                        </div>
                                    </div>
                                    <div className="row">
                                        <div className="col-md-5">
                                            <div className="form-group">
                                                <label for="country">Country</label>
                                                <input type="text" className="form-control" id="country" value={this.state.country} />
                                            </div>
                                        </div>
                                        <div className="col-md-5">
                                            <div className="form-group">
                                                <label for="state">State</label>
                                                <input type="text" className="form-control" id="state" value={this.state.state} />
                                            </div>
                                        </div>
                                    </div>
                                    <div className="row mb-4">
                                        <div className="col-md-10">
                                            <div className="form-group">
                                                <label for="phonenumber">Phone Number:</label>
                                                <input type="number" className="form-control" id="phonenumber" value={this.state.mobile} />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
                <Footer />
            </>
        )
    }
}
