import React, { Component } from 'react'
import { Link } from 'react-router-dom'
import Footer from '../../Footer'
import '../style.css'
import Axios from 'axios'
import {Api} from '../../../Const/Api'

export default class WorkspaceRequest extends Component {
    constructor() {
        super()
        this.state = {
            first_name: '',
            last_name: '',
            email: '',
            mobile: '',
            business_industry: '',
            business_name: '',
            description: '',
            plan_type: ''
        }
    }
    componentDidMount() {
        Axios.get(Api.BaseUrl + `/api/joinplan/${this.props.match.params.id}`, {
            'headers': { 'Authorization': localStorage.getItem('adminToken') },

        })
            .then(resp => {
                const userinfo = resp.data.data.list
                this.setState(userinfo)
            })
    }

    render() {
        const userinfo = this.state

       
        return (
            <>
                <section className="wrapper px-4 ">
                    <div className="container">
                        <div className="page-title mb-4">
                            <div className="row align-items-center">
                                <div className="col-md-5">
                                    <div className="page-title-inner">
                                        <h4>View Entry</h4>
                                    </div>
                                </div>
                                <div className="col-md-7">
                                    <nav aria-label="breadcrumb">
                                        <ol className="breadcrumb mb-2 justify-content-end p-0 bg-transparent align-items-center">
                                            <li className="breadcrumb-item"><Link to="/">Dashboard</Link></li>
                                            <li className="breadcrumb-item"><Link to="/the-desk">the desk</Link></li>
                                            <li className="breadcrumb-item"><Link to="/workspace">Workspace request</Link></li>
                                            <li className="breadcrumb-item active" aria-current="page">View entry</li>
                                        </ol>
                                    </nav>
                                </div>
                            </div>
                        </div>
                        <div className="view-entry">
                            <div className="row mb-3 align-items-center">
                                <div className="col-md-6">
                                    <div className="form-name text-center text-md-left">
                                        <h5 className="mb-md-0">Workspace request</h5>
                                    </div>
                                </div>
                                <div className="col-md-6 text-center text-md-right">
                                    <Link to="/the-desk"><button className="btn btn-outline-warning btn-warning-cus px-4 fs-14 rounded-50">Back to entries</button></Link>
                                </div>
                            </div>
                            <div className="card border-0  px-md-4 shadow-sm">
                                <div className="card-body hire-devs px-md-2">
                                    <div className="row">
                                        <div className="col-md-5">
                                            <div className="form-group">
                                                <label for="fname">First Name</label>
                                                <input type="text" className="form-control" id="fname" value={userinfo.first_name} />
                                            </div>
                                        </div>
                                        <div className="col-md-5">
                                            <div className="form-group">
                                                <label for="lname">Last Name</label>
                                                <input type="text" className="form-control" id="lname" value={userinfo.last_name} />
                                            </div>
                                        </div>
                                    </div>
                                    <div className="row">
                                        <div className="col-md-5">
                                            <div className="form-group">
                                                <label for="email">Email</label>
                                                <input type="email" className="form-control" id="email" value={userinfo.email} />
                                            </div>
                                        </div>
                                        <div className="col-md-5">
                                            <div className="form-group">
                                                <label for="phonenumber">Phone Number</label>
                                                <input type="number" className="form-control" id="phonenumber" value={userinfo.mobile} />
                                            </div>
                                        </div>
                                    </div>
                                    <div className="row">
                                        <div className="col-md-5">
                                            <div className="form-group">
                                                <label for="businessindustry">Business industry</label>
                                                <input type="text" className="form-control" id="businessindustry" value={userinfo.business_industry} />
                                            </div>
                                        </div>
                                        <div className="col-md-5">
                                            <div className="form-group">
                                                <label for="businessname">Business name</label>
                                                <input type="text" className="form-control" id="businessname" value={userinfo.business_name} />
                                            </div>
                                        </div>
                                    </div>
                                    <div className="row">
                                        <div className="col-md-5">
                                            <div className="form-group">
                                                <label for="businessindustry">Plan type</label>
                                                <input type="text" className="form-control" id="businessindustry" value={userinfo.plan_type} />
                                            </div>
                                        </div>
                                        
                                    </div>
                                    <div className="row mb-4">
                                        <div className="col-md-10">
                                            <div className="form-group">
                                                <label for="notes">Notes</label>
                                                <textarea rows="5" className="form-control" id="notes" value={userinfo.description} ></textarea>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
                <Footer />
            </>
        )
    }
}
