import React, { Component } from 'react'
import { Link } from 'react-router-dom'
import Footer from '../../Footer'
import AddImage from '../../../Assets/img/addimage-icon.png'
import '../style.css'
import Axios from 'axios'
import { Editor } from 'react-draft-wysiwyg'
import { EditorState } from 'draft-js'
import 'react-draft-wysiwyg/dist/react-draft-wysiwyg.css'
import { stateToHTML } from "draft-js-export-html"
import { addWorkspace } from '../../../Action/workspace'
import { connect } from 'react-redux'
import { Uploadfile } from '../../../Action/commonAction'
import CircularProgress from '@material-ui/core/CircularProgress';
import { Api } from '../../../Const/Api'
class AddWorkspace extends Component {
    constructor(props) {
        super(props);
        this.state = {
            title: '',
            desc: '',
            imgUrl: '',
            image: '',
            selectedDate: new Date(),
            editorState: EditorState.createEmpty(),
            fileuploadloader: false,
            fileuploadloadermsg: '',
            status: '',
            respMessage: ''
        }
    }

    // componentDidUpdate(prevProp, prevState){
    //     // console.log("prevProp", prevProp)
    //     if (prevProp.WorkspaceState.message !== this.props.WorkspaceState.message) {
    //         this.setState({
    //             loader : false,
    //             message: this.props.WorkspaceState.message,
    //             showpopup: true,
    //         })
    //     }

    //     if (prevProp.WorkspaceState.loading !== this.props.WorkspaceState.loading) {
    //         this.setState({
    //             loader : this.props.WorkspaceState.loading
    //         })
    //     }
    // }
    componentDidUpdate(prevProp) {
        if (prevProp.FileUploadState.loading !== this.props.FileUploadState.loading) {
            this.setState({
                imgUrl: this.props.FileUploadState.resp,
                fileuploadloader: this.props.FileUploadState.loading,
                fileuploadloadermsg: this.props.FileUploadState.error
            })
        }
    }

    imageupload = (e) => {
        this.setState({
            fileuploadloader: true
        })
        this.props.FileUploadDispatch(e.target.files[0])
    }

    submitInfo = (e) => {
        e.preventDefault()
        const data = {
            title: this.state.title,
            image_url: this.state.imgUrl,
            description: this.state.editorContentHtml
        }
        Axios.post(Api.BaseUrl + "/api/workspace", data,
            { 'headers': { 'Authorization': localStorage.getItem('adminToken') } })
            .then(resp => {
                this.setState({
                    status: resp.data.status,
                    respMessage : resp.data.message
                }, () => {
                    if (this.state.status === 1) {
                        this.setState({
                            title: '',
                            desc: '',
                            imgUrl: '',
                            image: '',
                            selectedDate: new Date(),
                            editorState: EditorState.createEmpty(),
                        })
                    }
                })
            })



    }

    onEditorStateChange = (editorState) => {
        this.setState({
            editorState: editorState,
            editorContentHtml: stateToHTML(editorState.getCurrentContent())
        });
    };
    render() {
        return (
            <>
                <section className="wrapper px-4 ">
                    <div className="container">
                        <div className="page-title mb-4">
                            <div className="row align-items-center">
                                <div className="col-md-5">
                                    <div className="page-title-inner">
                                        <h4>Add Workspace</h4>
                                    </div>
                                </div>
                                <div className="col-md-7">
                                    <nav aria-label="breadcrumb">
                                        <ol className="breadcrumb mb-2 justify-content-end p-0 bg-transparent align-items-center">
                                            <li className="breadcrumb-item"><Link to="/">Dashboard</Link></li>
                                            <li className="breadcrumb-item"><Link to="#">Content Mangement</Link></li>
                                            <li className="breadcrumb-item"><Link to="/workspace">Workspace</Link></li>
                                            <li className="breadcrumb-item active" aria-current="page">Add workspace</li>
                                        </ol>
                                    </nav>
                                </div>
                            </div>
                        </div>

                        <div className="view-entry">
                            <div className="row mb-3 align-items-center">
                                <div className="col-md-6">

                                </div>
                                <div className="col-md-6 text-center text-md-right">
                                    <Link to="/workspace" ><button className="btn btn-outline-warning px-4 fs-14 rounded-50">Back to press</button></Link>
                                </div>
                            </div>
                            <div className="card border-0  px-md-4 shadow-sm">
                                <div className="card-body hire-devs px-md-2">
                                    <div className="row">
                                        <div className="col-md-8">
                                            <div className="form-group pb-4">
                                                <label for="pressTitle">Title</label>
                                                <input type="text" className="form-control" id="pressTitle" name={"title"} value={this.state.title}
                                                    onChange={(e) => this.setState({ title: e.target.value })} />
                                            </div>
                                        </div>
                                        <div className="col-md-8">
                                            <div className="form-group">
                                                <label>Image</label>
                                                <input type="file" className="form-control d-none" id="addpressimage" onChange={this.imageupload} />

                                                <div className="add-image">
                                                    {this.state.imgUrl === '' ?
                                                        <label className="text-center w-100" for="addpressimage">
                                                            <img src={AddImage} alt="Add Image icon" className="img-fluid icon" />
                                                            <h5 className="fs-18 pt-2">Click to add image</h5>
                                                            <p className="text-secondary mb-0 fs-14">Jpeg or PNG not larger than 10MB</p>
                                                        </label>
                                                        :
                                                        <label className="text-center w-100" for="addpressimage">
                                                            <div className="uplaodedImage">
                                                                <img src={this.state.imgUrl} alt="Events image" className="img-fluid" />
                                                            </div>
                                                        </label>
                                                    }
                                                    {this.state.fileuploadloader && this.state.fileuploadloader && <CircularProgress />}
                                                    {this.state.fileuploadloadermsg}
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="row">
                                        <div className="col-md-8">
                                            <div className="form-group">
                                                <label for="pressdescription">Description</label>
                                                <Editor
                                                    editorState={this.state.editorState}
                                                    wrapperClassName="demo-wrapper"
                                                    editorClassName="demo-editor"
                                                    onEditorStateChange={this.onEditorStateChange}
                                                    ref="content"
                                                />
                                            </div>
                                        </div>
                                    </div>
                                    <div className="row">
                                        <div className="col-md-12">
                                            <div className="form-group text-center">
                                                <button className="btn btn-warning px-4 text-white" onClick={this.submitInfo}>Post Press</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    {/* {this.state.loader && this.state.loader && <CircularProgress  />} */}
                </section>

                {/* <Quicknotification
                    show={this.state.showpopup}
                    message={this.state.message}
                    closetab={() => this.setState({ showpopup: false })}
                    closesecond={4000}
                    timeout={true}
                /> */}
                <Footer />
            </>
        )
    }
}


const mapStateToProps = state => {
    return {
        FileUploadState: state.FileuploadReducer,
    }
}
const mapDispatchToProps = (dispatch) => {
    return {
        DispatchWorkspaceAdd: (data) => { dispatch(addWorkspace(data)) },
        FileUploadDispatch: (file) => { dispatch(Uploadfile(file)) },
    }
}
export default connect(mapStateToProps, mapDispatchToProps)(AddWorkspace)
