import React, { Component } from 'react'
import { Link } from 'react-router-dom'
import Footer from '../../Footer'
import '../style.css'
import Axios from 'axios'
import {logout} from '../../../Action/applicationRequestAction'
import {Api} from '../../../Const/Api'

export default class SubmitIdea extends Component {
    constructor(props) {
        super(props);
        this.state = {
            title: '',
            first_name: '',
            last_name: '',
            mobile: '',
            email: '',
            city: '',
            state: '',
            about_self: '',
            about_product: '',
            file_url: ''
        }
    }
    componentDidMount() {
        
        Axios.get(Api.BaseUrl+`/api/submitidea/${this.props.match.params.id}`, {
            'headers': { 'Authorization': localStorage.getItem('adminToken') }
        })
            .then(resp => {
                if (resp.data.status === 2) {
                    logout()
                }
                const data = resp.data.data.list
                this.setState({
                    title: data.title,
                    first_name: data.first_name,
                    last_name: data.last_name,
                    mobile: data.mobile,
                    email: data.email,
                    city: data.city,
                    state: data.state,
                    about_self: data.about_self,
                    about_product: data.about_product,
                    file_url: data.file_url

                })

            })
            .catch(err => {
                console.log('errrr-------messgae', err)

            });

    }


    render() {
        return (
            <>
                <section className="wrapper px-4 ">
                    <div className="container">
                        <div className="page-title mb-4">
                            <div className="row align-items-center">
                                <div className="col-md-5">
                                    <div className="page-title-inner">
                                        <h4>View Entry</h4>
                                    </div>
                                </div>
                                <div className="col-md-7">
                                    <nav aria-label="breadcrumb">
                                        <ol className="breadcrumb mb-2 justify-content-end p-0 bg-transparent align-items-center">
                                            <li className="breadcrumb-item"><Link to="/">Dashboard</Link></li>
                                            <li className="breadcrumb-item"><Link to="/application-request">Application request</Link></li>
                                            <li className="breadcrumb-item"><Link to="/application-request/view-incubation-entry">Apply for Incubation</Link></li>
                                            <li className="breadcrumb-item active" aria-current="page">View Entry</li>
                                        </ol>
                                    </nav>
                                </div>
                            </div>
                        </div>

                        <div className="view-entry">
                            <div className="row mb-3 align-items-center">
                                <div className="col-md-6">
                                    <div className="form-name text-center text-md-left">
                                        <h5 className="mb-md-0">Apply for Incubation</h5>
                                    </div>
                                </div>
                                <div className="col-md-6 text-center text-md-right">
                                    <Link to="/application-request"><button className="btn btn-outline-warning px-4 fs-14 rounded-50">Back to entries</button></Link>
                                </div>
                            </div>
                            <div className="card border-0  px-md-4 shadow-sm">
                                <div className="card-body px-md-2">
                                    <div className="headings mb-4">
                                        <h4 className="text-warning border-bottom pb-2">About yourself</h4>
                                    </div>
                                    <div className="row">
                                        <div className="col-md-4">
                                            <div className="form-group">
                                                <label for="title">Title</label>
                                                <input type="text" className="form-control" id="title" defaultValue={this.state.title} />
                                            </div>
                                        </div>
                                        <div className="col-md-4">
                                            <div className="form-group">
                                                <label for="fname">First Name :</label>
                                                <input type="text" className="form-control" id="fname" defaultValue={this.state.first_name} />
                                            </div>
                                        </div>
                                        <div className="col-md-4">
                                            <div className="form-group">
                                                <label for="lname">Last Name :</label>
                                                <input type="text" className="form-control" id="lname" defaultValue={this.state.last_name} />
                                            </div>
                                        </div>

                                        <div className="col-md-6">
                                            <div className="form-group">
                                                <label for="phonenumber">Phone Number:</label>
                                                <input type="number" className="form-control" id="phonenumber" defaultValue={this.state.mobile} />
                                            </div>
                                        </div>
                                        <div className="col-md-6">
                                            <div className="form-group">
                                                <label for="email">Email Address:</label>
                                                <input type="email" className="form-control" id="email" defaultValue={this.state.email} />
                                            </div>
                                        </div>

                                        <div className="col-md-12">
                                            <div className="form-group">
                                                <label for="aboutyourself">Tell us about yourself (2000 characters max)</label>
                                                <textarea className="form-control" rows="7" id="aboutyourself" defaultValue={this.state.about_self}></textarea>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="row">
                                        <div className="col-md-4">
                                            <div className="form-group">
                                                <label for="title">City</label>
                                                <input type="text" className="form-control" id="title" defaultValue={this.state.city} />
                                            </div>
                                        </div>

                                        <div className="col-md-4">
                                            <div className="form-group">
                                                <label for="fname">State</label>
                                                <input type="text" className="form-control" id="fname" defaultValue={this.state.state} />
                                            </div>
                                        </div>
                                    </div>

                                    <div className="headings mt-4 mb-4">
                                        <h4 className="text-warning border-bottom pb-2">Your product/Idea</h4>
                                    </div>
                                    <div className="row">
                                        <div className="col-md-12">
                                            <div className="form-group mb-5">
                                                <label for="aboutyourself">Tell us about your product/Idea (2000 characters max)</label>
                                                <textarea className="form-control" rows="7" id="aboutyourself" defaultValue={this.state.about_product}></textarea>
                                            </div>
                                        </div>
                                        <div className="col-md-12">
                                            <div className="form-group mb-5">
                                                <label for="attachpresentaion">Attach a presentation, if any. (PDF, PowerPoint or Word Documents only)</label>
                                                <p className="text-lightgray"></p>
                                                <a href={this.state.file_url} download target="_blank">Click to download</a>

                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
                <Footer />
            </>
        )
    }
}
