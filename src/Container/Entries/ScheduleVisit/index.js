import React, { Component } from 'react'
import { Link } from 'react-router-dom'
import Footer from '../../Footer'
import '../style.css'
import Axios from 'axios'
import {logout} from '../../../Action/applicationRequestAction'
import {Api} from '../../../Const/Api'



export default class ScheduleVisit extends Component {
    constructor(props) {
        super(props);
        this.state = {
            title: '',
            first_name: '',
            last_name: '',
            mobile: '',
            email: '',
            organization: '',
            group_organization: '',
            group_size: '',
            participant_names: '',
            reason: '',
            info: ''
        }
    }
    componentDidMount() {
        Axios.get(Api.BaseUrl+`​/api/schedulevisit/${this.props.match.params.id}`, {
            'headers': { 'Authorization': localStorage.getItem('adminToken') }
        })
            .then(resp => {
                if (resp.data.status === 2) {
                    logout()
                }
                const data = resp.data.data.list
                this.setState({
                    title: data.title,
                    first_name: data.first_name,
                    last_name: data.last_name,
                    mobile: data.mobile,
                    email: data.email,
                    organization: data.organization,
                    group_organization: data.group_organization,
                    group_size: data.group_size,
                    participant_names: data.participant_names,
                    reason: data.reason,
                    info: data.info

                })

            })
            .catch(err => {
                console.log('errrr-------messgae', err)

            });

    }


    render() {

        return (
            <>
                <section className="wrapper px-4 ">
                    <div className="container">
                        <div className="page-title mb-4">
                            <div className="row align-items-center">
                                <div className="col-md-5">
                                    <div className="page-title-inner">
                                        <h4>View Entry</h4>
                                    </div>
                                </div>
                                <div className="col-md-7">
                                    <nav aria-label="breadcrumb">
                                        <ol className="breadcrumb mb-2 justify-content-end p-0 bg-transparent align-items-center">
                                            <li className="breadcrumb-item"><Link to="/">Dashboard</Link></li>
                                            <li className="breadcrumb-item"><Link to="/application-request">Application request</Link></li>
                                            <li className="breadcrumb-item"><Link to="/application-request/#schedule">Schedule a visit</Link></li>
                                            <li className="breadcrumb-item active" aria-current="page">View Entry</li>
                                        </ol>
                                    </nav>
                                </div>
                            </div>
                        </div>

                        <div className="view-entry">
                            <div className="row mb-3 align-items-center">
                                <div className="col-md-6">
                                    <div className="form-name text-center text-md-left">
                                        <h5 className="mb-md-0">Schedule a visit</h5>
                                    </div>
                                </div>
                                <div className="col-md-6 text-center text-md-right">
                                    <Link to="/application-request/schedule-tab"><button className="btn btn-outline-warning px-4 fs-14 rounded-50">Back to entries</button></Link>
                                </div>
                            </div>
                            <div className="card border-0  px-md-4 shadow-sm">
                                <div className="card-body px-md-2">
                                    <div className="headings mb-4">
                                        <h4 className="text-warning border-bottom pb-2">Individual Information</h4>
                                    </div>
                                    <div className="row">
                                        <div className="col-md-4">
                                            <div className="form-group">
                                                <label for="title">Title</label>
                                                <input type="text" className="form-control" id="title" value={this.state.title} />
                                            </div>
                                        </div>
                                        <div className="col-md-4">
                                            <div className="form-group">
                                                <label for="fname">First Name :</label>
                                                <input type="text" className="form-control" id="fname" value={this.state.first_name} />
                                            </div>
                                        </div>
                                        <div className="col-md-4">
                                            <div className="form-group">
                                                <label for="lname">Last Name :</label>
                                                <input type="text" className="form-control" id="lname" value={this.state.last_name} />
                                            </div>
                                        </div>

                                        <div className="col-md-6">
                                            <div className="form-group">
                                                <label for="phonenumber">Phone Number:</label>
                                                <input type="number" className="form-control" id="phonenumber" value={this.state.mobile} />
                                            </div>
                                        </div>
                                        <div className="col-md-6">
                                            <div className="form-group">
                                                <label for="email">Email Address:</label>
                                                <input type="email" className="form-control" id="email" value={this.state.email} />
                                            </div>
                                        </div>
                                        <div className="col-md-6">
                                            <div className="form-group">
                                                <label for="email">Organization :</label>
                                                <input type="email" className="form-control" id="email" value={this.state.organization} />
                                            </div>
                                        </div>
                                    </div>

                                    <div className="headings mt-4 mb-4">
                                        <h4 className="text-warning border-bottom pb-2">Group Information</h4>
                                    </div>
                                    <div className="row">
                                        <div className="col-md-6">
                                            <div className="form-group">
                                                <label for="organization">Organization:</label>
                                                <input type="text" className="form-control" id="organization" value={this.state.group_organization} />
                                            </div>
                                        </div>
                                        <div className="col-md-6">
                                            <div className="form-group">
                                                <label for="groupsize">Group Size:</label>
                                                <input type="text" className="form-control" id="groupsize" value={this.state.group_size} />
                                            </div>
                                        </div>
                                        <div className="col-md-12">
                                            <div className="form-group">
                                                <label for="participantnames">Participant Names:</label>
                                                <textarea className="form-control" rows="7" id="participantnames" value={this.state.participant_names}></textarea>
                                            </div>
                                        </div>
                                        <div className="col-md-12">
                                            <div className="form-group">
                                                <label for="reasontour">Reason for the tour:</label>
                                                <textarea className="form-control" rows="7" id="reasontour" value={this.state.reason}></textarea>
                                            </div>
                                        </div>
                                        <div className="col-md-12">
                                            <div className="form-group mb-5">
                                                <label for="additionalinfo">Additional info:</label>
                                                <textarea className="form-control" rows="7" id="additionalinfo" value={this.state.info}></textarea>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
                <Footer />
            </>
        )
    }
}
