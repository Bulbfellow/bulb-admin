import React, { Component } from 'react'
import { Link } from 'react-router-dom'
import Footer from '../../Footer'
import '../style.css'
import Axios from 'axios'
import { updateAdminList } from '../../../Action/manageAdmin'
import { connect } from 'react-redux'
import Quicknotification from '../../../Component/Quicknotification'
import CircularProgress from '@material-ui/core/CircularProgress';
import {Api} from '../../../Const/Api'

class EditAdmin extends Component {
    constructor() {
        super();
        this.state = {
            first_name: '',
            last_name: '',
            email: '',
            mobile: '',
            role: '',
            showpopup: false,
            message: "",
            loader: false
        }
    }
    componentDidMount() {
        Axios.get(Api.BaseUrl+`/api/admin/${this.props.match.params.id}`, {
            'headers': { 'Authorization': localStorage.getItem('adminToken') },
        }).then((resp) => {
            const adminData = resp.data.data.list
            this.setState(adminData)
        })
    }

    componentDidUpdate(prevProp) {
        if (prevProp.UpdateadminState.message !== this.props.UpdateadminState.message) {
            this.setState({
                message: this.props.UpdateadminState.message,
            })
        }

        if (prevProp.UpdateadminState.loading !== this.props.UpdateadminState.loading) {
            this.setState({
                loader : this.props.UpdateadminState.loading,
            })
        }
    }

    handleChange = (e) => {
        this.setState({ [e.target.name]: e.target.value })
    }

    handleSubmit = () => {
        this.setState({ loader: true, showpopup : true
        });
        let data = {
            admin_id: this.state.admin_id,
            first_name: this.state.first_name,
            last_name: this.state.last_name,
            email: this.state.email,
            mobile: this.state.mobile,
            role: this.state.role
        }
        this.props.updateAdminDispatch(data)
    }

    render() {
        const adminData = this.state

        return (
            <>
                <section className="wrapper px-4 ">
                    <div className="container">
                        <div className="page-title mb-4">
                            <div className="row align-items-center">
                                <div className="col-md-5">
                                    <div className="page-title-inner">
                                        <h4>Edit Admin</h4>
                                    </div>
                                </div>
                                <div className="col-md-7">
                                    <nav aria-label="breadcrumb">
                                        <ol className="breadcrumb mb-2 justify-content-end p-0 bg-transparent align-items-center">
                                            <li className="breadcrumb-item"><Link to="/">Dashboard</Link></li>
                                            <li className="breadcrumb-item"><Link to="/manage-admin">Manage admin</Link></li>
                                            <li className="breadcrumb-item active" aria-current="page">Edit admin</li>
                                        </ol>
                                    </nav>
                                </div>
                            </div>
                        </div>

                        <div className="view-entry">
                            <div className="row mb-3 align-items-center">
                                <div className="col-md-12 text-right">
                                    <Link to="/manage-admin"><button className="btn btn-outline-warning btn-warning-cus px-4 fs-14 rounded-50">Back to admin</button></Link>
                                </div>
                            </div>
                            <div className="card border-0  px-md-4 shadow-sm">
                                <div className="card-body hire-devs px-md-2">
                                    <div className="row pt-3">
                                        <div className="col-md-5">
                                            <div className="form-group">
                                                <label for="fname">First name</label>
                                                <input type="text" className="form-control" id="fname" value={adminData.first_name} name="first_name" onChange={this.handleChange} />
                                            </div>
                                        </div>
                                        <div className="col-md-5">
                                            <div className="form-group">
                                                <label for="lname">Last name</label>
                                                <input type="text" className="form-control" id="lname" value={adminData.last_name} name="last_name" onChange={this.handleChange} />
                                            </div>
                                        </div>
                                    </div>
                                    <div className="row">
                                        <div className="col-md-5">
                                            <div className="form-group">
                                                <label for="emailaddress">Email address</label>
                                                <input type="email" className="form-control" id="emailaddress" value={adminData.email} onChange={this.handleChange}/>
                                            </div>
                                        </div>
                                        <div className="col-md-5">
                                            <div className="form-group">
                                                <label for="mobilenumber">Mobile number</label>
                                                <input type="number" className="form-control" id="mobilenumber" value={adminData.mobile} onChange={this.handleChange}/>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="row mb-4">
                                        <div className="col-md-5">
                                            <div className="form-group">
                                                <label for="role">Role</label>
                                                <select className="form-control" id="role" value={adminData.role} name="role" onChange={this.handleChange}>
                                                    <option value="Super-Admin">Super Admin</option>
                                                    <option value="Admin">Admin</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="row">
                                        <div className="col-md-12">
                                            <div className="form-group text-center">
                                                <button className="btn btn-warning px-4 text-white" onClick={this.handleSubmit}>Save edit</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>{this.state.loader && this.state.loader && <CircularProgress />}
                </section>
                <Quicknotification
                    show={this.state.showpopup}
                    message={this.state.message}
                    closetab={() => this.setState({ showpopup: false })}
                    closesecond={4000}
                    timeout={true}
                />
                <Footer />
            </>
        )
    }
}
const mapStateToProps = state => {
    return {
        UpdateadminState: state.updateAdmin
    }
}
const mapDispatchToProps = (dispatch) => {
    return {
        updateAdminDispatch: (data) => { dispatch(updateAdminList(data)) }
    }
}
export default connect(mapStateToProps, mapDispatchToProps)(EditAdmin)