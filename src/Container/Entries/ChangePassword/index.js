import React, { Component } from 'react'
import { Link } from 'react-router-dom'
import Footer from '../../Footer'
import '../style.css'
import Axios from 'axios';
import { Api } from '../../../Const/Api'
import { logout } from '../../../Action/applicationRequestAction'

export default class ChangePassword extends Component {
    constructor(props) {
        super(props);
        this.state = {
            old_password: '',
            password: '',
            admin_id: '',
            id: "",
            hiddenpassword: true,
            hiddenconfirmpassword: true,
            confirmpassword: "",

        }
    }




    componentDidMount() {
        this.setState({
            id: localStorage.getItem('adminId')
        })
    }


    // Password = () => {
    //     this.setState({ hiddenpassword: !this.state.hiddenpassword });
    // }

    // confirmPassword = () => {
    //     this.setState({ hiddenconfirmpassword: !this.state.hiddenconfirmpassword });
    // }

    ChangePassword = (e) => {
        const { password, confirmpassword } = this.state;
        if (password !== confirmpassword) {
            this.setState({
                showpopup: true, message: "Passwords don't match"
            })
            alert("Passwords don't match");
        } else {
            Axios.post(Api.BaseUrl + "​/api/adminresetpassword", {
                old_password: this.state.old_password,
                password: confirmpassword,
                admin_id: this.state.id
            }, {
                'headers': { 'Authorization': localStorage.getItem('adminToken') },

            })
                .then((resp) => {
                    alert(resp.data.message)
                    if (resp.data.status === 2) {
                        logout()
                        return false
                    }
                })
                .catch(err => {
                    console.log("error", err.message);
                });
        };
    }


    render() {
        const { hiddenpassword, hiddenconfirmpassword, showpopup, message } = this.state
        return (
            <>
                <section className="wrapper px-4 ">
                    <div className="container">
                        <div className="page-title mb-4">
                            <div className="row align-items-center">
                                <div className="col-md-5">
                                    <div className="page-title-inner">
                                        <h4>Change password</h4>
                                    </div>
                                </div>
                                <div className="col-md-7">
                                    <nav aria-label="breadcrumb">
                                        <ol className="breadcrumb mb-2 justify-content-end p-0 bg-transparent align-items-center">
                                            <li className="breadcrumb-item"><Link to="/">Dashboard</Link></li>
                                            <li className="breadcrumb-item"><Link to="/account">Account</Link></li>
                                            <li className="breadcrumb-item active" aria-current="page">Change password</li>
                                        </ol>
                                    </nav>
                                </div>
                            </div>
                        </div>

                        <div className="view-entry">
                            <div className="row mb-3 align-items-center">
                                <div className="col-md-12 text-right">
                                    <Link to="/account"><button className="btn btn-outline-warning btn-warning-cus px-4 fs-14 rounded-50">Back to account</button></Link>
                                </div>
                            </div>
                            <div className="card border-0  px-md-4 shadow-sm">
                                <div className="card-body hire-devs px-md-2">
                                    <div className="row pt-3">
                                        <div className="col-md-12">
                                            <div className="form-group">
                                                <label for="oldpassword">Old password</label>
                                                <input type="password" className="form-control" id="oldpassword" name="old_password" onChange={(e)=> this.setState({old_password : e.target.value})} />
                                            </div>
                                        </div>
                                        {/* <div className="col-md-12">
                                            <div className="form-group">
                                                <div className="form-group">
                                                    <label for="newpassword">New password</label>
                                                    <input type="password" className="form-control" id="newpassword" />
                                                </div>
                                            </div>
                                        </div>
                                        <div className="col-md-12 mb-4">
                                            <div className="form-group">
                                                <div className="form-group">
                                                    <label for="confirmpassword">Confirm password</label>
                                                    <input type="password" className="form-control" id="confirmpassword" name="confirm_password" onChange={this.handleChange} />
                                                </div>
                                            </div>
                                        </div> */}

                                        <div className="col-md-12">
                                            <div className="form-group">
                                                <label for="oldpassword">Password</label>
                                                <input
                                                    type={hiddenpassword ? "password" : "text"}
                                                    className="form-control"
                                                    placeholder="New Password"
                                                    onChange={(e) => this.setState({ password: e.target.value })}
                                                    required
                                                />
                                                <span className="showeye" onClick={this.Password}>  </span>                                                
                                                </div>
                                        </div>
                                    

                                    <div className="col-md-12">
                                        <div className="form-group">
                                            <label for="oldpassword">Password</label>
                                            <input
                                                type={hiddenconfirmpassword ? "password" : "text"}
                                                className="form-control"
                                                placeholder="Confirm Password"
                                                onChange={(e) => this.setState({ confirmpassword: e.target.value })}
                                                required
                                            />
                                            <span className="showeye" onClick={this.confirmPassword}> </span>
                                        </div>
                                    </div>

                                    </div>
                                    <div className="row">
                                        <div className="col-md-12">
                                            <div className="form-group text-center">
                                                <button className="btn btn-warning px-4 text-white" onClick={this.ChangePassword}>Change password</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
                <Footer />
            </>
        )
    }
}
