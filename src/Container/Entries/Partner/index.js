import React, { Component } from 'react'
import { Link } from 'react-router-dom'
import Footer from '../../Footer'
import '../style.css'
import Axios from 'axios'
import { Api } from '../../../Const/Api'

export default class Partner extends Component {
    constructor(props) {
        super(props);
        this.state = {

            first_name: '',
            last_name: '',
            company_name: '',
            mobile: '',
            notes: '',
            type: '',
            file_url: ''
        }
    }
    componentDidMount() {
        Axios.get(Api.BaseUrl+`/api/partnership/${this.props.match.params.id}`, {
            'headers': { 'Authorization': localStorage.getItem('adminToken') }
        })
            .then(resp => {
                if (resp.data.status === 2) {
                    localStorage.clear();
                    window.location.pathname = "/"
                    return false
                }
                const data = resp.data.data.list
                this.setState({
                    first_name: data.first_name,
                    last_name: data.last_name,
                    company_name: data.company_name,
                    mobile: data.mobile,
                    notes: data.notes,
                    type: data.type,
                    file_url: data.file_url

                })

            })
            .catch(err => {
                console.log('errrr-------messgae', err)

            });

    }

    render() {
 
        return (
            <>
                <section className="wrapper px-4 ">
                    <div className="container">
                        <div className="page-title mb-4">
                            <div className="row align-items-center">
                                <div className="col-md-5">
                                    <div className="page-title-inner">
                                        <h4>View Entry</h4>
                                    </div>
                                </div>
                                <div className="col-md-7">
                                    <nav aria-label="breadcrumb">
                                        <ol className="breadcrumb mb-2 justify-content-end p-0 bg-transparent align-items-center">
                                            <li className="breadcrumb-item"><Link to="/">Dashboard</Link></li>
                                            <li className="breadcrumb-item"><Link to="/application-request">Application request</Link></li>
                                            <li className="breadcrumb-item"><Link to="/application-request/#partner">Partner with us</Link></li>
                                            <li className="breadcrumb-item active" aria-current="page">View Entry</li>
                                        </ol>
                                    </nav>
                                </div>
                            </div>
                        </div>

                        <div className="view-entry">
                            <div className="row mb-3 align-items-center">
                                <div className="col-md-6">
                                    <div className="form-name text-center text-md-left">
                                        <h5 className="mb-md-0">Partner with us</h5>
                                    </div>
                                </div>
                                <div className="col-md-6 text-center text-md-right">
                                    <Link to="/application-request"><button className="btn btn-outline-warning px-4 fs-14 rounded-50">Back to entries</button></Link>
                                </div>
                            </div>
                            <div className="card border-0  px-md-4 shadow-sm">
                                <div className="card-body hire-devs px-md-2">
                                    <div className="row">
                                        <div className="col-md-5">
                                            <div className="form-group">
                                                <label for="fullname">First name</label>
                                                <input type="text" className="form-control" value={this.state.first_name} />
                                            </div>
                                        </div>
                                        <div className="col-md-5">
                                            <div className="form-group">
                                                <label for="workmail">Last name</label>
                                                <input type="email" className="form-control" value={this.state.last_name} />
                                            </div>
                                        </div>
                                    </div>
                                    <div className="row">
                                        <div className="col-md-5">
                                            <div className="form-group">
                                                <label for="companyname">Company name</label>
                                                <input type="text" className="form-control" id="companyname" value={this.state.company_name} />
                                            </div>
                                        </div>
                                        <div className="col-md-5">
                                            <div className="form-group">
                                                <label for="phonenumber">Phone Number</label>
                                                <input type="number" className="form-control" id="phonenumber" value={this.state.mobile} />
                                            </div>
                                        </div>
                                    </div>
                                    <div className="row mb-3">
                                        <div className="col-md-10">
                                            <div className="form-group">
                                                <label for="notes">Notes</label>
                                                <textarea className="form-control" rows="4" id="notes" value={this.state.notes}></textarea>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="row">
                                        <div className="col-md-12">
                                            <div className="form-group">
                                                <p className="text-lightgray">Resume/CV:
                                                <a href={this.state.file_url } download target="_blank" >Click to download</a>

                                                 </p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
                <Footer />
            </>
        )
    }
}
