import React, { Component } from 'react'
import { Link } from 'react-router-dom'
import Footer from '../../Footer'
import '../style.css'
import { connect } from 'react-redux'
import { AddAdminAction } from '../../../Action/manageAdmin'
import Quicknotification from '../../../Component/Quicknotification'
import CircularProgress from '@material-ui/core/CircularProgress';

let errormsg = "This field is require"

class AddAdmin extends Component {
    constructor(props) {
        super(props);
        this.state = {
            fields: {
                fname: '',
                lname: '',
                email: '',
                phone: '',
                role: ''
            },
            errors: {},
            showpopup: false,
            message: "",
            loader : false
        }
    }

    componentDidUpdate(prevProp) {
        if (prevProp.AddadminState.message !== this.props.AddadminState.message) {
            this.setState({
                loader : false,
                message: this.props.AddadminState.message,
                showpopup: true,
            })
        }

        if (prevProp.AddadminState.loading !== this.props.AddadminState.loading) {
            this.setState({
                loader : this.props.AddadminState.loading
            })
        }
    }


    handleChange = (e) => {
        let fields = this.state.fields;
        fields[e.target.name] = e.target.value
        this.setState({ fields })
    }

    handleSubmit = (e) => {
        e.preventDefault();
        if (this.validationForm()) {
            let fields = {};
            fields['fname'] = '';
            fields['lname'] = '';
            fields['email'] = '';
            fields['phone'] = '';
            fields['role'] = '';
            this.setState({ fields: fields , loader : true });
            let data = {
                first_name: this.state.fields.fname,
                last_name: this.state.fields.lname,
                email: this.state.fields.email,
                mobile: this.state.fields.phone,
                role: this.state.fields.role
            }
            this.props.addAdminDispatch(data)
        }
    }

    validationForm() {
        let fields = this.state.fields;
        let errors = {};
        let formIsValid = true;

        if (!fields['fname']) {
            formIsValid = false;
            errors['fname'] = errormsg
        }
        if (typeof fields['fname'] !== "undefined") {
            if (!fields['fname'].match(/^[a-zA-Z.]/)) {
                errors["fname"] = errormsg
            }
        }

        if (!fields['lname']) {
            formIsValid = false;
            errors['lname'] = errormsg
        }
        if (typeof fields['lname'] !== "undefined") {
            if (!fields['lname'].match(/^[a-zA-Z.]/)) {
                errors["lname"] = errormsg
            }
        }

        if (!fields['email']) {
            formIsValid = false;
            errors["email"] = errormsg
        }
        if (typeof fields['email'] !== "undefined") {
            var pattern = new RegExp(/^(("[\w-\s]+")|([\w-]+(?:\.[\w-]+)*)|("[\w-\s]+")([\w-]+(?:\.[\w-]+)*))(@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$)|(@\[?((25[0-5]\.|2[0-4][0-9]\.|1[0-9]{2}\.|[0-9]{1,2}\.))((25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\.){2}(25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\]?$)/i)
            if (!pattern.test(fields['email'])) {
                formIsValid = false;
                errors["email"] = errormsg
            }
        }
        if (!fields['phone']) {
            formIsValid = false;
            errors["phone"] = errormsg
        }
        if (!fields['role']) {
            formIsValid = false;
            errors["role"] = errormsg
        }

        this.setState({ errors: errors });
        return formIsValid;


    }

    render() {
        return (
            <>
                <section className="wrapper px-4 ">
                    <div className="container">
                        <div className="page-title mb-4">
                            <div className="row align-items-center">
                                <div className="col-md-5">
                                    <div className="page-title-inner">
                                        <h4>Add Admin</h4>
                                    </div>
                                </div>
                                <div className="col-md-7">
                                    <nav aria-label="breadcrumb">
                                        <ol className="breadcrumb mb-2 justify-content-end p-0 bg-transparent align-items-center">
                                            <li className="breadcrumb-item"><Link to="/">Dashboard</Link></li>
                                            <li className="breadcrumb-item"><Link to="/manage-admin">Manage admin</Link></li>
                                            <li className="breadcrumb-item active" aria-current="page">Add admin</li>
                                        </ol>
                                    </nav>
                                </div>
                            </div>
                        </div>

                        <div className="view-entry">
                            <div className="row mb-3 align-items-center">
                                <div className="col-md-12 text-right">
                                    <Link to="/manage-admin"><button className="btn btn-outline-warning btn-warning-cus px-4 fs-14 rounded-50">Back to admin</button></Link>
                                </div>
                            </div>
                            <div className="card border-0  px-md-4 shadow-sm">
                                <div className="card-body hire-devs px-md-2">
                                    <div className="row pt-3">
                                        <div className="col-md-5">
                                            <div className="form-group">
                                                <label for="fname">First name</label>
                                                <input type="text" className="form-control" id="fname" name="fname" value={this.state.fields.fname} onChange={this.handleChange} />
                                                <div className="errorMsg" >
                                                    {this.state.errors.fname}
                                                </div>
                                            </div>
                                        </div>
                                        <div className="col-md-5">
                                            <div className="form-group">
                                                <label for="lname">Last name</label>
                                                <input type="text" className="form-control" id="lname" name="lname" value={this.state.fields.lname} onChange={this.handleChange} />
                                                <div className="errorMsg" >
                                                    {this.state.errors.lname}
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="row">
                                        <div className="col-md-5">
                                            <div className="form-group">
                                                <label for="emailaddress">Email address</label>
                                                <input type="email" className="form-control" id="emailaddress" name="email" value={this.state.fields.email} onChange={this.handleChange} />
                                                <div className="errorMsg" >
                                                    {this.state.errors.email}
                                                </div>
                                            </div>
                                        </div>
                                        <div className="col-md-5">
                                            <div className="form-group">
                                                <label for="mobilenumber">Mobile number</label>
                                                <input type="number" className="form-control" id="mobilenumber" name="phone" value={this.state.fields.phone} onChange={this.handleChange} />
                                                <div className="errorMsg" >
                                                    {this.state.errors.phone}
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="row mb-4">
                                        <div className="col-md-5">
                                            <div className="form-group">
                                                <label for="role">Role</label>
                                                <select className="form-control" id="role" name="role" value={this.state.fields.role} onChange={this.handleChange} >
                                                    <option selected defaultValue='' >-Select-</option>
                                                    <option value="SuperAdmin">Super Admin</option>
                                                    <option value="Admin">Admin</option>
                                                </select>
                                                <div className="errorMsg" >
                                                    {this.state.errors.role}
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                    <div className="row">
                                        <div className="col-md-12">
                                            <div className="form-group text-center">
                                                <button className="btn btn-warning px-4 text-white" onClick={this.handleSubmit}>Add admin</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>{this.state.loader && this.state.loader && <CircularProgress  />}
                </section>
                <Quicknotification
                    show={this.state.showpopup}
                    message={this.state.message}
                    closetab={() => this.setState({ showpopup: false })}
                    closesecond={4000}
                    timeout={true}
                />
                <Footer />
            </>
        )
    }
}
const mapStateToProps = state => {
    return {
        AddadminState: state.addAdmin
    }
}
const mapDispatchToProps = (dispatch) => {
    return {
        addAdminDispatch: (data) => { dispatch(AddAdminAction(data)) }
    }
}
export default connect(mapStateToProps, mapDispatchToProps)(AddAdmin)