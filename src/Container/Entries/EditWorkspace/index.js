import React, { Component } from 'react'
import { Link } from 'react-router-dom'
import Footer from '../../Footer'
import '../style.css'
import { connect } from 'react-redux'
import { logout } from '../../../Action/applicationRequestAction'
import { Editor } from 'react-draft-wysiwyg';
import { EditorState, convertFromHTML, ContentState } from 'draft-js';
import 'react-draft-wysiwyg/dist/react-draft-wysiwyg.css';
import { stateToHTML } from "draft-js-export-html";
import { Api } from '../../../Const/Api'
import Axios from 'axios'
// import { updateWorkspace } from '../../../Action/workspace'
import { Uploadfile } from '../../../Action/commonAction'
import CircularProgress from '@material-ui/core/CircularProgress';

class EditWorkspace extends Component {
    constructor(props) {
        super(props);
        this.state = {
            title: '',
            image_url: '',
            description: '',
            fileuploadloader: false,
            fileuploadloadermsg: '',
            respMessage: ''
        }
    }
    componentDidMount() {
        let id = this.props.match.params.id
        Axios.get(Api.BaseUrl + "/api/workspace/" + id)
            .then(resp => {
                const getData = resp.data.data.list
                this.setState({
                    title: getData.title,
                    image_url: getData.image_url,
                    editorState: EditorState.createWithContent(ContentState.createFromBlockArray(convertFromHTML(getData.description)))
                })
                if (resp.data.status === "2") {
                    logout()
                    return false
                }
            })
            .catch(err => {
                console.log("error", err)
            });
    }


    componentDidUpdate(prevProp) {
        if (prevProp.FileUploadState.loading !== this.props.FileUploadState.loading) {
            this.setState({
                image_url: this.props.FileUploadState.resp,
                fileuploadloader: this.props.FileUploadState.loading,
                fileuploadloadermsg: this.props.FileUploadState.error
            })
        }
    }

    onEditorStateChange = (editorState) => {
        this.setState({
            editorState,
            editorContentHtml: stateToHTML(editorState.getCurrentContent())
        });
    };

    imageUpload = (e) => {
        this.setState({
            fileuploadloader: true
        })
        this.props.FileUploadDispatch(e.target.files[0])
    }

 
    handleChange = (e) => {
        this.setState({
            [e.target.name]: e.target.value
        })
    }
    handleSubmit = (e) => {
        e.preventDefault()
        this.setState({
            editorContentHtml: stateToHTML(this.state.editorState.getCurrentContent())
        }, () => {
            let data = {
                id: this.props.match.params.id,
                title: this.state.title,
                image_url: this.state.image_url,
                description: this.state.editorContentHtml,
            }
            Axios.put(Api.BaseUrl + "/api/workspace/" + data.id, {
                            title:data.title,
                            image_url:data.image_url,
                            description:data.description,
                        },
            { 'headers': { 'Authorization': localStorage.getItem('adminToken') } })
            .then(resp => {
                this.setState({
                    respMessage : resp.data.message
                })
            })
        })
    };

    render() {
        return (
            <>
                <section className="wrapper px-4 ">
                    <div className="container">
                        <div className="page-title mb-4">
                            <div className="row align-items-center">
                                <div className="col-md-5">
                                    <div className="page-title-inner">
                                        <h4>Edit Workspace</h4>
                                    </div>
                                </div>
                                <div className="col-md-7">
                                    <nav aria-label="breadcrumb">
                                        <ol className="breadcrumb mb-2 justify-content-end p-0 bg-transparent align-items-center">
                                            <li className="breadcrumb-item"><Link to="/">Dashboard</Link></li>
                                            <li className="breadcrumb-item"><Link to="#">Content Mangement</Link></li>
                                            <li className="breadcrumb-item"><Link to="/workspace">Workspace</Link></li>
                                            <li className="breadcrumb-item active" aria-current="page">Edit workspace</li>
                                        </ol>
                                    </nav>
                                </div>
                            </div>
                        </div>

                        <div className="view-entry">
                            <div className="row mb-3 align-items-center">
                                <div className="col-md-6">

                                </div>
                                <div className="col-md-6 text-center text-md-right">
                                    <Link to="/workspace"><button className="btn btn-outline-warning btn-warning-cus px-4 fs-14 rounded-50">Back to workspace</button></Link>
                                </div>
                            </div>
                            <div className="card border-0  px-md-4 shadow-sm">
                                <div className="card-body hire-devs px-md-2">
                                    <div className="row">
                                        <div className="col-md-8">
                                            <div className="form-group pb-4">
                                                <label for="pressTitle">Title</label>
                                                <input type="text" className="form-control" id="pressTitle" name="title" value={this.state.title} onChange={this.handleChange} />
                                            </div>
                                        </div>
                                        <div className="col-md-8">
                                            <div className="form-group">
                                                <label>Image</label>
                                                <input type="file" className="form-control d-none" id="editpressimage" onChange={this.imageUpload} />
                                                <div className="add-image">
                                                    <label className="text-center w-100 parallax" for="editpressimage">
                                                        <div className="uplaodedImage" >
                                                            <img src={this.state.image_url} alt="Events image" className="img-fluid" for="editpressimage" />
                                                            <div className="overlay-remove position-absolute">
                                                                <i className="fas fa-lg fa-trash"></i>
                                                            </div>
                                                        </div>
                                                    </label>
                                                </div>
                                                {this.state.fileuploadloader && this.state.fileuploadloader && <CircularProgress />}
                                                    {this.state.fileuploadloadermsg}
                                            </div>
                                        </div>
                                    </div>
                                    <div className="row mb-5">
                                        <div className="col-md-8">
                                            <div className="form-group">
                                                <label for="pressdescription">Description</label>
                                                <Editor
                                                    editorState={this.state.editorState}
                                                    wrapperClassName="demo-wrapper"
                                                    editorClassName="demo-editor"
                                                    onEditorStateChange={this.onEditorStateChange}
                                                />
                                            </div>
                                        </div>
                                    </div>
                                    <div className="row">
                                        <div className="col-md-12">
                                            <div className="form-group text-center">
                                                <button className="btn btn-warning px-4 text-white" onClick={this.handleSubmit}>Save edit</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
                <Footer />
            </>
        )
    }
}
const mapStateToProps = state => {
    return {
        FileUploadState: state.FileuploadReducer,
    }
}
const mapDispatchToProps = (dispatch) => {
    return {
        // DispatchWorkspaceUpdate: (data) => { dispatch(updateWorkspace(data)) },
        FileUploadDispatch: (file) => { dispatch(Uploadfile(file)) },
    }
}
export default connect(mapStateToProps, mapDispatchToProps)(EditWorkspace)