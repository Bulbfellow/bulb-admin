import React, { Component } from 'react'
import { Link } from 'react-router-dom'
import Footer from '../../Footer'
import '../style.css'
import Axios from 'axios'
import {Api} from '../../../Const/Api'

export default class EnterpriseRequest extends Component {
    constructor() {
        super()
        this.state = {
            full_name: '',
            work_mail: '',
            company_name: '',
            mobile: '',
            notes: ''
        }
    }

    componentDidMount() {
        Axios.get(Api.BaseUrl +`/api/innovate/${this.props.match.params.id}`, {
            'headers': { 'Authorization': localStorage.getItem('adminToken') },

        })
            .then(resp => {
                const userinfo = resp.data.data.list
                this.setState(userinfo)
                
            })
    }



    render() {
        const userinfo = this.state

        return (
            <>
                <section className="wrapper px-4 ">
                    <div className="container">
                        <div className="page-title mb-4">
                            <div className="row align-items-center">
                                <div className="col-md-5">
                                    <div className="page-title-inner">
                                        <h4>View Entry</h4>
                                    </div>
                                </div>
                                <div className="col-md-7">
                                    <nav aria-label="breadcrumb">
                                        <ol className="breadcrumb mb-2 justify-content-end p-0 bg-transparent align-items-center">
                                            <li className="breadcrumb-item"><Link to="/">Dashboard</Link></li>
                                            <li className="breadcrumb-item"><Link to="/the-desk">the desk</Link></li>
                                            <li className="breadcrumb-item"><Link to="/workspace">Enterprise request</Link></li>
                                            <li className="breadcrumb-item active" aria-current="page">View entry</li>
                                        </ol>
                                    </nav>
                                </div>
                            </div>
                        </div>
                        
                        <div className="view-entry">
                            <div className="row mb-3 align-items-center">
                                <div className="col-md-6">
                                    <div className="form-name text-center text-md-left">
                                        <h5 className="mb-md-0">Enterprise request</h5>
                                    </div>
                                </div>
                                <div className="col-md-6 text-center text-md-right">
                                    <Link to="/the-desk/enterprise-tab"><button className="btn btn-outline-warning btn-warning-cus px-4 fs-14 rounded-50">Back to entries</button></Link>
                                </div>
                            </div>
                            <div className="card border-0  px-md-4 shadow-sm">
                                <div className="card-body hire-devs px-md-2">
                                    <div className="row">
                                        <div className="col-md-5">
                                            <div className="form-group">
                                                <label for="fullname">Full Name</label>
                                                <input type="text" className="form-control" id="fullname" value={userinfo.full_name} />
                                            </div>
                                        </div>
                                        <div className="col-md-5">
                                            <div className="form-group">
                                                <label for="workmail">Work mail</label>
                                                <input type="email" className="form-control" id="workmail" value={userinfo.work_mail} />
                                            </div>
                                        </div>
                                    </div>
                                    <div className="row">
                                        <div className="col-md-5">
                                            <div className="form-group">
                                                <label for="companyname">Company name</label>
                                                <input type="text" className="form-control" id="companyname" value={userinfo.company_name} />
                                            </div>
                                        </div>
                                        <div className="col-md-5">
                                            <div className="form-group">
                                                <label for="phonenumber">Phone Number</label>
                                                <input type="number" className="form-control" id="phonenumber" value={userinfo.mobile} />
                                            </div>
                                        </div>
                                    </div>
                                    <div className="row mb-4">
                                        <div className="col-md-10">
                                            <div className="form-group">
                                                <label for="notes">Notes</label>
                                                <textarea rows="5" className="form-control" id="notes" value={userinfo.notes} ></textarea>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
                <Footer />
            </>
        )
    }
}
