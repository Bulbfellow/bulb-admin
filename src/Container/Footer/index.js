import React, { Component } from 'react'
import './style.css'

export default class Footer extends Component {
    render() {
        return (
            <>
                <footer className="py-4">
                    <div className="container">
                        <div className="row">
                            <div className="col-12 text-center">
                                <p className="mb-0 fs-12">2019 thebulb</p>
                            </div>
                        </div>
                    </div>
                </footer>
            </>
        )
    }
}
