import React from 'react';
import Header from '../Header';
// import { Dateonly, MonthName } from '../../Const/function'
import './style.css';


const styles = {

};
const Dashboard = () => {

  const cards = [
    {
      icon: require("../../Assets/img/total-requests.svg"),
      title: "Total Requests",
      number: 30,
    },
    {
      icon: require("../../Assets/img/pending-requests.svg"),
      title: "Pending Requests",
      number: 18,
    },
    {
      icon: require("../../Assets/img/approved-requests.svg"),
      title: "Approved Requests",
      number: 22,
    },
    {
      icon: require("../../Assets/img/scheduled-meetings.svg"),
      title: "Scheduled Meetings",
      number: 6,
    },
  ]

        return (
            <>
              <Header dashboard={true} />

                <section style={{ paddingLeft: 53, paddingRight: 46, marginTop: 20}}>
                  <div
                    style={{
                      display: "grid",
                      gridTemplateColumns: "repeat(4, 21%)",
                      columnGap: "5%",
                      marginBottom: 60
                    }}
                  >
                    {cards.map(card => <div
                      style={{
                        height: 192,
                        background: "white",
                        padding: "11px 14px"
                      }}
                    >
                      <div>
                        <select style={{ width: 64, height: 16, fontSize: 10}}>
                          <option option="daily">Daily</option>
                          <option option="weekly">Weekly</option>
                          <option option="monthly" selected>Monthly</option>
                          <option option="annualy">Annualy</option>
                        </select>
                      </div>
                      <div style={{ display: "flex", flexDirection: "column", alignItems: "center"}}>
                        <img style={{ marginBottom: 20 }} src={card.icon} />
                        <p style={{ fontSize: 16, color: "#050038"}}>{card.title}</p>
                        <p style={{ fontSize: 16, fontWeight: "bold", color: "#050038"}}>{card.number}</p>
                      </div>
                    </div>)}
                  </div>
                  <div style={{
                    display: "grid",
                    gridTemplateColumns: "50% 40%",
                    columnGap: "10%",
                    marginBottom: 70,
                  }}>
                    <div style={{
                      background: "white",
                      border: ".1px solid rgba(59,107,211,.2)",
                      borderRadius: 4
                    }}>
                      <div
                        style={{
                          height: 70,
                          width: "100%",
                          background: "#050038",
                          borderRadius: "4px 4px 0 0",
                          display: "flex",
                          alignItems: "center",
                          justifyContent: "flex-end",
                          paddingRight: 56
                        }}
                      >
                        <div
                          style={{
                            background: "white",
                            width: 208,
                            height: 46,
                            border: ".1px solid rgba(59,107,211,.2)",
                            borderRadius: 4,
                            marginRight: 91,
                            display: "flex",
                            alignItems: "center",
                            justifyContent: "center",
                          }}
                        >
                          <img style={{ marginRight: 27, cursor: "pointer"}} src={require("../../Assets/img/back-arrow.svg")} />
                          <span style={{ color: "#050038", fontSize: 14 }}>May 2021</span>
                          <img style={{ marginLeft: 27, cursor: "pointer"}} src={require("../../Assets/img/forward-arrow.svg")} />
                        </div>
                        <div
                          style={{
                            display: "flex",
                            alignItems: "center",
                            justifyContent: "center",
                            fontSize: 15,
                            color: "white",
                            fontWeight: "bold",
                            cursor: "pointer"
                          }}
                        >
                          View all
                        </div>
                      </div>
                      <div style={{ overflowY: "scroll", maxHeight: "50vh"}}>
                        <div style={{
                          borderBottom: ".1px solid rgba(59,107,211,.2)",
                          display: "grid",
                          gridTemplateColumns: "25% 75%"
                        }}>
                          <div
                            style={{
                              paddingTop: 12,
                              paddingLeft: 22,
                              border: ".1px solid rgba(59,107,211,.2)",
                            }}
                          >
                            <div style={{ fontSize: 15, color: "rgba(0, 0, 0, 0.7)", marginBottom: 47}}>9th Saturday</div>
                            <div style={{ fontSize: 15, color: "#050038"}}>08:00 AM</div>
                          </div>
                          <div
                            style={{
                              padding: "30px 41px 36px 21px"
                            }}
                          >
                            <div
                              style={{
                                border: ".1px solid rgba(59,107,211,.2)",
                                borderRadius: 4,
                                borderLeft: "5px solid #3B6BD3",
                                padding: "17px 31px",
                                fontSize: 15
                              }} 
                            >
                              <p>Zoom meeting with <span style={{ color: "#123070", opacity: .93, fontWeight: "bold"}}>James Stone</span></p>
                              <p style={{ color: "#12278C" }}>https://zoommeeting.com</p>
                            </div>
                          </div>
                        </div>
                        <div style={{
                          borderBottom: ".1px solid rgba(59,107,211,.2)",
                          display: "grid",
                          gridTemplateColumns: "25% 75%"
                        }}>
                          <div
                            style={{
                              paddingTop: 12,
                              paddingLeft: 22,
                              borderRight: ".1px solid rgba(59,107,211,.2)",
                            }}
                          >
                            <div style={{ fontSize: 15, color: "rgba(0, 0, 0, 0.7)", marginBottom: 47}}>9th Saturday</div>
                            <div style={{ fontSize: 15, color: "#050038"}}>08:00 AM</div>
                          </div>
                          <div
                            style={{
                              padding: "30px 41px 36px 21px"
                            }}
                          >
                            <div
                              style={{
                                border: ".1px solid rgba(59,107,211,.2)",
                                borderRadius: 4,
                                borderLeft: "5px solid #35935A",
                                padding: "17px 31px",
                                fontSize: 15
                              }} 
                            >
                              <p>Zoom meeting with <span style={{ color: "#123070", opacity: .93, fontWeight: "bold"}}>Onyeka and sons</span></p>
                              <p style={{ color: "#12278C" }}>https://zoommeeting.com</p>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div
                      style={{
                        background: "white",
                        borderRadius: 4,
                        padding: "24px 0 24px 0",
                        overflowY: "scroll"
                      }}
                    >
                      <div style={{
                        padding: "0 18px 18px 31px",
                        borderBottom: ".1px solid rgba(59,107,211,.2)"
                      }}>
                        <div
                          style={{
                            display: "flex",
                            justifyContent: "space-between",
                            marginBottom: 50
                          }}
                        >
                          <div style={{ fontSize: 18, color: "#050038", fontWeight: "bold"}}>Expected visits for the month</div>
                          <div style={{ fontSize: 15, color: "#3B6BD3", fontWeight: "bold"}}>View all</div>
                        </div>
                        <div style={{ display: "flex",  justifyContent: "flex-end"}}>
                          <select style={{ width: 110, height: 38, fontSize: 15, color: "#050038", padding: 5}}>
                            <option value="05/2021">May 2021</option>
                          </select>
                        </div>
                      </div>
                      <div style={{ paddingRight: 18, paddingLeft: 31, marginTop: 10}}>
                        <div
                          style={{
                            display: "flex",
                            justifyContent: "space-between",
                            fontSize: 15,
                            color: "#050038",
                            marginBottom: 12,
                            marginTop: 15,
                            borderBottom: ".1px solid rgba(59,107,211,.2)",
                            paddingBottom: 10
                          }}
                        >
                          <div>Paystack design team</div>
                          <div>
                            <div style={{ marginBottom: 17}}>15-05-2021</div>
                            <div
                              style={{
                                padding: "2px 8px",
                                background: "rgba(251, 191, 25, 0.08)",
                                borderRadius: 2,
                                fontSize: 10,
                                color: "#FBBF19"
                              }}
                            >Upcoming visit</div>
                          </div>
                        </div>
                        <div
                          style={{
                            display: "flex",
                            justifyContent: "space-between",
                            fontSize: 15,
                            color: "#050038",
                            marginBottom: 12,
                            marginTop: 15,
                            borderBottom: ".1px solid rgba(59,107,211,.2)",
                            paddingBottom: 10
                          }}
                        >
                          <div>Paystack design team</div>
                          <div>
                            <div style={{ marginBottom: 17}}>15-05-2021</div>
                            <div
                              style={{
                                padding: "2px 8px",
                                background: "rgba(251, 191, 25, 0.08)",
                                borderRadius: 2,
                                fontSize: 10,
                                color: "#FBBF19"
                              }}
                            >Upcoming visit</div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </section>
            </>
        )
}

export default (Dashboard)
