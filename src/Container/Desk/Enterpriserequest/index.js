import React, { useEffect, useState } from 'react'
import { connect } from 'react-redux'
import {Enterprisemultiple,enterpriseDelete,enterprise} from '../../../Action/desk'
import Datatable from '../../../Component/Datatable'
import { Link } from 'react-router-dom'
import Deletemodal from '../../../Component/Deletemodal'
import Delete from '../../../Assets/img/delete-dark.svg'
import Archive from '../../../Assets/img/archive-dark.svg'
import { convertDatePickerTime } from '../../../Const/function'

const Enterprisedesk = ({ enterprisedata,enterprise,enterprisedelete,enterpriseMultiplerow }) => {
	let datas = enterprisedata.resp

	const [deleteid, setDeleteid] = useState('')
	const [datatype, setDatatype] = useState(99)
	const [finaldata, setFinaldata] = useState([])
	const [deleteids, setdeleteids] = useState([])
	const [singlearchive, setSinglearchive] = useState('')
	const [toggleCleared, setToggleCleared] = React.useState(false);
	const [storedeleteis, setStoredeleteis] = useState([])

	let tabledata = []
	let idss = []
	
	const singleArchive = () => {
		let userid = singlearchive
		let apidata = {
			ids: [userid],
			status: "3"
		}
		enterpriseMultiplerow(apidata)
		enterprise(datatype)
		updatedData()
	}

	// "full_name": "string",
	// "work_mail": "string@gmail.com",
	// "company_name": "string",
	// "mobile": "1234567890",
	// "notes": "string",
	// "status": "1",
	// "view_status": "1",
	// "rowcreateddatetime": "2020-01-17T11:39:17.000Z",
	// "rowmodifieddatetime": "2020-01-17T11:39:17.000Z"


	const updatedData = () => {
		(datas.length > 0) && (datas.map((user) => {
			let data = {
				name: user.full_name,
				date: convertDatePickerTime(user.rowcreateddatetime),
				mobile: user.mobile,
				company_name : user.company_name,
				status: user.view_status,
				email : user.work_mail,
				notes : user.notes,
				deleteid: user.sr_no,
				view: <Link to={`/the-desk/view-enterprise/${user.sr_no}`}><button className="btn btn-warning text-white px-4 py-1 viewentrybtn cursor">View Entry</button></Link>,
				delete: <> <span onClick={() => setDeleteid(user.sr_no)} className="mr-3"><img src={Delete} alt="Delete" title="Delete" data-toggle="modal" data-target="#remove" className="img-fluid" /></span>
					<span onClick={() => setSinglearchive(user.sr_no)}><img src={Archive} alt="Archive" title="Archive" data-toggle="modal" data-target="#archive" className="img-fluid" /></span></>
			}
			tabledata.push(data)
		}))
		setFinaldata(tabledata)
	}

	useEffect(() => {
		updatedData()
	}, [enterprisedata.resp])

	useEffect(() => {
		idss = deleteids
	}, [deleteids])

	useEffect(() => {
		enterprise(datatype)
	}, [])

	useEffect(() => {
		enterprise(datatype)
	}, [datatype])

	useEffect(() => {
		var arr = []
		storedeleteis.map((user) => {
			arr.push(user.deleteid)
		})
		setdeleteids(arr)
	}, [storedeleteis])

	const setDataType = (e) => {
		setDatatype(e)
		enterprise(e)
		updatedData()
	}

	const archive = (e) => {
		setDatatype(e)
		enterprise(e)
		updatedData()
	}

	const deleteUserInfo = () => {
		let userid = deleteid
		enterprisedelete(userid)
		setTimeout(() => {
			enterprise(datatype)
		}, 500);
	}

	const handleChangeRow = (e) => {
		setStoredeleteis(e.selectedRows)
	}

	function deleteDataRow() {
		let apidata = {
			ids: deleteids,
			status: "0"
		}
		enterpriseMultiplerow(apidata)
		enterprise(datatype)
		updatedData()
		setdeleteids([])
		setToggleCleared(!toggleCleared)
	}

	const archiveData = () => {
		let apidata = {
			ids: deleteids,
			status: "3"
		}

		enterpriseMultiplerow(apidata)
		enterprise(datatype)
		updatedData()
		setdeleteids([])
		setToggleCleared(!toggleCleared)

	}

	const backData = (e) => {
		setDatatype(e)
		enterprise(e)
		updatedData()
	}

	return <>
		<Datatable
			tabledata={finaldata}
			archiveDataList={() => archive(3)}
			changeHandle={(e) => setDataType(e.target.value)}
			value={datatype}
			deleteids={idss}
			handleChangeRow={handleChangeRow}
			deleteDataRow={deleteDataRow}
			archiveDataRow={archiveData}
			backData={() => backData(99)}
			toggleCleared={toggleCleared}
			datas={storedeleteis}

		/>
		<Deletemodal
			deleteid={deleteUserInfo}
			message={"Are you sure you want to delete this entry"}
			buttonname={"Delete"}
		/>
		<div className="modal fade" id="archive" role="dialog" aria-labelledby="removeLabel" aria-hidden="true">
			<div className="modal-dialog mt-0" role="document">
				<div className="modal-content border-0 shadow">
					<div className="modal-header border-0 px-md-4">
						<div className="headings">
							<h5 className="modal-title font-weight-bold pb-1" id="removeLabel">Archive entry</h5>
							<p className="fs-14 mb-0">Are you sure you want to archive this entry</p>
						</div>
						<button type="button" className="close outline-none" data-dismiss="modal" aria-label="Close">
							<span aria-hidden="true">&times;</span>
						</button>
					</div>
					<div className="modal-footer border-0 px-md-4 pt-2">
						<span className="mb-0 text-secondary pr-3 text-decoration-none d-inline-block cursor" data-dismiss="modal">Cancel</span>
						<span onClick={singleArchive} className="mb-0 text-warning text-decoration-none d-inline-block cursor" data-dismiss="modal">Archive</span>
					</div>
				</div>
			</div>
		</div>
	</>

}
const mapStateToProps = state => {
	return {
		enterprisedata: state.enterprise
	}
}
const mapDispatchToProps = (dispatch) => {
	return {
		enterprise: (datatype) => { dispatch(enterprise(datatype)) },
		enterprisedelete: (userid) => { dispatch(enterpriseDelete(userid)) },
		enterpriseMultiplerow: (data) => { dispatch(Enterprisemultiple(data)) }
	}
}
export default connect(mapStateToProps, mapDispatchToProps)(Enterprisedesk)
