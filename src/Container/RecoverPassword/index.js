import React, { Component } from 'react'
import { Link } from 'react-router-dom'
import Logo from '../../Assets/img/logo-black.png'
import Quicknotification from '../../Component/Quicknotification'
import Axios from 'axios'
import {Api} from '../../Const/Api'

export default class RecoverPassword extends Component {
    constructor() {
        super()
        this.state = {
            showMsg: false,
            mail: "",
            message: ''
        }
    }

    Submit = () => {
        Axios.post(Api.BaseUrl+'/api/adminforgotpassword', {
            mail: this.state.mail
        })
            .then(res => {
                this.setState({
                    showMsg: true,
                    message: res.data.message,
                    mail : ''
                })
            })
    }


    render() {
        return (
            <>
                <div className="login-page">
                    <div className="container">
                        <div className="row min-vh-100 py-5 align-items-center">
                            <div className="col-md-6 col-lg-4">
                                <div className="login-form form-inner">
                                    <div className="logo mb-3">
                                        <img src={Logo} alt="Logo" className="img-fluid ml-2 ml-4 pl-2" />
                                    </div>
                                    <div className="card border-0 shadow">
                                        <div className="card-body">
                                            <h5>Recover your password</h5>
                                            <p className="fs-14 pb-2">Please enter your email address below to receive instructions to reset your password</p>
                                            <div className="form-group mb-4 pb-4">
                                                <input type="email"
                                                    className="form-control"
                                                    placeholder="Email address"
                                                    required
                                                    value={this.state.mail}
                                                    onChange={(e) => this.setState({ mail: e.target.value })}
                                                />
                                            </div>
                                            <div className="form-group mb-2">
                                                <button className="btn btn-warning text-white btn-block text-uppercase" onClick={this.Submit}>Submit</button>
                                            </div>
                                            <div className="forget-password">
                                                <p className="fs-14 text-dark text-center">Know your password? <Link to="/" className="text-decoration-none text-warning">Login</Link></p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <Quicknotification
                        show={this.state.showMsg}
                        message={this.state.message}
                        closetab={() => this.setState({showMsg : false})}
                        bgblock={true}
                        closesecond={6000}
                        timeout={true}
                    />
                </div>
            </>
        )
    }
}
