import React, { useState } from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import ProfileImg from '../../Assets/img/admin-user.svg';
import SearchInput from "../../Component/SearchInput";
import './style.css';

const Header = (props) => {

  const [active, setActive] = useState("Submit an idea");

  const navigationTitles  = [
    "Submit an idea",
    "Apply for incubation",
    "Hire developers",
    "Request a tour",
    "Partner with us",
    "Newsletter"
  ]

  const Logout = () => {
    props.loginstatus('0')
    localStorage.removeItem('loginStatus');
    localStorage.removeItem('adminToken');
    localStorage.removeItem('adminName')
    localStorage.removeItem('adminId')
    localStorage.removeItem('adminRole')
    window.location.pathname = "/"
  }

  const styles = {
    button: {
      width: 155,
      height: 44,
      borderRadius: 4,
      marginRight: 8,
      display: "flex",
      color: "#050038",
      border: "0.1px solid rgba(59,107,211,.2)",
      justifyContent: "center",
      backgroundColor: "white",
      alignItems: "center",
      cursor: "pointer"
    },
    active: {
      backgroundColor: "rgba(59, 107, 211, 0.06",
      color: "#3B6BD3",
      fontWeight: "bold"
    }
  }

  const [admiName, adminRole, adminId] = ["Wande Adams", "MD", 1]

  return (
    <>
        <header className="position-relative mb-3">
            <div className="header-top py-3">
                <div className="container">
                    <div className="row">
                        <div className="col-md-8 d-flex align-items-center">
                            <SearchInput />
                        </div>
                        <div className="col-md-4">
                            <div className="admin-profileMenu dropdown ml-auto d-table">
                                <Link className="text-decoration-none d-flex align-items-center" to="#" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    <div className="admin-dp mr-2">
                                        <img src={ProfileImg} alt="Profile Image" className="img-fluid" />
                                    </div>
                                    <div className="admin-detail pt-1">
                                        <h5 className="admin-name mb-0">{admiName}</h5>
                                        <p className="text-warnings fs-14 mb-0">{adminRole}</p>
                                    </div>
                                </Link>
                                <div className="dropdown-menu left-unset right-0" aria-labelledby="dropdownMenuLink">
                                    <Link to={`/account/${adminId}`} className="dropdown-item"><i className="far fa-user-circle"></i> Account</Link>
                                    <Link to="#" onClick={Logout} className="dropdown-item"><i className="fas fa-sign-out-alt"></i> Logout</Link>
                                </div>
                            </div>
                        </div>
                    </div>
                    {!props.dashboard && <div
                      style={{
                        display: "flex",
                        flexDirection: "row",
                        marginTop: 105
                      }}
                    >
                      {navigationTitles.map(title => {
                        const isActive = active === title ? styles.active : {};
                        return (
                          <div
                            style={{...styles.button, ...isActive}}
                            onClick={() => setActive(title)}
                          >
                            {title}
                          </div>
                        )
                      })}
                    </div>}
                </div>
            </div>
        </header>
    </>
)
}
const mapStateToProps = (state) => {
    return {
        admininfo: state.login
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        loginstatus: d => dispatch({ type: "LOGIN_STATUS", payload: d }),

    }
}
export default connect(mapStateToProps, mapDispatchToProps)(Header);
