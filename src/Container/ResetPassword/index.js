import Axios from 'axios';
import queryString from 'query-string';
import React, { Component } from 'react';
import { Api } from '../../Const/Api';
export default class Resetpassword extends Component {
	constructor() {
		super()
		this.state = {
			password: "",
			confirmpassword: "",
			hiddenpassword: true,
			hiddenconfirmpassword: true,
			showpopup: false,
			message: ''
		}
	}

	Password = () => {
		this.setState({ hiddenpassword: !this.state.hiddenpassword });
	}
	confirmPassword = () => {
		this.setState({ hiddenconfirmpassword: !this.state.hiddenconfirmpassword });
	}

	resetPassword = () => {
		const { password, confirmpassword } = this.state;

		if (password !== confirmpassword) {
			this.setState({
				showpopup: true, message: "Passwords don't match"
			})
			// alert("Passwords don't match");
		} else {
			// alert('right')
			let url = this.props.location.search;
			let params = queryString.parse(url);
			Axios.post(Api.BaseUrl+'/api/adminreset', {
				password: confirmpassword,
				token: params.token,
			})
				.then(res => {
					this.setState({
						showpopup: true,
						message: "Your password has been set sucessfully"
					})
					// console.log('api for the reset password ', res)
					window.location.pathname = "/"
				})
		}
	}
	render() {
		const { hiddenpassword, hiddenconfirmpassword, showpopup, message } = this.state

		return (
			<>
				
			</>
		)
	}
}
