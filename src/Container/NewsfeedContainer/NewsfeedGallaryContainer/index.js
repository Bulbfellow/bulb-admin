import React from 'react'
import { Link } from 'react-router-dom'
import { newsfeedgetGallery, newsfeedGalleryDelete } from '../../../Action/newsfeed'
import { convertdate, convertDatePickerTime } from '../../../Const/function'
import { connect } from 'react-redux'
import Deletemodal from '../../../Component/Deletemodal'
import Search from '../../../Component/Search'
import { SearchGalleryList } from '../../../Action/searchAction'

class NewsfeedGallary extends React.Component {
	constructor(props) {
		super(props)
		this.state = {
			deleteid: '',
			currentPage: 1,
			data: [],
			dataLength: '',
			firstIndex: '',
			datalimit: 7,
			searchvalue: '',
			paginationDetail: []
		}
	}
	componentDidMount() {
		this.props.getGalleryDispatch(this.state.currentPage)
	}

	componentDidUpdate(prevProps, prevState) {
		// if (prevState.currentPage !== this.state.currentPage) {
		// 	this.props.getGalleryDispatch(this.state.currentPage)
		// }
		// if (prevProps.newsfeedGallerydata.resp !== this.props.newsfeedGallerydata.resp) {
		// 	const lastCount = (this.state.currentPage * this.state.datalimit)
		// 	const initialCount = lastCount - this.state.datalimit
		// 	this.setState({
		// 		data: this.props.newsfeedGallerydata.resp,
		// 		dataLength: this.props.newsfeedGallerydata.resp.length,
		// 		firstIndex: initialCount
		// 	})
		// }
		if (prevState.currentPage !== this.state.currentPage) {
			if (prevState.searchvalue === '') {
				this.props.getGalleryDispatch(this.state.currentPage)
			} else {
				const filter = {
					Search: this.state.searchvalue,
					currentPage: this.state.currentPage
				}
				this.props.SearchGalleryListDispatch(filter)
			}
		}
		//------- ! Set State of Data on currentPage Change Without Search !
		if (prevProps.newsfeedGallerydata !== this.props.newsfeedGallerydata) {
			const Lastcount = (this.state.currentPage) * this.state.datalimit
			const initialCount = Lastcount - this.state.datalimit
			this.setState({
				data: this.props.newsfeedGallerydata.resp,
				paginationDetail: this.props.newsfeedGallerydata.pagination,
				dataLength: this.props.newsfeedGallerydata.resp.length,
				firstIndex: initialCount
			})
		}
		//--------!  Hiting Search Api/NOrmal Get Api on Change of Search Value!
		if (prevState.searchvalue !== this.state.searchvalue) {
			if (this.state.searchvalue !== '') {
				const filter = {
					Search: this.state.searchvalue,
					currentPage: this.state.currentPage
				}
				this.props.SearchGalleryListDispatch(filter)
			} else {
				this.props.getGalleryDispatch(this.state.currentPage)
			}
		}

		// ---- Set State of Data on Searching of Value | ----------
		if (prevProps.SearchedData !== this.props.SearchedData) {
			const Lastcount = (this.state.currentPage) * this.state.datalimit
			const initialCount = Lastcount - this.state.datalimit
			this.setState({
				data: this.props.SearchedData.resp,
				paginationDetail: this.props.SearchedData.pagination,
				dataLength: this.props.SearchedData.resp.length,
				firstIndex: initialCount
			})
		}
	}
	handleOnSearch = (e) => {
		this.setState({
			searchvalue: e.target.value,
			currentPage: 1
		})
	}
	handleDelete = () => {
		this.props.galleryDeleteDispatch(this.state.deleteid)
		setTimeout(() => {

			if (this.state.dataLength > 1) {
				this.props.getGalleryDispatch(this.state.currentPage)
				this.setState({ currentPage: this.state.currentPage })
			}
			else {
				if (this.state.currentPage > 1) {
					this.setState({ currentPage: this.state.currentPage - 1 })
				}

			}

		}, 1000);
	}

	handlePrev = () => {
		if (this.state.currentPage > 1) {
			this.setState({
				currentPage: this.state.currentPage - 1
			})

		}
	}

	handleNext = (pageCount) => {
		if (this.state.currentPage < pageCount) {
			this.setState({
				currentPage: this.state.currentPage + 1
			})

		}

	}

	render() {
		const data = this.state.data
		const { firstIndex, dataLength } = this.state
		const pageCount = this.state.paginationDetail.pageCount
		const itemCount = this.state.paginationDetail.itemCount
		let count = firstIndex
		return (
			<div className="tab-inner bg-white  px-4 py-3">
				<div className="tab-inner-head pb-3">
					<div className="row align-items-center mb-3">
						<div className="col-md-6">
							<Search
								onChange={this.handleOnSearch}
								value={this.state.searchvalue}
							/>
						</div>
						<div className="col-md-6">
							<div className="show-pages d-flex justify-content-end align-items-center">
								<p className="mb-0 mr-3">Showing <span>{firstIndex + 1}</span> - <span> {firstIndex + dataLength}</span> of <span>{itemCount}</span></p>
								<div className="prev-next">
									<span className="prev"><button className="fas fa-angle-left" onClick={this.handlePrev}></button></span>
									<span className="next"><button className="fas fa-angle-right" onClick={() => this.handleNext(pageCount)}></button></span>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div className="tab-inner-body">
					<div className="add-events-btn text-right mb-3">
						<Link to="/add-gallery"><button className="btn btn-outline-warning px-5 rounded-50 fs-14">Add Gallery</button></Link>
					</div>
					<div className="table-responsive">
						<table className="table border">
							<thead>
								<tr className="bg-cusgray">
									<td className="border-top-0">S/N</td>
									<td className="border-top-0">Event</td>
									<td className="border-top-0 text-center">No. of photos</td>
									<td className="border-top-0">Date created</td>
									<td className="border-top-0">Action</td>
								</tr>
							</thead>
							<tbody>
								{data.length > 0 && data.map((user,i) => {
									return (
										<tr key={i}> 
											<td>{count = count + 1}</td>
											<td>
												<div className="events d-flex align-items-center" >
													<div className="events-inner d-flex align-items-center">
														<img src={user.image_urls[0]} alt="Events" className="img-fluid mr-4" />
														<div className="events-inner-details">
															<h5 className="fs-16">{user.title}</h5>
															<p className="mb-0 fs-14">{user.venue}</p>
														</div>
													</div>
												</div>
											</td>
											<td><p className="mb-0 text-center">{user.image_urls.length}</p></td>
											<td>
												<p className="mb-0 date-time"><span>{convertDatePickerTime(user.rowcreateddatetime)}</span></p>
											</td>
											<td className="all-action">
												<div className="dropdown ml-2">
													<span id="dropdownMenu" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i className="fa fa-ellipsis-v"></i></span>
													<div className="dropdown-menu" aria-labelledby="dropdownMenu">
														<Link className="dropdown-item" to={`/edit-gallery/${user.sr_no}`}>Edit</Link>
														<span className="dropdown-item" data-toggle="modal" data-target="#remove" onClick={() => this.setState({ deleteid: user.sr_no })}>Remove</span>
													</div>
												</div>
											</td>
										</tr>
									)
								})}



							</tbody>
						</table>
					</div>
				</div>
				<Deletemodal
					deleteid={this.handleDelete}
					message={"Do you Want to delete Gallery Data"}
					buttonname={"Delete"}
				/>
			</div>

		)
	}
}

const mapStateToProps = state => {
	return {
		newsfeedGallerydata: state.NewsfeedGalleryGet,
		SearchedData: state.SearchGalleryData
	}
}

const mapDispatchToProps = (dispatch) => {
	return {
		getGalleryDispatch: (pageNO) => { dispatch(newsfeedgetGallery(pageNO)) },
		galleryDeleteDispatch: (deleteid) => { dispatch(newsfeedGalleryDelete(deleteid)) },
		SearchGalleryListDispatch: (filter) => { dispatch(SearchGalleryList(filter)) }
	}
}
export default connect(mapStateToProps, mapDispatchToProps)(NewsfeedGallary)