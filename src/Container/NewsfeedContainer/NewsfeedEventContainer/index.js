import React from 'react'
import { Link } from 'react-router-dom'
import { getEvent, eventDelete } from '../../../../src/Action/newsfeed'
import { connect } from 'react-redux'
import Deletemodal from '../../../Component/Deletemodal'
import { Dateonly, Commondateconvert, MonthName } from '../../../Const/function'
import { SearchEventList } from '../../../Action/searchAction'
import Search from '../../../Component/Search'

class NewsfeedEvent extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            eventData: [],
            deleteid: '',
            currentPage: 1,
            data: [],
            dataLength: '',
            firstIndex: '',
            datalimit: 7,
            paginationDetail: [],
            searchvalue: ''
        }
    }
    componentDidMount() {
        this.props.getEventDispatch(this.state.currentPage)
    }



    componentDidUpdate(prevProps, prevState) {
        // if (prevState.currentPage !== this.state.currentPage) {
        //     this.props.getEventDispatch(this.state.currentPage)
        // }
        // if (prevProps.newsfeedEventdata.resp !== this.props.newsfeedEventdata.resp) {
        //     const lastCount = (this.state.currentPage * this.state.datalimit)
        //     const initialCount = lastCount - this.state.datalimit
        //     this.setState({

        //         data: this.props.newsfeedEventdata.resp,
        //         dataLength: this.props.newsfeedEventdata.resp.length,
        //         firstIndex : initialCount
        //     })

        // }

        //-----Checking Coundition on Next and Privious Page---------
        if (prevState.currentPage !== this.state.currentPage) {
            if (prevState.searchvalue === '') {
                this.props.getEventDispatch(this.state.currentPage)
            } else {
                const filter = {
                    Search: this.state.searchvalue,
                    currentPage: this.state.currentPage
                }
                this.props.SearchEventListDispatch(filter)
            }
        }
        //------- ! Set State of Data on currentPage Change Without Search !
        if (prevProps.newsfeedEventdata !== this.props.newsfeedEventdata) {
            const Lastcount = (this.state.currentPage) * this.state.datalimit
            const initialCount = Lastcount - this.state.datalimit
            this.setState({
                data: this.props.newsfeedEventdata.resp,
                paginationDetail: this.props.newsfeedEventdata.pagination,
                dataLength: this.props.newsfeedEventdata.resp.length,
                firstIndex: initialCount
            })
        }
        //--------!  Hiting Search Api/NOrmal Get Api on Change of Search Value!
        if (prevState.searchvalue !== this.state.searchvalue) {
            if (this.state.searchvalue !== '') {
                const filter = {
                    Search: this.state.searchvalue,
                    currentPage: this.state.currentPage
                }
                this.props.SearchEventListDispatch(filter)
            } else {
                this.props.getEventDispatch(this.state.currentPage)
            }
        }

        // ---- Set State of Data on Searching of Value | ----------
        if (prevProps.SearchedData !== this.props.SearchedData) {

            const Lastcount = (this.state.currentPage) * this.state.datalimit
            const initialCount = Lastcount - this.state.datalimit
            this.setState({
                data: this.props.SearchedData.resp,
                paginationDetail: this.props.SearchedData.pagination,
                dataLength: this.props.SearchedData.resp.length,
                firstIndex: initialCount
            })
        }

    }

    handlePrev = () => {
        if (this.state.currentPage > 1) {
            this.setState({
                currentPage: this.state.currentPage - 1
            })
        }
    }

    handleNext = (pageCount) => {
        if (this.state.currentPage < pageCount) {
            this.setState({
                currentPage: this.state.currentPage + 1
            })
        }
    }

    handleDelete = () => {
        this.props.eventDeleteDispatch(this.state.deleteid)
        setTimeout(() => {
            this.props.getEventDispatch(this.state.currentPage)

            if (this.state.dataLength > 1) {
                this.props.getEventDispatch(this.state.currentPage)
                this.setState({ currentPage: this.state.currentPage })
            }
            else {
                if (this.state.currentPage > 1) {
                    this.setState({ currentPage: this.state.currentPage - 1 })
                }
            }
        }, 1000);
    }

    handleOnSearch = (e) => {
        this.setState({
            searchvalue: e.target.value,
            currentPage: 1
        })
    }

    render() {
        const data = this.state.data
        const { firstIndex, dataLength } = this.state
        const pageCount = this.state.paginationDetail.pageCount
        const itemCount = this.state.paginationDetail.itemCount
        let count = firstIndex
        

        return (
            <div className="tab-inner bg-white  px-4 py-3">
                <div className="tab-inner-head pb-3">
                    <div className="row align-items-center mb-3">
                        <div className="col-md-6">
                            <Search
                                onChange={this.handleOnSearch}
                                value={this.state.searchvalue}
                            />
                        </div>
                        <div className="col-md-6">
                            <div className="show-pages d-flex justify-content-end align-items-center">
                                <p className="mb-0 mr-3">Showing <span>{firstIndex + 1}</span> - <span>{firstIndex + dataLength}</span> of <span>{itemCount}</span></p>
                                <div className="prev-next">
                                    <span className="prev"><button className="fas fa-angle-left" onClick={this.handlePrev}></button></span>
                                    <span className="next"><button className="fas fa-angle-right" onClick={() => this.handleNext(pageCount)}></button></span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="tab-inner-body">
                    <div className="add-events-btn text-right mb-3">
                        <Link to="/add-event"><button className="btn btn-outline-warning px-5 rounded-50 fs-14">Add Event</button></Link>
                    </div>
                    <div className="table-responsive">
                        <table className="table border">
                            <thead>
                                <tr className="bg-cusgray">
                                    <td className="border-top-0">S/N</td>
                                    <td className="border-top-0">Event</td>
                                    <td className="border-top-0">Date created</td>
                                    <td className="border-top-0">Action</td>
                                </tr>
                            </thead>
                            <tbody>
                                {data.length > 0 && data.map((user) => {
                                    return (
                                        <tr key={user.sr_no}>
                                            <td>{count = count + 1}</td>
                                            <td>
                                                <div className="events d-flex align-items-center">
                                                    <div className="date text-center mr-4 pr-2">
                                                        <h3 className="mb-0">{Dateonly(user.time)}</h3>
                                                        <p className="mb-0">{MonthName(user.time).slice(0, 3)}</p>
                                                    </div>
                                                    <div className="events-inner d-flex align-items-center">
                                                        <img src={user.image_url} alt="Events" className="img-fluid mr-4" />
                                                        <div className="events-inner-details">
                                                            <h5 className="fs-16">{user.title}</h5>
                                                            <p className="mb-0 fs-14">{user.venue}</p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </td>
                                            <td>
                                                <p className="mb-0 date-time"><span>{Commondateconvert(user.date, 1)}</span></p>
                                            </td>
                                            <td className="all-action cursor">
                                                <div className="dropdown ml-2">
                                                    <span id="dropdownMenu" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i className="fa fa-ellipsis-v"></i></span>
                                                    <div className="dropdown-menu " aria-labelledby="dropdownMenu">
                                                        <Link className="dropdown-item" to={`/edit-events/${user.sr_no}`}>Edit</Link>
                                                        <span className="dropdown-item" data-toggle="modal" data-target="#remove" onClick={() => this.setState({ deleteid: user.sr_no })}>Remove</span>
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                    )
                                })}
                            </tbody>
                        </table>
                    </div>
                </div>
                <Deletemodal
                    deleteid={this.handleDelete}
                    message={"Do you Want to delete Event Data"}
                    buttonname={"Delete"}
                />
            </div>
        )
    }
}
const mapStateToProps = state => {
    return {
        newsfeedEventdata: state.NewsfeedEventGet,
        SearchedData: state.SearchEventData
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        getEventDispatch: (pageNO) => { dispatch(getEvent(pageNO)) },
        eventDeleteDispatch: (deleteid) => { dispatch(eventDelete(deleteid)) },
        SearchEventListDispatch: (filter) => { dispatch(SearchEventList(filter)) }
    }
}
export default connect(mapStateToProps, mapDispatchToProps)(NewsfeedEvent)