import React from 'react'
import { Link } from 'react-router-dom'
import { addTag, newsfeedDeleteTag, newsfeedGetTag } from '../../../../src/Action/newsfeed'
import { connect } from 'react-redux'
import Deletemodal from '../../../Component/Deletemodal'
import { convertDatePickerTime } from '../../../Const/function'
import Search from '../../../Component/Search'
import { SearchTagList } from '../../../Action/searchAction'

class NewsfeedTags extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            tagData: [],
            deleteid: '',
            currentPage: 1,
            data: [],
            dataLength: '',
            firstIndex: '',
            datalimit: 7,
            searchvalue: '',
            paginationDetail: []

        }
    }

    componentDidMount() {
        this.props.getTagDispatch(this.state.currentPage)
    }

    componentDidUpdate(prevProps, prevState) {
        // if (prevState.currentPage !== this.state.currentPage) {
        //     this.props.getTagDispatch(this.state.currentPage)
        // }
        // if (prevProps.newsfeedTagdata.resp !== this.props.newsfeedTagdata.resp) {
        // 	const lastCount = (this.state.currentPage * this.state.datalimit)
        // 	const initialCount = lastCount - this.state.datalimit
        // 	this.setState({
        // 		data: this.props.newsfeedTagdata.resp,
        // 		dataLength: this.props.newsfeedTagdata.resp.length,
        // 		firstIndex: initialCount
        // 	})
        // }
        if (prevState.currentPage !== this.state.currentPage) {
            if (prevState.searchvalue === '') {
                this.props.getTagDispatch(this.state.currentPage)
            } else {
                const filter = {
                    Search: this.state.searchvalue,
                    currentPage: this.state.currentPage
                }
                this.props.SearchTagListDispatch(filter)
            }
        }
        //------- ! Set State of Data on currentPage Change Without Search !
        if (prevProps.newsfeedTagdata !== this.props.newsfeedTagdata) {
            const Lastcount = (this.state.currentPage) * this.state.datalimit
            const initialCount = Lastcount - this.state.datalimit
            this.setState({
                data: this.props.newsfeedTagdata.resp,
                paginationDetail: this.props.newsfeedTagdata.pagination,
                dataLength: this.props.newsfeedTagdata.resp.length,
                firstIndex: initialCount
            })
        }
        //--------!  Hiting Search Api/NOrmal Get Api on Change of Search Value!
        if (prevState.searchvalue !== this.state.searchvalue) {
            if (this.state.searchvalue !== '') {
                const filter = {
                    Search: this.state.searchvalue,
                    currentPage: this.state.currentPage
                }
                this.props.SearchTagListDispatch(filter)
            } else {
                this.props.getTagDispatch(this.state.currentPage)
            }
        }

        // ---- Set State of Data on Searching of Value | ----------
        if (prevProps.SearchedData !== this.props.SearchedData) {
            const Lastcount = (this.state.currentPage) * this.state.datalimit
            const initialCount = Lastcount - this.state.datalimit
            console.log("SearchedData", this.props.SearchedData)
            this.setState({
                data: this.props.SearchedData.resp,
                paginationDetail: this.props.SearchedData.pagination,
                dataLength: this.props.SearchedData.resp.length,
                firstIndex: initialCount
            })
        }


    }
    handleOnSearch = (e) => {
        this.setState({
            searchvalue: e.target.value,
            currentPage: 1
        })
    }

    handleDelete = () => {
        this.props.tagDeleteDispatch(this.state.deleteid)
        setTimeout(() => {
            if (this.state.dataLength > 1) {
                this.props.getTagDispatch(this.state.currentPage)
                this.setState({ currentPage: this.state.currentPage })
            }
            else {
                if (this.state.currentPage > 1) {
                    this.setState({ currentPage: this.state.currentPage - 1 })
                }
            }
        }, 1000);
    }

    handlePrev = () => {
        if (this.state.currentPage > 1) {
            this.setState({
                currentPage: this.state.currentPage - 1
            })
        }
    }

    handleNext = (pageCount) => {
        if (this.state.currentPage < pageCount) {
            this.setState({
                currentPage: this.state.currentPage + 1
            })
        }
    }


    render() {
        // const data = this.state.data
        // const { firstIndex, dataLength } = this.state
        // const pageCount = this.props.newsfeedTagdata.pagination.pageCount
        // const itemCount = this.props.newsfeedTagdata.pagination.itemCount
        // let count = firstIndex
        const data = this.state.data
        const { firstIndex, dataLength } = this.state

        const pageCount = this.state.paginationDetail.pageCount
        const itemCount = this.state.paginationDetail.itemCount
        let count = firstIndex

        return (
            <div className="tab-inner bg-white  px-4 py-3">
                <div className="tab-inner-head pb-3">
                    <div className="row align-items-center mb-3">
                        <div className="col-md-6">
                            <Search
                                onChange={this.handleOnSearch}
                                value={this.state.searchvalue}
                            />
                        </div>
                        <div className="col-md-6">
                            <div className="show-pages d-flex justify-content-end align-items-center">
                                <p className="mb-0 mr-3">Showing <span>{firstIndex + 1}</span> - <span> {firstIndex + dataLength}</span> of <span>{itemCount}</span></p>
                                <div className="prev-next">
                                    <span className="prev"><button className="fas fa-angle-left" onClick={this.handlePrev}></button></span>
                                    <span className="next"><button className="fas fa-angle-right" onClick={() => this.handleNext(pageCount)}></button></span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="tab-inner-body">
                    <div className="add-events-btn text-right mb-3">
                        <Link to="/add-tag"><button className="btn btn-outline-warning px-5 rounded-50 fs-14">Add tags</button></Link>
                    </div>
                    <div className="table-responsive">
                        <table className="table border">
                            <thead>
                                <tr className="bg-cusgray">
                                    <td className="border-top-0">S/N</td>
                                    <td className="border-top-0">Title</td>
                                    <td className="border-top-0">Date created</td>
                                    <td className="border-top-0">Action</td>
                                </tr>
                            </thead>
                            <tbody>
                                {data.length > 0 && data.map((user) => {
                                    return (
                                        <tr>
                                            <td>{count = count + 1}</td>
                                            <td>
                                                <p className="mb-0">{user.title}</p>
                                            </td>
                                            <td>
                                                <p className="mb-0 date-time"><span>{convertDatePickerTime(user.rowcreateddatetime)}</span></p>
                                            </td>
                                            <td className="all-action">
                                                <div className="dropdown ml-2">
                                                    <span id="dropdownMenu" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i className="fa fa-ellipsis-v"></i></span>
                                                    <div className="dropdown-menu" aria-labelledby="dropdownMenu">
                                                        <Link className="dropdown-item" to={`/edit-tag/${user.sr_no}`}>Edit</Link>
                                                        <span className="dropdown-item" data-toggle="modal" data-target="#remove" onClick={() => this.setState({ deleteid: user.sr_no })}>Remove</span>
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                    )
                                })}
                            </tbody>
                        </table>
                    </div>
                </div>
                <Deletemodal
                    deleteid={this.handleDelete}
                    message={"Do you Want to delete Tag Data"}
                    buttonname={"Delete"}
                />
            </div>
        )
    }
}
const mapStateToProps = state => {
    return {
        newsfeedTagdata: state.NewsfeedTagGet,
        SearchedData: state.SearchTagData
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        getTagDispatch: (pageNO) => { dispatch(newsfeedGetTag(pageNO)) },
        tagDeleteDispatch: (deleteid) => { dispatch(newsfeedDeleteTag(deleteid)) },
        SearchTagListDispatch: (filter) => { dispatch(SearchTagList(filter)) }
    }
}
export default connect(mapStateToProps, mapDispatchToProps)(NewsfeedTags)