import React, { useState } from 'react';
import { connect } from 'react-redux';
import { Link, withRouter } from 'react-router-dom';
import ProfileImg from '../../Assets/img/admin-user.svg';
import SearchInput from "../../Component/SearchInput";
import './style.css';

const Header = (props) => {
  const contentEntity = props.history.location.pathname.split("/")[2];
  const [active, setActive] = useState(contentEntity);

  const navigationTitles  = [
    "Recent",
    "Blog",
    "Press",
    "Gallery",
    "Career",
    "About",
    "Community"
  ]

  const Logout = () => {
    props.loginstatus('0')
    localStorage.removeItem('loginStatus');
    localStorage.removeItem('adminToken');
    localStorage.removeItem('adminName')
    localStorage.removeItem('adminId')
    localStorage.removeItem('adminRole')
    window.location.pathname = "/"
  }

  const styles = {
    button: {
      marginRight: 8,
      fontSize: 14,
      color: "rgba(112, 112, 112, 0.7)",
      paddingBottom: 4,
      cursor: "pointer"
    },
    active: {
      color: "#050038",
      fontWeight: "bold",
      borderBottom: "0.1px solid #050038",
    }
  }

  const [admiName, adminRole, adminId] = ["Wande Adams", "MD", 1]

  return (
    <>
        <header className="position-relative mb-3">
            <div className="header-top py-3">
                <div className="container">
                    <div className="row">
                        <div className="col-md-8 d-flex align-items-center">
                            <SearchInput />
                        </div>
                        <div className="col-md-4">
                            <div className="admin-profileMenu dropdown ml-auto d-table">
                                <Link className="text-decoration-none d-flex align-items-center" to="#" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    <div className="admin-dp mr-2">
                                        <img src={ProfileImg} alt="Profile Image" className="img-fluid" />
                                    </div>
                                    <div className="admin-detail pt-1">
                                        <h5 className="admin-name mb-0">{admiName}</h5>
                                        <p className="text-warnings fs-14 mb-0">{adminRole}</p>
                                    </div>
                                </Link>
                                <div className="dropdown-menu left-unset right-0" aria-labelledby="dropdownMenuLink">
                                    <Link to={`/account/${adminId}`} className="dropdown-item"><i className="far fa-user-circle"></i> Account</Link>
                                    <Link to="#" onClick={Logout} className="dropdown-item"><i className="fas fa-sign-out-alt"></i> Logout</Link>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div
                      style={{
                        display: "flex",
                        flexDirection: "row",
                        justifyContent: "space-between",
                        width: "90%",
                        marginTop: 105,
                        borderBottom: "0.1px solid rgba(59,107,211,.2)",
                      }}
                    >
                      {navigationTitles.map(title => {
                        const isActive = active === title.toLowerCase() ? styles.active : {};
                        return (
                          <div
                            style={{...styles.button, ...isActive}}
                            onClick={() => {
                              setActive(title)
                              props.history.push(`/content-manager/${title.toLowerCase()}`)
                            }}
                          >
                            {title}
                          </div>
                        )
                      })}
                    </div>
                </div>
            </div>
        </header>
    </>
)
}
const mapStateToProps = (state) => {
    return {
        admininfo: state.login
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        loginstatus: d => dispatch({ type: "LOGIN_STATUS", payload: d }),

    }
}
export default withRouter(connect(mapStateToProps, mapDispatchToProps)(Header));
