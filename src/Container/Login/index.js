import React from 'react'
import AuthenticationFlowSidebar from '../../Component/AuthenticationFlowSidebar'
import AuthenticationForm from '../../Component/AuthenticationForm'

const Login = () => {
  return (
    <>
      <div className="login">
          <div style={{ display: "grid", gridTemplateColumns: "35% 65%"}}>
            <AuthenticationFlowSidebar />
            <AuthenticationForm page="login" />
          </div>
      </div>
    </>
  )
}

export default Login;


