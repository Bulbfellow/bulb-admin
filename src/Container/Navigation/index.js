import React, { useState } from 'react';
import { connect } from 'react-redux';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
import Login from '../../Container/Login';
import ResetPassword from '../../Container/ResetPassword';
import SetPassword from '../../Container/Setpassword';
import Activity from '../../Pages/Activity';
import ApplyIncubation from '../../Pages/ApplicationRequest/ApplyIncubation';
import SingleIncubation from '../../Pages/ApplicationRequest/ApplyIncubation/SingleIncubation';
import Partner from '../../Pages/ApplicationRequest/Partner';
import SinglePartner from '../../Pages/ApplicationRequest/Partner/SinglePartner';
import SubmitIdea from '../../Pages/ApplicationRequest/SubmitIdea';
import ScheduleMeeting from '../../Pages/ApplicationRequest/SubmitIdea/ScheduleMeeting';
import SingleIdea from '../../Pages/ApplicationRequest/SubmitIdea/SingleIdea';
import Careers from '../../Pages/Careers';
import About from '../../Pages/ContentManager/About';
import CreateStaff from '../../Pages/ContentManager/About/CreateStaff';
import ShowSingleStaff from '../../Pages/ContentManager/About/ShowSingleStaff';
import Blog from '../../Pages/ContentManager/Blog';
import CreateBlog from '../../Pages/ContentManager/Blog/CreateBlog';
import SingleBlog from '../../Pages/ContentManager/Blog/ShowSingleBlog';
import Career from '../../Pages/ContentManager/Career';
import Community from '../../Pages/ContentManager/Community';
import CreateCommunity from '../../Pages/ContentManager/Community/CreateCommunity';
import SingleCommunity from '../../Pages/ContentManager/Community/ShowSingleCommunity';
import Gallery from '../../Pages/ContentManager/Gallery';
import Press from '../../Pages/ContentManager/Press';
import CreatePress from '../../Pages/ContentManager/Press/CreatePress';
import SinglePress from '../../Pages/ContentManager/Press/ShowSinglePress';
import Recent from '../../Pages/ContentManager/Recent';
import SingleRecent from '../../Pages/ContentManager/Recent/ShowSingleRecent';
import ManageAdmin from '../../Pages/ManageAdmin';
import NewsFeeds from '../../Pages/NewsFeeds';
import TheDesk from '../../Pages/TheDesk';
import Workspace from '../../Pages/Workspace';
import Dashboard from '../Dashboard';
import Account from '../Entries/Account';
import AddAdmin from '../Entries/AddAdmin';
import AddWorkspace from '../Entries/AddWorkspace';
import AddCareers from '../Entries/CareersEntries/AddCareers';
import EditCareers from '../Entries/CareersEntries/EditCareers';
import CareersEntriesRequest from '../Entries/CareersEntries/ViewCareersApplicationRequest';
import ChangePassword from '../Entries/ChangePassword';
import EditAdmin from '../Entries/EditAdmin';
import EditWorkspace from '../Entries/EditWorkspace';
import AddEvents from '../Entries/NewsFeedEntries/AddEvents';
import AddGallery from '../Entries/NewsFeedEntries/AddGallery';
import AddPress from '../Entries/NewsFeedEntries/AddPress';
import AddTag from '../Entries/NewsFeedEntries/AddTag';
import EditEvents from '../Entries/NewsFeedEntries/EditEvents';
import EditGallery from '../Entries/NewsFeedEntries/EditGallery';
import EditPress from '../Entries/NewsFeedEntries/EditPress';
import EditTag from '../Entries/NewsFeedEntries/EditTag';
import RecoverPassword from '../RecoverPassword';
import Sidebar from '../Sidebar';

const Navigation = ({ loginstatus }) => {

    let loginStatus = loginstatus.loginstatus
    let adminrole = loginstatus.adminRole

    const [sidebar, setsidebar] = useState(false)

    const [viewentry, setViewentry] = useState('')

    return (
        <Router>
            {localStorage.getItem('token') ?
                <div className="mainDiv d-flex min-vh-100 justify-content-between">
                    <div className={`sidebar ${sidebar === true && "showSidebar"}`} >
                        <Sidebar />
                    </div>
                    <div className="rightSide w-100 d-flex flex-column" style={{ backgroundColor: "#E5E5E5"}}>
                        <div className={`overlay-menu ${sidebar === true && "show-overlay"}`}>
                            <p className="position-absolute" onClick={() => setsidebar(!sidebar)}><i className="fas fa-times"></i></p>
                        </div>
                        <Switch>
                            <Route exact path='/' component={Dashboard} />
                            <Route path='/application-request/submitIdea/single' component={SingleIdea} />
                            <Route path='/application-request/submitIdea/schedule' component={ScheduleMeeting} />
                            <Route path='/application-request/submitIdea' component={SubmitIdea} />
                            <Route path='/application-request/partner/single' component={SinglePartner} />
                            <Route path='/application-request/partner' component={Partner} />
                            <Route path='/application-request/incubation/single' component={SingleIncubation} />
                            <Route path='/application-request/incubation' component={ApplyIncubation} />
                            <Route path='/news-feeds' component={NewsFeeds} />
                            <Route path='/content-manager/recent/single' component={SingleRecent} />
                            <Route path='/content-manager/recent' component={Recent} />
                            <Route path='/content-manager/blog/single' component={SingleBlog} />
                            <Route path='/content-manager/blog/create' component={CreateBlog} />
                            <Route path='/content-manager/blog' component={Blog} />
                            <Route path='/content-manager/career' component={Career} />
                            <Route path='/content-manager/gallery' component={Gallery} />
                            <Route path='/content-manager/about/single' component={ShowSingleStaff} />
                            <Route path='/content-manager/about/create' component={CreateStaff} />
                            <Route path='/content-manager/about' component={About} />
                            <Route path='/content-manager/press/single' component={SinglePress} />
                            <Route path='/content-manager/press/create' component={CreatePress} />
                            <Route path='/content-manager/press' component={Press} />
                            <Route path='/content-manager/community/single' component={SingleCommunity} />
                            <Route path='/content-manager/community/create' component={CreateCommunity} />
                            <Route path='/content-manager/community' component={Community} />
                            <Route path='/add-event' component={AddEvents} />
                            <Route path='/add-press' component={AddPress} />
                            <Route path='/add-tag' component={AddTag} />
                            <Route path='/add-gallery' component={AddGallery} />
                            <Route path={`/edit-events/:id`} component={EditEvents} />
                            <Route path={`/edit-press/:id`} component={EditPress} />
                            <Route path={`/edit-tag/:id`} component={EditTag} />
                            <Route path={`/edit-gallery/:id`} component={EditGallery} />
                            <Route path='/post-career' component={AddCareers} />
                            <Route path={`/edit-careers/:id`} component={EditCareers} /> 
                            <Route path='/view-career-request/:id' component={CareersEntriesRequest} />
                            <Route path='/add-workspace' component={AddWorkspace} />
                            <Route path={`/edit-workspace/:id`} component={EditWorkspace} />
                            <Route path='/add-admin' component={AddAdmin} />
                            <Route path={`/edit-admin/:id`} component={EditAdmin} />
                            <Route path={`/account/:id`} component={Account} />
                            <Route path='/change-password' component={ChangePassword} />
                            <Route path='/careers' component={Careers} />
                            <Route path='/workspace' component={Workspace} />
                            <Route path='/the-desk' component={() => <TheDesk call={setViewentry} />} />
                            <Route path='/activity' component={Activity} />
                            <Route path='/manage-admin' component={adminrole === "Admin" ? Dashboard : ManageAdmin} />
                        </Switch>
                    </div>
                </div> :
                <Switch>
                    <Route exact path='/' component={Login} />
                    <Route path='/recover-password' component={ResetPassword} />
                    <Route exact path='/set-password' component={SetPassword} />
                    <Route exact path='/forgot-password' component={RecoverPassword} />
                </Switch>
            }
        </Router >
    )
}
const mapStateToProps = (state) => {
    return {
      loginstatus: state.login,
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
    }
}
export default connect(mapStateToProps, mapDispatchToProps)(Navigation);

