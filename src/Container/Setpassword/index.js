import React, { Component } from 'react'
import { Link } from 'react-router-dom'
import Logo from '../../Assets/img/logo-black.png'
import Axios from 'axios'
import queryString from 'query-string';
import Quicknotification from '../../Component/Quicknotification'
import {Api} from '../../Const/Api'

export default class SetPassword extends Component {
	constructor() {
		super()
		this.state = {
			password: "",
			confirmpassword: "",
			hiddenpassword: true,
			hiddenconfirmpassword: true,
			showpopup: false,
			message: ''
		}
	}

	Password = () => {
		this.setState({ hiddenpassword: !this.state.hiddenpassword });
	}
	confirmPassword = () => {
		this.setState({ hiddenconfirmpassword: !this.state.hiddenconfirmpassword });
	}

	resetPassword = () => {
		const { password, confirmpassword } = this.state;

		if (password !== confirmpassword) {
			this.setState({
				showpopup: true, message: "Passwords don't match"
			})
			// alert("Passwords don't match");
		} else {
			// alert('right')
			let url = this.props.location.search;
			let params = queryString.parse(url);
			console.log("params----", params.token);
			Axios.post(Api.BaseUrl+'/api/adminreset', {
				password: confirmpassword,
				token: params.token,
			})
				.then(res => {
					console.log('res',res)
					this.setState({
						showpopup: true,
						message: "Your password has been set sucessfully"
					})
					window.location.pathname = "/"
				})
		}
	}
	render() {
		const { hiddenpassword, hiddenconfirmpassword, showpopup, message } = this.state

		return (
			<>
				<div className="login-page">
					<div className="container">
						<div className="row min-vh-100 py-5 align-items-center">
							<div className="col-md-6 col-lg-4">
								<div className="login-form form-inner">
									<div className="logo mb-3">
										<img src={Logo} alt="Logo" className="img-fluid ml-2 ml-4 pl-2" />
									</div>
									<div className="card border-0 shadow">
										<div className="card-body">
											<h5 className="mb-4">Set your password</h5>
											<div className="form-group pb-2">
												<div className="view-pass position-relative">
													<input
														type={hiddenpassword ? "password" : "text"}
														className="form-control"
														placeholder="New Password"
														onChange={(e) => this.setState({ password: e.target.value })}
														required
													/>
													<span className="showeye" onClick={this.Password}> {hiddenpassword ? <i className="fa fa-eye-slash"></i>
														: <i className="fa fa-eye"></i>} </span>                                                </div>
											</div>
											<div className="form-group pb-4">
												<div className="view-pass position-relative">
													<input
														type={hiddenconfirmpassword ? "password" : "text"}
														className="form-control"
														placeholder="Confirm Password"
														onChange={(e) => this.setState({ confirmpassword: e.target.value })}
														required
													/>
													<span className="showeye" onClick={this.confirmPassword}> {hiddenconfirmpassword ? <i className="fa fa-eye-slash"></i>
														: <i className="fa fa-eye"></i>} </span>
												</div>
											</div>
											<div className="form-group mb-4">
												<button className="btn btn-warning text-white btn-block text-uppercase" onClick={this.resetPassword}>Set Password</button>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<Quicknotification
						show={this.state.showpopup}
						message={this.state.message}
						closetab={() => this.setState({showpopup : false})}
						bgblock={true}
						closesecond={4000}
						timeout={true}
					/>
				</div>
			</>
		)
	}
}
