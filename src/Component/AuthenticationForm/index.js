
import { makeStyles } from '@material-ui/core/styles';
import Axios from 'axios';
import React, { useState } from 'react';
import { connect } from 'react-redux';
import Login from "../../Component/Forms/login";
// import ResetPassword from "../../Component/Forms/resetPassword";
import { Api } from '../../Const/Api';



const useStyles = makeStyles((theme) => ({
  root: {
    '& .MuiTextField-root': {
      margin: theme.spacing(1),
      width: '25ch',
    },
    width: "65%"
  },
  textField: {
    width: '7ch',
  }
}));

const AuthenticationForm = (page) => {

  const [loading, setLoading] = useState(false)
  const classes = useStyles();
  const [values, setValues] = React.useState({
    email: '',
    password: '',
    showPassword: false,
  });

  const handleChange = (prop) => (event) => {
    setValues({ ...values, [prop]: event.target.value });
  };

  const handleClickShowPassword = () => {
    setValues({ ...values, showPassword: !values.showPassword });
  };

  const handleMouseDownPassword = (event) => {
    event.preventDefault();
  };

const handleKeyDown = (e) => {
    if (e.key === 'Enter') {
      setLoading(true);
      Axios.post(Api.BaseUrl+'/api/login', {
        email: values.email,
        password: values.password
      })
      .then(res => {
        localStorage.setItem('token', res.data.payload.token);
        window.location.href = "/"
        setLoading(false)
      })
      .catch(error => {
        setLoading(false)
        console.log("error", error)
      });
    }
}

function Submit() {
  setLoading(true);
  Axios.post(Api.BaseUrl+'/api/login', {
    email: values.email,
    password: values.password
  })
  .then(res => {
    localStorage.setItem('token', res.data.payload.token);
    window.location.href = "/"
    setLoading(false)
  })
  .catch(error => {
    setLoading(false)
    console.log("error", error)
  })
}

  const styles = {
    container: {
      display: "flex",
      alignItems: "center",
      justifyContent: "center",
      width: "100%"
    },
    formDiv: {
      border: "0.2px solid #050038",
      height: 610,
      width: "100%",
      display: "flex",
      flexDirection: "column",
      alignItems: "center",
      boxSizing: "border-box",
      borderRadius: 6
    },
    textInputField: {
      width: "70%",
      height: 50,
      marginTop: 90
    },
    hyperlink: {
      color: "#050038",
      textDecoration: "underline",
      cursor: "pointer"
    },
    textInput: {
      fontWeight: "700",
      color: "#050038",
      fontSize: 16
    }
  }
  
  return (
    <div style={styles.container}>
      <form className={classes.root} noValidate autoComplete="off" onKeyDown={handleKeyDown}>
        <div style={styles.formDiv}>
          <Login
            styles={styles}
            handleChange={handleChange}
            handleClickShowPassword={handleClickShowPassword}
            handleMouseDownPassword={handleMouseDownPassword}
            loading={loading}
            Submit={Submit}
            values={values}
          />
        </div>
      </form>
    </div>
  )
}

const mapStateToProps = (state) => {
  return {
    ...state
  }
}
const mapDispatchToProps = (dispatch) => {
  return {
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(AuthenticationForm);
