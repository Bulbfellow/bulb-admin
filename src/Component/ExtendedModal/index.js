import React from 'react';
import Modal from '../Modal';


const ExtendedModal = ({ showModal, closeModal, children }) => {
  return (
    <div>
      {showModal && <Modal onClose={closeModal}>
        <div
          style={{
            height: 301,
            width: 517,
            borderRadius: 4,
            background: "white",
            paddingTop: 25,
            marginTop: 120,
            position: "relative"
          }}
        >
          <div
            style={{
              display: "flex",
              flexDirection: "row",
              justifyContent: "flex-end",
              marginBottom: 32,
              paddingRight: 40,
              cursor: "pointer"
            }}
            onClick={() => closeModal()}
          >
            <img src={require("../../Assets/img/cancel.svg")} />
          </div>
          {children}
        </div>
      </Modal>}
    </div>
  )
}

export default ExtendedModal;
