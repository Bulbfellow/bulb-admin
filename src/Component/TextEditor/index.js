import { ContentState, EditorState } from "draft-js";
import draftToHtml from "draftjs-to-html";
import htmlToDraft from "html-to-draftjs";
import React, { Component } from "react";
import { Editor } from "react-draft-wysiwyg";
import 'react-draft-wysiwyg/dist/react-draft-wysiwyg.css';

class TextEditor extends Component {
    constructor(props) {
        super(props);

        const { defaultValue } = this.props;

        this.state = {
            editorState: false
        }

        if(defaultValue) {
            const contentBlock = htmlToDraft(defaultValue);

            const contentState = ContentState.createFromBlockArray(contentBlock.contentBlocks); 

            const editorState = EditorState.createWithContent(contentState);

            this.state.editorState = editorState;
        }
    }
    
    onChange = editorState => {
        const html = draftToHtml(editorState);

        //check if editor is empty
        if(editorState.blocks.length === 1 && editorState.blocks[0].text === "") {
            this.props.onChange && this.props.onChange("");
            return;
        }

        this.props.onChange &&
            this.props.onChange(html);
    }
    
    render() {
        const { editorState } = this.state;
        const { disabled, placeholder } = this.props;
        return (
            <Editor
                toolbar={{
                    options: ["inline", "list", "blockType"]
                }}
                editorStyle={{
                    height: "200px",
                    padding: "12px",
                    width: "100%",
                    backgroundColor:  disabled ? "#efefef" :"white",
                    color: "#050038",
                    fontSize: 16,
                    fontWeight: "bold"
                }}
                toolbarStyle={{
                    backgroundColor: "rgba(229, 229, 229, .6)"
                }}
                wrapperStyle={{
                    border: "0.5px solid rgba(69, 101, 255, .45)",
                    borderRadius: "6px",
                    overflow: "hidden"
                }}
                onChange={this.onChange}
                defaultEditorState={editorState}
                placeholder = {placeholder}
                readOnly={disabled?true:false}
                require
            />
        )
    }
}

export default TextEditor;