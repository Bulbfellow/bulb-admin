import React from 'react';

const ProfileOverlay = ({ type, width, height, children }) => {
  return (
    <div style={{
      width: type ? 248 : width ,
      height:  type === "Executive" ? 265 : height,
      backgroundColor: "rgba(0, 0, 0, .2)",
      position: "absolute",
      display: "flex",
      color: "#fff",
      flexDirection: "column",
      padding: 15
      }}
    >
      {children}
    </div>
  )
}

export default ProfileOverlay
