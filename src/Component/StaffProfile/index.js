import React from 'react';
import { withRouter } from 'react-router-dom';
import ProfileOverlay from "../ProfileOverlay";


const StaffProfile = ({
  name,
  role,
  imageStyle,
  setHoveredPicture,
  hoveredPicture,
  history,
  businessUnit,
  linkedinUrl,
  pickedPictures,
  setPickedPictures
}) => {
  const [firstname] = name.split(" ");
  return (
    <div
      style={{ position: "relative" }}
      onMouseEnter={() => setHoveredPicture(firstname.toLowerCase())}
      onMouseLeave={() => setHoveredPicture("")}
    >
      {(hoveredPicture === firstname.toLowerCase() || pickedPictures.length > 0) && 
        <ProfileOverlay
          type={businessUnit}
          height={218}
        >
          <div
            style={{
              display: "flex",
              flexDirection: "row",
              marginBottom: 3,
              justifyContent: "flex-end",
              marginBottom: 90
            }}
          >
            <input
              type="checkbox"
              style={{ transform: "scale(1.3)"}}
              onChange={() => setPickedPictures(firstname.toLowerCase())}
            />
          </div>
          <div
            style={{
              display: "flex",
              flexDirection: "row",
              justifyContent: "center",
              alignItems: "center"
            }}
          >
            <div
              style={{
                background: "#FFFFFF",
                border: "0.4px solid rgba(59, 107, 211, 0.8)",
                boxShadow: "0px 2px 2px rgba(59, 107, 211, 0.08)",
                borderRadius: 6,
                width: 111,
                height: 48,
                display: "flex",
                alignItems: "center",
                justifyContent: "center",
                fontSize: 15,
                fontWeight: "bold",
                color: "#050038",
                cursor: "pointer"
              }}
              onClick={() => history.push("/content-manager/about/single", { name, role, businessUnit, linkedinUrl })}
            >
              View
            </div>
            
          </div>
        </ProfileOverlay>
      }
      <img style={imageStyle} src={require(`../../Assets/img/${firstname}.png`)} />
      <div
        style={{
          textAlign: "center",
          width: 254,
          marginTop: 15,
          cursor: "pointer"
        }}
      >
        <p
          style={{ fontSize: 22, marginBottom: 0, color: "#28314F" }}
        >
          {name}
        </p>
        <p
          style={{ fontSize: 18, marginBottom: 5, color: "#707070" }}
        >
          {role}
        </p>
      </div>
    </div>
  )
}

export default withRouter(StaffProfile);
