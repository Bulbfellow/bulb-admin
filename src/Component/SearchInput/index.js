// import PermissionModal from "@/components/PermissionModal";
// import { getSearchResults } from "@/store/app/actions";
// import { fetchEmployerPermission } from "@/store/permission/actions";
// import { checkPermission } from "@/utils";
import React, { Component } from "react";
import { connect } from "react-redux";
import { withRouter } from "react-router-dom";
import "./style.css";


class SearchInput extends Component {
  constructor(props) {
    super(props);
    this.state = {
    //   showPermissionModal: false,
    //   AllowScheduleInterview:
    //   checkPermission(this.props?.role?.permissions, `"allPermissions"`) ||
    //  checkPermission(this.props?.role?.permissions, `"canPostJobs","canShortlistApplicants","canScheduleInterviews","canSendMessages","canSearchForCandidates","canMakePaymentForPlans","canChangeAccountSettings"`)||
    //   checkPermission(this.props?.role?.permissions, `canSearchForCandidates`)||checkPermission(this.props?.role?.permissions, `["canPostJobs","canShortlistApplicants","canScheduleInterviews","canSendMessages","canSearchForCandidates","canMakePaymentForPlans","canChangeAccountSettings"]`)
    };
  }
  
  componentDidMount=()=>{
    // this.props.fetchEmployerPermission()
  }
  render() {
    let typingTimer;
    const handleOnkeyUp = () => {
      const searchInput = document.getElementById("search").value.trim();
      clearTimeout(typingTimer);
      if (searchInput) {
        return (typingTimer = setTimeout(doneTyping, 2000));
      }
      return;
    };

    const handleOnkeyPress = () => {
      clearTimeout(typingTimer);
    };

    
    function doneTyping() {
      const searchInput = document.getElementById("search").value.trim();
      clearTimeout(typingTimer);
      fetchSearch(searchInput);
    }

    const closePermissionModal = () => {
      this.setState({
        ...this.state,
        showPermissionModal: false,
      });
    };
    const handleSubmit = (e) => {
      e.preventDefault();
      clearTimeout(typingTimer);
      const formElements = e.target.elements;
      let payload;

      Array.prototype.forEach.call(formElements, (element) => {
        payload = element.value;
      });

      const trimPayload = payload.trim();
      //return if payload is empty
      if (!trimPayload) {
        return;
      }
      fetchSearch(trimPayload);
    };





    const fetchSearch = (search) => {
      if (!this.state.AllowScheduleInterview) {
        this.setState({
          ...this.state,
          showPermissionModal: true,
        });
        return
      }
      
      this.props.getSearchResults(search);
      this.props.history.replace({ pathname: "/dashboard/search", state: search });
      document.getElementById("search").value = "";
    };

    clearTimeout(typingTimer);
    return (
      <div className="dashboard-header__search-box"
      >
        <img
          src={require("../../Assets/img/lens.svg")}
          alt="search icon"
          className="dashboard-header__search-icon"
        />
          <input
            type="text"
            className="dashboard-header__input"
            placeholder="Search"
            name="search"
            id="search"
            autoComplete="off"
          />
      </div>
    );
  }
}
const mapStateToProps = (state) => {
  // const {
  //   permissions: {
  //     profile: {
  //       userrole: { role={} }={},
  //     },
  //   },
  // } = state;
  return {
    // role,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    // getSearchResults: (id) => dispatch(getSearchResults(id)),
    // fetchEmployerPermission: () => dispatch(fetchEmployerPermission()),
  };
};
export default withRouter(
  connect(mapStateToProps, mapDispatchToProps)(SearchInput)
);
