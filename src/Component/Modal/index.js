import React, { Component } from "react";
import "./style.css";


class Modal extends Component {
    onModalClick = (e) => {
        if(e.target.classList.contains("safe-area")) {
            this.props.onClose() &&
                this.props.onClose();
        }
    }
    
    render() {
        return(
            <div className="pace-modal active safe-area" onClick={this.onModalClick}>
                <div className="container h-100 safe-area">
                    <div className="row justify-content-center safe-area">
                        <div className="col-md-10 col-lg-6">
                            {this.props.children}
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

export default Modal;