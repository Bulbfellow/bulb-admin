import React from 'react';
import AuthFlowBackground from "../../Assets/img/AuthFlowSidebar.svg";
import BulbLogo from "../../Assets/img/logo-white.svg";

const AuthenticationFlowSidebar = () => {

  const styles = {
    sidebar: {
      width: "100%",
      height: "100vh",
      backgroundImage: `url(${AuthFlowBackground})`,
      display: "flex",
      alignItems: "center",
      justifyContent: "center"
    }
  }
  
  return (
    <div style={styles.sidebar}>
      <img src={BulbLogo} alt="logo" />
    </div>
  )
}

export default AuthenticationFlowSidebar;
