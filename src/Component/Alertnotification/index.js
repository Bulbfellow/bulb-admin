import React from 'react'
import { makeStyles } from '@material-ui/core/styles';
import Snackbar from '@material-ui/core/Snackbar';
import IconButton from '@material-ui/core/IconButton';
import CloseIcon from '@material-ui/icons/Close';

const useStyles = makeStyles(theme => ({
    close: {
      padding: theme.spacing(0.5),
    },
  }));
const AlertNotification = ({ show, message ,autoHideDuration}) => {
    const handleClose = (event, reason) => {
        if (reason === 'clickaway') {
          return;
        }
        show(false);
      };
    
    const classes = useStyles();

    return (
        <Snackbar
            anchorOrigin={{
                vertical: 'top',
                horizontal: 'right',
            }}
            autoHideDuration={1000}
            open={show}
            message={message}
        />
    )
}
export default AlertNotification