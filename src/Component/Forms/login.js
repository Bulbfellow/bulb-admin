import FormControl from '@material-ui/core/FormControl';
import IconButton from '@material-ui/core/IconButton';
import InputAdornment from '@material-ui/core/InputAdornment';
import InputLabel from '@material-ui/core/InputLabel';
import OutlinedInput from '@material-ui/core/OutlinedInput';
import Visibility from '@material-ui/icons/Visibility';
import VisibilityOff from '@material-ui/icons/VisibilityOff';
import React from 'react';
import { Link } from 'react-router-dom';
import Button from '../Button';


const login = ({ styles, handleChange, Submit, values, loading, handleClickShowPassword, handleMouseDownPassword}) => {
  return (
    <>
      <FormControl style={styles.textInputField} variant="outlined">
        <InputLabel htmlFor="outlined-adornment-amount">Email address (company mail)</InputLabel>
        <OutlinedInput
          id="outlined-adornment-amount"
          value={values.email}
          style={styles.textInput}
          onChange={handleChange('email')}
          labelWidth={220}
          disabled={loading}
          startAdornment={<InputAdornment position="start"></InputAdornment>}
        />
      </FormControl>
      <FormControl style={styles.textInputField} variant="outlined">
        <InputLabel htmlFor="outlined-adornment-password">Password</InputLabel>
        <OutlinedInput
          id="outlined-adornment-password"
          type={values.showPassword ? 'text' : 'password'}
          value={values.password}
          style={styles.textInput}
          disabled={loading}
          onChange={handleChange('password')}
          startAdornment={<InputAdornment position="start"></InputAdornment>}
          endAdornment={
            <InputAdornment position="end">
              <IconButton
                aria-label="toggle password visibility"
                onClick={handleClickShowPassword}
                onMouseDown={handleMouseDownPassword}
                edge="end"
                size="small"
              >
                {values.showPassword ? <Visibility /> : <VisibilityOff />}
              </IconButton>
            </InputAdornment>
          }
          labelWidth={70}
        />
      </FormControl>
      <div
        style={{
          fontSize: 15,
          width: "70%",
          display: "flex",
          flexDirection: "row",
          justifyContent: "flex-end",
          marginTop: 5,
        }}
      >
        <Link to="/recover-password" style={{ color: "#050038", cursor: "pointer"}}>
          Forgot Password?
        </Link>
      </div>
      <Button loading={loading} onClick={Submit} style={styles.textInputField} text="Sign in" buttonCollor="#050038" />

      <div style={{ marginTop: 100, fontSize: 15, fontWeight: "400" }}>
        I agree to this company’s{" "}
        <span style={styles.hyperlink}>Privacy policy</span> and{" "}
        <span style={styles.hyperlink}>Terms of Service</span>
      </div>
    </>
  )
}

export default login;






