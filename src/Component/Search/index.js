import React from 'react'

const Search = ({ onChange, value }) => {
    return (
        <div className="search-table d-flex align-items-center">
            <label for="search" className="mr-3 mb-0">Search:</label>
            <div className="form-group mb-0 position-relative">
                <input type="seacrh" id="search" className="form-control" onChange={onChange} value={value} />
                <i className="fas fa-search position-absolute"></i>
            </div>
        </div>
    )

}
export default Search

// const filteredItems = tabledata.filter(item => item.name && item.name.includes(filterText));