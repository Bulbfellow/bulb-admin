import React from 'react'
const Input = ({ type, placeholder, className, name, onchange }) => {

    return (
        <input
            type={type}
            placeholder={placeholder}
            className={className}
            name={name}
            onChange={onchange}
        />
    )
}
export default Input