import React from 'react'
import { Link } from 'react-router-dom'
const Deletemodal = ({ deleteid, message, buttonname,  }) => {
    return (
        <div className="modal fade" id="remove" role="dialog" aria-labelledby="removeLabel" aria-hidden="true">
            <div className="modal-dialog mt-0" role="document">
                <div className="modal-content border-0 shadow">
                    <div className="modal-header border-0 px-md-4">
                        <div className="headings">
                            <h5 className="modal-title font-weight-bold pb-1" id="removeLabel">Delete entry</h5>
                            <p className="fs-14 mb-0">{message}</p>
                        </div>
                        <button type="button" className="close outline-none" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div className="modal-footer border-0 px-md-4 pt-2">
                        <span className="mb-0 text-secondary pr-3 text-decoration-none d-inline-block cursor" data-dismiss="modal">Cancel</span>
                        <span onClick={deleteid} className="mb-0 text-warning text-decoration-none d-inline-block cursor" data-dismiss="modal">{buttonname}</span>
                    </div>
                </div>
            </div>
        </div>
    )
}
export default Deletemodal