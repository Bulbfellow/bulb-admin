import React from 'react'
import ReactExport from "react-export-excel";
const ExcelFile = ReactExport.ExcelFile;
const ExcelSheet = ReactExport.ExcelFile.ExcelSheet;
const ExcelColumn = ReactExport.ExcelFile.ExcelColumn;

const Exportexcel = ({ data }) => {
    let arr = []
    for(var key in data[0]) {
        if(key === "view" || key === "delete" || key=== "deleteid" || key ==="status"){
            continue;
        }
        arr.push(key) 
    }      
    return (
        <ExcelFile element={<button>Download Excel</button>}>
            <ExcelSheet data={data} name="Employees">
                    {arr.map((entry) => {
                        return <ExcelColumn label={entry} value={entry} />
                    })}       
            </ExcelSheet>
        </ExcelFile>
    )
}
export default Exportexcel