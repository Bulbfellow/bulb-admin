import React, { useState, useEffect } from 'react'
import DataTable from 'react-data-table-component';
import DeleteLight from '../../Assets/img/delete-light.svg'
import ArchiveLight from '../../Assets/img/archive-light.svg'
import Print from '../../Assets/img/print.svg'
import CircularProgress from '@material-ui/core/CircularProgress';
import ReactExport from "react-export-excel";
const ExcelFile = ReactExport.ExcelFile;
const ExcelSheet = ReactExport.ExcelFile.ExcelSheet;
const ExcelColumn = ReactExport.ExcelFile.ExcelColumn;

const FilterComponent = ({ filterText, onFilter }) => (
    <div className="filter-cus d-flex align-items-center">
        <label className="mb-0 mr-2">Search :</label>
        <div class="position-relative">
            <input id="search" type="search" className="form-control" placeholder="Filter By Name" value={filterText} onChange={onFilter} />
            <i class="fas fa-search position-absolute"></i>
        </div>
    </div>
);

const Dropdown = ({ changeHandle, value, showbutton }) => (
    <div className="all-action pt-3 d-flex align-items-center">
        <div className="custom-control custom-checkbox d-flex p-0 m-0">
            {showbutton && showbutton ?
                <div className="dropdown m-0">
                    <select className="form-control" value={value} onChange={changeHandle}>
                        <option value="99">All</option>
                        <option value="2">Read</option>
                        <option value="1">Unread</option>
                    </select>
                </div> : ''}
        </div>
    </div>
);

const Icons = ({ deleteDataRow, showbutton, archiveDataRow, arr, data }) => (
    <div className="all-action d-flex align-items-center">
        <div className="action-icons">
            <span onClick={deleteDataRow} className="mr-4 d-inline-block"><img src={DeleteLight} alt="Delete" title="Delete" className="img-fluid" /></span>
            {showbutton && showbutton ?
                <span onClick={archiveDataRow} className="mr-4 d-inline-block"><img src={ArchiveLight} alt="Archive" title="Archive" className="img-fluid" /></span> : ''}
            <ExcelFile element={<span className="d-inline-block"><img src={Print} alt="Print" title="Export" className="img-fluid" /></span>}>
                <ExcelSheet data={data} name="Employees">
                    {arr.map((entry) => {
                        return <ExcelColumn label={entry} value={entry} />
                    })}
                </ExcelSheet>
            </ExcelFile>
        </div>
    </div>
)

const Buttons = ({ backData, archiveDataList, setshowbutton, showbutton }) => (
    <div className="text-right">
        {showbutton && showbutton ?
            <button onClick={() => { archiveDataList(); setshowbutton(false) }} className="btn btn-outline-warning px-5">Archive</button>
            :
            <button onClick={() => { backData(); setshowbutton(true) }} className="btn btn-outline-warning px-5">Back</button>
        }
    </div>
)

const Circular = () => (
    <div style={{ padding: '24px' }}>
        <CircularProgress size={75} />
    </div>
);


const Datatable = ({ tabledata, datas, archiveDataList, deleteids, toggleCleared, changeHandle, value, backData, handleChangeRow, deleteDataRow, archiveDataRow }) => {
    const [filterText, setFilterText] = React.useState('');
    const [resetPaginationToggle, setResetPaginationToggle] = React.useState(false);
    const filteredItems = tabledata.filter(item => item.name && item.name.includes(filterText));
    const exportdata = datas.filter(item => item.name && item.name.includes(filterText));

    const [showbutton, setshowbutton] = useState(true)
    const [pending, setPending] = React.useState(true);


    let arr = []
    for (var key in exportdata[0]) {
        if (key === "view" || key === "delete" || key === "deleteid" || key === "status") {
            continue;
        }
        arr.push(key)
    }

    useEffect(() => {
        if (tabledata.length > 0) {
            setPending(false)
        }
    }, [tabledata])


    const dropdown = React.useMemo(() => {
        return <Dropdown changeHandle={changeHandle} value={value} showbutton={showbutton} />;
    }, [changeHandle, value, showbutton]);

    const icons = React.useMemo(() => {
        return <Icons deleteDataRow={deleteDataRow} showbutton={showbutton} archiveDataRow={archiveDataRow} arr={arr} data={exportdata} />;
    }, [showbutton, deleteids, toggleCleared]);

    const buttons = React.useMemo(() => {
        return <Buttons backData={backData} archiveDataList={archiveDataList} setshowbutton={setshowbutton} showbutton={showbutton} />;
    }, [showbutton,]);

    const subHeaderComponentMemo = React.useMemo(() => {
        const handleClear = () => {
            if (filterText) {
                setResetPaginationToggle(!resetPaginationToggle);
                setFilterText('');
            }
        };
        return <FilterComponent onFilter={e => setFilterText(e.target.value)} onClear={handleClear} filterText={filterText} />;
    }, [filterText, resetPaginationToggle]);



    const columns = [
        {
            name: '',
            selector: 'name',
        },
        {
            name: '',
            selector: 'title',
        },
        {
            name: '',
            selector: 'date',
        },
        {
            name: '',
            selector: 'view',
        },
        {
            name: '',
            selector: 'delete',
        }
    ];

    const conditionalRowStyles = [
        {
            when: row => row.status === "1",
            style: {
                backgroundColor: 'rgb(253, 191, 20, .1)',
                color: '#10152C',
                '&:hover': {
                    cursor: 'pointer',
                },
            },
        },
        {
            when: row => row.status === "2",
            style: {
                backgroundColor: 'transparent',
                color: '#10152C',
                '&:hover': {
                    cursor: 'pointer',
                },
            },
        },
    ];
    return <DataTable
        columns={columns}
        data={filteredItems}
        selectableRows
        Clicked
        pagination={true}
        conditionalRowStyles={conditionalRowStyles}
        paginationPerPage={8}
        paginationRowsPerPageOptions={[8, 10, 15, 20, 30]}
        onSelectedRowsChange={(state) => handleChangeRow(state)}
        subHeaderAlign={"left"}
        subHeader={true}
        subHeaderComponent={[subHeaderComponentMemo, dropdown, icons, buttons]}
        noHeader={true}
        clearSelectedRows={toggleCleared}
        progressComponent={<Circular />}
        progressPending={pending}
    />

}
export default Datatable