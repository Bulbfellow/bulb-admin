import React from 'react'

const Quicknotification = ({ show, closetab, message, bgblock, closesecond, timeout }) => {
    timeout && timeout &&
        setTimeout(() => {
            closetab()
        }, closesecond == null ? 5000 : closesecond);
    return (
        <>
            {
                show === true ? <div className="alert-msg d-block">
                    {bgblock === true ?
                        <div className="bg-overlay"></div> : ''}
                    <div className="msg right-msg position-fixed">
                        <div className="msg-inner d-flex align-items-center justify-content-between" >
                            <p className="mb-0 mr-4 pr-2 fs-14">{message}</p>
                            <p style={{ cursor: "pointer" }} onClick={closetab} className="mb-0">  <i className="fa fa-times"></i></p>
                        </div>
                    </div>
                </div> : ''
            }
        </>
    )
}
export default Quicknotification