import React from 'react';
import MaterialTable from 'material-table';

export default function MaterialTableDemo({ tabledata ,messgae}) {
  // console.log('Table-----++++', tabledata)
  const [state, setState] = React.useState({
    columns: [
      { title: 'Name', field: 'name' },
      { title: 'Date', field: 'date' },
      { title: 'Contact', field: 'mobile' },
      { title: 'view', field: 'view' },
      { title: 'delete', field: 'delete' }
    ],
  });
  var data = tabledata

  return (
    <React.Fragment>
      {/* No of data ={data.length} */}
      {messgae}
      <MaterialTable
        title="Editable Example"
        columns={state.columns}
        data={data}
        style={{ width: '100%' }}
      />
    </React.Fragment>
  );
}