import Button from '@material-ui/core/Button';
import { createMuiTheme, ThemeProvider } from '@material-ui/core/styles';
import React from 'react';


const myButton = ({ style = {}, text, buttonCollor, onClick, loading}) => {
  const theme = createMuiTheme({
    palette: {
      primary: {
        main: buttonCollor,
      },
    },
  });

return (
  <ThemeProvider theme={theme}>
    <Button disabled={loading} onClick={onClick} variant="contained" color="primary" style={style}>
      {text}
    </Button>
  </ThemeProvider>
  )
}

export default myButton;
